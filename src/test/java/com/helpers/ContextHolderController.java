package com.helpers;

import com.data.voter.SingleVoter;

import com.services.context.RequestContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnNotWebApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ConditionalOnNotWebApplication
public class ContextHolderController {

    @Autowired
    private RequestContextHolder contextHolder;

    public ContextHolderController(final RequestContextHolder contextHolder) {
        this.contextHolder = contextHolder;
    }

    @PutMapping("/setCurrentUser")
    @ResponseBody
    public String setCurrentUser() {
        this.contextHolder.setCurrentVoter(new SingleVoter().setName("James"));
        return this.contextHolder.getCurrentVoter().get().getName();
    }

    @GetMapping("/getCurrentUser")
    @ResponseBody
    public String getCurrentUser() {
        if(this.contextHolder.getCurrentVoter().isPresent()) {
            return this.contextHolder.getCurrentVoter().get().getName();
        }
        return "NOUSERPRESENT";
    }
}
