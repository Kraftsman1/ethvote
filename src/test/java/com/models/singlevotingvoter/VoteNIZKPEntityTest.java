package com.models.singlevotingvoter;

import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VoteNIZKPEntityTest {

    @Autowired
    SingleVotingVoterRepository votingVoterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    VotingsRepository votingsRepository;

    @After
    public void tearDown() {
        votingVoterRepository.deleteAll();
        userRepository.deleteAll();
        votingsRepository.deleteAll();
    }

    @Test
    public void savingAndRetrievingWorks() {

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();

        long userId = userRepository.save(userEntity).getId();
        long votingId = votingsRepository.save(votingEntity).getId();

        VoteNIZKPEntity proof1 = new VoteNIZKPEntity()
                .setA("a1")
                .setB("b1")
                .setC("c1")
                .setR("r1")
                .setMessageIndex(0);

        VoteNIZKPEntity proof2 = new VoteNIZKPEntity()
                .setA("a2")
                .setB("b2")
                .setC("c2")
                .setR("r2")
                .setMessageIndex(1);

        SingleVotingVoterEntity entity = new SingleVotingVoterEntity(userEntity, votingEntity);
        entity
                .addNonInteractiveZeroKnowledgeProof(proof1)
                .addNonInteractiveZeroKnowledgeProof(proof2);

        votingVoterRepository.save(entity);

        SingleVotingVoterEntity returnedEntity = votingVoterRepository.findOneByUserAndVotingWithNIZKPFetched(userId,votingId).get();

        Assert.assertEquals(2,returnedEntity.getNonInteractiveZeroKnowledgeProof().values().size());

        Assert.assertEquals(0,returnedEntity.getNonInteractiveZeroKnowledgeProof().get(0).getMessageIndex().intValue());
        Assert.assertEquals("a1",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(0).getA());
        Assert.assertEquals("b1",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(0).getB());
        Assert.assertEquals("c1",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(0).getC());
        Assert.assertEquals("r1",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(0).getR());

        Assert.assertEquals(1,returnedEntity.getNonInteractiveZeroKnowledgeProof().get(1).getMessageIndex().intValue());
        Assert.assertEquals("a2",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(1).getA());
        Assert.assertEquals("b2",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(1).getB());
        Assert.assertEquals("c2",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(1).getC());
        Assert.assertEquals("r2",returnedEntity.getNonInteractiveZeroKnowledgeProof().get(1).getR());
    }
}
