package com.models.singlevotingvoter;

import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SingleVotingVoterRepositoryTest {

    @Autowired
    SingleVotingVoterRepository votingVoterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    VotingsRepository votingsRepository;

    @Test
    public void savingWorks() {
        String username = "Dummy";
        String votingName = "Voting";
        String alpha = "alpha";
        String beta = "beta";
        String key = "key";
        Long numberOfVoterInVoting = 42L;
        UserEntity userEntity = new UserEntity()
                .setUsername(username);
        VotingEntity votingEntity = new VotingEntity()
                .setTitle(votingName);

        long userId = userRepository.save(userEntity).getId();
        long votingId = votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity entity = new SingleVotingVoterEntity(userEntity,votingEntity)
                .setVoterNumber(numberOfVoterInVoting)
                .setCreatedSecretSharesToTrue()
                .setAlpha(alpha)
                .setBeta(beta)
                .setPublicKey(key)
                .setCanVote(true)
                .setIsTrustee(true);
        votingVoterRepository.save(entity);
        Optional<SingleVotingVoterEntity> optEntity = votingVoterRepository.findById(new SingleVotingVoterEntityId(userId,votingId));
        Assert.assertTrue(optEntity.isPresent());
        Assert.assertEquals(numberOfVoterInVoting,optEntity.get().getVoterNumber());
        Assert.assertTrue(optEntity.get().createdSecretShares());
        Assert.assertEquals(username,optEntity.get().getUserEntity().getUsername());
        Assert.assertEquals(votingName,optEntity.get().getVotingEntity().getTitle());
        Assert.assertEquals(alpha,optEntity.get().getAlpha());
        Assert.assertEquals(beta,optEntity.get().getBeta());
        Assert.assertEquals(key,optEntity.get().getPublicKey());
        Assert.assertTrue(optEntity.get().getCanVote());
        Assert.assertTrue(optEntity.get().getIsTrustee());
    }

}
