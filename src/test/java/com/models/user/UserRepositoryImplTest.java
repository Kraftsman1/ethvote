package com.models.user;

import com.models.createdvotings.CreatedVotingsEntity;
import com.models.createdvotings.CreatedVotingsRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRepositoryImplTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    private CreatedVotingsRepository createdVotingsRepository;

    @Test
    public void getByIdEmptyWorks() {
        Optional<UserEntity> optUser = userRepository.findOneByIdWithVotingsFetched(27L);
        Assert.assertFalse(optUser.isPresent());
    }

    @Test
    public void getOneByIdWorksIfOneThere() {
        String username = "John Doe";
        UserEntity userEntity = new UserEntity().setUsername(username);
        Long id = userRepository.save(userEntity).getId();

        Optional<UserEntity> optUser = userRepository.findOneByIdWithVotingsFetched(id);
        Assert.assertTrue(optUser.isPresent());
    }

    @Test
    public void userVotingsAreEagerlyLoaded() {
        String username = "John Doe";
        UserEntity userEntity = new UserEntity().setUsername(username);
        userRepository.save(userEntity);

        VotingEntity votingEntity1 = new VotingEntity().setTitle("title1");
        VotingEntity votingEntity2 = new VotingEntity().setTitle("title2");
        VotingEntity votingEntity3 = new VotingEntity().setTitle("title3");

        votingsRepository.save(votingEntity1);
        votingsRepository.save(votingEntity2);
        votingsRepository.save(votingEntity3);

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity,votingEntity1);
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity,votingEntity2);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity,votingEntity3);

        singleVotingVoterRepository.save(singleVotingVoterEntity1);
        singleVotingVoterRepository.save(singleVotingVoterEntity2);
        singleVotingVoterRepository.save(singleVotingVoterEntity3);

        userEntity.addVoting(singleVotingVoterEntity1);
        userEntity.addVoting(singleVotingVoterEntity2);
        userEntity.addVoting(singleVotingVoterEntity3);

        Long id = userRepository.save(userEntity).getId();

        Optional<UserEntity> optUser = userRepository.findOneByIdWithVotingsFetched(id);
        Assert.assertTrue(optUser.isPresent());
        Assert.assertEquals(Arrays.asList("title1","title2","title3"),optUser
                .get()
                .getVotings()
                .stream()
                .map(s -> s.getVotingEntity().getTitle()).collect(Collectors.toList()));
    }

    @Test
    public void getOneByIdWithParticipatingVotingWorks() {
        String username = "John";
        UserEntity userEntity = new UserEntity()
                .setUsername(username);
        UserEntity storedUserEntity = userRepository.save(userEntity);

        VotingEntity votingEntity = new VotingEntity();
        VotingEntity storedVotingEntity = votingsRepository.save(votingEntity);

        Optional<UserEntity> optStoredEntityWithVotings = userRepository.findOneByIdWithCreatedVotingsFetched(storedUserEntity.getId());
        UserEntity storedEntityWithVotings = optStoredEntityWithVotings.get();

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(storedEntityWithVotings,storedVotingEntity);
        CreatedVotingsEntity createdVoting = createdVotingsRepository.save(createdVotingsEntity);

        Assert.assertEquals(0,storedEntityWithVotings.getCreatedVotings().size());
        storedEntityWithVotings.addCreatedVoting(createdVoting);
        userRepository.save(storedEntityWithVotings);

        optStoredEntityWithVotings = userRepository.findOneByIdWithCreatedVotingsFetched(storedUserEntity.getId());
        storedEntityWithVotings = optStoredEntityWithVotings.get();

        Assert.assertEquals(1,storedEntityWithVotings.getCreatedVotings().size());
        Assert.assertEquals(storedVotingEntity.getId(),storedEntityWithVotings.getCreatedVotings().get(0).getVotingEntity().getId());
        Assert.assertEquals(storedEntityWithVotings.getId(),storedEntityWithVotings.getCreatedVotings().get(0).getUserEntity().getId());
    }

    @Test
    public void getOneByIdWithInvolvedVotingWorks() {
        String username = "John";
        UserEntity userEntity = new UserEntity()
                .setUsername(username);
        UserEntity storedUserEntity = userRepository.save(userEntity);

        VotingEntity votingEntity = new VotingEntity();
        VotingEntity storedVotingEntity = votingsRepository.save(votingEntity);

        Optional<UserEntity> optStoredEntityWithVotings = userRepository.findOneByIdWithInvolvedVotingsFetched(storedUserEntity.getId());
        UserEntity storedEntityWithVotings = optStoredEntityWithVotings.get();

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(storedEntityWithVotings,storedVotingEntity);
        CreatedVotingsEntity createdVoting = createdVotingsRepository.save(createdVotingsEntity);

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        SingleVotingVoterEntity savedVoting = singleVotingVoterRepository.save(singleVotingVoterEntity);

        Assert.assertEquals(0,storedEntityWithVotings.getCreatedVotings().size());
        Assert.assertEquals(0,storedEntityWithVotings.getVotings().size());

        storedEntityWithVotings.addCreatedVoting(createdVoting);
        storedEntityWithVotings.addVoting(savedVoting);

        userRepository.save(storedEntityWithVotings);

        optStoredEntityWithVotings = userRepository.findOneByIdWithInvolvedVotingsFetched(storedUserEntity.getId());
        storedEntityWithVotings = optStoredEntityWithVotings.get();

        Assert.assertEquals(1,storedEntityWithVotings.getCreatedVotings().size());
        Assert.assertEquals(storedVotingEntity.getId(),storedEntityWithVotings.getCreatedVotings().get(0).getVotingEntity().getId());
        Assert.assertEquals(storedEntityWithVotings.getId(),storedEntityWithVotings.getCreatedVotings().get(0).getUserEntity().getId());

        Assert.assertEquals(1,storedEntityWithVotings.getVotings().size());
        Assert.assertEquals(storedVotingEntity.getId(),storedEntityWithVotings.getVotings().get(0).getVotingEntity().getId());
        Assert.assertEquals(storedEntityWithVotings.getId(),storedEntityWithVotings.getVotings().get(0).getUserEntity().getId());
    }

    @Test
    public void findOneByExternalIdOrUsernameAndEmailWorksExternalId() {
        String externalId = "externalId";
        UserEntity userEntity = new UserEntity().setExternalId(externalId);
        userRepository.save(userEntity);
        Optional<UserEntity> retrievedUserEntity = userRepository.findOneByExternalIdOrUsernameAndEmail(externalId,"u","e");
        Assert.assertTrue(retrievedUserEntity.isPresent());
    }

    @Test
    public void findOneByExternalIdOrUsernameAndEmailWorksEmailAndUserName() {
        String mail = "mail";
        String username = "username";
        UserEntity userEntity = new UserEntity().setUsername(username).setEmail(mail);
        userRepository.save(userEntity);
        Optional<UserEntity> retrievedUserEntity = userRepository.findOneByExternalIdOrUsernameAndEmail("id",username,mail);
        Assert.assertTrue(retrievedUserEntity.isPresent());
    }

    @Test
    public void findOneByExternalIdOrUsernameAndEmailReturnsEmpty() {
        String mail = "mail";
        String username = "username";
        String wrongUserName = "wrongUserName";
        UserEntity userEntity = new UserEntity().setUsername(username).setEmail(mail);
        userRepository.save(userEntity);
        Optional<UserEntity> retrievedUserEntity = userRepository.findOneByExternalIdOrUsernameAndEmail("id",wrongUserName,mail);
        Assert.assertFalse(retrievedUserEntity.isPresent());
    }

    @Test
    public void findOneByUsernameAndEmailReturnsEmpty() {
        String mail = "mail";
        String username = "username";
        String wrongUserName = "wrongUserName";
        UserEntity userEntity = new UserEntity().setUsername(username).setEmail(mail);
        userRepository.save(userEntity);
        Optional<UserEntity> retrievedUserEntity = userRepository.findOneByUsernameAndEmail(wrongUserName,mail);
        Assert.assertFalse(retrievedUserEntity.isPresent());
    }

    @Test
    public void findOneByUsernameAndEmailReturnsData() {
        String mail = "mail";
        String username = "username";
        UserEntity userEntity = new UserEntity().setUsername(username).setEmail(mail);
        userRepository.save(userEntity);
        Optional<UserEntity> retrievedUserEntity = userRepository.findOneByUsernameAndEmail(username,mail);
        Assert.assertTrue(retrievedUserEntity.isPresent());
    }
}
