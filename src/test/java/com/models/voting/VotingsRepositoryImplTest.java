package com.models.voting;

import com.models.singlevotingvoter.CommitmentEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VotingsRepositoryImplTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Test
    public void getEmptyWorks() {
        Long missingId = 1L;
        Optional<VotingEntity> votingEntity = votingsRepository.findOneByIdWithEligibleVotersFetched(missingId);
        Assert.assertFalse(votingEntity.isPresent());
    }

    @Test
    public void getOneWorksIfOneThere() {
        VotingEntity votingEntity = new VotingEntity();
        Long id = votingsRepository.save(votingEntity).getId();

        Optional<VotingEntity> retVotingEntity = votingsRepository.findOneByIdWithEligibleVotersFetched(id);
        Assert.assertTrue(retVotingEntity.isPresent());
    }

    @Test
    public void getOneWorksAndVotersAreFetched() {
        UserEntity userEntity1 = new UserEntity().setUsername("u1");
        UserEntity userEntity2 = new UserEntity().setUsername("u2");
        UserEntity userEntity3 = new UserEntity().setUsername("u3");

        userRepository.save(userEntity1);
        userRepository.save(userEntity2);
        userRepository.save(userEntity3);

        VotingEntity votingEntity = new VotingEntity();
        Long id = votingsRepository.save(votingEntity).getId();

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity);
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity);

        singleVotingVoterRepository.save(singleVotingVoterEntity1);
        singleVotingVoterRepository.save(singleVotingVoterEntity2);
        singleVotingVoterRepository.save(singleVotingVoterEntity3);

        votingEntity.addEligibleVoter(singleVotingVoterEntity1);
        votingEntity.addEligibleVoter(singleVotingVoterEntity2);
        votingEntity.addEligibleVoter(singleVotingVoterEntity3);

        votingsRepository.save(votingEntity);

        Optional<VotingEntity> retVotingEntity = votingsRepository.findOneByIdWithEligibleVotersFetched(id);
        Assert.assertTrue(retVotingEntity.isPresent());
        Assert.assertEquals(new HashSet<>(Arrays.asList("u1","u2","u3")),retVotingEntity.get().getAllEligibleVoters()
                                    .stream()
                                    .map(s -> s.getUserEntity().getUsername())
                                    .collect(Collectors.toSet()));
    }

    @Test
    public void getEmptyWithCommitmentsWorks() {
        Long missingId = 1L;
        Optional<VotingEntity> votingEntity = votingsRepository.findOneByIdWithEligibleVotersWithCommitmentsFetched(missingId);
        Assert.assertFalse(votingEntity.isPresent());
    }

    @Test
    public void getOneWithCommitmentsWorksIfOneThere() {
        VotingEntity votingEntity = new VotingEntity();
        Long id = votingsRepository.save(votingEntity).getId();

        Optional<VotingEntity> retVotingEntity = votingsRepository.findOneByIdWithEligibleVotersWithCommitmentsFetched(id);
        Assert.assertTrue(retVotingEntity.isPresent());
    }

    @Test
    public void getOneWithCommitmentsWorksAndVotersAreFetched() {
        UserEntity userEntity1 = new UserEntity().setUsername("u1");
        UserEntity userEntity2 = new UserEntity().setUsername("u2");
        UserEntity userEntity3 = new UserEntity().setUsername("u3");

        userRepository.save(userEntity1);
        userRepository.save(userEntity2);
        userRepository.save(userEntity3);

        VotingEntity votingEntity = new VotingEntity();
        Long id = votingsRepository.save(votingEntity).getId();

        CommitmentEntity commitmentEntity1 = new CommitmentEntity().setForCoefficientNumber(0).setCommitment("c1");
        CommitmentEntity commitmentEntity2 = new CommitmentEntity().setForCoefficientNumber(0).setCommitment("c2");
        CommitmentEntity commitmentEntity3 = new CommitmentEntity().setForCoefficientNumber(0).setCommitment("c3");

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity).addCommitments(commitmentEntity1);
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity).addCommitments(commitmentEntity2).addCommitments(commitmentEntity3);

        singleVotingVoterRepository.save(singleVotingVoterEntity1);
        singleVotingVoterRepository.save(singleVotingVoterEntity2);
        singleVotingVoterRepository.save(singleVotingVoterEntity3);

        votingEntity.addEligibleVoter(singleVotingVoterEntity1);
        votingEntity.addEligibleVoter(singleVotingVoterEntity2);
        votingEntity.addEligibleVoter(singleVotingVoterEntity3);

        votingsRepository.save(votingEntity);

        Optional<VotingEntity> retVotingEntity = votingsRepository.findOneByIdWithEligibleVotersWithCommitmentsFetched(id);
        Assert.assertTrue(retVotingEntity.isPresent());
        Assert.assertEquals(new HashSet<>(Arrays.asList("u1","u2","u3")),retVotingEntity.get().getAllEligibleVoters()
                .stream()
                .map(s -> s.getUserEntity().getUsername())
                .collect(Collectors.toSet()));

        Assert.assertEquals("c1",retVotingEntity.get().getAllEligibleVoters().get(0).getCommitments().get(0).getCommitment());
    }

}
