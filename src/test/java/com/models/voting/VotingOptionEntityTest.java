package com.models.voting;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VotingOptionEntityTest {

    @Autowired
    private VotingsRepository votingsRepository;

    @Test()
    public void getOptionNamesWorks() {
        String optionOne = "option one";
        Long optionPrimeNumberOne = 2L;
        VotingOptionEntity vE1 = new VotingOptionEntity()
                .setOptionName(optionOne)
                .setOptionPrimeNumber(optionPrimeNumberOne);

        String optionTwo = "option two";
        Long optionPrimeNumberTwo = 3L;
        VotingOptionEntity vE2 = new VotingOptionEntity()
                .setOptionName(optionTwo)
                .setOptionPrimeNumber(optionPrimeNumberTwo);

        String optionThree = "option three";
        Long optionPrimeNumberThree = 5L;
        VotingOptionEntity vE3 = new VotingOptionEntity()
                .setOptionName(optionThree)
                .setOptionPrimeNumber(optionPrimeNumberThree);

        VotingEntity votingEntity = new VotingEntity()
                .addVotingOption(vE1)
                .addVotingOption(vE2)
                .addVotingOption(vE3);

        Long id = votingsRepository.save(votingEntity).getId();
        List<VotingOptionEntity> keys =  votingsRepository.findById(id).get().getVotingOptions();
        Assert.assertEquals(Arrays.asList(optionOne,optionTwo,optionThree),keys.stream().map(e -> e.getOptionName()).collect(Collectors.toList()));
        Assert.assertEquals(Arrays.asList(optionPrimeNumberOne,optionPrimeNumberTwo,optionPrimeNumberThree),keys.stream().map(e -> e.getOptionPrimeNumber()).collect(Collectors.toList()));
    }
}
