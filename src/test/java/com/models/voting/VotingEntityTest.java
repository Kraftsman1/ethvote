package com.models.voting;

import com.models.createdvotings.CreatedVotingsEntity;
import com.models.createdvotings.CreatedVotingsRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VotingEntityTest {

    @Autowired
    private VotingsRepository votingsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    private CreatedVotingsRepository createdVotingsRepository;

    @Test
    public void sameObjectStoreReturnsSameId() {
        VotingEntity votingsEntity = new VotingEntity();
        Long idOne = votingsRepository.save(votingsEntity).getId();
        Long idTwo = votingsRepository.save(votingsEntity).getId();
        Assert.assertNotNull(idOne);
        Assert.assertNotNull(idTwo);
        Assert.assertEquals(idOne,idTwo);
    }

    @Test
    public void differentObjectStoreReturnsDifferentId() {
        VotingEntity votingsEntityOne = new VotingEntity();
        VotingEntity votingsEntityTwo = new VotingEntity();
        Long idOne = votingsRepository.save(votingsEntityOne).getId();
        Long idTwo = votingsRepository.save(votingsEntityTwo).getId();
        Assert.assertNotNull(idOne);
        Assert.assertNotNull(idTwo);
        Assert.assertNotEquals(idOne,idTwo);
    }

    @Test
    public void titleCanBeStored() {
        String title = "title";
        VotingEntity votingEntity = new VotingEntity().setTitle(title);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(title,votingsRepository.findById(id).get().getTitle());
    }

    @Test
    public void descriptionCanBeStored() {
        String description = "description";
        VotingEntity votingEntity = new VotingEntity().setDescription(description);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(description,votingsRepository.findById(id).get().getDescription());
    }

    @Test
    public void votingEndTimeCanBeStored() {
        Long votingEndTime = 42L;
        VotingEntity votingEntity = new VotingEntity().setTimeVotingIsFinished(votingEndTime);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(votingEndTime,votingsRepository.findById(id).get().getTimeVotingIsFinished());
    }

    @Test
    public void longDescriptionCanBeStored() {
        String desc = "";

        for(int i=0;i<1000;i++) {
            desc += 'a';
        }

        VotingEntity votingEntity = new VotingEntity().setDescription(desc);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(desc,votingsRepository.findById(id).get().getDescription());
    }

    @Test
    public void securityThresholdCanBeStored() {
        Long securityThreshold = 7L;
        VotingEntity votingEntity = new VotingEntity().setSecurityThreshold(securityThreshold);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(securityThreshold,votingsRepository.findById(id).get().getSecurityThreshold());
    }

    @Test
    public void numberOfEligibleVotersCanBeStored() {
        Long numberOfEligibleVoters = 42L;
        VotingEntity votingEntity = new VotingEntity().setNumberOfEligibleVoters(numberOfEligibleVoters);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(numberOfEligibleVoters,votingsRepository.findById(id).get().getNumberOfEligibleVoters());
    }

    @Test
    public void phaseNumberCanBeStored() {
        int phaseNumber = 27;
        VotingEntity votingEntity = new VotingEntity().setPhaseNumber(phaseNumber);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(phaseNumber,votingsRepository.findById(id).get().getPhaseNumber());
    }

    @Test
    public void securityParametersCanBeStored() {
        String prime = "7";
        VotingCalculationParametersEntity calculationParametersEntity = new VotingCalculationParametersEntity().setPrimeP(prime);
        VotingEntity votingEntity = new VotingEntity().setCalculationParametersEntity(calculationParametersEntity);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(prime,votingsRepository.findById(id).get().getCalculationParametersEntity().getPrimeP());
    }

    @Test
    public void votingOptionEntityCanBeStored() {
        String votingOption = "opt";
        VotingOptionEntity votingOptionEntity = new VotingOptionEntity().setOptionName(votingOption);
        VotingEntity votingEntity = new VotingEntity().addVotingOption(votingOptionEntity);
        Long id = votingsRepository.save(votingEntity).getId();
        Assert.assertEquals(votingOption,votingsRepository.findById(id).get().getVotingOptions().get(0).getOptionName());
    }

    @Test
    public void addingVoterWorks() {
        String username = "John";
        UserEntity userEntity = new UserEntity()
                .setUsername(username);
        userRepository.save(userEntity);

        VotingEntity votingEntity = new VotingEntity();
        votingsRepository.save(votingEntity);

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);
        singleVotingVoterRepository.save(singleVotingVoterEntity);

        votingEntity.addEligibleVoter(singleVotingVoterEntity);
        votingsRepository.save(votingEntity);

    }

    @Test
    public void votingAdministratorCanBeStored() {
        String username = "John";
        UserEntity userEntity = new UserEntity()
                .setUsername(username);
        UserEntity storedEntity = userRepository.save(userEntity);

        VotingEntity votingEntity = new VotingEntity();
        VotingEntity storedVotingEntity = votingsRepository.save(votingEntity);
        storedVotingEntity = votingsRepository.findById(storedVotingEntity.getId()).get();

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(storedEntity,storedVotingEntity);
        CreatedVotingsEntity createdVoting = createdVotingsRepository.save(createdVotingsEntity);

        storedVotingEntity.setVotingAdministrator(createdVoting);
        votingsRepository.save(storedVotingEntity);

        Assert.assertEquals(storedEntity.getId(),
                votingsRepository.findById(storedVotingEntity.getId())
                        .get()
                        .getVotingAdministrator()
                        .getUserEntity()
                        .getId()
        );
    }

}
