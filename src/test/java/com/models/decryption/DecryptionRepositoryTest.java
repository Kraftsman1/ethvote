package com.models.decryption;

import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DecryptionRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotingsRepository votingsRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    private DecryptionRepository decryptionRepository;

    VotingEntity votingEntity = null;
    UserEntity userEntity = null;
    SingleVotingVoterEntity savedSingleVoterEntity = null;

    @Before
    public void setup() {
        userEntity = new UserEntity();
        votingEntity = new VotingEntity();

        userEntity = userRepository.saveAndFlush(userEntity);
        votingEntity = votingsRepository.saveAndFlush(votingEntity);

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity, votingEntity);
        savedSingleVoterEntity = singleVotingVoterRepository.saveAndFlush(singleVotingVoterEntity);
    }

    @Test
    public void decryptionFactorAndSignatureCanBeSet() {
        String decryptionFactor = "dec";
        String signature = "sign";

        int updatedColumns = decryptionRepository.setDecryption(votingEntity.getId(), userEntity.getId(), decryptionFactor, signature);

        savedSingleVoterEntity = singleVotingVoterRepository.findOne(userEntity.getId(), votingEntity.getId()).get();

        Assert.assertEquals(1,updatedColumns);
        Assert.assertEquals(decryptionFactor,savedSingleVoterEntity.getDecryptionFactor());
        Assert.assertEquals(signature,savedSingleVoterEntity.getDecryptionFactorSignature());
        Assert.assertEquals(true,savedSingleVoterEntity.participatedInDecryption());
    }

    @Test
    public void setDecryptionnFactorReturns0IfNotThere() {
        long notExisting = 27l;
        int updatedColumns = decryptionRepository.setDecryption(notExisting, notExisting, "d", "s");
        Assert.assertEquals(0,updatedColumns);
    }

    @Test
    public void decryptionProofCanBeSet() {
        String a = "a0";
        String b = "b0";
        String r = "r0";
        String signature = "sign0";

        int updatedColumns = decryptionRepository.setDecryptionProof(votingEntity.getId(), userEntity.getId(), a, b,r,signature);

        savedSingleVoterEntity = singleVotingVoterRepository.findOne(userEntity.getId(), votingEntity.getId()).get();

        Assert.assertEquals(1,updatedColumns);
        Assert.assertEquals(a,savedSingleVoterEntity.getDecryptionNIZKProof().getA());
        Assert.assertEquals(b,savedSingleVoterEntity.getDecryptionNIZKProof().getB());
        Assert.assertEquals(r,savedSingleVoterEntity.getDecryptionNIZKProof().getR());
        Assert.assertEquals(signature,savedSingleVoterEntity.getDecryptionNIZKProof().getSignature());
        Assert.assertTrue(savedSingleVoterEntity.getDecryptionNIZKProof().isProofAdded());
    }

    @Test
    public void setProofReturns0IfNotThere() {
        long notExisting = 27l;
        int updatedColumns = decryptionRepository.setDecryptionProof(notExisting, notExisting, "a", "b","r","signature");
        Assert.assertEquals(0,updatedColumns);
    }
}
