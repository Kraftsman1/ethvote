package com.controller;

import com.data.voter.SingleVoter;

import com.data.votingprogress.SingleVoterProgress;
import com.data.votingprogress.VotingProgress;
import com.data.votingvoterprogress.VotingVoterProgress;
import com.data.votings.*;
import com.data.votingsforvoterwithprogress.VotingsForVoterWithProgress;
import com.enums.Phases;
import com.enums.Role;
import com.exceptions.NoSuchVotingException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.roles.RoleService;
import com.services.data.votings.VotingsDataService;
import com.services.datamodelchecker.DataModelCheckerService;
import com.services.ethereum.EthereumService;
import com.services.hash.HashService;
import com.services.mail.MailService;
import com.services.mail.MailTextCreatorService;
import com.services.secrets.FakeSecretsService;
import com.services.secrets.SecretsService;
import com.services.task.TaskScheduler;
import com.services.votingssummary.VotingsSummaryService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class VotingsDataControllerTest {

    @MockBean
    private VotingsDataService votingsDataService;

    @MockBean
    private MailService mailService;

    @MockBean
    private MailTextCreatorService mailTextCreatorService;

    @MockBean
    private HashService hashService;

    @MockBean
    private EthereumService ethereumService;

    @MockBean
    private TaskScheduler taskScheduler;

    @MockBean
    private VotingsSummaryService votingsSummaryService;

    @MockBean
    private DataModelCheckerService dataModelCheckerService;

    private SecretsService secretsService;

    RequestContextHolder requestContextHolder = new FakeRequestContextHolder();

    RoleService mockRoleService = Mockito.mock(RoleService.class);

    VotingsDataController votingsDataController;

    ControllerTestHelper helper;

    @Before
    public void setup() {
        requestContextHolder = new FakeRequestContextHolder();
        secretsService = new FakeSecretsService();

        when(dataModelCheckerService.checkDataModel()).thenReturn(false);

        votingsDataController = new VotingsDataController(
                votingsDataService,
                mockRoleService,
                mailService,
                mailTextCreatorService,
                votingsSummaryService,
                hashService,
                ethereumService,
                requestContextHolder,
                dataModelCheckerService,
                taskScheduler);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(votingsDataController);
    }

    @Test
    public void shouldReturnPermissionDeniedIfNoKeySpecified() throws Exception {
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/phaseNumber/4")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
        Assert.assertEquals("The request won't be fulfilled as the the user is not authenticated.",response.getContentAsString());
    }

    @Test
    public void getVotingsForPhaseZeroShouldReturnValue() throws Exception {
        int phaseNumber = 0;
        Long voterId = 27L;

        List<Voting> votings = new ArrayList<>();
        votings.add(new Voting().setTitle("Special title").setPhase(phaseNumber));

        List<VotingVoterProgress> votingVoterProgresses = new ArrayList<>();
        votingVoterProgresses.add(new VotingVoterProgress()
                .setVotingId(1L)
                .setCreatedSecretShares(true)
                .setVoted(true)
                .setParticipatedInDecryption(false));

        List<VotingOfficials> votingOfficialsList = new ArrayList<>();

        VotingsForVoterWithProgress votingsForVoterWithProgress = new VotingsForVoterWithProgress(votings,votingVoterProgresses,votingOfficialsList);

        when(votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(voterId,phaseNumber)).thenReturn(votingsForVoterWithProgress);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/phaseNumber/"+phaseNumber)
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"votings\":[{\"phase\":0,\"timeVotingIsFinished\":-1,\"options\":[],\"id\":0,\"title\":\"Special title\"}],\"votingOfficials\":[],\"votingsVoterProgress\":[{\"participatedInDecryption\":false,\"usesCustomKey\":false,\"phaseNumber\":0,\"votingId\":1,\"createdSecretShares\":true,\"voted\":true}]}",response.getContentAsString());
    }

    @Test
    public void getVotingsForPhaseOneShouldReturnValue() throws Exception {
        int phaseNumber = 1;
        Long voterId = 27L;

        List<Voting> votings = new ArrayList<>();
        votings.add(new Voting().setTitle("Special title").setPhase(phaseNumber));

        List<VotingVoterProgress> votingVoterProgresses = new ArrayList<>();
        votingVoterProgresses.add(new VotingVoterProgress()
                .setVotingId(1L)
                .setCreatedSecretShares(true)
                .setVoted(true)
                .setParticipatedInDecryption(false));

        List<VotingOfficials> votingOfficialsList = new ArrayList<>();

        VotingsForVoterWithProgress votingsForVoterWithProgress = new VotingsForVoterWithProgress(votings,votingVoterProgresses,votingOfficialsList);

        when(votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(voterId,phaseNumber)).thenReturn(votingsForVoterWithProgress);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/phaseNumber/"+phaseNumber)
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"votings\":[{\"phase\":1,\"timeVotingIsFinished\":-1,\"options\":[],\"id\":0,\"title\":\"Special title\"}],\"votingOfficials\":[],\"votingsVoterProgress\":[{\"participatedInDecryption\":false,\"usesCustomKey\":false,\"phaseNumber\":0,\"votingId\":1,\"createdSecretShares\":true,\"voted\":true}]}",response.getContentAsString());
    }

    @Test
    public void getVotingsForPhaseTwoShouldReturnValue() throws Exception {
        int phaseNumber = 2;
        Long voterId = 27L;

        List<Voting> votings = new ArrayList<>();
        votings.add(new Voting().setTitle("Special title").setPhase(phaseNumber));

        List<VotingVoterProgress> votingVoterProgresses = new ArrayList<>();
        votingVoterProgresses.add(new VotingVoterProgress()
                .setVotingId(1L)
                .setCreatedSecretShares(true)
                .setVoted(true)
                .setParticipatedInDecryption(false));

        List<VotingOfficials> votingOfficialsList = new ArrayList<>();

        VotingsForVoterWithProgress votingsForVoterWithProgress = new VotingsForVoterWithProgress(votings,votingVoterProgresses,votingOfficialsList);

        when(votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(voterId,phaseNumber)).thenReturn(votingsForVoterWithProgress);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/phaseNumber/"+phaseNumber)
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"votings\":[{\"phase\":2,\"timeVotingIsFinished\":-1,\"options\":[],\"id\":0,\"title\":\"Special title\"}],\"votingOfficials\":[],\"votingsVoterProgress\":[{\"participatedInDecryption\":false,\"usesCustomKey\":false,\"phaseNumber\":0,\"votingId\":1,\"createdSecretShares\":true,\"voted\":true}]}",response.getContentAsString());
    }

    @Test
    public void getCreatedVotingsIsOnlyForVotingAdministrators() throws Exception {
        Long voterId = 27L;

        when(mockRoleService.hasRole(voterId, Role.VOTING_ADMINISTRATOR)).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/createdVotings")
                .setUser(new SingleVoter().setId(voterId).setPublicKey("key"))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getCreatedVotingsWorks() throws Exception {
        Long voterId = 27L;

        Voting votingOne = new Voting()
                .setId(42L)
                .setTitle("Best star wars movie")
                .setDescription("Which is the best start wars movie?");

        Votings votings = new Votings().addVoting(votingOne);

        when(mockRoleService.hasRole(voterId, Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getAllVotingsByVotingAdministratorId(voterId)).thenReturn(votings);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/createdVotings")
                .setUser(new SingleVoter().setId(voterId).setPublicKey("key"))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(votings.toJson().toString(), response.getContentAsString());
    }

    @Test
    public void storeDataReturnsForbiddenIfVoterIsNotAuthenticated() throws Exception {
        String validVoting = "{\"phase\":5,\"eligibleVoters\":[" +
                "{\"name\":\"voter1\",\"id\":3,\"position\":0,\"isTrustee\":\"true\", \"canVote\":\"false\"}," +
                "{\"name\":\"voter2\",\"id\":4,\"position\":1,\"isTrustee\":\"true\", \"canVote\":\"true\"}," +
                "{\"name\":\"voter3\",\"id\":5,\"position\":2,\"isTrustee\":\"true\", \"canVote\":\"false\"}]," +
                "\"calculationParameters\":{\"primeQ\":\"37\",\"primeP\":\"31\",\"generatorG\":\"2\"},\"options\":[{\"optionPrimeNumber\":2,\"optionName\":\"Foo Option 1\"},{\"optionPrimeNumber\":3,\"optionName\":\"Foo Option 2\"},{\"optionPrimeNumber\":5,\"optionName\":\"Foo Option 3\"}],\"securityThreshold\":2,\"description\":\"Foo Description\",\"numberOfEligibleVoters\":3,\"id\":42,\"title\":\"Foo Title\"}";
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings")
                .setContent(validVoting)
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void storeDataReturnsForbiddenIfVoterIsNotVotingAdministrator() throws Exception {
        String validVoting = "{\"phase\":5,\"eligibleVoters\":[" +
                "{\"name\":\"voter1\",\"id\":3,\"position\":0,\"isTrustee\":\"true\", \"canVote\":\"false\"}," +
                "{\"name\":\"voter2\",\"id\":4,\"position\":1,\"isTrustee\":\"true\", \"canVote\":\"true\"}," +
                "{\"name\":\"voter3\",\"id\":5,\"position\":2,\"isTrustee\":\"true\", \"canVote\":\"false\"}]," +
                "\"calculationParameters\":{\"primeQ\":\"37\",\"primeP\":\"31\",\"generatorG\":\"2\"},\"options\":[{\"optionPrimeNumber\":2,\"optionName\":\"Foo Option 1\"},{\"optionPrimeNumber\":3,\"optionName\":\"Foo Option 2\"},{\"optionPrimeNumber\":5,\"optionName\":\"Foo Option 3\"}],\"securityThreshold\":2,\"description\":\"Foo Description\",\"numberOfEligibleVoters\":3,\"id\":42,\"title\":\"Foo Title\"}";
        Long voterId = 27L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings")
                .setContent(validVoting)
                .setUser(new SingleVoter().setId(voterId).setPublicKey("key"))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void storeDataWorks() throws Exception {
        Long voterId = 27L;
        ArgumentCaptor<NewlyCreatedVoting> votingsArgumentCaptor = ArgumentCaptor.forClass(NewlyCreatedVoting.class);

        Voting voting = new Voting()
                .setId(42L);

        when(votingsDataService.addVoting(votingsArgumentCaptor.capture())).thenReturn(Optional.of(voting));
        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);

        String validVoting = "{\"phase\":5,\"eligibleVoters\":[" +
                "{\"name\":\"voter1\",\"id\":3,\"position\":0,\"isTrustee\":\"true\", \"canVote\":\"false\"}," +
                "{\"name\":\"voter2\",\"id\":4,\"position\":1,\"isTrustee\":\"true\", \"canVote\":\"true\"}," +
                "{\"name\":\"voter3\",\"id\":5,\"position\":2,\"isTrustee\":\"true\", \"canVote\":\"false\"}]," +
                "\"calculationParameters\":{\"primeQ\":\"37\",\"primeP\":\"31\",\"generatorG\":\"2\"},\"options\":[{\"optionPrimeNumber\":2,\"optionName\":\"Foo Option 1\"},{\"optionPrimeNumber\":3,\"optionName\":\"Foo Option 2\"},{\"optionPrimeNumber\":5,\"optionName\":\"Foo Option 3\"}],\"securityThreshold\":2,\"description\":\"Foo Description\",\"numberOfEligibleVoters\":3,\"id\":42,\"title\":\"Foo Title\"}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings")
                .setContent(validVoting)
                .setUser(new SingleVoter().setId(voterId).setPublicKey("key"))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals("{\"votings\":[{\"phase\":0,\"timeVotingIsFinished\":-1,\"options\":[],\"id\":42}]}",response.getContentAsString());
        Assert.assertEquals(voterId, votingsArgumentCaptor.getValue().getVotingAdministrator().getId());
    }

    @Test
    public void internalErrorPropagated() throws Exception {
        Long voterId = 27L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.addVoting(any())).thenReturn(Optional.empty());

        String validVoting = "{\"phase\":5,\"eligibleVoters\":[" +
                "{\"name\":\"voter1\",\"id\":3,\"position\":0,\"isTrustee\":\"true\", \"canVote\":\"false\"}," +
                "{\"name\":\"voter2\",\"id\":4,\"position\":1,\"isTrustee\":\"true\", \"canVote\":\"true\"}," +
                "{\"name\":\"voter3\",\"id\":5,\"position\":2,\"isTrustee\":\"true\", \"canVote\":\"false\"}]," +
                "\"calculationParameters\":{\"primeQ\":\"37\",\"primeP\":\"31\",\"generatorG\":\"2\"},\"options\":[{\"optionPrimeNumber\":2,\"optionName\":\"Foo Option 1\"},{\"optionPrimeNumber\":3,\"optionName\":\"Foo Option 2\"},{\"optionPrimeNumber\":5,\"optionName\":\"Foo Option 3\"}],\"securityThreshold\":2,\"description\":\"Foo Description\",\"numberOfEligibleVoters\":3,\"id\":42,\"title\":\"Foo Title\"}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings")
                .setContent(validVoting)
                .setUser(new SingleVoter().setId(voterId).setPublicKey("key"))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
        Assert.assertEquals("Internal server error",response.getContentAsString());

    }

    @Test
    public void getVotingsOverviewVoterNeedsToBeKnown() throws Exception {
        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/1/votingsOverview")
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getVotingsOverviewVoterNeedsToBeVotingAdministrator() throws Exception {
        Long voterId = 27L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/1/votingsOverview")
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getVotingsOverviewNotFoundPropagated() throws Exception {
        Long voterId = 27L;
        Long votingId = 42L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/42/votingsOverview")
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void getVotingsOverviewVoterNeedsToBeVotingAdministratorForRequestedVoting() throws Exception {
        Long voterId = 27L;
        Long votingId = 42L;

        Long differentVoterId = 2L;

        VotingProgress votingProgress = new VotingProgress()
                .setVotingAdministrator(new SingleVoter().setId(differentVoterId));

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(Optional.of(votingProgress));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/42/votingsOverview")
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void getVotingsOverviewWorks() throws Exception {
        Long voterId = 27L;
        Long votingId = 42L;
        Long securityThreshold = 7L;

        VotingProgress votingProgress = new VotingProgress()
                .setVotingAdministrator(new SingleVoter().setId(voterId))
                .setVotersProgressList(Arrays.asList())
                .setSecurityThreshold(securityThreshold);

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(Optional.of(votingProgress));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/42/votingsOverview")
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals("{\"phaseNumber\":0,\"securityThreshold\":7,\"votersProgress\":[]}",response.getContentAsString());
    }

    @Test
    public void invalidDataReturnsError() throws Exception {
        Long voterId = 27L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.addVoting(any())).thenReturn(Optional.empty());

        String votingWithoutTitle = "{\"description\":\"Foo Description\",\"options\":[{\"optionName\":\"Foo Option 1\"},{\"optionName\":\"Foo Option 2\"},{\"optionName\":\"Foo Option 3\"}]}";

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings")
                .setContent(votingWithoutTitle)
                .setUser(new SingleVoter().setId(voterId).setPublicKey("key"))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void getVotingsSummaryReturns404IfNotFound() throws Exception {
        Long voterId = 27L;
        Long votingId = 42L;

        when(votingsDataService.getVotingSummary(votingId)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/42/votingSummary")
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void getVotingsSummaryReturnsDataIfThere() throws Exception {
        Long voterId = 27L;
        Long votingId = 42L;

        JSONObject jsonObject = new JSONObject()
                .put("dummyKey","dummyValue");

        when(votingsDataService.getVotingSummary(votingId)).thenReturn(Optional.of(jsonObject));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/42/votingSummary")
                .setUser(new SingleVoter().setId(voterId))
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(jsonObject.toString(),response.getContentAsString());
    }

    @Test
    public void setPhaseNumberReturnsFalseIfNotAllowed() throws Exception {
        long voteId = 42;
        int phaseNumber = 27;

        when(votingsDataService.setPhaseNumber(voteId,phaseNumber)).thenReturn(Optional.empty());

        JSONObject jsonObject = new JSONObject()
                .put("phaseNumber",phaseNumber);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/phaseNumber")
                .setContent(jsonObject.toString())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void setPhaseNumberReturnsForbiddenIfNotIncreasedByOne() throws Exception {
        long voteId = 42;
        int phaseNumber = 27;
        long voterId = 9L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(voteId)).thenReturn(Optional.of(
                new VotingProgress()
                        .setPhaseNumber(0)
                        .setVotingAdministrator(new SingleVoter().setId(voterId))));

        JSONObject jsonObject = new JSONObject()
                .put("phaseNumber",phaseNumber);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/phaseNumber")
                .setContent(jsonObject.toString())
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
        Assert.assertEquals("Phase number can only be increased by 1.",response.getContentAsString());
    }

    @Test
    public void setPhaseNumberReturnsTrueIfEverythingIsOk() throws Exception {
        long voteId = 42;
        int phaseNumber = Phases.VOTING_PHASE.getNumber();
        long voterId = 7L;

        when(votingsDataService.getVotingProgress(voteId)).thenReturn(Optional.of(
                new VotingProgress()
                        .setVotersProgressList(Arrays.asList(
                                new SingleVoterProgress().setVoter(new SingleVoter().setId(1L).setEmail("1@1")).setCanVote(true),
                                new SingleVoterProgress().setVoter(new SingleVoter().setId(1L).setEmail("2@2")).setCanVote(false),
                                new SingleVoterProgress().setVoter(new SingleVoter().setId(1L).setEmail("3@3")).setCanVote(true)
                        ))
                    .setVotingAdministrator(new SingleVoter().setId(voterId))
                    .setPhaseNumber(phaseNumber-1)

        ));

        Voting savedVoting = new Voting()
                .setPhase(phaseNumber);

        when(votingsDataService.setPhaseNumber(voteId,phaseNumber)).thenReturn(Optional.of(savedVoting));
        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(mailTextCreatorService.createVoteMailText(any(),any(),any())).thenReturn("Dummy");
        votingsDataService.setPhaseNumber(voteId,phaseNumber);


        JSONObject jsonObject = new JSONObject()
                .put("phaseNumber",phaseNumber)
                .put("sendNotificationMail", true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/phaseNumber")
                .setContent(jsonObject.toString())
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(mailService,times(1)).sendMail(eq("1@1"),anyString(),anyString());
        verify(mailService,times(0)).sendMail(eq("2@2"),anyString(),anyString());
        verify(mailService,times(1)).sendMail(eq("3@3"),anyString(),anyString());

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(savedVoting.toJson().toString(),response.getContentAsString());
    }

    @Test
    public void setPhaseNumberReturnsFalseIfVotingIsOngoing() throws Exception {
        long voteId = 42;
        int phaseNumber = Phases.VOTING_PHASE.getNumber()+1;
        long voterId = 9L;

        long dateFarInTheFuture = 154879607374500L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(voteId)).thenReturn(Optional.of(
                new VotingProgress()
                        .setPhaseNumber(phaseNumber-1)
                        .setVotingAdministrator(new SingleVoter().setId(voterId))
                        .setTimeVotingIsFinished(dateFarInTheFuture)));

        JSONObject jsonObject = new JSONObject()
                .put("phaseNumber",phaseNumber);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/phaseNumber")
                .setContent(jsonObject.toString())
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
        Assert.assertEquals("Cannot increase phase number since the voting is still in progress.",response.getContentAsString());
    }

    @Test
    public void setVotingEndTimeReturnsFalseIfNotPermitted() throws Exception {
        long voteId = 42;
        long voterId = 7L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/timeVotingFinishes")
                .setContent("{\"votingEndTime\": 5}")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void setVotingEndTimeReturnsFalseIfAlreadyInVotingPhase() throws Exception {
        long voteId = 42;
        long voterId = 7L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(voteId)).thenReturn(
                Optional.of(new VotingProgress()
                        .setPhaseNumber(Phases.VOTING_PHASE.getNumber())
                        .setVotingAdministrator(new SingleVoter().setId(voterId))

                )
        );

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/timeVotingFinishes")
                .setContent("{\"votingEndTime\": 5}")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void setVotingEndTimeReturnsOkIfEverythingWentFine() throws Exception {
        long votingId = 42;
        long voterId = 47L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(
                Optional.of(new VotingProgress()
                        .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber())
                        .setVotingAdministrator(new SingleVoter().setId(voterId))
                )
        );

        Voting savedVoting = new Voting()
                .setTimeVotingIsFinished(7);

        when(votingsDataService.setTimeVotingIsFinished(votingId,7)).thenReturn(Optional.of(
                savedVoting
        ));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+votingId+"/timeVotingFinishes")
                .setContent("{\"votingEndTime\": 7}")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(taskScheduler,times(1)).schedulePhaseUpdateTask(votingId,7L);

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(savedVoting.toJson().toString(),response.getContentAsString());
    }

    @Test
    public void setVotingEndTimeReturnsFalseIfError() throws Exception {
        long votingId = 42;
        long voterId = 7L;

        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(
                Optional.of(new VotingProgress()
                        .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber())
                        .setVotingAdministrator(new SingleVoter().setId(voterId))
                )
        );

        when(votingsDataService.setTimeVotingIsFinished(votingId,7)).thenReturn(Optional.empty());

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+votingId+"/timeVotingFinishes")
                .setContent("{\"votingEndTime\": 7}")
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
    }

    @Test
    public void setPhaseNumberReturnsServerErrorIfPublishPhaseFails() throws Exception {
        long voteId = 42;
        int phaseNumber = Phases.PUBLISH_PHASE.getNumber();
        long voterId = 7L;

        when(votingsDataService.getVotingProgress(voteId)).thenReturn(Optional.of(
                new VotingProgress()
                    .setVotingAdministrator(new SingleVoter().setId(voterId))
                    .setPhaseNumber(phaseNumber-1)
        ));

        Voting savedVoting = new Voting()
                .setPhase(phaseNumber);

        when(votingsDataService.setPhaseNumber(voteId,phaseNumber)).thenReturn(Optional.of(savedVoting));
        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsSummaryService.createVotingsSummary(voteId)).thenReturn(Optional.empty());

        JSONObject jsonObject = new JSONObject()
                .put("phaseNumber",phaseNumber)
                .put("sendNotificationMail",false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/phaseNumber")
                .setContent(jsonObject.toString())
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),response.getStatus());
    }

    @Test
    public void setPhaseNumberPublishPhaseWorks() throws Exception {
        long voteId = 42;
        int phaseNumber = Phases.PUBLISH_PHASE.getNumber();
        long voterId = 7L;
        String hash = "d9920dc69e7b8352ea5774041afeaf8eeebd1c4985bae1368c2a5559c12bcb56";
        String transactionId = "8352ea5774041afe";

        when(votingsDataService.getVotingProgress(voteId)).thenReturn(Optional.of(
                new VotingProgress()
                        .setVotingAdministrator(new SingleVoter().setId(voterId))
                        .setPhaseNumber(phaseNumber-1)
        ));

        Voting savedVoting = new Voting()
                .setPhase(phaseNumber);

        JSONObject summaryJsonObject = new JSONObject().put("dummy","dummyContent");

        when(votingsDataService.setPhaseNumber(voteId,phaseNumber)).thenReturn(Optional.of(savedVoting));
        when(votingsDataService.setVotingSummary(eq(voteId),any(),anyString())).thenReturn(Optional.of(savedVoting));
        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);
        when(votingsSummaryService.createVotingsSummary(voteId)).thenReturn(Optional.of(
                summaryJsonObject
        ));
        when(hashService.createSHA256Hash(summaryJsonObject.toString())).thenReturn(hash);
        when(ethereumService.sendMessageToTestNetwork(hash)).thenReturn(Optional.of(transactionId));

        JSONObject jsonObject = new JSONObject()
                .put("phaseNumber",phaseNumber)
                .put("sendNotificationMail",false);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.PUT)
                .setUrl("/api/votings/"+voteId+"/phaseNumber")
                .setContent(jsonObject.toString())
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        verify(votingsDataService,times(1)).setVotingSummary(voteId,summaryJsonObject,transactionId);
        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }

    @Test
    public void deleteVotingReturnsForbiddenIfCurrentVoterIsNotKnown() throws Exception {
        Long votingId = 27L;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/votings/"+ votingId)
                .doUnauthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void deleteVotingReturnsForbiddenIfCurrentVoterIsNotVotingAdministratorOfVoting() throws Exception {
        Long votingId = 27L;
        Long voterId = 42L;

        Long otherId = 7L;

        when(mockRoleService.hasRole(voterId,Role.ADMINISTRATOR)).thenReturn(false);
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(
                Optional.of(new VotingProgress().setVotingAdministrator(new SingleVoter().setId(otherId)))
        );

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/votings/"+ votingId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void deleteVotingReturnsNotFoundIfNotThere() throws Exception {
        Long votingId = 27L;
        Long voterId = 42L;

        when(votingsDataService.deleteVoting(votingId)).thenThrow(new NoSuchVotingException("dummy"));
        when(votingsDataService.getVotingProgress(votingId)).thenReturn(
                Optional.of(new VotingProgress().setVotingAdministrator(new SingleVoter().setId(voterId)))
        );
        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/votings/"+ votingId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.NOT_FOUND.value(),response.getStatus());
    }

    @Test
    public void deleteVotingReturnsOkIfItWorked() throws Exception {
        Long votingId = 27L;
        Long voterId = 42L;

        when(votingsDataService.getVotingProgress(votingId)).thenReturn(
                Optional.of(new VotingProgress().setVotingAdministrator(new SingleVoter().setId(voterId)))
        );
        when(mockRoleService.hasRole(voterId,Role.VOTING_ADMINISTRATOR)).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.DELETE)
                .setUrl("/api/votings/"+ votingId)
                .setVoterId(voterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
    }

}
