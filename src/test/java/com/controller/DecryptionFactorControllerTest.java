package com.controller;

import com.data.decryptionfactor.DecryptionFactors;
import com.data.decryptionfactor.SingleDecryptionFactor;
import com.exceptions.DuplicatedVoteException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.vote.VoteService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class DecryptionFactorControllerTest {

    private RequestContextHolder requestContextHolder;

    @MockBean
    private VoteService voteService;

    private ControllerTestHelper helper;

    @Before()
    public void setup(){
        requestContextHolder = new FakeRequestContextHolder();
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(new DecryptionFactorController(voteService,requestContextHolder));
    }

    @Test
    public void userTriesToSpecifyDecryptionFactorOfOtherPermissionDenied() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionFactors/voters/"+voterId)
                .setVoterId(differentVoterId)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void invalidContentReturnsBadRequest() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionFactors/voters/"+voterId)
                .setVoterId(voterId)
                .setContent("invalid")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void internalErrorIsPropagated() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        when(voteService.setDecryptionFactor(anyLong(),anyLong(),any())).thenThrow(new DuplicatedVoteException("dummy message"));

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionFactors/voters/"+voterId)
                .setVoterId(voterId)
                .setContent(createValidInput())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
        Assert.assertEquals("dummy message",response.getContentAsString());
    }

    @Test
    public void settingDecryptionFactorWorks() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        ArgumentCaptor<SingleDecryptionFactor> argumentCaptor = ArgumentCaptor.forClass(SingleDecryptionFactor.class);
        when(voteService.setDecryptionFactor(anyLong(),anyLong(),argumentCaptor.capture())).thenReturn(true);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionFactors/voters/"+voterId)
                .setVoterId(voterId)
                .setContent(createValidInput())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals("d1",argumentCaptor.getValue().getDecryptionFactor());
        Assert.assertEquals("sign",argumentCaptor.getValue().getSignature());
    }

    @Test
    public void getAllDecryptionFactorsWorks() throws Exception {
        Long votingsId = 1L;
        DecryptionFactors decryptionFactors = new DecryptionFactors()
                .addDecryptionFactor(new SingleDecryptionFactor().setDecryptionFactor("d1").setSignature("sign1"))
                .addDecryptionFactor(new SingleDecryptionFactor().setDecryptionFactor("d2").setSignature("sign2"));

        when(voteService.getAllDecryptionFactors(votingsId)).thenReturn(decryptionFactors);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.GET)
                .setUrl("/api/votings/"+votingsId+"/decryptionFactors")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.OK.value(),response.getStatus());
        Assert.assertEquals(decryptionFactors.toJson().toString(),response.getContentAsString());
    }

    private String createValidInput() {
        JSONObject validJsonObject = new JSONObject()
                .put("decryptionFactor","d1")
                .put("signature","sign");

        return validJsonObject.toString();
    }

}

