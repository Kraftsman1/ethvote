package com.controller;

import com.data.proofs.DecryptionNIZKProof;
import com.data.voter.SingleVoter;
import com.exceptions.DuplicatedVoteException;
import com.service.context.FakeRequestContextHolder;
import com.services.context.RequestContextHolder;
import com.services.data.proofs.DecryptionNIZKProofService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class DecryptionNIZKPControllerTest {

    private RequestContextHolder requestContextHolder;

    private ControllerTestHelper helper;

    private DecryptionNIZKPController decryptionNIZKPController;

    @Mock
    private DecryptionNIZKProofService mockDecryptionNIZKProofService;

    @Before
    public void setup() {
        requestContextHolder = new FakeRequestContextHolder();
        mockDecryptionNIZKProofService = Mockito.mock(DecryptionNIZKProofService.class);
        decryptionNIZKPController = new DecryptionNIZKPController(mockDecryptionNIZKProofService, requestContextHolder);
        helper = new ControllerTestHelper(requestContextHolder)
                .setController(decryptionNIZKPController);
    }

    @Test
    public void voterTriesToSpecifyZeroKnowledgeProofForOtherVoter() throws Exception {
        long votingsId = 1;
        long voterId = 4;
        long differentVoterId = 5;

        SingleVoter voter = new SingleVoter().setName("u").setId(differentVoterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),response.getStatus());
    }

    @Test
    public void invalidData() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .setContent("invalidContent")
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.BAD_REQUEST.value(),response.getStatus());
    }

    @Test
    public void conflictIsReturnedIfServiceReturnError() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        when(mockDecryptionNIZKProofService.setNIZKProofs(eq(votingsId),eq(voterId),any())).thenThrow(new DuplicatedVoteException("dummy message"));

        DecryptionNIZKProof decryptionNIZKProof = new DecryptionNIZKProof()
                .setA("a")
                .setB("b")
                .setR("r")
                .setSignature("sign");

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .setContent(decryptionNIZKProof.toJson().toString())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CONFLICT.value(),response.getStatus());
        Assert.assertEquals("dummy message",response.getContentAsString());
    }

    @Test
    public void createdIsReturnedIfEverythingWorked() throws Exception {
        long votingsId = 1;
        long voterId = 4;

        SingleVoter voter = new SingleVoter().setName("u").setId(voterId);

        when(mockDecryptionNIZKProofService.setNIZKProofs(eq(votingsId),eq(voterId),any())).thenReturn(true);

        DecryptionNIZKProof decryptionNIZKProof = new DecryptionNIZKProof()
                .setA("a")
                .setB("b")
                .setR("r")
                .setSignature("sign");

        MockHttpServletResponse response = helper
                .setMethod(HttpMethod.POST)
                .setUrl("/api/votings/"+votingsId+"/decryptionZeroKnowledgeProofs/voters/"+voterId)
                .setUser(voter)
                .setContent(decryptionNIZKProof.toJson().toString())
                .doAuthenticatedRequest();

        Assert.assertEquals(HttpStatus.CREATED.value(),response.getStatus());
        Assert.assertEquals("\"Zero knowledge proof for decryption was added.\"",response.getContentAsString());
    }



}
