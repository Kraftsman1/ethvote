package com.data.proofs;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VoteNIZKProofsTest {

    @Test
    public void returnValidJsonObject() {
        SingleVoteNIZKProof proof1 = new SingleVoteNIZKProof()
                .setMessageIndex(0)
                .setA("a1")
                .setB("b1")
                .setC("c1")
                .setR("r1");
        SingleVoteNIZKProof proof2 = new SingleVoteNIZKProof()
                .setMessageIndex(1)
                .setA("a2")
                .setB("b2")
                .setC("c2")
                .setR("r2");

        VoteNIZKProofs proofs = new VoteNIZKProofs()
                .setFromVoterId(1L)
                .setFromVoterName("v1")
                .addProof(proof1)
                .addProof(proof2);

        JSONObject jsonObject = proofs.toJson();

        Assert.assertEquals(2,jsonObject.getJSONArray("proofs").length());
        Assert.assertEquals(1L,jsonObject.getLong("fromVoterId"));
        Assert.assertEquals("v1",jsonObject.getString("fromVoterName"));
        Assert.assertEquals(proof1.toJson().toString(),jsonObject.getJSONArray("proofs").get(0).toString());
        Assert.assertEquals(proof2.toJson().toString(),jsonObject.getJSONArray("proofs").get(1).toString());
    }
}
