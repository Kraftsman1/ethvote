package com.data.votings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingCalculationParametersTest {

    @Test
    public void returnValidJsonObject() {
        String primeP = "31";
        String primeQ = "37";
        String generatorG = "4";
        VotingCalculationParameters votingCalculationParameters = new VotingCalculationParameters()
                .setPrimeP(primeP)
                .setPrimeQ(primeQ)
                .setGeneratorG(generatorG);
        JSONObject json = votingCalculationParameters.toJson();
        Assert.assertEquals(primeP,json.get("primeP"));
        Assert.assertEquals(primeQ,json.get("primeQ"));
        Assert.assertEquals(generatorG,json.get("generatorG"));
    }
}
