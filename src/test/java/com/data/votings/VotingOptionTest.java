package com.data.votings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class VotingOptionTest {

    @Test
    public void returnValidJsonObject() {
        String optionName = "foo";
        Long optionPrimeNumber = 29L;
        VotingOption option = new VotingOption()
                .setOptionName(optionName)
                .setOptionPrimeNumber(optionPrimeNumber);
        JSONObject json = option.toJson();
        Assert.assertEquals(optionName,json.get("optionName"));
        Assert.assertEquals(optionPrimeNumber,json.get("optionPrimeNumber"));
    }
}
