package com.data.votings;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class EligibleVoterTest {

    @Test
    public void returnValidJsonObject() {
        String userName = "voter1";
        Long id = 27L;
        Long position = 42L;
        EligibleVoter eligibleVoter = new EligibleVoter().setName(userName).setId(27L).setPosition(42L).setCanVote(true).setIsTrustee(true);
        JSONObject json = eligibleVoter.toJson();
        Assert.assertEquals(id,json.get("id"));
        Assert.assertEquals(position,json.get("position"));
        Assert.assertEquals(userName,json.get("name"));
        Assert.assertTrue(json.getBoolean("isTrustee"));
        Assert.assertTrue(json.getBoolean("canVote"));
    }
}
