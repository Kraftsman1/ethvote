package com.data.decryptionfactor;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class SingleDecryptionFactorTest {

    @Test
    public void returnValidJsonObject() {
        long fromVoterId = 27;
        String fromVoterName = "voterName";
        long fromVoterPosition = 5;
        String decryptionFactor = "decryptionFactor";

        SingleDecryptionFactor singleDecryptionFactorTest = new SingleDecryptionFactor()
                .setFromVoterId(fromVoterId)
                .setFromVoterName(fromVoterName)
                .setFromVoterPosition(fromVoterPosition)
                .setDecryptionFactor(decryptionFactor);


        JSONObject object = singleDecryptionFactorTest.toJson();

        Assert.assertEquals(fromVoterId,object.getLong("fromVoterId"));
        Assert.assertEquals(fromVoterName,object.getString("fromVoterName"));
        Assert.assertEquals(fromVoterPosition,object.getLong("fromVoterPosition"));
        Assert.assertEquals(decryptionFactor,object.getString("decryptionFactor"));
    }
}
