package com.interceptors;

import com.helpers.NoopController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class CorsInterceptorTest {

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new NoopController())
                .addInterceptors(new CorsInterceptor())
                .build();
    }

    @Test
    public void protectedUrlWithoutJwtTokenShouldNotPassButSetRightHeaders() throws Exception {
        MvcResult result = mockMvc.perform(get("/notImportant")).andReturn();
        Assert.assertEquals("*",result.getResponse().getHeader("Access-Control-Allow-Origin"));
        Assert.assertEquals("*",result.getResponse().getHeader("Access-Control-Allow-Methods"));
        Assert.assertEquals("RefreshToken",result.getResponse().getHeader("Access-Control-Expose-Headers"));
        Assert.assertEquals("Authorization, Content-Type",result.getResponse().getHeader("Access-Control-Allow-Headers"));
    }

    @Test
    public void assureOnlyAddedOnce() throws Exception {
        HttpServletRequest servletRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse servletResponse = Mockito.mock(HttpServletResponse.class);

        when(servletResponse.containsHeader(any())).thenReturn(true);

        CorsInterceptor corsInterceptor = new CorsInterceptor();
        corsInterceptor.preHandle(servletRequest,servletResponse,null);

        verify(servletResponse,times(0)).addHeader(any(),any());
    }
}