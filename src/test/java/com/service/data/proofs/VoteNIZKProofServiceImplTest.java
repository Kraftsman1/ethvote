package com.service.data.proofs;

import com.data.proofs.SingleVoteNIZKProof;
import com.data.proofs.VoteNIZKProofs;
import com.data.voter.VoterEntityWithIdSetter;
import com.enums.Phases;
import com.exceptions.IllegalVoteException;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.singlevotingvoter.VoteNIZKPEntity;
import com.models.user.UserEntity;
import com.models.voting.VotingEntity;
import com.models.voting.VotingsRepository;
import com.services.data.proofs.VoteNIZKProofService;
import com.services.data.proofs.VoteNIZKProofServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class VoteNIZKProofServiceImplTest {

    private SingleVotingVoterRepository mockSingleShareRepository;

    private VotingsRepository mockVotingsRepository;

    private VoteNIZKProofService voteNIZKProofService;

    @Before
    public void setup() {
        mockSingleShareRepository = Mockito.mock(SingleVotingVoterRepository.class);
        mockVotingsRepository = Mockito.mock(VotingsRepository.class);
        voteNIZKProofService = new VoteNIZKProofServiceImpl(mockSingleShareRepository,mockVotingsRepository);
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsReturnsNoSuchVotingIfVotingNotExisting() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;
        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.empty());
        voteNIZKProofService.setNIZKProofs(votingId,voterId, new VoteNIZKProofs());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsThrowsExceptionIfAlreadySpecified() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();

        VoteNIZKPEntity voteNIZKPEntity = new VoteNIZKPEntity();
        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity)
                .addNonInteractiveZeroKnowledgeProof(voteNIZKPEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        voteNIZKProofService.setNIZKProofs(votingId,voterId, new VoteNIZKProofs());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsThrowsExceptionIfVotingClosed() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity().setPhaseNumber(Phases.DECRYPTION_PHASE.getNumber());

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        voteNIZKProofService.setNIZKProofs(votingId,voterId, new VoteNIZKProofs());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setProofsThrowsExceptionIfTimeIsOver() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity()
                .setPhaseNumber(Phases.VOTING_PHASE.getNumber())
                .setTimeVotingIsFinished(27L);

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        voteNIZKProofService.setNIZKProofs(votingId,voterId, new VoteNIZKProofs());
    }

    @Test
    public void savingNIZKProofInvokesTheRightCall() throws IllegalVoteException {
        long votingId = 7L;
        long voterId = 9L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity()
                .setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);

        when(mockSingleShareRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        voteNIZKProofService.setNIZKProofs(votingId,voterId,
                new VoteNIZKProofs()
                    .addProof(new SingleVoteNIZKProof()
                        .setA("a1")
                        .setB("b1")
                        .setC("c1")
                        .setR("r1")
                        .setMessageIndex(0)
                        .setSignature("sign1"))
                    .addProof(new SingleVoteNIZKProof()
                        .setA("a2")
                        .setB("b2")
                        .setC("c2")
                        .setR("r2")
                        .setMessageIndex(1)
                        .setSignature("sign2")));

        ArgumentCaptor<SingleVotingVoterEntity> singleVotingVoterEntityArgumentCaptor = ArgumentCaptor.forClass(SingleVotingVoterEntity.class);
        verify(mockSingleShareRepository,times(1)).saveAndFlush(singleVotingVoterEntityArgumentCaptor.capture());
        SingleVotingVoterEntity savedSingleVotingVoterEntity = singleVotingVoterEntityArgumentCaptor.getValue();

        Assert.assertEquals(0L,savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(0).getMessageIndex().longValue());
        Assert.assertEquals("a1",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(0).getA());
        Assert.assertEquals("b1",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(0).getB());
        Assert.assertEquals("c1",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(0).getC());
        Assert.assertEquals("r1",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(0).getR());
        Assert.assertEquals("sign1",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(0).getSignature());

        Assert.assertEquals(1L,savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(1).getMessageIndex().longValue());
        Assert.assertEquals("a2",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(1).getA());
        Assert.assertEquals("b2",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(1).getB());
        Assert.assertEquals("c2",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(1).getC());
        Assert.assertEquals("sign2",savedSingleVotingVoterEntity.getNonInteractiveZeroKnowledgeProof().get(1).getSignature());
    }

    @Test
    public void getNIZKProofsReturnsEmptyIfNotAvailable() {
        long votingId = 7;

        when(mockVotingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.empty());

        Optional<List<VoteNIZKProofs>> optProofs = voteNIZKProofService.getNIZKProofs(votingId);
        Assert.assertFalse(optProofs.isPresent());
    }

    @Test
    public void getNIZKProofsReturnsData() {
        long votingId = 7;

        VotingEntity votingEntity = new VotingEntity();
        UserEntity userEntity1 = new VoterEntityWithIdSetter().setId(1L).setUsername("v1");
        UserEntity userEntity2 = new VoterEntityWithIdSetter().setId(2L).setUsername("v2");
        UserEntity userEntity3 = new VoterEntityWithIdSetter().setId(3L).setUsername("v3");

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity);
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity);

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3);

        singleVotingVoterEntity2.setVotedToTrue();
        singleVotingVoterEntity3.setVotedToTrue();

        singleVotingVoterEntity2
                .addNonInteractiveZeroKnowledgeProof(
                    new VoteNIZKPEntity()
                        .setA("a1")
                        .setB("b1")
                        .setC("c1")
                        .setR("r1")
                        .setMessageIndex(0)
                        .setSignature("sign1"))
                .addNonInteractiveZeroKnowledgeProof(
                     new VoteNIZKPEntity()
                         .setA("a2")
                         .setB("b2")
                         .setC("c2")
                         .setR("r2")
                         .setMessageIndex(1)
                         .setSignature("sign2"));

        singleVotingVoterEntity3
                .addNonInteractiveZeroKnowledgeProof(
                    new VoteNIZKPEntity()
                            .setA("a3")
                            .setB("b3")
                            .setC("c3")
                            .setR("r3")
                            .setMessageIndex(0)
                            .setSignature("sign3"));

        when(mockVotingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        Optional<List<VoteNIZKProofs>> optProofs = voteNIZKProofService.getNIZKProofs(votingId);
        Assert.assertTrue(optProofs.isPresent());
        List<VoteNIZKProofs> proofs = optProofs.get();

        Assert.assertEquals(2,proofs.size());
        Assert.assertEquals(2,proofs.get(0).getSingleVoteNIZKProofs().size());

        Assert.assertEquals(2L,proofs.get(0).getFromVoterId());
        Assert.assertEquals("v2",proofs.get(0).getFromVoterName());

        Assert.assertEquals("a1",proofs.get(0).getSingleVoteNIZKProofs().get(0).getA());
        Assert.assertEquals("b1",proofs.get(0).getSingleVoteNIZKProofs().get(0).getB());
        Assert.assertEquals("c1",proofs.get(0).getSingleVoteNIZKProofs().get(0).getC());
        Assert.assertEquals("r1",proofs.get(0).getSingleVoteNIZKProofs().get(0).getR());
        Assert.assertEquals("sign1",proofs.get(0).getSingleVoteNIZKProofs().get(0).getSignature());
        Assert.assertEquals(0L,proofs.get(0).getSingleVoteNIZKProofs().get(0).getMessageIndex().longValue());

        Assert.assertEquals("a2",proofs.get(0).getSingleVoteNIZKProofs().get(1).getA());
        Assert.assertEquals("b2",proofs.get(0).getSingleVoteNIZKProofs().get(1).getB());
        Assert.assertEquals("c2",proofs.get(0).getSingleVoteNIZKProofs().get(1).getC());
        Assert.assertEquals("r2",proofs.get(0).getSingleVoteNIZKProofs().get(1).getR());
        Assert.assertEquals("sign2",proofs.get(0).getSingleVoteNIZKProofs().get(1).getSignature());
        Assert.assertEquals(1L,proofs.get(0).getSingleVoteNIZKProofs().get(1).getMessageIndex().longValue());

        Assert.assertEquals(3L,proofs.get(1).getFromVoterId());
        Assert.assertEquals("v3",proofs.get(1).getFromVoterName());

        Assert.assertEquals("a3",proofs.get(1).getSingleVoteNIZKProofs().get(0).getA());
        Assert.assertEquals("b3",proofs.get(1).getSingleVoteNIZKProofs().get(0).getB());
        Assert.assertEquals("c3",proofs.get(1).getSingleVoteNIZKProofs().get(0).getC());
        Assert.assertEquals("r3",proofs.get(1).getSingleVoteNIZKProofs().get(0).getR());
        Assert.assertEquals("sign3",proofs.get(1).getSingleVoteNIZKProofs().get(0).getSignature());
        Assert.assertEquals(0L,proofs.get(1).getSingleVoteNIZKProofs().get(0).getMessageIndex().longValue());
    }

}
