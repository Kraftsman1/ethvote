package com.service.data.vote;

import com.data.dataforkeyderivation.DataForKeyDerivation;
import com.data.decryptionfactor.DecryptionFactors;
import com.data.decryptionfactor.SingleDecryptionFactor;
import com.data.voter.VoterEntityWithIdSetter;
import com.data.votes.SingleVote;
import com.data.votes.Votes;
import com.enums.Phases;
import com.exceptions.*;
import com.models.createdvotings.CreatedVotingsEntity;
import com.models.decryption.DecryptionRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.helper.VotingEntityWithSetters;
import com.models.voting.VotingsRepository;
import com.services.data.vote.VoteServiceImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class VoteServiceImplTest {

    private VotingsRepository votingsRepository;
    private SingleVotingVoterRepository singleVotingVoterRepository;
    private DecryptionRepository decryptionRepository;
    private VoteServiceImpl voteService;
    private final long votingsId = 42;

    @Before
    public void setup() {
        votingsRepository = Mockito.mock(VotingsRepository.class);
        singleVotingVoterRepository = Mockito.mock(SingleVotingVoterRepository.class);
        decryptionRepository = Mockito.mock(DecryptionRepository.class);
        voteService = new VoteServiceImpl(votingsRepository,singleVotingVoterRepository,decryptionRepository);
    }

    @Test
    public void getDataForKeyDerivationReturnsEmptyIfNotAvailable() {
        Long votingId = 27L;
        when(votingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.empty());
        Assert.assertFalse(voteService.getDataForKeyDerivation(votingId).isPresent());
    }

    @Test
    public void getDataForKeyDerivationReturnsValidData() {
        Long id = 27L;
        String generatorG = "123";
        String primeP = "456";
        String primeQ = "789";

        String key1 = "key1";
        String key2 = "key2";
        String key3 = "key3";

        String name1 = "name1";
        String name2 = "name2";
        String name3 = "name3";

        long id1 = 0L;
        long id2 = 1L;
        long id3 = 2L;

        long securityThreshold = 42L;
        long numberOfEligibleVoters = 3L;

        VotingCalculationParametersEntity votingCalculationParametersEntity = new VotingCalculationParametersEntity()
                .setGeneratorG(generatorG)
                .setPrimeP(primeP)
                .setPrimeQ(primeQ);

        UserEntity voter1 = new VoterEntityWithIdSetter()
                .setId(id1)
                .setUsername(name1);

        UserEntity voter2 = new VoterEntityWithIdSetter()
                .setId(id2)
                .setUsername(name2);

        UserEntity voter3 = new VoterEntityWithIdSetter()
                .setId(id3)
                .setUsername(name3);

        VotingEntityWithSetters votingEntity = new VotingEntityWithSetters();
        votingEntity
                .setId(id)
                .setSecurityThreshold(securityThreshold)
                .setCalculationParametersEntity(votingCalculationParametersEntity)
                .setNumberOfEligibleVoters(numberOfEligibleVoters);

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(voter1,votingEntity).setVoterNumber(0L).setPublicKey(key1).setIsTrustee(false).setCanVote(true);
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(voter2,votingEntity).setVoterNumber(1L).setPublicKey(key2).setIsTrustee(true).setCanVote(false);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(voter3,votingEntity).setVoterNumber(2L).setPublicKey(key3).setIsTrustee(true).setCanVote(false);

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(voter2,votingEntity);

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3)
                .setVotingAdministrator(createdVotingsEntity);

        when(votingsRepository.findOneByIdWithEligibleVotersFetched(id)).thenReturn(Optional.of(votingEntity));

        Optional<DataForKeyDerivation> dataForKeyDerivation = voteService.getDataForKeyDerivation(id);
        Assert.assertTrue(dataForKeyDerivation.isPresent());
        Assert.assertEquals(id2,dataForKeyDerivation.get().getVotingAdministratorId());

        JSONObject jsonObject = dataForKeyDerivation.get().toJson();
        JSONObject calculations = jsonObject.getJSONObject("calculationParameters");

        JSONArray voters = jsonObject.getJSONArray("eligibleVoters");
        JSONObject voterObject1 = voters.getJSONObject(0);
        JSONObject voterObject2 = voters.getJSONObject(1);
        JSONObject voterObject3 = voters.getJSONObject(2);

        Assert.assertEquals(numberOfEligibleVoters,jsonObject.getLong("numberOfEligibleVoters"));
        Assert.assertEquals(securityThreshold,jsonObject.getLong("securityThreshold"));
        Assert.assertEquals(securityThreshold,jsonObject.getLong("securityThreshold"));
        Assert.assertEquals(primeP,calculations.getString("primeP"));
        Assert.assertEquals(primeQ,calculations.getString("primeQ"));
        Assert.assertEquals(generatorG,calculations.getString("generatorG"));

        Assert.assertEquals(id1,voterObject1.getLong("id"));
        Assert.assertEquals(key1,voterObject1.getString("publicKey"));
        Assert.assertEquals(name1,voterObject1.getString("name"));
        Assert.assertEquals(0,voterObject1.getLong("position"));
        Assert.assertFalse(voterObject1.getBoolean("isTrustee"));
        Assert.assertTrue(voterObject1.getBoolean("canVote"));

        Assert.assertEquals(id2,voterObject2.getLong("id"));
        Assert.assertEquals(key2,voterObject2.getString("publicKey"));
        Assert.assertEquals(name2,voterObject2.getString("name"));
        Assert.assertEquals(1,voterObject2.getLong("position"));
        Assert.assertTrue(voterObject2.getBoolean("isTrustee"));
        Assert.assertFalse(voterObject2.getBoolean("canVote"));

        Assert.assertEquals(id3,voterObject3.getLong("id"));
        Assert.assertEquals(key3,voterObject3.getString("publicKey"));
        Assert.assertEquals(name3,voterObject3.getString("name"));
        Assert.assertEquals(2,voterObject3.getLong("position"));
        Assert.assertTrue(voterObject3.getBoolean("isTrustee"));
        Assert.assertFalse(voterObject3.getBoolean("canVote"));
    }

    @Test
    public void setVoteForVoterWorks() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String alpha = "alpha";
        String beta = "beta";
        String signature = "signature";
        SingleVote singleVote = new SingleVote()
                .setAlpha(alpha)
                .setBeta(beta)
                .setSignature(signature);

        VotingEntity votingEntity = new VotingEntity()
                .setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getVotingEntity()).thenReturn(votingEntity);

        when(singleVotingVoterEntity.getCanVote()).thenReturn(true);
        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));

        boolean successful = voteService.setVoteForVoter(voterId,votingId, singleVote);

        verify(singleVotingVoterEntity,times(1)).setAlpha(alpha);
        verify(singleVotingVoterEntity,times(1)).setBeta(beta);
        verify(singleVotingVoterEntity,times(1)).setVoteSignature(signature);
        verify(singleVotingVoterEntity,times(1)).setVotedToTrue();
        verify(singleVotingVoterRepository,times(1)).saveAndFlush(singleVotingVoterEntity);
        Assert.assertTrue(successful);
    }

    @Test
    (expected = NoSuchVotingException.class)
    public void setVoteForVoterReturnsFalseIfVotingIsNotThere() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String alpha = "alpha";
        String beta = "beta";
        String signature = "signature";
        SingleVote singleVote = new SingleVote()
                .setAlpha(alpha)
                .setBeta(beta)
                .setSignature(signature);

        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.empty());
        boolean successful = voteService.setVoteForVoter(voterId,votingId, singleVote);
        Assert.assertFalse(successful);
    }

    @Test
    (expected = DuplicateException.class)
    public void setVoteForVoterReturnsFalseIfAlreadyVoted() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String alpha = "alpha";
        String beta = "beta";
        String signature = "signature";
        SingleVote singleVote = new SingleVote()
                .setAlpha(alpha)
                .setBeta(beta)
                .setSignature(signature);

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getCanVote()).thenReturn(true);
        when(singleVotingVoterEntity.getVoted()).thenReturn(true);
        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));

        boolean successful = voteService.setVoteForVoter(voterId,votingId, singleVote);
        Assert.assertFalse(successful);
    }

    @Test
    (expected = NotPermittedException.class)
    public void setVoteForVoterReturnsFalseIfNotAllowed() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String alpha = "alpha";
        String beta = "beta";
        String signature = "signature";
        SingleVote singleVote = new SingleVote()
                .setAlpha(alpha)
                .setBeta(beta)
                .setSignature(signature);

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getCanVote()).thenReturn(false);
        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));

        boolean successful = voteService.setVoteForVoter(voterId,votingId, singleVote);
        Assert.assertFalse(successful);
    }

    @Test
    (expected = VotingClosedException.class)
    public void setVoteForVoterReturnsFalseVotingTimeIsOver() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String alpha = "alpha";
        String beta = "beta";
        String signature = "signature";
        SingleVote singleVote = new SingleVote()
                .setAlpha(alpha)
                .setBeta(beta)
                .setSignature(signature);

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getCanVote()).thenReturn(true);
        when(singleVotingVoterEntity.getVoted()).thenReturn(false);

        when(singleVotingVoterEntity.getVotingEntity()).thenReturn(
                new VotingEntity()
                        .setTimeVotingIsFinished(1234L)
                        .setPhaseNumber(Phases.VOTING_PHASE.getNumber()));
        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));

        boolean successful = voteService.setVoteForVoter(voterId,votingId, singleVote);
        Assert.assertFalse(successful);
    }

    @Test
    (expected = VotingClosedException.class)
    public void setVoteForVoterReturnsFalseIfPhaseIsNotCorrect() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String alpha = "alpha";
        String beta = "beta";
        String signature = "signature";
        SingleVote singleVote = new SingleVote()
                .setAlpha(alpha)
                .setBeta(beta)
                .setSignature(signature);

        VotingEntity votingEntity = Mockito.mock(VotingEntity.class);
        when(votingEntity.getPhaseNumber()).thenReturn(Phases.DECRYPTION_PHASE.getNumber());

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getCanVote()).thenReturn(true);
        when(singleVotingVoterEntity.getVoted()).thenReturn(false);
        when(singleVotingVoterEntity.getVotingEntity()).thenReturn(votingEntity);
        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));

        boolean successful = voteService.setVoteForVoter(voterId,votingId, singleVote);
        Assert.assertFalse(successful);
    }

    @Test
    public void getAllVotesWorks() {
        Long votingId = 27L;
        UserEntity userEntity1 = new VoterEntityWithIdSetter().setId(1L).setUsername("u1");
        UserEntity userEntity2 = new VoterEntityWithIdSetter().setId(2L).setUsername("u2");
        UserEntity userEntity3 = new VoterEntityWithIdSetter().setId(3L).setUsername("u3");

        VotingEntity votingEntity = new VotingEntity();

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity).setAlpha("a1").setBeta("b1").setVoteSignature("sign1").setVotedToTrue();
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity).setAlpha("a2").setBeta("b2").setVoteSignature("sign2").setVotedToTrue();
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity).setAlpha("a3").setBeta("b3").setVoteSignature("sign3").setVotedToTrue();

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3);

        when(votingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        Votes votes = voteService.getAllVotesForVoting(votingId);
        Assert.assertEquals(3,votes.getVotes().size());
        Assert.assertEquals(new Long(1),votes.getVotes().get(0).getFromVoterId());
        Assert.assertEquals("u1",votes.getVotes().get(0).getFromVoterName());
        Assert.assertEquals("a1",votes.getVotes().get(0).getAlpha());
        Assert.assertEquals("b1",votes.getVotes().get(0).getBeta());
        Assert.assertEquals("sign1",votes.getVotes().get(0).getSignature());
        Assert.assertEquals(new Long(2),votes.getVotes().get(1).getFromVoterId());
        Assert.assertEquals("u2",votes.getVotes().get(1).getFromVoterName());
        Assert.assertEquals("a2",votes.getVotes().get(1).getAlpha());
        Assert.assertEquals("b2",votes.getVotes().get(1).getBeta());
        Assert.assertEquals("sign2",votes.getVotes().get(1).getSignature());
        Assert.assertEquals(new Long(3),votes.getVotes().get(2).getFromVoterId());
        Assert.assertEquals("u3",votes.getVotes().get(2).getFromVoterName());
        Assert.assertEquals("a3",votes.getVotes().get(2).getAlpha());
        Assert.assertEquals("b3",votes.getVotes().get(2).getBeta());
        Assert.assertEquals("sign3",votes.getVotes().get(2).getSignature());
    }

    @Test
    public void getAllVotesFiltersOutEmptyVotes() {
        Long votingId = 27L;
        UserEntity userEntity1 = new VoterEntityWithIdSetter().setId(1L).setUsername("u1");
        UserEntity userEntity2 = new VoterEntityWithIdSetter().setId(2L).setUsername("u2");
        UserEntity userEntity3 = new VoterEntityWithIdSetter().setId(3L).setUsername("u3");

        VotingEntity votingEntity = new VotingEntity();

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1,votingEntity).setAlpha("a1").setBeta("b1").setVotedToTrue();
        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2,votingEntity);
        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3,votingEntity);

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3);

        when(votingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        Votes votes = voteService.getAllVotesForVoting(votingId);
        Assert.assertEquals(1,votes.getVotes().size());
        Assert.assertEquals(new Long(1),votes.getVotes().get(0).getFromVoterId());
        Assert.assertEquals("u1",votes.getVotes().get(0).getFromVoterName());
        Assert.assertEquals("a1",votes.getVotes().get(0).getAlpha());
        Assert.assertEquals("b1",votes.getVotes().get(0).getBeta());
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setDecryptionFactorOfVoterReturnsFalseIfEntityNotThere() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String decryptionFactor = "decryptionFactor";
        String signature = "sign";

        SingleDecryptionFactor singleDecryptionFactor = new SingleDecryptionFactor()
                .setDecryptionFactor(decryptionFactor)
                .setSignature(signature);

        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.empty());
        boolean successful = voteService.setDecryptionFactor(voterId,votingId,singleDecryptionFactor);
        Assert.assertFalse(successful);
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setDecryptionFactorOfVoterReturnsFalseIfAlreadySet() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String decryptionFactor = "decryptionFactor";
        String signature = "sign";

        SingleDecryptionFactor singleDecryptionFactor = new SingleDecryptionFactor()
                .setDecryptionFactor(decryptionFactor)
                .setSignature(signature);

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        when(singleVotingVoterEntity.getDecryptionFactor()).thenReturn("factor");

        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        boolean successful = voteService.setDecryptionFactor(voterId,votingId,singleDecryptionFactor);
        Assert.assertFalse(successful);
    }

    @Test
    (expected = IllegalVoteException.class)
    public void setDecryptionFactorOfVoterReturnsFalseIfWrongPhase() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String decryptionFactor = "decryptionFactor";
        String signature = "sign";

        SingleDecryptionFactor singleDecryptionFactor = new SingleDecryptionFactor()
                .setDecryptionFactor(decryptionFactor)
                .setSignature(signature);

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        VotingEntity votingEntity = Mockito.mock(VotingEntity.class);

        when(singleVotingVoterEntity.getVotingEntity()).thenReturn(votingEntity);
        when(votingEntity.getPhaseNumber()).thenReturn(Phases.KEY_DERIVATION_PHASE.getNumber());

        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        boolean successful = voteService.setDecryptionFactor(voterId,votingId,singleDecryptionFactor);
        Assert.assertFalse(successful);
    }

    @Test
    public void setDecryptionFactorOfVoterReturnsTrueIfSuccessful() throws IllegalVoteException {
        long voterId = 42;
        long votingId = 27;
        String decryptionFactor = "decryptionFactor";
        String signature = "sign";

        SingleDecryptionFactor singleDecryptionFactor = new SingleDecryptionFactor()
                .setDecryptionFactor(decryptionFactor)
                .setSignature(signature);

        SingleVotingVoterEntity singleVotingVoterEntity = Mockito.mock(SingleVotingVoterEntity.class);
        VotingEntity votingEntity = Mockito.mock(VotingEntity.class);

        when(singleVotingVoterEntity.getVotingEntity()).thenReturn(votingEntity);
        when(votingEntity.getPhaseNumber()).thenReturn(Phases.DECRYPTION_PHASE.getNumber());

        when(singleVotingVoterRepository.findOne(voterId,votingId)).thenReturn(Optional.of(singleVotingVoterEntity));
        when(decryptionRepository.setDecryption(votingId,voterId,decryptionFactor,signature)).thenReturn(1);
        boolean successful = voteService.setDecryptionFactor(voterId,votingId,singleDecryptionFactor);
        Assert.assertTrue(successful);
        verify(decryptionRepository,times(1)).setDecryption(votingId,voterId,decryptionFactor,signature);
    }

    @Test
    public void getAllDecryptionFactorsWork() {
        long voterId1 = 3;
        long voterId2 = 5;
        long voterId3 = 9;

        long voterPosition1 = 0;
        long voterPosition2 = 1;
        long voterPosition3 = 2;

        String voterName1 = "v1";
        String voterName2 = "v2";
        String voterName3 = "v3";

        long votingId = 7;

        UserEntity voterEntity1 = new VoterEntityWithIdSetter().setId(voterId1).setUsername(voterName1);
        UserEntity voterEntity2 = new VoterEntityWithIdSetter().setId(voterId2).setUsername(voterName2);
        UserEntity voterEntity3 = new VoterEntityWithIdSetter().setId(voterId3).setUsername(voterName3);

        VotingEntity votingEntity = new VotingEntity();

        SingleVotingVoterEntity singleVotingVoterEntityWithKey1 = new SingleVotingVoterEntity(voterEntity1,votingEntity)
                .setVoterNumber(voterPosition1)
                .setDecryptionFactor("d1")
                .setDecryptionFactorSignature("sign1");
        SingleVotingVoterEntity singleVotingVoterEntityWithoutKey = new SingleVotingVoterEntity(voterEntity2,votingEntity)
                .setVoterNumber(voterPosition2);
        SingleVotingVoterEntity singleVotingVoterEntityWithKey2 = new SingleVotingVoterEntity(voterEntity3,votingEntity)
                .setVoterNumber(voterPosition3)
                .setDecryptionFactor("d2")
                .setDecryptionFactorSignature("sign2");

        votingEntity
                .addEligibleVoter(singleVotingVoterEntityWithKey1)
                .addEligibleVoter(singleVotingVoterEntityWithoutKey)
                .addEligibleVoter(singleVotingVoterEntityWithKey2);

        when(votingsRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        DecryptionFactors decryptionFactors = voteService.getAllDecryptionFactors(votingId);
        Assert.assertEquals(2,decryptionFactors.getAllDecryptionFactors().size());

        Assert.assertEquals(new Long(voterId1),decryptionFactors.getAllDecryptionFactors().get(0).getFromVoterId());
        Assert.assertEquals(new Long(voterId3),decryptionFactors.getAllDecryptionFactors().get(1).getFromVoterId());

        Assert.assertEquals(voterName1,decryptionFactors.getAllDecryptionFactors().get(0).getFromVoterName());
        Assert.assertEquals(voterName3,decryptionFactors.getAllDecryptionFactors().get(1).getFromVoterName());

        Assert.assertEquals(new Long(0),decryptionFactors.getAllDecryptionFactors().get(0).getFromVoterPosition());
        Assert.assertEquals(new Long(2),decryptionFactors.getAllDecryptionFactors().get(1).getFromVoterPosition());

        Assert.assertEquals("d1",decryptionFactors.getAllDecryptionFactors().get(0).getDecryptionFactor());
        Assert.assertEquals("d2",decryptionFactors.getAllDecryptionFactors().get(1).getDecryptionFactor());

        Assert.assertEquals("sign1",decryptionFactors.getAllDecryptionFactors().get(0).getSignature());
        Assert.assertEquals("sign2",decryptionFactors.getAllDecryptionFactors().get(1).getSignature());
    }


}
