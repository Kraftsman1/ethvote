package com.service.data.votings;

import com.data.voter.SingleVoter;
import com.data.votingprogress.SingleVoterProgress;
import com.data.votingprogress.VotingProgress;
import com.data.votings.*;
import com.data.votingsforvoterwithprogress.VotingsForVoterWithProgress;
import com.enums.Phases;
import com.exceptions.NoSuchVotingException;
import com.models.createdvotings.CreatedVotingsEntity;
import com.models.createdvotings.CreatedVotingsEntityId;
import com.models.createdvotings.CreatedVotingsRepository;
import com.models.voting.VotingCalculationParametersEntity;
import com.models.voting.VotingEntity;
import com.models.helper.VotingEntityWithSetters;
import com.models.voting.VotingsRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.services.data.votings.VotingsDataServiceImpl;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VotingsDataServiceImplTest {

    private Long id = 42L;
    private String title = "title";
    private String description = "description";

    private VotingsRepository mockRepository;

    private VotingsDataServiceImpl votingsDataService;

    private UserRepository mockUserRepository;

    private CreatedVotingsRepository mockCreatedVotingsRepository;

    private SingleVotingVoterRepository mockSingleVotingVoterRepository;

    @Autowired
    private VotingsRepository realRepository;

    @Autowired
    private SingleVotingVoterRepository singleVotingVoterRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CreatedVotingsRepository createdVotingsRepository;

    @Before
    public void setup() {
        mockRepository = Mockito.mock(VotingsRepository.class);
        mockUserRepository = Mockito.mock(UserRepository.class);
        mockSingleVotingVoterRepository = Mockito.mock(SingleVotingVoterRepository.class);
        mockCreatedVotingsRepository = Mockito.mock(CreatedVotingsRepository.class);

        votingsDataService = new VotingsDataServiceImpl(mockRepository,mockSingleVotingVoterRepository,mockUserRepository,mockCreatedVotingsRepository);
    }

    @Test
    public void getAllDataTest() {
        VotingEntity votingEntity = new VotingEntityWithSetters()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setSecurityThreshold(2)
                .setNumberOfEligibleVoters(3)
                .setPhaseNumber(0)
                .setCalculationParametersEntity(new VotingCalculationParametersEntity());

        List<VotingEntity> retValues = Arrays.asList(votingEntity);

        when(mockRepository.findAll()).thenReturn(retValues);

        Votings votings = votingsDataService.getAllVotings();
        Assert.assertEquals(retValues.size(),votings.getVotings().size());
        Assert.assertEquals(id,votings.getVotings().get(0).getId());
    }

    @Test
    public void getAllVotingsByUserIdTest() {
        votingsDataService = new VotingsDataServiceImpl(realRepository,singleVotingVoterRepository,mockUserRepository,mockCreatedVotingsRepository);

        VotingEntity votingEntity = new VotingEntityWithSetters()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setSecurityThreshold(2)
                .setNumberOfEligibleVoters(3)
                .setPhaseNumber(0)
                .setCalculationParametersEntity(new VotingCalculationParametersEntity());

        UserEntity userEntity = new UserEntity();
        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity).setCreatedSecretSharesToTrue();

        userEntity.addVoting(singleVotingVoterEntity);

        List<VotingEntity> retValues = Arrays.asList(votingEntity);

        when(mockUserRepository.findOneByIdWithVotingsFetched(id)).thenReturn(Optional.of(userEntity));

        Votings votings = votingsDataService.getAllVotingsByVoterId(id);
        Assert.assertEquals(retValues.size(),votings.getVotings().size());
        Assert.assertEquals(id,votings.getVotings().get(0).getId());
    }

    @Test
    public void getAllVotingsByVotingAdministratorWorksEmpty() {
        Long votingAdminId = 42L;
        when(mockUserRepository.findOneByIdWithCreatedVotingsFetched(votingAdminId)).thenReturn(Optional.empty());
        Votings votings = votingsDataService.getAllVotingsByVotingAdministratorId(votingAdminId);
        Assert.assertEquals(0,votings.getVotings().size());
    }

    @Test
    public void getAllVotingsByVotingAdministratorWorksWithData() {
        VotingEntity votingEntity1 = new VotingEntityWithSetters().setId(1L).setTitle("t1").setDescription("d1").setPhaseNumber(0);
        VotingEntity votingEntity2 = new VotingEntityWithSetters().setId(2L).setTitle("t2").setDescription("d2").setPhaseNumber(0);
        VotingEntity votingEntity3 = new VotingEntityWithSetters().setId(3L).setTitle("t3").setDescription("d3").setPhaseNumber(0);

        UserEntity userEntity = new UserEntity();

        CreatedVotingsEntity createdVotingsEntity1 = new CreatedVotingsEntity(userEntity, votingEntity1);
        CreatedVotingsEntity createdVotingsEntity2 = new CreatedVotingsEntity(userEntity, votingEntity2);
        CreatedVotingsEntity createdVotingsEntity3 = new CreatedVotingsEntity(userEntity, votingEntity3);

        userEntity
                .addCreatedVoting(createdVotingsEntity1)
                .addCreatedVoting(createdVotingsEntity2)
                .addCreatedVoting(createdVotingsEntity3);

        Long votingAdminId = 42L;
        when(mockUserRepository.findOneByIdWithCreatedVotingsFetched(votingAdminId)).thenReturn(Optional.of(userEntity));
        Votings votings = votingsDataService.getAllVotingsByVotingAdministratorId(votingAdminId);
        Assert.assertEquals(3,votings.getVotings().size());
        Assert.assertEquals("t2",votings.getVotings().get(1).getTitle());
    }

    @Test
    public void getAllVotingsByUserIdAndPhaseNumberTest() {
        votingsDataService = new VotingsDataServiceImpl(realRepository,singleVotingVoterRepository,mockUserRepository,mockCreatedVotingsRepository);

        int searchedPhaseNumber = 4;
        int notSearchedPhaseNumber = 3;

        VotingEntity votingEntityWithRightPhaseNumber = new VotingEntityWithSetters()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setSecurityThreshold(2)
                .setNumberOfEligibleVoters(3)
                .setPhaseNumber(searchedPhaseNumber)
                .setCalculationParametersEntity(new VotingCalculationParametersEntity());

        VotingEntity votingEntityWithWrongPhaseNumber = new VotingEntityWithSetters()
                .setId(id+1)
                .setTitle(title)
                .setDescription(description)
                .setSecurityThreshold(2)
                .setNumberOfEligibleVoters(3)
                .setPhaseNumber(notSearchedPhaseNumber)
                .setCalculationParametersEntity(new VotingCalculationParametersEntity());

        UserEntity userEntity = new UserEntity().setUsername("user");

        CreatedVotingsEntity createdVotingsEntity1 = new CreatedVotingsEntity(userEntity,votingEntityWithRightPhaseNumber);
        CreatedVotingsEntity createdVotingsEntity2 = new CreatedVotingsEntity(userEntity,votingEntityWithWrongPhaseNumber);

        votingEntityWithRightPhaseNumber.setVotingAdministrator(createdVotingsEntity1);
        votingEntityWithWrongPhaseNumber.setVotingAdministrator(createdVotingsEntity2);

        SingleVotingVoterEntity searchedVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntityWithRightPhaseNumber).setCreatedSecretSharesToTrue();
        SingleVotingVoterEntity notSearchedVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntityWithWrongPhaseNumber).setCreatedSecretSharesToTrue();

        userEntity
                .addVoting(searchedVoterEntity)
                .addVoting(notSearchedVoterEntity);

        when(mockUserRepository.findOneByIdWithVotingsFetched(id)).thenReturn(Optional.of(userEntity));

        VotingsForVoterWithProgress votingsForVoterWithProgress = votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(id,searchedPhaseNumber);

        Assert.assertEquals("{\"votings\":[{\"phase\":4,\"timeVotingIsFinished\":-1,\"options\":[],\"description\":\"description\",\"id\":42,\"title\":\"title\",\"transactionId\":\"\"}],\"votingOfficials\":[{\"votingId\":42,\"trustees\":[],\"votingAdministrator\":\"user\"}],\"votingsVoterProgress\":[{\"participatedInDecryption\":false,\"usesCustomKey\":false,\"phaseNumber\":4,\"votingId\":42,\"createdSecretShares\":true,\"voted\":false}]}",votingsForVoterWithProgress.toJson().toString());
    }

    @Test
    public void filterOutNonTrusteesKeyDerivationPhaseWorks() {

        VotingEntity votingEntity1 = new VotingEntityWithSetters()
                .setId(1L)
                .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber());

        VotingEntity votingEntity2 = new VotingEntityWithSetters()
                .setId(2L)
                .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber());

        VotingEntity votingEntity3 = new VotingEntityWithSetters()
                .setId(3L)
                .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber());

        UserEntity userEntity = new UserEntity().setUsername("user");

        CreatedVotingsEntity createdVotingsEntity1 = new CreatedVotingsEntity(userEntity,votingEntity1);
        CreatedVotingsEntity createdVotingsEntity2 = new CreatedVotingsEntity(userEntity,votingEntity2);
        CreatedVotingsEntity createdVotingsEntity3 = new CreatedVotingsEntity(userEntity,votingEntity3);

        votingEntity1.setVotingAdministrator(createdVotingsEntity1);
        votingEntity2.setVotingAdministrator(createdVotingsEntity2);
        votingEntity3.setVotingAdministrator(createdVotingsEntity3);

        SingleVotingVoterEntity entity1 = new SingleVotingVoterEntity(userEntity,votingEntity1).setIsTrustee(true);
        SingleVotingVoterEntity entity2 = new SingleVotingVoterEntity(userEntity,votingEntity2).setIsTrustee(false);
        SingleVotingVoterEntity entity3 = new SingleVotingVoterEntity(userEntity,votingEntity3).setIsTrustee(true);

        userEntity
                .addVoting(entity1)
                .addVoting(entity2)
                .addVoting(entity3);

        when(mockUserRepository.findOneByIdWithVotingsFetched(id)).thenReturn(Optional.of(userEntity));
        VotingsForVoterWithProgress votingsForVoterWithProgress = votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(id,Phases.KEY_DERIVATION_PHASE.getNumber());
        Assert.assertEquals(2,votingsForVoterWithProgress.toJson().getJSONArray("votings").length());
    }

    @Test
    public void filterOutNonVotersVotingPhaseWorks() {
        VotingEntity votingEntity1 = new VotingEntityWithSetters()
                .setId(1L)
                .setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        VotingEntity votingEntity2 = new VotingEntityWithSetters()
                .setId(2L)
                .setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        VotingEntity votingEntity3 = new VotingEntityWithSetters()
                .setId(3L)
                .setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        UserEntity userEntity = new UserEntity().setUsername("user");

        CreatedVotingsEntity createdVotingsEntity1 = new CreatedVotingsEntity(userEntity,votingEntity1);
        CreatedVotingsEntity createdVotingsEntity2 = new CreatedVotingsEntity(userEntity,votingEntity2);
        CreatedVotingsEntity createdVotingsEntity3 = new CreatedVotingsEntity(userEntity,votingEntity3);

        votingEntity1.setVotingAdministrator(createdVotingsEntity1);
        votingEntity2.setVotingAdministrator(createdVotingsEntity2);
        votingEntity3.setVotingAdministrator(createdVotingsEntity3);

        SingleVotingVoterEntity entity1 = new SingleVotingVoterEntity(userEntity,votingEntity1).setCanVote(true);
        SingleVotingVoterEntity entity2 = new SingleVotingVoterEntity(userEntity,votingEntity2).setCanVote(false);
        SingleVotingVoterEntity entity3 = new SingleVotingVoterEntity(userEntity,votingEntity3).setCanVote(true);

        userEntity
                .addVoting(entity1)
                .addVoting(entity2)
                .addVoting(entity3);

        when(mockUserRepository.findOneByIdWithVotingsFetched(id)).thenReturn(Optional.of(userEntity));
        VotingsForVoterWithProgress votingsForVoterWithProgress = votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(id,Phases.VOTING_PHASE.getNumber());
        Assert.assertEquals(2,votingsForVoterWithProgress.toJson().getJSONArray("votings").length());
    }

    @Test
    public void filterOutNonTrusteesDecryptionPhaseWorks() {
        VotingEntity votingEntity1 = new VotingEntityWithSetters()
                .setId(1L)
                .setPhaseNumber(Phases.DECRYPTION_PHASE.getNumber());

        VotingEntity votingEntity2 = new VotingEntityWithSetters()
                .setId(2L)
                .setPhaseNumber(Phases.DECRYPTION_PHASE.getNumber());

        VotingEntity votingEntity3 = new VotingEntityWithSetters()
                .setId(3L)
                .setPhaseNumber(Phases.DECRYPTION_PHASE.getNumber());

        UserEntity userEntity = new UserEntity().setUsername("user");

        CreatedVotingsEntity createdVotingsEntity1 = new CreatedVotingsEntity(userEntity,votingEntity1);
        CreatedVotingsEntity createdVotingsEntity2 = new CreatedVotingsEntity(userEntity,votingEntity2);
        CreatedVotingsEntity createdVotingsEntity3 = new CreatedVotingsEntity(userEntity,votingEntity3);

        votingEntity1.setVotingAdministrator(createdVotingsEntity1);
        votingEntity2.setVotingAdministrator(createdVotingsEntity2);
        votingEntity3.setVotingAdministrator(createdVotingsEntity3);

        SingleVotingVoterEntity entity1 = new SingleVotingVoterEntity(userEntity,votingEntity1).setIsTrustee(true);
        SingleVotingVoterEntity entity2 = new SingleVotingVoterEntity(userEntity,votingEntity2).setIsTrustee(false);
        SingleVotingVoterEntity entity3 = new SingleVotingVoterEntity(userEntity,votingEntity3).setIsTrustee(true);

        userEntity
                .addVoting(entity1)
                .addVoting(entity2)
                .addVoting(entity3);

        when(mockUserRepository.findOneByIdWithVotingsFetched(id)).thenReturn(Optional.of(userEntity));
        VotingsForVoterWithProgress votingsForVoterWithProgress = votingsDataService.getAllVotingsByVoterIdAndPhaseNumber(id,Phases.DECRYPTION_PHASE.getNumber());
        Assert.assertEquals(2,votingsForVoterWithProgress.toJson().getJSONArray("votings").length());
    }

    @Test
    public void addVotingTest() {
        UserEntity userEntity1 = Mockito.mock(UserEntity.class);
        UserEntity userEntity2 = Mockito.mock(UserEntity.class);
        UserEntity userEntity3 = Mockito.mock(UserEntity.class);

        when(mockUserRepository.findOneByIdWithVotingsFetched(23L)).thenReturn(Optional.of(userEntity1));
        when(mockUserRepository.findOneByIdWithVotingsFetched(27L)).thenReturn(Optional.of(userEntity2));
        when(mockUserRepository.findOneByIdWithVotingsFetched(42L)).thenReturn(Optional.of(userEntity3));

        when(userEntity1.getPublicKey()).thenReturn("k1");
        when(userEntity2.getPublicKey()).thenReturn("k2");
        when(userEntity3.getPublicKey()).thenReturn("k3");

        NewlyCreatedVoting voting = new NewlyCreatedVoting()
                .setTitle(title)
                .setDescription(description)
                .addVotingOption(new VotingOption().setOptionName("Option 1"))
                .addVotingOption(new VotingOption().setOptionName("Option 2"))
                .addVotingOption(new VotingOption().setOptionName("Option 3"))
                .addParticipatingVoter(new EligibleVoter().setId(23L).setName("foo").setPosition(0L).setIsTrustee(true).setCanVote(false))
                .addParticipatingVoter(new EligibleVoter().setId(27L).setName("bar").setPosition(1L).setIsTrustee(false).setCanVote(true))
                .addParticipatingVoter(new EligibleVoter().setId(42L).setName("baz").setPosition(2L).setIsTrustee(true).setCanVote(true))
                .setVotingAdministrator(new SingleVoter().setId(27L));

        when(mockUserRepository.findOneByIdWithCreatedVotingsFetched(27L)).thenReturn(Optional.of(userEntity2));

        VotingEntity savedVotingEntity = new VotingEntityWithSetters()
                .setId(4L)
                .setTitle("title")
                .setPhaseNumber(0);

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(userEntity2, savedVotingEntity);

        when(mockCreatedVotingsRepository.saveAndFlush(any())).thenReturn(createdVotingsEntity);
        when(mockRepository.saveAndFlush(any())).thenReturn(savedVotingEntity);

        Optional<Voting> savedVoting = votingsDataService.addVoting(voting);

        ArgumentCaptor<VotingEntity> argument = ArgumentCaptor.forClass(VotingEntity.class);
        verify(mockRepository,times(2)).saveAndFlush(argument.capture());

        ArgumentCaptor<SingleVotingVoterEntity> singleVotingVoterEntityArgumentCaptor = ArgumentCaptor.forClass(SingleVotingVoterEntity.class);
        verify(mockSingleVotingVoterRepository,times(3)).saveAndFlush(singleVotingVoterEntityArgumentCaptor.capture());

        List<SingleVotingVoterEntity> savedEntities = singleVotingVoterEntityArgumentCaptor.getAllValues();
        Assert.assertEquals(Arrays.asList(0L,1L,2L),savedEntities.stream().map(SingleVotingVoterEntity::getVoterNumber).collect(Collectors.toList()));
        Assert.assertEquals(Arrays.asList("k1","k2","k3"),savedEntities.stream().map(SingleVotingVoterEntity::getPublicKey).collect(Collectors.toList()));
        Assert.assertEquals(Arrays.asList(true,false,true),savedEntities.stream().map(SingleVotingVoterEntity::getIsTrustee).collect(Collectors.toList()));
        Assert.assertEquals(Arrays.asList(false,true,true),savedEntities.stream().map(SingleVotingVoterEntity::getCanVote).collect(Collectors.toList()));

        VotingEntity votingEntity = argument.getAllValues().get(1);

        Assert.assertTrue(savedVoting.isPresent());
        Assert.assertEquals("title",savedVoting.get().getTitle());
        Assert.assertEquals(title,votingEntity.getTitle());
        Assert.assertEquals(description,votingEntity.getDescription());
        Assert.assertEquals("Option 1",votingEntity.getVotingOptions().get(0).getOptionName());
        Assert.assertEquals("Option 2",votingEntity.getVotingOptions().get(1).getOptionName());
        Assert.assertEquals("Option 3",votingEntity.getVotingOptions().get(2).getOptionName());
        Assert.assertEquals(userEntity2,votingEntity.getVotingAdministrator().getUserEntity());

        verify(userEntity1,times(1)).setNumberOfVotingsWithTrusteeRole(1L);
        verify(userEntity2,times(0)).setNumberOfVotingsWithTrusteeRole(1L);
        verify(userEntity3,times(1)).setNumberOfVotingsWithTrusteeRole(1L);
    }

    @Test
    public void getVotingsProgressPropagatesEmptyIfNotThere() {
        Long votingId = 73L;
        when(mockRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.empty());

        Assert.assertFalse(votingsDataService.getVotingProgress(votingId).isPresent());
    }

    @Test
    public void getVotingsProgressWorks() {
        Long votingId = 73L;

        VotingEntity votingEntity = new VotingEntity();

        UserEntity userEntity1 = new UserEntity().setUsername("u1");
        UserEntity userEntity2 = new UserEntity().setUsername("u2");
        UserEntity userEntity3 = new UserEntity().setUsername("u3");

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(userEntity1, votingEntity)
                .setVoterNumber(1L)
                .setCreatedSecretSharesToTrue()
                .setVotedToTrue()
                .setCanVote(true)
                .setIsTrustee(false)
                .setCreatedOwnKeyToTrue();

        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(userEntity2, votingEntity)
                .setVoterNumber(2L)
                .setParticipatedInDecryptionToTrue()
                .setVotedToTrue()
                .setCanVote(false)
                .setIsTrustee(false);

        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(userEntity3, votingEntity)
                .setVoterNumber(3L)
                .setCreatedSecretSharesToTrue()
                .setCanVote(true)
                .setIsTrustee(true);

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(userEntity2,votingEntity);

        votingEntity
                .addEligibleVoter(singleVotingVoterEntity1)
                .addEligibleVoter(singleVotingVoterEntity2)
                .addEligibleVoter(singleVotingVoterEntity3)
                .setPhaseNumber(3)
                .setVotingAdministrator(createdVotingsEntity)
                .setSecurityThreshold(42L)
                .setTimeVotingIsFinished(123456L)
                .setTransactionId("dummyTransactionId");


        when(mockRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.of(votingEntity));

        VotingProgress votingProgress = votingsDataService.getVotingProgress(votingId).get();
        List<SingleVoterProgress> singleVoterProgresses = votingProgress.getVotersProgressList();

        Assert.assertEquals(3,votingProgress.getPhaseNumber());
        Assert.assertEquals(42L,votingProgress.getSecurityThreshold());
        Assert.assertEquals(123456L,votingProgress.getTimeVotingIsFinished().longValue());
        Assert.assertEquals("dummyTransactionId",votingProgress.getTransactionId());

        Assert.assertEquals(3,singleVoterProgresses.size());

        Assert.assertEquals(1L,singleVoterProgresses.get(0).getPosition());
        Assert.assertEquals("u1",singleVoterProgresses.get(0).getVoter().getName());
        Assert.assertTrue(singleVoterProgresses.get(0).getParticipatedInKeyGeneration());
        Assert.assertTrue(singleVoterProgresses.get(0).getParticipatedInVoting());
        Assert.assertFalse(singleVoterProgresses.get(0).getParticipatedInDecryption());
        Assert.assertTrue(singleVoterProgresses.get(0).getCanVote());
        Assert.assertFalse(singleVoterProgresses.get(0).getIsTrustee());
        Assert.assertTrue(singleVoterProgresses.get(0).getCreatedCustomKey());

        Assert.assertEquals(2L,singleVoterProgresses.get(1).getPosition());
        Assert.assertEquals("u2",singleVoterProgresses.get(1).getVoter().getName());
        Assert.assertFalse(singleVoterProgresses.get(1).getParticipatedInKeyGeneration());
        Assert.assertTrue(singleVoterProgresses.get(1).getParticipatedInVoting());
        Assert.assertTrue(singleVoterProgresses.get(1).getParticipatedInDecryption());
        Assert.assertFalse(singleVoterProgresses.get(1).getCanVote());
        Assert.assertFalse(singleVoterProgresses.get(1).getCanVote());
        Assert.assertFalse(singleVoterProgresses.get(1).getCreatedCustomKey());

        Assert.assertEquals(3L,singleVoterProgresses.get(2).getPosition());
        Assert.assertEquals("u3",singleVoterProgresses.get(2).getVoter().getName());
        Assert.assertTrue(singleVoterProgresses.get(2).getParticipatedInKeyGeneration());
        Assert.assertFalse(singleVoterProgresses.get(2).getParticipatedInVoting());
        Assert.assertFalse(singleVoterProgresses.get(2).getParticipatedInDecryption());
        Assert.assertTrue(singleVoterProgresses.get(2).getCanVote());
        Assert.assertTrue(singleVoterProgresses.get(2).getCanVote());
        Assert.assertFalse(singleVoterProgresses.get(2).getCreatedCustomKey());
    }

    @Test
    public void testWithRealRepository() {
        votingsDataService = new VotingsDataServiceImpl(realRepository,singleVotingVoterRepository,userRepository,createdVotingsRepository);

        Long userId1 = userRepository.save(new UserEntity().setPublicKey("k1").setUsername("n1")).getId();
        Long userId2 = userRepository.save(new UserEntity().setPublicKey("k2").setUsername("n2")).getId();
        Long userId3 = userRepository.save(new UserEntity().setPublicKey("k3").setUsername("n3")).getId();

        NewlyCreatedVoting voting = new NewlyCreatedVoting()
                .setTitle(title)
                .setDescription(description)
                .addVotingOption(new VotingOption().setOptionName("Option 1").setOptionPrimeNumber(2))
                .addVotingOption(new VotingOption().setOptionName("Option 2").setOptionPrimeNumber(3))
                .addVotingOption(new VotingOption().setOptionName("Option 3").setOptionPrimeNumber(5))
                .addParticipatingVoter(new EligibleVoter().setName("n1").setId(userId1).setIsTrustee(false).setCanVote(true))
                .addParticipatingVoter(new EligibleVoter().setName("n3").setId(userId3).setIsTrustee(true).setCanVote(true))
                .setNumberOfEligibleVoters(3)
                .setPhase(4)
                .setSecurityThreshold(42)
                .setVotingCalculationParameters(
                        new VotingCalculationParameters()
                        .setGeneratorG("g")
                        .setPrimeP("p")
                        .setPrimeQ("q")
                )
                .setVotingAdministrator(new SingleVoter().setId(userId2));


        votingsDataService.addVoting(voting);

        Assert.assertEquals(1,votingsDataService.getAllVotingsByVoterId(userId1).getVotings().size());
        Assert.assertEquals(0,votingsDataService.getAllVotingsByVoterId(userId2).getVotings().size());
        Assert.assertEquals(1,votingsDataService.getAllVotingsByVoterId(userId3).getVotings().size());

        Assert.assertEquals(1, userRepository.findOneByIdWithCreatedVotingsFetched(userId2).get().getCreatedVotings().size());
        Assert.assertEquals(userId2, userRepository
                .findOneByIdWithCreatedVotingsFetched(userId2)
                .get()
                .getCreatedVotings()
                .get(0)
                .getUserEntity()
                .getId()
        );

        Assert.assertEquals(userId2, userRepository
                .findOneByIdWithCreatedVotingsFetched(userId2)
                .get()
                .getCreatedVotings()
                .get(0)
                .getVotingEntity()
                .getVotingAdministrator()
                .getUserEntity()
                .getId()
        );
    }

    @Test
    public void setPhaseNumberReturnsFalseIfNoVotingWithThatIdIsPresent() {
        int phaseNumber = 27;
        when(mockRepository.updatePhaseNumber(id,phaseNumber)).thenReturn(0);
        Assert.assertFalse(votingsDataService.setPhaseNumber(id,phaseNumber).isPresent());
    }

    @Test
    public void setPhaseNumberInvokesRightCalls() {
        int phaseNumber = 27;
        String title = "title";
        when(mockRepository.updatePhaseNumber(id,phaseNumber)).thenReturn(1);
        when(mockRepository.findById(id)).thenReturn(Optional.of(new VotingEntityWithSetters()
                .setId(id)
                .setTitle(title)
                .setPhaseNumber(phaseNumber)));

        Optional<Voting> votingEntity = votingsDataService.setPhaseNumber(id,phaseNumber);

        verify(mockRepository,times(1)).updatePhaseNumber(id,phaseNumber);
        Assert.assertTrue(votingEntity.isPresent());
        Assert.assertEquals(title,votingEntity.get().getTitle());
    }

    @Test
    public void setVotingEndTimeReturnsFalseIfNoVotingWithThatIdIsPresent() {
        when(mockRepository.findById(id)).thenReturn(Optional.empty());
        Optional<Voting> optionalVoting = votingsDataService.setTimeVotingIsFinished(id,7L);
        Assert.assertFalse(optionalVoting.isPresent());
    }

    @Test
    public void setVotingEndTimeReturnsFalseIfNotInKeyDerivationPhase() {
        VotingEntity votingEntity = new VotingEntityWithSetters().setPhaseNumber(Phases.VOTING_PHASE.getNumber());

        when(mockRepository.findById(id)).thenReturn(Optional.of(votingEntity));
        Optional<Voting> optionalVoting = votingsDataService.setTimeVotingIsFinished(id,7L);
        Assert.assertFalse(optionalVoting.isPresent());
    }

    @Test
    public void setVotingEndTimeReturnsFalseIfAlreadySet() {
        VotingEntity votingEntity = new VotingEntityWithSetters()
                .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber())
                .setTimeVotingIsFinished(7L);

        when(mockRepository.findById(id)).thenReturn(Optional.of(votingEntity));
        Optional<Voting> optionalVoting = votingsDataService.setTimeVotingIsFinished(id,7L);
        Assert.assertFalse(optionalVoting.isPresent());
    }

    @Test
    public void setVotingEndTimeInvokesTheRightCalls() {
        Long votingEndTime = 7L;
        Long votingId = 9L;

        VotingEntity votingEntity = Mockito.mock(VotingEntity.class);
        when(votingEntity.getPhaseNumber()).thenReturn(Phases.KEY_DERIVATION_PHASE.getNumber());
        when(votingEntity.getTimeVotingIsFinished()).thenReturn(-1L);
        when(votingEntity.getId()).thenReturn(votingId);

        when(mockRepository.saveAndFlush(any())).thenReturn(
                new VotingEntityWithSetters()
                        .setId(votingId)
                        .setPhaseNumber(Phases.KEY_DERIVATION_PHASE.getNumber())
                        .setTimeVotingIsFinished(votingEndTime));

        when(mockRepository.findById(id)).thenReturn(Optional.of(votingEntity));
        Optional<Voting> optionalVoting = votingsDataService.setTimeVotingIsFinished(id,votingEndTime);

        verify(votingEntity,times(1)).setTimeVotingIsFinished(votingEndTime);
        verify(mockRepository,times(1)).saveAndFlush(votingEntity);

        Assert.assertTrue(optionalVoting.isPresent());
        Assert.assertEquals(votingId.longValue(), optionalVoting.get().getId().longValue());
        Assert.assertEquals(votingEndTime.longValue(), optionalVoting.get().getTimeVotingIsFinished());
    }

    @Test
    public void setVotingsSummaryReturnEmptyIfNotFound() {
        when(mockRepository.findById(id)).thenReturn(Optional.empty());
        Assert.assertFalse(votingsDataService.setVotingSummary(id, new JSONObject(),"").isPresent());
    }

    @Test
    public void setVotingsSummaryReturnsVotingObjectIfSuccessful() {
        JSONObject object = new JSONObject()
                .put("dummyKey","dummyValue");
        String transactionId = "transactionId";

        VotingEntity mockVotingEntity = Mockito.mock(VotingEntity.class);
        when(mockVotingEntity.setVotingSummary(object.toString())).thenReturn(mockVotingEntity);
        when(mockVotingEntity.setTransactionId(transactionId)).thenReturn(mockVotingEntity);

        when(mockRepository.findById(id)).thenReturn(Optional.of(mockVotingEntity));
        when(mockRepository.saveAndFlush(mockVotingEntity)).thenReturn(
                new VotingEntityWithSetters()
                        .setId(id)
                        .setPhaseNumber(Phases.PUBLISH_PHASE.getNumber())
                        .setVotingSummary(object.toString()));

        Optional<Voting> optionalVoting = votingsDataService.setVotingSummary(id, object, transactionId);
        verify(mockVotingEntity,times(1)).setVotingSummary(object.toString());
        verify(mockVotingEntity,times(1)).setTransactionId(transactionId);
        verify(mockRepository,times(1)).saveAndFlush(mockVotingEntity);

        Assert.assertTrue(optionalVoting.isPresent());
    }

    @Test
    public void getVotingsSummaryReturnsEmptyIfNotThere() {
        when(mockRepository.findById(id)).thenReturn(Optional.empty());
        Optional<JSONObject> jsonObjectOptional = votingsDataService.getVotingSummary(id);
        Assert.assertFalse(jsonObjectOptional.isPresent());
    }

    @Test
    public void getVotingsSummaryReturnsJsonObjectIfThere() {
        JSONObject jsonObject = new JSONObject().put("dummyKey","dummyValue");

        when(mockRepository.findById(id)).thenReturn(
                Optional.of(new VotingEntity().setVotingSummary(
                        jsonObject.toString()
                )));

        Optional<JSONObject> jsonObjectOptional = votingsDataService.getVotingSummary(id);
        Assert.assertTrue(jsonObjectOptional.isPresent());
        Assert.assertEquals(jsonObject.toString(),jsonObjectOptional.get().toString());
    }

    @Test
    (expected = NoSuchVotingException.class)
    public void deleteVotingThrowsExceptionIfNotThere() throws NoSuchVotingException {
        Long votingId = 42L;
        when(mockRepository.findOneByIdWithEligibleVotersFetched(votingId)).thenReturn(Optional.empty());
        votingsDataService.deleteVoting(votingId);
    }

    @Test
    public void testDeleteWithRealRepository() throws NoSuchVotingException {
        long trusteeCountUser1 = 3L;
        long trusteeCountUser2 = 5L;
        long trusteeCountUser3 = 7L;

        UserEntity userEntity1 = new UserEntity().setNumberOfVotingsWithTrusteeRole(trusteeCountUser1);
        UserEntity savedUserEntity1 = userRepository.save(userEntity1);

        UserEntity userEntity2 = new UserEntity().setNumberOfVotingsWithTrusteeRole(trusteeCountUser2);
        UserEntity savedUserEntity2 = userRepository.save(userEntity2);

        UserEntity userEntity3 = new UserEntity().setNumberOfVotingsWithTrusteeRole(trusteeCountUser3);
        UserEntity savedUserEntity3 = userRepository.save(userEntity3);

        VotingEntity votingEntity = new VotingEntity();
        VotingEntity savedVotingEntity = realRepository.save(votingEntity);

        SingleVotingVoterEntity singleVotingVoterEntity1 = new SingleVotingVoterEntity(savedUserEntity1,savedVotingEntity).setIsTrustee(true);
        SingleVotingVoterEntity savedSingleVotingVoterEntity1 = singleVotingVoterRepository.save(singleVotingVoterEntity1);

        SingleVotingVoterEntity singleVotingVoterEntity2 = new SingleVotingVoterEntity(savedUserEntity2,savedVotingEntity).setIsTrustee(true);
        SingleVotingVoterEntity savedSingleVotingVoterEntity2 = singleVotingVoterRepository.save(singleVotingVoterEntity2);

        SingleVotingVoterEntity singleVotingVoterEntity3 = new SingleVotingVoterEntity(savedUserEntity3,savedVotingEntity).setIsTrustee(false);
        SingleVotingVoterEntity savedSingleVotingVoterEntity3 = singleVotingVoterRepository.save(singleVotingVoterEntity3);

        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(savedUserEntity2,savedVotingEntity);
        CreatedVotingsEntity savedVotingsEntity =  createdVotingsRepository.save(createdVotingsEntity);

        savedVotingEntity
                .addEligibleVoter(savedSingleVotingVoterEntity1)
                .addEligibleVoter(savedSingleVotingVoterEntity2)
                .addEligibleVoter(savedSingleVotingVoterEntity3)
                .setVotingAdministrator(savedVotingsEntity);

        savedVotingEntity = realRepository.save(votingEntity);

        savedUserEntity1.addVoting(savedSingleVotingVoterEntity1);
        savedUserEntity1 = userRepository.save(savedUserEntity1);

        savedUserEntity2.addVoting(savedSingleVotingVoterEntity2);
        savedUserEntity2.addCreatedVoting(savedVotingsEntity);
        savedUserEntity2 = userRepository.save(savedUserEntity2);

        savedUserEntity3.addVoting(savedSingleVotingVoterEntity3);
        savedUserEntity3 = userRepository.save(savedUserEntity3);

        Assert.assertEquals(1,userRepository.findOneByIdWithVotingsFetched(savedUserEntity1.getId()).get().getVotings().size());
        Assert.assertEquals(1,userRepository.findOneByIdWithVotingsFetched(savedUserEntity2.getId()).get().getVotings().size());
        Assert.assertEquals(1,userRepository.findOneByIdWithVotingsFetched(savedUserEntity3.getId()).get().getVotings().size());

        Assert.assertEquals(1,userRepository.findOneByIdWithCreatedVotingsFetched(savedUserEntity2.getId()).get().getCreatedVotings().size());

        Assert.assertTrue(singleVotingVoterRepository.findOne(savedUserEntity1.getId(),votingEntity.getId()).isPresent());
        Assert.assertTrue(singleVotingVoterRepository.findOne(savedUserEntity2.getId(),votingEntity.getId()).isPresent());
        Assert.assertTrue(singleVotingVoterRepository.findOne(savedUserEntity3.getId(),votingEntity.getId()).isPresent());

        Assert.assertTrue(createdVotingsRepository.findById(new CreatedVotingsEntityId(savedUserEntity2.getId(),votingEntity.getId())).isPresent());

        Assert.assertTrue(realRepository.findById(votingEntity.getId()).isPresent());

        votingsDataService = new VotingsDataServiceImpl(realRepository,singleVotingVoterRepository,userRepository,createdVotingsRepository);
        votingsDataService.deleteVoting(savedVotingEntity.getId());

        Assert.assertEquals(0,userRepository.findOneByIdWithVotingsFetched(savedUserEntity1.getId()).get().getVotings().size());
        Assert.assertEquals(0,userRepository.findOneByIdWithVotingsFetched(savedUserEntity2.getId()).get().getVotings().size());
        Assert.assertEquals(0,userRepository.findOneByIdWithVotingsFetched(savedUserEntity3.getId()).get().getVotings().size());

        Assert.assertEquals(trusteeCountUser1 - 1,userRepository.findOneByIdWithVotingsFetched(savedUserEntity1.getId()).get().getNumberOfVotingsWithTrusteeRole().longValue());
        Assert.assertEquals(trusteeCountUser2 - 1,userRepository.findOneByIdWithVotingsFetched(savedUserEntity2.getId()).get().getNumberOfVotingsWithTrusteeRole().longValue());
        Assert.assertEquals(trusteeCountUser3,userRepository.findOneByIdWithVotingsFetched(savedUserEntity3.getId()).get().getNumberOfVotingsWithTrusteeRole().longValue());

        Assert.assertEquals(0,userRepository.findOneByIdWithCreatedVotingsFetched(savedUserEntity2.getId()).get().getCreatedVotings().size());

        Assert.assertFalse(singleVotingVoterRepository.findOne(savedUserEntity1.getId(),votingEntity.getId()).isPresent());
        Assert.assertFalse(singleVotingVoterRepository.findOne(savedUserEntity2.getId(),votingEntity.getId()).isPresent());
        Assert.assertFalse(singleVotingVoterRepository.findOne(savedUserEntity3.getId(),votingEntity.getId()).isPresent());

        Assert.assertFalse(createdVotingsRepository.findById(new CreatedVotingsEntityId(savedUserEntity2.getId(),votingEntity.getId())).isPresent());

        Assert.assertFalse(realRepository.findById(votingEntity.getId()).isPresent());

        Assert.assertTrue(userRepository.findOneById(savedUserEntity1.getId()).isPresent());
        Assert.assertTrue(userRepository.findOneById(savedUserEntity2.getId()).isPresent());
        Assert.assertTrue(userRepository.findOneById(savedUserEntity3.getId()).isPresent());
    }

}
