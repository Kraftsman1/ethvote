package com.service.data.voters;

import com.data.voter.*;
import com.exceptions.DeleteVoterException;
import com.exceptions.NoSuchVoterException;
import com.exceptions.VoterInVotingException;
import com.models.createdvotings.CreatedVotingsEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.user.UserEntity;
import com.models.user.UserRepository;
import com.models.voting.VotingEntity;
import com.services.data.voters.VotersServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class VotersServiceImplTest {

    private UserRepository mockUserRepository;
    private VotersServiceImpl voterServiceImpl;

    private final Long id = 27L;
    private final String voterName = "testVoter";
    private final String voterMail = "voter@mail";
    private final String voterExternalId = "voterId";
    private final String voterKey = "testKey";

    @Before
    public void setup() {
        mockUserRepository = Mockito.mock(UserRepository.class);
        this.voterServiceImpl = new VotersServiceImpl(mockUserRepository);
    }

    @Test
    public void retrievingVoterWithKeyThereWorks() {
        UserEntity userPublicKeyEntity = new UserEntity()
                .setUsername(voterName)
                .setPublicKey(voterKey);

        when(mockUserRepository.findOneById(id)).thenReturn(Optional.of(userPublicKeyEntity));

        Optional<String> voterWithKey = voterServiceImpl.getPublicKeyOfVoter(id);
        verify(mockUserRepository,times(1)).findOneById(id);
        Assert.assertEquals(voterKey,voterWithKey.get());
    }

    @Test
    public void getVoterIfNotThereWorks() {
        when(mockUserRepository.findOneByUsername("notThere")).thenReturn(Optional.empty());
        Assert.assertFalse(voterServiceImpl.getVoter("notThere").isPresent());
    }

    @Test
    public void getVoterWorksIfThere() {
        UserEntity userEntity = new UserEntity().setUsername(voterName);
        when(mockUserRepository.findOneByUsername(voterName)).thenReturn(Optional.of(userEntity));
        Optional<SingleVoter> optionalVoter = voterServiceImpl.getVoter(voterName);
        Assert.assertEquals(voterName,optionalVoter.get().getName());
    }

    @Test
    public void getVotersByUserNameWorksEmptyResult() {
        String voter1 = "voter1";
        String voter2 = "voter2";
        String voter3 = "voter3";

        List<String> voterNames = Arrays.asList(voter1,voter2,voter3);
        when(mockUserRepository.findByUsernameIn(voterNames))
                .thenReturn(Arrays.asList());

        Voters voters = voterServiceImpl.getAllVotersByVoternames(voterNames);
        Assert.assertEquals(Arrays.asList(),voters.getVoters());
    }

    @Test
    public void getVotersByUsernameWorks() {
        String voter1 = "voter1";
        String voter2 = "voter2";
        String voter3 = "voter3";

        List<String> voterNames = Arrays.asList(voter1,voter2,voter3);

        UserEntity userEntity1 = new UserEntity().setUsername(voter1);
        UserEntity userEntity3 = new UserEntity().setUsername(voter3);

        when(mockUserRepository.findByUsernameIn(voterNames))
                .thenReturn(Arrays.asList(userEntity1,userEntity3));

        Voters voters = voterServiceImpl.getAllVotersByVoternames(voterNames);
        Assert.assertEquals(2,voters.getVoters().size());
        Assert.assertEquals(voter1,voters.getVoters().get(0).getName());
        Assert.assertEquals(voter3,voters.getVoters().get(1).getName());
    }

    @Test
    public void retrievingVoterWithKeyMissingWorks() {
        UserEntity userPublicKeyEntity = new UserEntity()
                .setUsername(voterName);

        when(mockUserRepository.findOneById(id)).thenReturn(Optional.of(userPublicKeyEntity));

        Optional<String> voterWithKey = voterServiceImpl.getPublicKeyOfVoter(id);
        verify(mockUserRepository,times(1)).findOneById(id);
        Assert.assertFalse(voterWithKey.isPresent());
    }

    @Test
    public void creatingNewWithSameVoterNameReturnsFalseWorks() {
        String voterName = "John Doe";
        when(mockUserRepository.findOneByUsername(voterName)).thenReturn(Optional.of(new UserEntity()));
        Assert.assertFalse(voterServiceImpl.createVoter(voterName));
    }

    @Test
    public void creatingNewVoterWorksIfItDoesNotAlreadyExist() {
        String voterName = "John Doe";
        ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
        when(mockUserRepository.findOneByUsername(voterName)).thenReturn(Optional.empty());
        Assert.assertTrue(voterServiceImpl.createVoter(voterName));
        verify(mockUserRepository,times(1)).saveAndFlush(captor.capture());
        Assert.assertEquals(voterName,captor.getValue().getUsername());
    }

    @Test
    public void creatingNewVoterWorksWithKeyGenerated() {
        String voterName = "John Doe";
        ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
        when(mockUserRepository.findOneByUsername(voterName)).thenReturn(Optional.empty());
        Assert.assertTrue(voterServiceImpl.createVoter(voterName));
        verify(mockUserRepository,times(1)).saveAndFlush(captor.capture());
        Assert.assertEquals(voterName,captor.getValue().getUsername());
    }

    @Test
    public void retrievingKeyWithVoterMissing() {
        when(mockUserRepository.findOneById(id)).thenReturn(Optional.empty());

        Optional<String> key = voterServiceImpl.getPublicKeyOfVoter(id);
        verify(mockUserRepository,times(1)).findOneById(id);
        Assert.assertFalse(key.isPresent());
    }

    @Test
    public void getAllVotersWithEmptyWorks() {
        List<UserEntity> returnedUsers = Arrays.asList();
        when(mockUserRepository.findAll()).thenReturn(returnedUsers);
        List<SingleVoter> voter =  voterServiceImpl.getAllVoters().getVoters();
        Assert.assertEquals(0,voter.size());
    }

    @Test
    public void getAllVotersWithOneWorks() {
        UserEntity e = new VoterEntityWithIdSetter().setId(27L).setUsername(voterName).setPublicKey(voterKey);
        List<UserEntity> returnedKeys = Arrays.asList(e);
        when(mockUserRepository.findAll()).thenReturn(returnedKeys);
        List<SingleVoter> voters =  voterServiceImpl.getAllVoters().getVoters();
        Assert.assertEquals(1,voters.size());
        Assert.assertEquals(id,voters.get(0).getId());
        Assert.assertEquals(voterName,voters.get(0).getName());
    }

    @Test
    public void getAllVotersWithMultipleWorks() {
        UserEntity e1 = new VoterEntityWithIdSetter().setId(27L).setUsername("e1").setPublicKey("k1");
        UserEntity e2 = new VoterEntityWithIdSetter().setId(42L).setUsername("e2").setPublicKey("k2");
        UserEntity e3 = new VoterEntityWithIdSetter().setId(87L).setUsername("e3").setPublicKey("k3");
        List<UserEntity> returnedKeys = Arrays.asList(e1, e2, e3);
        when(mockUserRepository.findAll()).thenReturn(returnedKeys);
        List<SingleVoter> voters = voterServiceImpl.getAllVoters().getVoters();
        Assert.assertEquals(3, voters.size());
        Assert.assertEquals(Long.valueOf(27), voters.get(0).getId());
        Assert.assertEquals("e1", voters.get(0).getName());
        Assert.assertEquals(Long.valueOf(42L), voters.get(1).getId());
        Assert.assertEquals("e2", voters.get(1).getName());
        Assert.assertEquals(Long.valueOf(87L), voters.get(2).getId());
        Assert.assertEquals("e3", voters.get(2).getName());
    }

    @Test
    public void getAllVotersWithKeysWithEmptyWorks() {
        List<UserEntity> returnedUsers = Arrays.asList();
        when(mockUserRepository.findAll()).thenReturn(returnedUsers);
        List<SingleVoter> votersWithKeys =  voterServiceImpl.getAllVoters().getVoters();
        Assert.assertEquals(0,votersWithKeys.size());
    }

    @Test
    public void getAllVotersWithKeysWithOneWorks() {
        UserEntity e = new VoterEntityWithIdSetter().setId(27L).setUsername(voterName).setPublicKey(voterKey);
        List<UserEntity> returnedKeys = Arrays.asList(e);
        when(mockUserRepository.findAll()).thenReturn(returnedKeys);
        List<SingleVoter> voters =  voterServiceImpl.getAllVoters().getVoters();
        Assert.assertEquals(1,voters.size());
        Assert.assertEquals(id,voters.get(0).getId());
        Assert.assertEquals(voterName,voters.get(0).getName());
        Assert.assertEquals(voterKey,voters.get(0).getPublicKey().get());
    }

    @Test
    public void getAllVotersWithKeysWithMultipleWorks() {
        UserEntity e1 = new VoterEntityWithIdSetter().setId(27L).setUsername("e1").setPublicKey("k1");
        UserEntity e2 = new VoterEntityWithIdSetter().setId(42L).setUsername("e2").setPublicKey("k2");
        UserEntity e3 = new VoterEntityWithIdSetter().setId(87L).setUsername("e3").setPublicKey("k3");
        List<UserEntity> returnedKeys = Arrays.asList(e1,e2,e3);
        when(mockUserRepository.findAll()).thenReturn(returnedKeys);
        List<SingleVoter> keys =  voterServiceImpl.getAllVoters().getVoters();
        Assert.assertEquals(3,keys.size());
        Assert.assertEquals(Long.valueOf(27),keys.get(0).getId());
        Assert.assertEquals("e1",keys.get(0).getName());
        Assert.assertEquals("k1",keys.get(0).getPublicKey().get());
        Assert.assertEquals(Long.valueOf(42L),keys.get(1).getId());
        Assert.assertEquals("e2",keys.get(1).getName());
        Assert.assertEquals("k2",keys.get(1).getPublicKey().get());
        Assert.assertEquals(Long.valueOf(87L),keys.get(2).getId());
        Assert.assertEquals("e3",keys.get(2).getName());
        Assert.assertEquals("k3",keys.get(2).getPublicKey().get());
    }

    @Test
    public void getAllVotersWhereKeyIsSpecifiedWithMultipleWorks() {
        UserEntity e1 = new VoterEntityWithIdSetter().setId(27L).setUsername("e1").setPublicKey("k1");
        UserEntity e2 = new VoterEntityWithIdSetter().setId(42L).setUsername("e2");
        UserEntity e3 = new VoterEntityWithIdSetter().setId(87L).setUsername("e3").setPublicKey("k3");
        List<UserEntity> returnedKeys = Arrays.asList(e1,e2,e3);
        when(mockUserRepository.findAll()).thenReturn(returnedKeys);
        Voters possibleVoters =  voterServiceImpl.getAllVotersWherePublicKeyIsSpecified();
        Assert.assertEquals(2,possibleVoters.getVoters().size());
        Assert.assertEquals(Long.valueOf(27),possibleVoters.getVoters().get(0).getId());
        Assert.assertEquals("e1",possibleVoters.getVoters().get(0).getName());
        Assert.assertEquals("k1",possibleVoters.getVoters().get(0).getPublicKey().get());
        Assert.assertEquals(Long.valueOf(87L),possibleVoters.getVoters().get(1).getId());
        Assert.assertEquals("e3",possibleVoters.getVoters().get(1).getName());
        Assert.assertEquals("k3",possibleVoters.getVoters().get(1).getPublicKey().get());
    }

    @Test
    public void keyIsGeneratedWhenFlagIsSet() {
        Voters voters = new Voters().setVoters(
                Arrays.asList(new SingleVoter().setName("v1").setEmail("v1@mail"))
        );

        ArgumentCaptor<Iterable<UserEntity>> captor = ArgumentCaptor.forClass(Iterable.class);

        voterServiceImpl.createVoters(voters,true);

        verify(mockUserRepository,times(1)).saveAll(captor.capture());
        UserEntity userEntity = captor.getValue().iterator().next();

        Assert.assertNotNull(userEntity.getPublicKey());
        Assert.assertNotNull(userEntity.getPrivateKey());
    }

    @Test
    public void createMultipleVotersWorks() {

        Voters voters = new Voters().setVoters(
                Arrays.asList(
                        new SingleVoter().setName("v1").setEmail("v1@mail"),
                        new SingleVoter().setName("v2").setEmail("v2@mail"),
                        new SingleVoter().setName("v3").setEmail("v3@mail"),
                        new SingleVoter().setName("v4").setEmail("v4@mail"),
                        new SingleVoter().setName("v5").setEmail("v5@mail")
                )
        );

        when(mockUserRepository.saveAll(any())).thenReturn(Arrays.asList(
                new VoterEntityWithIdSetter().setId(1L).setUsername("v1").setEmail("v1@mail"),
                new VoterEntityWithIdSetter().setId(2L).setUsername("v2").setEmail("v2@mail"),
                new VoterEntityWithIdSetter().setId(3L).setUsername("v3").setEmail("v3@mail"),
                new VoterEntityWithIdSetter().setId(4L).setUsername("v4").setEmail("v4@mail"),
                new VoterEntityWithIdSetter().setId(5L).setUsername("v5").setEmail("v5@mail")
        ));

        Voters returnedVoters = voterServiceImpl.createVoters(voters, false);

        ArgumentCaptor<Iterable<UserEntity>> captor = ArgumentCaptor.forClass(Iterable.class);
        verify(mockUserRepository,times(1)).saveAll(captor.capture());
        Assert.assertEquals(5,voters.getVoters().size());
        Assert.assertEquals("v1",returnedVoters.getVoters().get(0).getName());
        Assert.assertEquals("v2",returnedVoters.getVoters().get(1).getName());
        Assert.assertEquals("v3",returnedVoters.getVoters().get(2).getName());
        Assert.assertEquals("v4",returnedVoters.getVoters().get(3).getName());
        Assert.assertEquals("v5",returnedVoters.getVoters().get(4).getName());

        Assert.assertEquals(5,voters.getVoters().size());
        Assert.assertEquals("v1@mail",returnedVoters.getVoters().get(0).getEmail());
        Assert.assertEquals("v2@mail",returnedVoters.getVoters().get(1).getEmail());
        Assert.assertEquals("v3@mail",returnedVoters.getVoters().get(2).getEmail());
        Assert.assertEquals("v4@mail",returnedVoters.getVoters().get(3).getEmail());
        Assert.assertEquals("v5@mail",returnedVoters.getVoters().get(4).getEmail());

        Assert.assertEquals(1L,returnedVoters.getVoters().get(0).getId().longValue());
        Assert.assertEquals(2L,returnedVoters.getVoters().get(1).getId().longValue());
        Assert.assertEquals(3L,returnedVoters.getVoters().get(2).getId().longValue());
        Assert.assertEquals(4L,returnedVoters.getVoters().get(3).getId().longValue());
        Assert.assertEquals(5L,returnedVoters.getVoters().get(4).getId().longValue());
    }

    @Test
    (expected = NoSuchVoterException.class)
    public void deleteVoterThrowsExceptionIfNotThere() throws DeleteVoterException {
        Long voterId = 27L;
        when(mockUserRepository.findOneByIdWithInvolvedVotingsFetched(voterId)).thenReturn(Optional.empty());

        voterServiceImpl.deleteVoter(voterId);
    }

    @Test
    (expected = VoterInVotingException.class)
    public void deleteVoterThrowsExceptionIfCreatedVotingsAvailable() throws DeleteVoterException {
        Long voterId = 27L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();
        CreatedVotingsEntity createdVotingsEntity = new CreatedVotingsEntity(userEntity,votingEntity);

        userEntity.addCreatedVoting(createdVotingsEntity);

        when(mockUserRepository.findOneByIdWithInvolvedVotingsFetched(voterId)).thenReturn(Optional.of(userEntity));

        voterServiceImpl.deleteVoter(voterId);
    }

    @Test
    (expected = VoterInVotingException.class)
    public void deleteVoterThrowsExceptionIfInVoting() throws DeleteVoterException {
        Long voterId = 27L;

        UserEntity userEntity = new UserEntity();
        VotingEntity votingEntity = new VotingEntity();
        SingleVotingVoterEntity singleVotingVoterEntity = new SingleVotingVoterEntity(userEntity,votingEntity);

        userEntity.addVoting(singleVotingVoterEntity);

        when(mockUserRepository.findOneByIdWithInvolvedVotingsFetched(voterId)).thenReturn(Optional.of(userEntity));

        voterServiceImpl.deleteVoter(voterId);
    }

    @Test
    public void deleteVoterWorks() throws DeleteVoterException {
        Long voterId = 27L;

        UserEntity userEntity = new UserEntity();

        when(mockUserRepository.findOneByIdWithInvolvedVotingsFetched(voterId)).thenReturn(Optional.of(userEntity));

        boolean successful = voterServiceImpl.deleteVoter(voterId);

        verify(mockUserRepository,times(1)).delete(userEntity);
        Assert.assertTrue(successful);
    }

    @Test
    public void updateVotersDataReturnsEmptyIfNotFound() {
        String externalId = "externalId";
        String username = "username";
        String email = "email";

        when(mockUserRepository.findOneByExternalIdOrUsernameAndEmail(externalId, username, email)).thenReturn(Optional.empty());

        Optional<SingleVoter> userEntity = voterServiceImpl.upsertVotersData(externalId, username, email);
        Assert.assertFalse(userEntity.isPresent());
    }

    @Test
    public void externalIdIsUpdatedIfNotThere() {
        String externalId = "externalId";
        String username = "username";
        String email = "email";

        UserEntity userEntity = new UserEntity()
                .setUsername(username)
                .setEmail(email);

        when(mockUserRepository.findOneByExternalIdOrUsernameAndEmail(externalId, username, email)).thenReturn(Optional.of(userEntity));
        when(mockUserRepository.saveAndFlush(userEntity)).thenReturn(userEntity);

        Optional<SingleVoter> retrievedUserEntity = voterServiceImpl.upsertVotersData(externalId, username, email);

        ArgumentCaptor<UserEntity> argumentCaptor = ArgumentCaptor.forClass(UserEntity.class);

        Assert.assertTrue(retrievedUserEntity.isPresent());
        verify(mockUserRepository,times(1)).saveAndFlush(argumentCaptor.capture());
        Assert.assertEquals(externalId,argumentCaptor.getValue().getExternalId());
    }

    @Test
    public void emailIsUpdatedIfNotThere() {
        String externalId = "externalId";
        String username = "username";
        String email = "email";
        String otherEmail = "otherMail";

        UserEntity userEntity = new UserEntity()
                .setExternalId(externalId)
                .setUsername(username)
                .setEmail(otherEmail);

        when(mockUserRepository.findOneByExternalIdOrUsernameAndEmail(externalId, username, email)).thenReturn(Optional.of(userEntity));
        when(mockUserRepository.saveAndFlush(userEntity)).thenReturn(userEntity);

        Optional<SingleVoter> retrievedUserEntity = voterServiceImpl.upsertVotersData(externalId, username, email);

        ArgumentCaptor<UserEntity> argumentCaptor = ArgumentCaptor.forClass(UserEntity.class);

        Assert.assertTrue(retrievedUserEntity.isPresent());
        verify(mockUserRepository,times(1)).saveAndFlush(argumentCaptor.capture());
        Assert.assertEquals(email,argumentCaptor.getValue().getEmail());
    }

    @Test
    public void nothingUpdatedIfEverythingIsSame() {
        String externalId = "externalId";
        String username = "username";
        String email = "email";

        UserEntity userEntity = new UserEntity()
                .setExternalId(externalId)
                .setUsername(username)
                .setEmail(email);

        when(mockUserRepository.findOneByExternalIdOrUsernameAndEmail(externalId, username, email)).thenReturn(Optional.of(userEntity));

        Optional<SingleVoter> retrievedUserEntity = voterServiceImpl.upsertVotersData(externalId, username, email);

        Assert.assertTrue(retrievedUserEntity.isPresent());
        verify(mockUserRepository,times(0)).save(any());
    }

    @Test
    public void addVoterWithExternalIdAndUsernameAndEmailReturnsEmptyIfVoterAlreadyThere() {
        when(mockUserRepository.findOneByExternalIdOrUsernameAndEmail(voterExternalId,voterName,voterMail)).thenReturn(Optional.of(new UserEntity()));
        Assert.assertFalse(voterServiceImpl.addVoter(voterName,voterMail,voterExternalId,true).isPresent());
    }

    @Test
    public void addVoterWithExternalIdAndUsernameAndEmailWorks() {
        when(mockUserRepository.findOneByExternalIdOrUsernameAndEmail(voterExternalId,voterName,voterMail)).thenReturn(Optional.empty());

        ArgumentCaptor<UserEntity> argumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        when(mockUserRepository.saveAndFlush(argumentCaptor.capture())).thenReturn(new UserEntity());

        Optional<SingleVoter> optionalVoter = voterServiceImpl.addVoter(voterName,voterMail,voterExternalId,true);
        Assert.assertTrue(optionalVoter.isPresent());
        Assert.assertEquals(voterExternalId,argumentCaptor.getValue().getExternalId());
        Assert.assertEquals(voterName,argumentCaptor.getValue().getUsername());
        Assert.assertEquals(voterMail,argumentCaptor.getValue().getEmail());
        Assert.assertNotNull(argumentCaptor.getValue().getPublicKey());
    }

    @Test
    public void addVoterWithUsernameAndEmailReturnsEmptyIfVoterAlreadyThere() {
        when(mockUserRepository.findOneByUsernameAndEmail(voterName,voterMail)).thenReturn(Optional.of(new UserEntity()));
        Assert.assertFalse(voterServiceImpl.addVoter(voterName,voterMail,true).isPresent());

    }

    @Test
    public void addVoterWithUsernameAndEmailWorks() {
        when(mockUserRepository.findOneByUsernameAndEmail(voterName,voterMail)).thenReturn(Optional.empty());

        ArgumentCaptor<UserEntity> argumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        when(mockUserRepository.saveAndFlush(argumentCaptor.capture())).thenReturn(new UserEntity());

        Optional<SingleVoter> optionalVoter = voterServiceImpl.addVoter(voterName,voterMail,false);
        Assert.assertTrue(optionalVoter.isPresent());
        Assert.assertEquals(voterName,argumentCaptor.getValue().getUsername());
        Assert.assertEquals(voterMail,argumentCaptor.getValue().getEmail());
        Assert.assertNull(argumentCaptor.getValue().getPublicKey());
    }
}
