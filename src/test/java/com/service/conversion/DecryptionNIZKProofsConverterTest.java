package com.service.conversion;

import com.data.proofs.DecryptionNIZKProof;
import com.models.singlevotingvoter.DecryptionNIZKPEntity;
import com.services.conversion.DecryptionNIZKProofConverter;
import org.junit.Assert;
import org.junit.Test;

public class DecryptionNIZKProofsConverterTest {

    private String a = "a";
    private String b = "b";
    private String r = "r";
    private String signature = "signature";

    @Test
    public void toDataWorks() {
        DecryptionNIZKPEntity decryptionNIZKPEntity = new DecryptionNIZKPEntity()
                .setA(a)
                .setB(b)
                .setR(r)
                .setSignature(signature);

        DecryptionNIZKProof proof = DecryptionNIZKProofConverter.toData(decryptionNIZKPEntity);
        Assert.assertEquals(a,proof.getA());
        Assert.assertEquals(b,proof.getB());
        Assert.assertEquals(r,proof.getR());
        Assert.assertEquals(signature,proof.getSignature());
    }

    @Test
    public void toEntityWorks() {
        DecryptionNIZKProof decryptionNIZKProof = new DecryptionNIZKProof()
                .setA(a)
                .setB(b)
                .setR(r)
                .setSignature(signature);

        DecryptionNIZKPEntity proof = DecryptionNIZKProofConverter.toEntity(decryptionNIZKProof);
        Assert.assertEquals(a,proof.getA());
        Assert.assertEquals(b,proof.getB());
        Assert.assertEquals(r,proof.getR());
        Assert.assertEquals(signature,proof.getSignature());
    }
}
