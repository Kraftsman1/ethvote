package com.service.conversion;

import com.data.proofs.SingleVoteNIZKProof;
import com.data.proofs.VoteNIZKProofs;
import com.models.singlevotingvoter.VoteNIZKPEntity;
import com.services.conversion.VoteNIZKProofsConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class VoteNIZKProofsConverterTest {

    private Integer messageIndex = 42;
    private String a = "a";
    private String b = "b";
    private String c = "c";
    private String r = "r";
    private String signature = "sign";

    @Test
    public void proofEntityToProofList() {
        VoteNIZKPEntity voteNIZKPEntity = new VoteNIZKPEntity()
                .setMessageIndex(messageIndex)
                .setA(a)
                .setB(b)
                .setC(c)
                .setR(r)
                .setSignature(signature);

        List<VoteNIZKPEntity> voteNIZKPEntityList = Arrays.asList(voteNIZKPEntity);
        VoteNIZKProofs proofs = VoteNIZKProofsConverter.toData(voteNIZKPEntityList);
        Assert.assertEquals(1,proofs.getSingleVoteNIZKProofs().size());

        Assert.assertEquals(messageIndex,proofs.getSingleVoteNIZKProofs().get(0).getMessageIndex());
        Assert.assertEquals(a,proofs.getSingleVoteNIZKProofs().get(0).getA());
        Assert.assertEquals(b,proofs.getSingleVoteNIZKProofs().get(0).getB());
        Assert.assertEquals(c,proofs.getSingleVoteNIZKProofs().get(0).getC());
        Assert.assertEquals(r,proofs.getSingleVoteNIZKProofs().get(0).getR());
        Assert.assertEquals(signature,proofs.getSingleVoteNIZKProofs().get(0).getSignature());
    }

    @Test
    public void proofListToProofEntity() {
        SingleVoteNIZKProof singleVoteNIZKProof = new SingleVoteNIZKProof()
                .setMessageIndex(messageIndex)
                .setA(a)
                .setB(b)
                .setC(c)
                .setR(r)
                .setSignature(signature);

        VoteNIZKProofs proofs = new VoteNIZKProofs()
                .addProof(singleVoteNIZKProof);

        List<VoteNIZKPEntity> voteNIZKPEntities = VoteNIZKProofsConverter.toEntity(proofs);
        Assert.assertEquals(1,voteNIZKPEntities.size());
        Assert.assertEquals(messageIndex,voteNIZKPEntities.get(0).getMessageIndex());
        Assert.assertEquals(a,voteNIZKPEntities.get(0).getA());
        Assert.assertEquals(b,voteNIZKPEntities.get(0).getB());
        Assert.assertEquals(c,voteNIZKPEntities.get(0).getC());
        Assert.assertEquals(r,voteNIZKPEntities.get(0).getR());
        Assert.assertEquals(signature,voteNIZKPEntities.get(0).getSignature());
    }
}
