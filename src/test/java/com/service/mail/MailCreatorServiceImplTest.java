package com.service.mail;

import com.services.mail.MailTextCreatorService;
import com.services.mail.MailTextCreatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MailCreatorServiceImplTest {

    private MailTextCreatorService mailTextCreatorService;

    @Before
    public void setup() {
        mailTextCreatorService = new MailTextCreatorServiceImpl();
    }

    @Test
    public void validTextForPKIIsGenerated() {
        String text = mailTextCreatorService.createCallForPKI("Yoda","Luke Skywalker","http://aFarFarGalaxy.naboo");
        String expected = "Hi Yoda\n" +
                "\n" +
                "Luke Skywalker has chosen you to act as a trustee in a newly created voting.\n" +
                "\n" +
                "An RSA key pair has been created for you. If you want to create your own RSA key pair, you can go to\n" +
                "\n" +
                "http://aFarFarGalaxy.naboo\n" +
                "\n" +
                "where you can see this voting (and the other votings you act as a trustee) and create your own key pair.\n" +
                "\n" +
                "If you have any questions or feedback, feel free to contact me via mail:\n" +
                "\n" +
                "gflavio@student.ethz.ch\n" +
                "\n" +
                "Greetings,\n" +
                "Flavio Goldener";
        Assert.assertEquals(expected,text);
    }

    @Test
    public void validTextForKeyGenerationIsGenerated() {
        String text = mailTextCreatorService.createCallForKeyGeneration("Yoda","Luke Skywalker","http://aFarFarGalaxy.naboo");
        String expected = "Hi Yoda\n" +
                "\n" +
                "Luke Skywalker has chosen you to act as a trustee in a newly created voting.\n" +
                "\n" +
                "To make sure that Luke Skywalker can start the voting process, please go to\n" +
                "\n" +
                "http://aFarFarGalaxy.naboo\n" +
                "\n" +
                "where you can see this voting (and the other votings you act as a trustee) and generate your part of the encryption/decryption key.\n" +
                "\n" +
                "If you have any questions or feedback, feel free to contact me via mail:\n" +
                "\n" +
                "gflavio@student.ethz.ch\n" +
                "\n" +
                "Greetings,\n" +
                "Flavio Goldener";
        Assert.assertEquals(expected,text);
    }

    @Test
    public void validTextForVoteIsGenerated() {
        String text = mailTextCreatorService.createVoteMailText("Yoda","Luke Skywalker","http://aFarFarGalaxy.naboo");
        String expected = "Hi Yoda\n" +
                "\n" +
                "You can participate in Luke Skywalker's voting.\n" +
                "\n" +
                "To participate, please go to\n" +
                "\n" +
                "http://aFarFarGalaxy.naboo\n" +
                "\n" +
                "where you can cast your vote.\n" +
                "\n" +
                "If you have any questions or feedback, feel free to contact me via mail:\n" +
                "\n" +
                "gflavio@student.ethz.ch\n" +
                "\n" +
                "Greetings,\n" +
                "Flavio Goldener";

        Assert.assertEquals(expected,text);
    }

    @Test
    public void validTextForDecryptionIsGenerated() {
        String text = mailTextCreatorService.createDecryptionMailText("Yoda","Luke Skywalker","http://aFarFarGalaxy.naboo");
        String expected = "Hi Yoda\n" +
                "\n" +
                "Luke Skywalker's voting is over.\n" +
                "\n" +
                "To decrypt Luke Skywalker's voting's results, your help is needed. Please go to\n" +
                "\n" +
                "http://aFarFarGalaxy.naboo\n" +
                "\n" +
                "where you can see this voting (and the other votings you act as a trustee) and generate your decryption factor.\n" +
                "\n" +
                "If you have any questions or feedback, feel free to contact me via mail:\n" +
                "\n" +
                "gflavio@student.ethz.ch\n" +
                "\n" +
                "Greetings,\n" +
                "Flavio Goldener";

        Assert.assertEquals(expected,text);
    }
}
