package com.service.authentication;

import com.services.autentication.AuthenticationService;
import com.services.autentication.AuthenticationServiceImpl;
import com.services.environment.EnvironmentService;
import com.services.secrets.SecretsService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class AuthenticationServiceImplTest {

    private AuthenticationService authenticationService;

    private SecretsService mockSecretsService;

    private EnvironmentService mockEnvironmentService;

    @Before
    public void setup() {
        mockSecretsService = Mockito.mock(SecretsService.class);
        mockEnvironmentService = Mockito.mock(EnvironmentService.class);

        authenticationService = new AuthenticationServiceImpl(mockSecretsService, mockEnvironmentService);
    }

    @Test
    public void returnsFalseIfNotAdmin() {
        String notAdmin = "notAdmin";
        String truePassword = "truePassword";

        when(mockEnvironmentService.isDemoEnvironment()).thenReturn(false);
        when(mockSecretsService.getAdminPassword()).thenReturn(truePassword);

        Assert.assertFalse(authenticationService.authenticate(notAdmin, truePassword));
    }

    @Test
    public void returnsFalseIfNotRightAdminPasswordInProduction() {
        String admin = "admin";
        String truePassword = "truePassword";
        String wrongPassword = "wrongPassword";

        when(mockEnvironmentService.isDemoEnvironment()).thenReturn(false);
        when(mockSecretsService.getAdminPassword()).thenReturn(truePassword);

        Assert.assertFalse(authenticationService.authenticate(admin, wrongPassword));
    }

    @Test
    public void returnsTrueIfPasswordIsCorrect() {
        String admin = "admin";
        String truePassword = "truePassword";

        when(mockEnvironmentService.isDemoEnvironment()).thenReturn(false);
        when(mockSecretsService.getAdminPassword()).thenReturn(truePassword);

        Assert.assertTrue(authenticationService.authenticate(admin, truePassword));
    }

    @Test
    public void demoAuthenticationPasses() {
        when(mockEnvironmentService.isDemoEnvironment()).thenReturn(true);
        Assert.assertTrue(authenticationService.authenticate("some","any"));
    }

    @Test
    public void demoAuthenticationFailsForAdminWithWrongPassword() {
        when(mockEnvironmentService.isDemoEnvironment()).thenReturn(true);
        Assert.assertFalse(authenticationService.authenticate("admin","any"));
    }

    @Test
    public void demoAuthenticationSucceedsForAdminWithRightPassword() {
        when(mockEnvironmentService.isDemoEnvironment()).thenReturn(true);
        Assert.assertTrue(authenticationService.authenticate("admin","admin"));
    }
}
