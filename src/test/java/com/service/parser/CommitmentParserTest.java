package com.service.parser;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.SingleCommitment;
import com.exceptions.parser.*;
import com.services.parser.CommitmentParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CommitmentParserTest {

    private JSONObject validJsonObject;

    @Before
    public void setup() {
        JSONArray jsonCommitmentsArray = new JSONArray();
        jsonCommitmentsArray.put(new JSONObject().put("commitment","c1").put("coefficientNumber",0L).put("signature","sign1"));
        jsonCommitmentsArray.put(new JSONObject().put("commitment","c2").put("coefficientNumber",1L).put("signature","sign2"));
        jsonCommitmentsArray.put(new JSONObject().put("commitment","c3").put("coefficientNumber",2L).put("signature","sign3"));

        validJsonObject = new JSONObject()
                .put("commitments",jsonCommitmentsArray)
                .put("numberOfCommitments",3);
    }

    @Test
    public void validJsonCanBeParsed() throws ParserException {
        String validCommitment = validJsonObject.toString();
        CommitmentFromVoter commitmentFromVoter = CommitmentParser.parse(validCommitment);
        Assert.assertEquals(3L, commitmentFromVoter.getNumberOfCommitments().longValue());
        Assert.assertEquals(Arrays.asList("c1","c2","c3"), commitmentFromVoter.getSingleCommitmentList()
                        .stream()
                        .map(SingleCommitment::getCommitment)
                        .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList(0L,1L,2L), commitmentFromVoter.getSingleCommitmentList()
                .stream()
                .map(SingleCommitment::getCoefficientNumber)
                .collect(Collectors.toList()));

        Assert.assertEquals(Arrays.asList("sign1","sign2","sign3"), commitmentFromVoter.getSingleCommitmentList()
                .stream()
                .map(SingleCommitment::getSignature)
                .collect(Collectors.toList()));
    }

    @Test(expected = MissingFieldException.class)
    public void numberOfCommitmentsFieldMissingThrowsError() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).remove("coefficientNumber");
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = IllegalCoefficientValueException.class)
    public void tooSmallCoefficientValue() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).put("coefficientNumber",-2);
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = IllegalCoefficientValueException.class)
    public void tooLargeCoefficientValue() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).put("coefficientNumber",3);
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = IllegalCoefficientValueException.class)
    public void duplicatedCoefficientValue() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).put("coefficientNumber",1);
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = TooFewCommitmentsException.class)
    public void tooFewCommitmentsThrowsError() throws ParserException {
        validJsonObject.put("numberOfCommitments",4);
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = TooManyCommitmentsException.class)
    public void tooManyCommitmentsThrowsError() throws ParserException {
        validJsonObject.put("numberOfCommitments",2);
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingCommitmentValueThrowsError() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).remove("commitment");
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingCoefficientValueThrowsError() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).remove("coefficientNumber");
        CommitmentParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void missingSignatureThrowsError() throws ParserException {
        validJsonObject.getJSONArray("commitments").getJSONObject(0).remove("signature");
        CommitmentParser.parse(validJsonObject.toString());
    }
}
