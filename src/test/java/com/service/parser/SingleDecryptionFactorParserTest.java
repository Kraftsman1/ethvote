package com.service.parser;

import com.data.decryptionfactor.SingleDecryptionFactor;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.ParserException;
import com.services.parser.SingleDecryptionFactorParser;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SingleDecryptionFactorParserTest {

    private JSONObject validJsonObject;

    @Before
    public void setup() {
        validJsonObject = new JSONObject()
                .put("decryptionFactor","d1")
                .put("signature","sign1");
    }

    @Test
    public void canParseValidJson() throws ParserException {
        SingleDecryptionFactor singleDecryptionFactor = SingleDecryptionFactorParser.parse(validJsonObject.toString());
        Assert.assertEquals("d1",singleDecryptionFactor.getDecryptionFactor());
        Assert.assertEquals("sign1",singleDecryptionFactor.getSignature());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionIfDecryptionFactorIsMissing() throws ParserException {
        validJsonObject.remove("decryptionFactor");
        SingleDecryptionFactorParser.parse(validJsonObject.toString());
    }

    @Test(expected = MissingFieldException.class)
    public void throwsExceptionIfSignatureIsMissing() throws ParserException {
        validJsonObject.remove("signature");
        SingleDecryptionFactorParser.parse(validJsonObject.toString());
    }
}
