package com.service.context;

import com.helpers.ContextHolderController;
import com.services.context.RequestContextHolderImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
public class RequestContextHolderImplTest {

    private MockMvc mockMvc;

    @Autowired
    private RequestContextHolderImpl requestContextHolder;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(
                new ContextHolderController(requestContextHolder))
                .build();
    }

    @Test
    public void saveContextWorks() throws Exception {
        MvcResult result;

        result = this.mockMvc.perform(
                put("/setCurrentUser")
        ).andReturn();

        Assert.assertEquals("James",result.getResponse().getContentAsString());
    }

    @Test
    public void secondRequestCannotAccessStoredVariable() throws Exception {
        MvcResult result;

        result = this.mockMvc.perform(
                put("/setCurrentUser")
        ).andReturn();

        Assert.assertEquals("James",result.getResponse().getContentAsString());

        result = this.mockMvc.perform(
                get("/getCurrentUser")
        ).andReturn();

        Assert.assertEquals("NOUSERPRESENT",result.getResponse().getContentAsString());

    }
}