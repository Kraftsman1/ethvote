package com.models.user;

import com.strings.Strings;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepositoryCustom {

    private static final String FIND_BY_USERNAME_QUERY = "SELECT u FROM UserEntity AS u WHERE u.username = :username";
    private static final String FIND_BY_ID_QUERY = "SELECT u FROM UserEntity AS u WHERE u.id = :id";
    private static final String FIND_BY_EXTERNAL_ID_OR_USERNAME_AND_EMAIL_QUERY = "SELECT u FROM UserEntity AS u WHERE u.externalId = :externalId OR (u.username = :username AND u.email = :email)";
    private static final String FIND_BY_USERNAME_AND_EMAIL_QUERY = "SELECT u FROM UserEntity AS u WHERE u.username = :username AND u.email = :email";

    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<UserEntity> findOneByUsernameWithVotingsFetched(String username) {
        UserEntity userEntity;

        try {
            userEntity = em.createQuery(FIND_BY_USERNAME_QUERY, UserEntity.class)
                .setParameter(Strings.USERNAME.toString(), username)
                .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("UserEntity.participatingVotings"))
                .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(userEntity);
    }

    @Override
    public Optional<UserEntity> findOneByIdWithVotingsFetched(Long id) {
        UserEntity userEntity;

        try {
            userEntity = em.createQuery(FIND_BY_ID_QUERY, UserEntity.class)
                    .setParameter(Strings.ID.toString(), id)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("UserEntity.participatingVotings"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(userEntity);
    }

    @Override
    public Optional<UserEntity> findOneByIdWithCreatedVotingsFetched(Long id) {
        UserEntity userEntity;

        try {
            userEntity = em.createQuery(FIND_BY_ID_QUERY, UserEntity.class)
                    .setParameter(Strings.ID.toString(), id)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("UserEntity.createdVotings"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(userEntity);
    }

    @Override
    public Optional<UserEntity> findOneByIdWithInvolvedVotingsFetched(Long id) {
        UserEntity userEntity;

        try {
            userEntity = em.createQuery(FIND_BY_ID_QUERY, UserEntity.class)
                    .setParameter(Strings.ID.toString(), id)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("UserEntity.involvedVotings"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(userEntity);
    }

    @Override
    public Optional<UserEntity> findOneByExternalIdOrUsernameAndEmail(String externalId, String username, String email) {
        UserEntity userEntity;

        try {
            userEntity = em.createQuery(FIND_BY_EXTERNAL_ID_OR_USERNAME_AND_EMAIL_QUERY, UserEntity.class)
                    .setParameter(Strings.EXTERNAL_ID.toString(), externalId)
                    .setParameter(Strings.USERNAME.toString(), username)
                    .setParameter(Strings.EMAIL.toString(), email)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("UserEntity.participatingVotings"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(userEntity);
    }

    @Override
    public Optional<UserEntity> findOneByUsernameAndEmail(String username, String email) {
        UserEntity userEntity;

        try {
            userEntity = em.createQuery(FIND_BY_USERNAME_AND_EMAIL_QUERY, UserEntity.class)
                    .setParameter(Strings.USERNAME.toString(), username)
                    .setParameter(Strings.EMAIL.toString(), email)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("UserEntity.participatingVotings"))
                    .getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }

        return Optional.of(userEntity);
    }
}
