package com.models.createdvotings;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CreatedVotingsRepository extends JpaRepository<CreatedVotingsEntity, CreatedVotingsEntityId> {
}
