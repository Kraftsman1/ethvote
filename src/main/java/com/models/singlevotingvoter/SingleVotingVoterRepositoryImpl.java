package com.models.singlevotingvoter;

import com.strings.Strings;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Optional;

@Repository
public class SingleVotingVoterRepositoryImpl implements SingleVotingVoterRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<SingleVotingVoterEntity> findOne(long userId, long votingId) {
        SingleVotingVoterEntity singleVotingVoterEntity;

        try{
            singleVotingVoterEntity = getBaseQuery(userId, votingId).getSingleResult();
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
        return Optional.of(singleVotingVoterEntity);
    }

    @Override
    public Optional<SingleVotingVoterEntity> findOneByUserAndVotingWithCommitmentsFetched(long userId, long votingId) {
        SingleVotingVoterEntity singleVotingVoterEntity;

        try{
            singleVotingVoterEntity = getBaseQuery(userId, votingId)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("SingleVotingVoterEntity.commitments"))
                    .getSingleResult();
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
        return Optional.of(singleVotingVoterEntity);
    }

    @Override
    public Optional<SingleVotingVoterEntity> findOneByUserAndVotingWithSecretSharesFetched(long userId, long votingId) {
        SingleVotingVoterEntity singleVotingVoterEntity;

        try{
            singleVotingVoterEntity = getBaseQuery(userId, votingId)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("SingleVotingVoterEntity.secretShares"))
                    .getSingleResult();
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
        return Optional.of(singleVotingVoterEntity);
    }

    @Override
    public Optional<SingleVotingVoterEntity> findOneByUserAndVotingWithNIZKPFetched(long userId, long votingId) {
        SingleVotingVoterEntity singleVotingVoterEntity;

        try{
            singleVotingVoterEntity = getBaseQuery(userId, votingId)
                    .setHint(Strings.FETCH_GRAPH_HINT.toString(), em.getEntityGraph("SingleVotingVoterEntity.zeroKnowledgeProofs"))
                    .getSingleResult();
        }
        catch (NoResultException e) {
            return Optional.empty();
        }
        return Optional.of(singleVotingVoterEntity);
    }

    private TypedQuery<SingleVotingVoterEntity> getBaseQuery(long userId, long votingId) {
        return em.createQuery("SELECT v from SingleVotingVoterEntity AS v WHERE v.id.userId = :userId AND v.id.votingId = :votingId",SingleVotingVoterEntity.class)
                .setParameter("userId",userId)
                .setParameter("votingId",votingId);
    }

}
