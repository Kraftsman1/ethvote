package com.models.decryption;

import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterEntityId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface DecryptionRepository extends JpaRepository<SingleVotingVoterEntity, SingleVotingVoterEntityId> {

    @Transactional
    @Modifying
    @Query("UPDATE SingleVotingVoterEntity s SET s.decryptionFactor = ?3, s.decryptionFactorSignature = ?4, s.participatedInDecryption = true WHERE s.id.votingId = ?1 AND s.id.userId = ?2")
    int setDecryption(long votingId, long voterId, String decryptionFactor, String decryptionFactorSignature);

    @Transactional
    @Modifying
    @Query("UPDATE SingleVotingVoterEntity s SET s.decryptionNIZKPEntity.a = ?3, s.decryptionNIZKPEntity.b = ?4, s.decryptionNIZKPEntity.r = ?5, s.decryptionNIZKPEntity.signature = ?6, s.decryptionNIZKPEntity.proofAdded = true WHERE s.id.votingId = ?1 AND s.id.userId = ?2")
    int setDecryptionProof(long votingId, long voterId, String a, String b, String r, String signature);

}
