package com.models.role;

import javax.persistence.Embeddable;

@Embeddable
public class RolesEntity {

    private boolean administratorRole = false;

    private boolean votingAdministratorRole = false;

    private boolean registrarRole = false;

    public boolean hasAdministratorRole() {
        return administratorRole;
    }

    public RolesEntity setAdministratorRole(boolean administratorRole) {
        this.administratorRole = administratorRole;
        return this;
    }

    public boolean hasVotingAdministratorRole() {
        return votingAdministratorRole;
    }

    public RolesEntity setVotingAdministratorRole(boolean votingAdministratorRole) {
        this.votingAdministratorRole = votingAdministratorRole;
        return this;
    }

    public boolean hasRegistrarRole() {
        return registrarRole;
    }

    public RolesEntity setRegistrarRole(boolean registrarRole) {
        this.registrarRole = registrarRole;
        return this;
    }
}
