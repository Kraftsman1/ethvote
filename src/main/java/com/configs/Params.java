package com.configs;

public class Params {

    private Params() {
    }

    public static final long TOKEN_EXPIRATION_INTERVAL = 1800;
}
