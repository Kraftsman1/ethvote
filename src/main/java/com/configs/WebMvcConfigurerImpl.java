package com.configs;

import com.interceptors.AuthenticationInterceptor;
import com.interceptors.CorsInterceptor;
import com.interceptors.DemoAuthenticationInterceptor;
import com.services.environment.EnvironmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan("com/controller")
public class WebMvcConfigurerImpl implements WebMvcConfigurer {

    @Autowired
    private AuthenticationInterceptor authenticationInterceptor;

    @Autowired
    private DemoAuthenticationInterceptor demoAuthenticationInterceptor;

    @Autowired
    private CorsInterceptor corsInterceptor;

    @Autowired
    private EnvironmentService environmentService;

    @Autowired
    private Environment env;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedHeaders("*");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(corsInterceptor);
        if(env.getProperty("mode") != null && env.getProperty("mode").equals("demo")) {
            registry.addInterceptor(demoAuthenticationInterceptor);
        }
        else {
            registry.addInterceptor(authenticationInterceptor);
        }
    }

}
