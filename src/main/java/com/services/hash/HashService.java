package com.services.hash;

public interface HashService {

    String createSHA256Hash(String message);
}
