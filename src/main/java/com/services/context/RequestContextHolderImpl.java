package com.services.context;

import com.data.voter.SingleVoter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("RequestContextHolderImpl")
@Scope(value = "request",
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestContextHolderImpl implements RequestContextHolder {

    private SingleVoter currentVoter = null;

    public void setCurrentVoter(SingleVoter currentVoter) {
        this.currentVoter = currentVoter;
    }

    public Optional<SingleVoter> getCurrentVoter() {
        return Optional.ofNullable(currentVoter);
    }
}
