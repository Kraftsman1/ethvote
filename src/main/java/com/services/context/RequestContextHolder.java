package com.services.context;

import com.data.voter.SingleVoter;

import java.util.Optional;

public interface RequestContextHolder {

    void setCurrentVoter(SingleVoter currentVoter);

    Optional<SingleVoter> getCurrentVoter();
}
