package com.services.autentication;

public interface AuthenticationService {

    boolean authenticate(String username, String password);
}
