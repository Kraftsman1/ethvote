package com.services.data.proofs;

import com.data.proofs.DecryptionNIZKProof;
import com.enums.Phases;
import com.exceptions.DecryptionClosedException;
import com.exceptions.DuplicateException;
import com.exceptions.IllegalVoteException;
import com.exceptions.NoSuchVotingException;
import com.models.decryption.DecryptionRepository;
import com.models.singlevotingvoter.SingleVotingVoterEntity;
import com.models.singlevotingvoter.SingleVotingVoterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("DecryptionNIZKProofService")
public class DecryptionNIZKProofServiceImpl implements DecryptionNIZKProofService {

    private final SingleVotingVoterRepository singleVotingVoterRepository;

    private final DecryptionRepository decryptionRepository;

    @Autowired
    public DecryptionNIZKProofServiceImpl(SingleVotingVoterRepository singleVotingVoterRepository,
                                          DecryptionRepository decryptionRepository) {
        this.singleVotingVoterRepository = singleVotingVoterRepository;
        this.decryptionRepository = decryptionRepository;
    }

    @Override
    public boolean setNIZKProofs(long votingId, long voterId, DecryptionNIZKProof decryptionNIZKProof) throws IllegalVoteException {
        Optional<SingleVotingVoterEntity> optSingleVotingVoterEntity = singleVotingVoterRepository.findOne(voterId,votingId);
        if(!optSingleVotingVoterEntity.isPresent()) {
            throw new NoSuchVotingException("The voting could not be found.");
        }

        SingleVotingVoterEntity singleVotingVoterEntity = optSingleVotingVoterEntity.get();
        if(singleVotingVoterEntity.getDecryptionNIZKProof().isProofAdded()) {
            throw new DuplicateException("You already specified the zero knowledge proofs for decryption for this voting.");
        }

        if(singleVotingVoterEntity.getVotingEntity().getPhaseNumber() != Phases.DECRYPTION_PHASE.getNumber()) {
            throw new DecryptionClosedException("The decryption phase for this voting is over.");
        }

        int updatedRows = decryptionRepository.setDecryptionProof(votingId,voterId,decryptionNIZKProof.getA(),decryptionNIZKProof.getB(),decryptionNIZKProof.getR(),decryptionNIZKProof.getSignature());
        return updatedRows == 1;
    }
}
