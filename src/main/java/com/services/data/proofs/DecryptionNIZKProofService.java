package com.services.data.proofs;

import com.data.proofs.DecryptionNIZKProof;
import com.exceptions.IllegalVoteException;

public interface DecryptionNIZKProofService {

    boolean setNIZKProofs(long votingId, long voterId, DecryptionNIZKProof decryptionNIZKProof) throws IllegalVoteException;
}
