package com.services.data.voters;

import com.data.voter.SingleVoter;
import com.data.voter.Voters;
import com.exceptions.DeleteVoterException;

import java.util.List;
import java.util.Optional;

public interface VotersService {

    Optional<SingleVoter> getVoter(String voterName);

    Optional<String> getPublicKeyOfVoter(Long voterId);

    Voters getAllVoters();

    Voters getAllVotersByVoternames(List<String> voternames);

    Voters getAllVotersWherePublicKeyIsSpecified();

    boolean createVoter(String voterName);

    Voters createVoters(Voters voters);

    Voters createVoters(Voters voters, boolean generateKeys);

    boolean deleteVoter(Long voterId) throws DeleteVoterException;

    Optional<SingleVoter> upsertVotersData(String externalId, String voterName, String email);

    Optional<SingleVoter> addVoter(String voterName, String email, boolean generateKeys);

    Optional<SingleVoter> addVoter(String voterName, String email, String externalId, boolean generateKeys);
}
