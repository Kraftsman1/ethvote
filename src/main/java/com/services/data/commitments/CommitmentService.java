package com.services.data.commitments;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.Commitments;
import com.exceptions.IllegalVoteException;

public interface CommitmentService {

    CommitmentFromVoter getCommitmentsFromUser(long votingId, long userId);

    Commitments getAllCommitmentsForVoting(long votingId);

    void setCommitmentsForUser(long votingId, long userId, CommitmentFromVoter commitmentFromVoter) throws IllegalVoteException;
}
