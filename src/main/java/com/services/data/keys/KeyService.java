package com.services.data.keys;

import com.exceptions.IllegalVoteException;

import java.util.Optional;

public interface KeyService {

    Optional<String> getPrivateKey(Long voterId);

    boolean setPublicKeyOfVoter(Long voterId, String publicKey);

    Optional<String> getPublicKeyOfVoterInVoting(Long voterId, Long votingId);

    boolean setPublicKeyOfVoterInVoting(Long voterId, Long votingId, String publicKey) throws IllegalVoteException;
}
