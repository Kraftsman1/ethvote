package com.services.parser;

import com.data.secretshares.SecretSharesFromVoter;
import com.data.secretshares.SingleSecretShareFromVoter;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoContentException;
import com.exceptions.parser.ParserException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class SecretShareParser {

    private SecretShareParser() {
    }

    public static SecretSharesFromVoter parse(String jsonString) throws ParserException {
        SecretSharesFromVoter secretSharesFromVoter = parseWithException(jsonString);
        validate(secretSharesFromVoter,jsonString);
        return secretSharesFromVoter;
    }

    private static SecretSharesFromVoter parseWithException(String jsonString) throws NoContentException {
        SecretSharesFromVoter secretSharesFromVoter = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            secretSharesFromVoter = mapper.readValue(jsonString, SecretSharesFromVoter.class);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return secretSharesFromVoter;
    }

    private static void validate(SecretSharesFromVoter secretSharesFromVoter, String s) throws ParserException {
        if(secretSharesFromVoter.getFromVoterId()==null) {
            throw new MissingFieldException("No fromUserId field in \""+s+"\"");
        }
        if(secretSharesFromVoter.getFromVoterName()==null) {
            throw new MissingFieldException("No fromUserName field in \""+s+"\"");
        }
        if(secretSharesFromVoter.getAllShares().isEmpty()) {
            throw new MissingFieldException("No shares in \""+s+"\"");
        }

        for(SingleSecretShareFromVoter singleSecretShareForUser : secretSharesFromVoter.getAllShares()) {
            if(singleSecretShareForUser.getForVoterId()==null) {
                throw new MissingFieldException("Missing forUserId in one of the shares in \""+s+"\"");
            }
            if(singleSecretShareForUser.getForVoterName()==null) {
                throw new MissingFieldException("Missing forUsername in one of the shares in \""+s+"\"");
            }
            if(singleSecretShareForUser.getSignature()==null) {
                throw new MissingFieldException("Missing signature in one of the shares in \""+s+"\"");
            }
            if(singleSecretShareForUser.getSecretShare()==null) {
                throw new MissingFieldException("Missing secret share value in one of the shares in \""+s+"\"");
            }
        }
    }

}
