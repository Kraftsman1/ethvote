package com.services.parser;

import com.data.proofs.DecryptionNIZKProof;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoContentException;
import com.exceptions.parser.ParserException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class DecryptionNIZKProofParser {

    private DecryptionNIZKProofParser() {
    }

    public static DecryptionNIZKProof parse(String jsonString) throws ParserException {
        DecryptionNIZKProof decryptionNIZKProof = parseWithException(jsonString);
        validateSingleProof(decryptionNIZKProof,jsonString);
        return decryptionNIZKProof;
    }

    private static DecryptionNIZKProof parseWithException(String jsonString) throws NoContentException {
        DecryptionNIZKProof voteNIZKProofs = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            voteNIZKProofs = mapper.readValue(jsonString, DecryptionNIZKProof.class);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return voteNIZKProofs;
    }

    private static void validateSingleProof(DecryptionNIZKProof decryptionNIZKProof, String s) throws MissingFieldException {
        if(decryptionNIZKProof.getA()==null) {
            throw new MissingFieldException("Missing A number field in \""+s+"\"");
        }

        if(decryptionNIZKProof.getB()==null) {
            throw new MissingFieldException("Missing B number field in \""+s+"\"");
        }

        if(decryptionNIZKProof.getR()==null) {
            throw new MissingFieldException("Missing R number field in \""+s+"\"");
        }

        if(decryptionNIZKProof.getSignature()==null) {
            throw new MissingFieldException("Missing Signature number field in \""+s+"\"");
        }

    }
}
