package com.services.parser;

import com.data.votings.*;
import com.exceptions.parser.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import java.io.IOException;

public class VotingParser {

    private static final int NUMBER_OF_REQUIRED_OPTIONS = 2;
    private static final int NUMBER_OF_REQUIRED_VOTERS = 1;

    private VotingParser() {
    }

    public static NewlyCreatedVoting parse(String jsonString) throws ParserException {
        NewlyCreatedVoting voting = parseWithException(jsonString);
        validate(voting,jsonString);
        return voting;
    }

    private static NewlyCreatedVoting parseWithException(String jsonString) throws ParserException {
        NewlyCreatedVoting voting = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            voting = mapper.readValue(jsonString, NewlyCreatedVoting.class);
        } catch (InvalidFormatException e) {
            throw new InvalidFieldFormatException("Invalid field in \""+jsonString+"\"",e);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return voting;
    }

    private static void validate(NewlyCreatedVoting voting,String s) throws ParserException {
        if(voting.getTitle()==null) {
            throw new MissingFieldException("No title field in \""+s+"\"");
        }
        if(voting.getDescription()==null) {
            throw new MissingFieldException("No description field in \""+s+"\"");
        }
        if(voting.getNumberOfEligibleVoters()==-1) {
            throw new MissingFieldException("No number of eligible voters field in \""+s+"\"");
        }
        if(voting.getSecurityThreshold()==-1) {
            throw new MissingFieldException("No security threshold specified in \""+s+"\"");
        }

        if(voting.getVotingOptions().size()<NUMBER_OF_REQUIRED_OPTIONS) {
            throw new TooFewVotingOptionsException("Too few voting options provided in: \""+s+"\". Required are at least two");
        }

        if(voting.getEligibleVoters().size()<NUMBER_OF_REQUIRED_VOTERS) {
            throw new TooFewEligiblieVotersException("Too few eligible voters provided in: \""+s+"\". Required are at least two");
        }

        VotingCalculationParameters calculationParameters = voting.getVotingCalculationParameters();

        if(calculationParameters.getPrimeP()==null) {
            throw new MissingFieldException("No prime P specified in \""+s+"\"");
        }

        if(calculationParameters.getPrimeQ()==null) {
            throw new MissingFieldException("No prime Q specified in \""+s+"\"");
        }

        if(calculationParameters.getGeneratorG()==null) {
            throw new MissingFieldException("No generator G specified in \""+s+"\"");
        }

        for(VotingOption option : voting.getVotingOptions()) {
            if(option.getOptionName()==null) {
                throw new OptionWithoutNameException("There is an option without a name in: \""+s+"\"");
            }
            if(option.getOptionName().isEmpty()) {
                throw new OptionWithoutNameException("There is an option with an empty name in: \""+s+"\"");
            }
            if(option.getOptionPrimeNumber()==null) {
                throw new OptionWithoutPrimeNumberException("There is an option without an optionPrimeNumber in: \""+s+"\"");
            }
        }

        boolean[] positions = new boolean[voting.getEligibleVoters().size()];

        for(EligibleVoter voter : voting.getEligibleVoters()) {
            if(voter.getId()==null) {
                throw new MissingFieldException("There is a voter without an id in: \""+s+"\"");
            }
            if(voter.getName()==null) {
                throw new MissingFieldException("There is a voter without a name in: \""+s+"\"");
            }
            if(voter.getName().isEmpty()) {
                throw new InvalidFieldFormatException("There is voter an empty name in: \""+s+"\"");
            }
            if(voter.getPosition()==null) {
                throw new MissingFieldException("There is voter without a position in: \""+s+"\"");
            }
            if(voter.getPosition()<0) {
                throw new IllegalVoterPositionException("A position is negative in \""+s+"\"");
            }

            if(voter.getCanVote()==null) {
                throw new MissingFieldException("There is voter without can vote field in: \""+s+"\"");
            }

            if(voter.getIsTrustee()==null) {
                throw new MissingFieldException("There is voter without trustee field in: \""+s+"\"");
            }

            if(voter.getPosition()>=voting.getEligibleVoters().size()) {
                throw new IllegalVoterPositionException("A position is too high in \""+s+"\"");
            }

            if(positions[voter.getPosition().intValue()]) {
                throw new IllegalVoterPositionException("Two voters have the same position in \""+s+"\"");
            }
            positions[voter.getPosition().intValue()]=true;
        }
    }
}
