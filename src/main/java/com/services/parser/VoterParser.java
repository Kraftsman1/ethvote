package com.services.parser;

import com.data.voter.SingleVoter;
import com.data.voter.VotersToCreate;
import com.exceptions.parser.MissingFieldException;
import com.exceptions.parser.NoContentException;
import com.exceptions.parser.ParserException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class VoterParser {

    private VoterParser() {
    }

    public static VotersToCreate parse(String jsonString) throws ParserException {
        VotersToCreate voters = parseWithException(jsonString);
        validate(voters,jsonString);
        return voters;
    }

    private static VotersToCreate parseWithException(String jsonString) throws NoContentException {
        VotersToCreate voters = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            voters = mapper.readValue(jsonString, VotersToCreate.class);
        } catch (IOException e) {
            throw new NoContentException("No content in string \""+jsonString+"\"",e);
        }
        return voters;
    }

    private static void validate(VotersToCreate voters, String s) throws ParserException {
        if(voters.doCreateKeys() == null) {
            throw new MissingFieldException("No createKeys field in \"" + s + "\"");
        }

        for(SingleVoter singleVoter : voters.getVoters()) {
            validate(singleVoter,s);
        }
    }

    private static void validate(SingleVoter voter, String s) throws ParserException {
        if (voter.getName() == null) {
            throw new MissingFieldException("No name field in \"" + s + "\"");
        }
        if (voter.getEmail() == null) {
            throw new MissingFieldException("No email field in \"" + s + "\"");
        }
    }
}
