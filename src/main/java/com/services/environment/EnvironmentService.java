package com.services.environment;

public interface EnvironmentService {

    boolean isDemoEnvironment();

    void setIsDemoEnvironment(boolean value);

}
