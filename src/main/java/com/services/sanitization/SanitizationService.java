package com.services.sanitization;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SanitizationService {

    private SanitizationService() {
    }

    //ETH allowed characters:
    //# , - . / : = ? @ [ ] ^ { } ˜

    private static final String NO_SPECIAL_CHARACTERS = "^[a-zA-Z0-9 ]*$";

    public static boolean hasNoSpecialCharacters(String toTest) {
        if(toTest==null) {
            return false;
        }

        Pattern pattern = Pattern.compile(NO_SPECIAL_CHARACTERS);
        Matcher matcher = pattern.matcher(toTest);
        return matcher.matches();
    }
}
