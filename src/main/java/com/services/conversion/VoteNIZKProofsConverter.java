package com.services.conversion;

import com.data.proofs.SingleVoteNIZKProof;
import com.data.proofs.VoteNIZKProofs;
import com.models.singlevotingvoter.VoteNIZKPEntity;

import java.util.ArrayList;
import java.util.List;

public class VoteNIZKProofsConverter {

    private VoteNIZKProofsConverter() {
    }

    public static VoteNIZKProofs toData(Iterable<VoteNIZKPEntity> entities) {
        VoteNIZKProofs voteNIZKProofs = new VoteNIZKProofs();
        for(VoteNIZKPEntity entity : entities) {
            voteNIZKProofs.addProof(toData(entity));
        }
        return voteNIZKProofs;
    }

    public static SingleVoteNIZKProof toData(VoteNIZKPEntity voteNIZKPEntity) {
        return new SingleVoteNIZKProof()
                .setMessageIndex(voteNIZKPEntity.getMessageIndex())
                .setA(voteNIZKPEntity.getA())
                .setB(voteNIZKPEntity.getB())
                .setC(voteNIZKPEntity.getC())
                .setR(voteNIZKPEntity.getR())
                .setSignature(voteNIZKPEntity.getSignature());


    }

    public static List<VoteNIZKPEntity> toEntity(VoteNIZKProofs proofs) {
        List<VoteNIZKPEntity> voteNIZKPEntities = new ArrayList<>();
        for(SingleVoteNIZKProof proof : proofs.getSingleVoteNIZKProofs()) {
            voteNIZKPEntities.add(toEntity(proof));
        }
        return voteNIZKPEntities;
    }

    private static VoteNIZKPEntity toEntity(SingleVoteNIZKProof singleVoteNIZKProof) {
        return new VoteNIZKPEntity()
                .setMessageIndex(singleVoteNIZKProof.getMessageIndex())
                .setA(singleVoteNIZKProof.getA())
                .setB(singleVoteNIZKProof.getB())
                .setC(singleVoteNIZKProof.getC())
                .setR(singleVoteNIZKProof.getR())
                .setSignature(singleVoteNIZKProof.getSignature());
    }
}
