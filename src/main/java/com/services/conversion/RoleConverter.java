package com.services.conversion;

import com.data.roles.Roles;
import com.enums.Role;
import com.models.role.RolesEntity;

import java.util.*;

public class RoleConverter {

    private RoleConverter() {
    }

    public static Roles toData(RolesEntity rolesEntity) {
        Roles roles = new Roles();
        if(rolesEntity.hasAdministratorRole()) {
            roles.addRole(Role.ADMINISTRATOR);
        }
        if(rolesEntity.hasVotingAdministratorRole()) {
            roles.addRole(Role.VOTING_ADMINISTRATOR);
        }
        if(rolesEntity.hasRegistrarRole()) {
            roles.addRole(Role.REGISTRAR);
        }
        return roles;
    }

    public static Optional<Role> toEnum(String roleName) {
        for(Role role : Role.values()) {
            if(role.getRoleName().equals(roleName)) {
                return Optional.of(role);
            }
        }
        return Optional.empty();
    }
}
