package com.services.conversion;

import com.data.commitments.SingleCommitment;
import com.models.singlevotingvoter.CommitmentEntity;

public class CommitmentConverter {

    private CommitmentConverter() {
    }

    public static SingleCommitment toData(CommitmentEntity commitmentEntity) {
        return new SingleCommitment()
                .setCommitment(commitmentEntity.getCommitment())
                .setCoefficientNumber(commitmentEntity.getForCoefficientNumber())
                .setSignature(commitmentEntity.getSignature());
    }

    public static CommitmentEntity toEntity(SingleCommitment commitment) {
        return new CommitmentEntity()
                .setCommitment(commitment.getCommitment())
                .setForCoefficientNumber(commitment.getCoefficientNumber())
                .setSignature(commitment.getSignature());
    }
}
