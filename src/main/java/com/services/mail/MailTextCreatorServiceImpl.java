package com.services.mail;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("MailTextCreatorService")
public class MailTextCreatorServiceImpl implements MailTextCreatorService {

    private static final String MAIL_HEADER = "Hi %s" + System.lineSeparator() +
            System.lineSeparator();

    private static final String MAIL_FOOTER =
            "If you have any questions or feedback, feel free to contact me via mail:" + System.lineSeparator() +
            System.lineSeparator() +
            "gflavio@student.ethz.ch" + System.lineSeparator() +
            System.lineSeparator() +
            "Greetings," +  System.lineSeparator() +
            "Flavio Goldener";

    @Override
    public String createCallForPKI(String voterName, String votingAdminName, String url) {
        return String.format(
                MAIL_HEADER +
                        "%s has chosen you to act as a trustee in a newly created voting." + System.lineSeparator() +
                        System.lineSeparator() +
                        "An RSA key pair has been created for you. If you want to create your own RSA key pair, you can go to" + System.lineSeparator() +
                        System.lineSeparator() +
                        "%s" + System.lineSeparator() +
                        System.lineSeparator() +
                        "where you can see this voting (and the other votings you act as a trustee) and create your own key pair." + System.lineSeparator() +
                        System.lineSeparator() +
                        MAIL_FOOTER, voterName, votingAdminName, url);
    }


    @Override
    public String createCallForKeyGeneration(String voterName, String votingAdminName, String url) {
        return String.format(
                      MAIL_HEADER +
                        "%s has chosen you to act as a trustee in a newly created voting." + System.lineSeparator() +
                        System.lineSeparator() +
                        "To make sure that %s can start the voting process, please go to" + System.lineSeparator() +
                        System.lineSeparator() +
                        "%s" + System.lineSeparator() +
                        System.lineSeparator() +
                        "where you can see this voting (and the other votings you act as a trustee) and generate your part of the encryption/decryption key." + System.lineSeparator() +
                        System.lineSeparator() +
                        MAIL_FOOTER, voterName, votingAdminName, votingAdminName, url);
    }

    @Override
    public String createVoteMailText(String voterName, String votingAdminName, String url) {
        return String.format(
                      MAIL_HEADER +
                        "You can participate in %s's voting." + System.lineSeparator() +
                        System.lineSeparator() +
                        "To participate, please go to" + System.lineSeparator() +
                        System.lineSeparator() +
                        "%s" + System.lineSeparator() +
                        System.lineSeparator() +
                        "where you can cast your vote." + System.lineSeparator() +
                        System.lineSeparator() +
                        MAIL_FOOTER, voterName, votingAdminName, url);
    }

    @Override
    public String createDecryptionMailText(String voterName, String votingAdminName, String url) {
        return String.format(
                      MAIL_HEADER +
                        "%s's voting is over." + System.lineSeparator() +
                        System.lineSeparator() +
                        "To decrypt %s's voting's results, your help is needed. Please go to" + System.lineSeparator() +
                        System.lineSeparator() +
                        "%s" + System.lineSeparator() +
                        System.lineSeparator() +
                        "where you can see this voting (and the other votings you act as a trustee) and generate your decryption factor." + System.lineSeparator() +
                        System.lineSeparator() +
                        MAIL_FOOTER, voterName, votingAdminName, votingAdminName, url);
    }
}
