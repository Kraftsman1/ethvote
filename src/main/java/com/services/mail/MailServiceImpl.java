package com.services.mail;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

@Service
@Qualifier("MailService")
public class MailServiceImpl implements MailService{

    @Override
    public boolean sendMail(List<String> to, String subject, String messageText) {

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.host", "smtp.gmail.com");
        properties.setProperty("mail.smtp.port", "587");

        Session session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("DoNotReplyEVoting@gmail.com", "}7U#GwxDmHrN");
                    }
                });

        try {
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress("DoNotReplyEVoting@gmail.com"));

            for(String singleTo : to) {
                mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(singleTo));
            }

            mimeMessage.setSubject(subject);
            mimeMessage.setText(messageText);

            Transport.send(mimeMessage);
        } catch (MessagingException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sendMail(String to, String subject, String message) {
        return sendMail(Arrays.asList(to), subject, message);
    }
}
