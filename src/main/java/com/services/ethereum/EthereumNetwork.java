package com.services.ethereum;

import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Optional;

public class EthereumNetwork {

    private Web3j web3j;

    public EthereumNetwork(Web3j web3j) {
        this.web3j = web3j;
    }

    public Optional<BigInteger> getBalance(String address) {
        BigInteger res;
        try {
            EthGetBalance ethGetBalance = web3j.ethGetBalance(address,DefaultBlockParameterName.LATEST).send();
            res = ethGetBalance.getBalance();
        } catch (IOException e) {
            return Optional.empty();
        }

        return Optional.of(res);
    }

    public Optional<String> sendMessage(String message, String address, Credentials credentials) {
        String transactionHash;

        try {
            EthGetTransactionCount ethGetTransactionCount = web3j
                    .ethGetTransactionCount(address, DefaultBlockParameterName.LATEST).sendAsync().get();
            BigInteger nonce = ethGetTransactionCount.getTransactionCount();

            EthGasPrice price = web3j.ethGasPrice().send();
            BigInteger gasLimit = BigInteger.valueOf(23500);

            RawTransaction rawTransaction = RawTransaction.createTransaction(
                    nonce,
                    price.getGasPrice(),
                    gasLimit,
                    address,
                    BigInteger.valueOf(1),
                    message);

            byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
            String hexValue = Numeric.toHexString(signedMessage);

            EthSendTransaction ethSendTransaction =
                    web3j.ethSendRawTransaction(hexValue).send();
            transactionHash = ethSendTransaction.getTransactionHash();
        }
        catch (Exception e) {
            return Optional.empty();
        }
        return Optional.ofNullable(transactionHash);
    }
}
