package com.services.task;

public interface TaskScheduler {

    void schedulePhaseUpdateTask(Long votingId, Long timeToRunTask);
}
