package com.services.task;

import com.services.data.votings.VotingsDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Timer;

@Service
@Qualifier("TaskSchedulerImpl")
public class TaskSchedulerImpl implements TaskScheduler {

    private final VotingsDataService votingsDataService;

    @Autowired
    public TaskSchedulerImpl(VotingsDataService votingsDataService) {
        this.votingsDataService = votingsDataService;
    }

    public void schedulePhaseUpdateTask(Long votingId, Long timeToRunTask) {
        Date dateToRunTask = new Date(timeToRunTask);

        Timer timer = new Timer();
        timer.schedule(new UpdatePhaseTask()
                .setVotingId(votingId)
                .setVotingsDataService(votingsDataService), dateToRunTask);
    }
}
