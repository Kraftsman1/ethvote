package com.interceptors;

import com.data.voter.SingleVoter;
import com.services.context.RequestContextHolder;
import com.services.token.JwtTokenCoderService;
import com.services.token.JwtTokenCreatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Component
@Qualifier("DemoAuthenticationInterceptor")
public class DemoAuthenticationInterceptor implements HandlerInterceptor {

    private static final String UNPROTECTED_URL = "/api/authenticate";
    private static final String DOCUMENTATION_URL = "/v2/api-docs";

    private static final String TOKEN_PREFIX = "Bearer ";

    private final JwtTokenCoderService jwtTokenCoderService;

    private final JwtTokenCreatorService jwtTokenCreatorService;

    private final RequestContextHolder requestContextHolder;

    @Autowired
    public DemoAuthenticationInterceptor(
            JwtTokenCoderService jwtTokenCoderService,
            JwtTokenCreatorService jwtTokenCreatorService,
            RequestContextHolder requestContextHolder) {
        this.jwtTokenCoderService = jwtTokenCoderService;
        this.jwtTokenCreatorService = jwtTokenCreatorService;
        this.requestContextHolder = requestContextHolder;
    }

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        if(request.getRequestURI().equals(UNPROTECTED_URL) || request.getRequestURI().equals(DOCUMENTATION_URL)) {
            return true;
        }

        String token = request.getHeader("authorization");
        if(token==null) {
            response.getWriter().write("JwtToken to access protected URI \""+request.getRequestURI()+"\" is missing.");
            return false;
        }

        if(token.startsWith(TOKEN_PREFIX)) {
            token = token.substring(TOKEN_PREFIX.length());
        }

        Optional<SingleVoter> userOptional = jwtTokenCoderService.getVoterFromToken(token);

        if(!userOptional.isPresent()) {
            response.getWriter().write("JwtToken to access protected URI \""+request.getRequestURI()+"\" is invalid.");
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return false;
        }

        SingleVoter currentVoter = userOptional.get();
        requestContextHolder.setCurrentVoter(currentVoter);

        Long id = userOptional.get().getId();
        if(id==null) {
            return true;
        }

        Optional<String> optNewToken = jwtTokenCreatorService.createTokenForUser(id);
        if(!optNewToken.isPresent()) {
            response.getWriter().write("New JWT token could not be created.");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return false;
        }

        String newToken = optNewToken.get();
        response.addHeader("RefreshToken",newToken);
        return true;
    }

}
