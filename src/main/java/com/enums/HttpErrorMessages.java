package com.enums;

public enum HttpErrorMessages {
    MISSING_USERNAME_FIELD("No value for username in request body"),
    MISSING_PASSWORD_FIELD("No value for password in request body"),
    ILLEGAL_CHARACTER_IN_USERNAME_OR_PASSWORD("Illegal character in username or password"),
    WRONG_USERNAME_AND_PASSWORD("Wrong username and password combination");

    private final String text;

    HttpErrorMessages(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
