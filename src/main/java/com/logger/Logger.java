package com.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {

    private static final String LOG_FILE_NAME = "logs/log.txt";
    private PrintWriter writer;
    private static Logger logger = null;

    private Logger() {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(LOG_FILE_NAME);
            writer = new PrintWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fileWriter!=null) {
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized Logger get() {
        if(logger==null) {
            logger = new Logger();
        }
        return logger;
    }

    public void log(String text) {
        writer.println(text);
        writer.flush();
    }
}
