package com.ajp;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class TomcatConfiguration implements
        WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

    @Autowired
    private Environment env;

    private static final String PROTOCOL = "AJP/1.3";

    @Value("${tomcat.ajp.port:9090}")
    private int ajpPort;

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        if(env.getProperty("mode") != null && env.getProperty("mode").equals("demo")) {
            return;
        }
        Connector ajpConnector = new Connector(PROTOCOL);
        ajpConnector.setPort(ajpPort);
        factory.addAdditionalTomcatConnectors(ajpConnector);
    }
}

