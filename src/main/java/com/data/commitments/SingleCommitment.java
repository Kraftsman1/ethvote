package com.data.commitments;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class SingleCommitment implements JsonMappable {

    @JsonProperty("commitment")
    private String commitment;

    @JsonProperty("coefficientNumber")
    private Long coefficientNumber;

    @JsonProperty("signature")
    private String signature;

    public SingleCommitment setCommitment(String commitment) {
        this.commitment = commitment;
        return this;
    }

    public String getCommitment() {
        return commitment;
    }

    public SingleCommitment setCoefficientNumber(long coefficientNumber) {
        this.coefficientNumber = coefficientNumber;
        return this;
    }

    public Long getCoefficientNumber() {
        return coefficientNumber;
    }

    public String getSignature() {
        return signature;
    }

    public SingleCommitment setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("commitment",commitment)
                .put("coefficientNumber",coefficientNumber)
                .put("signature",signature);
    }
}
