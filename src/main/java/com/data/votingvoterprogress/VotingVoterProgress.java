package com.data.votingvoterprogress;

import com.data.JsonMappable;
import org.json.JSONObject;

public class VotingVoterProgress implements JsonMappable {

    private long votingId;

    private int phaseNumber;

    private boolean createdSecretShares = false;

    private boolean voted =  false;

    private boolean participatedInDecryption = false;

    private boolean usesCustomKey = false;

    public VotingVoterProgress setPhaseNumber(int phaseNumber) {
        this.phaseNumber = phaseNumber;
        return this;
    }

    public VotingVoterProgress setCreatedSecretShares(boolean createdSecretShares) {
        this.createdSecretShares = createdSecretShares;
        return this;
    }

    public VotingVoterProgress setVoted(boolean voted) {
        this.voted = voted;
        return this;
    }

    public VotingVoterProgress setParticipatedInDecryption(boolean participatedInDecryption) {
        this.participatedInDecryption = participatedInDecryption;
        return this;
    }

    public VotingVoterProgress setVotingId(long voterId) {
        this.votingId = voterId;
        return this;
    }

    public VotingVoterProgress setUsesCustomKey(boolean usesCustomKey) {
        this.usesCustomKey = usesCustomKey;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("votingId", votingId)
                .put("phaseNumber",phaseNumber)
                .put("createdSecretShares",createdSecretShares)
                .put("voted",voted)
                .put("participatedInDecryption",participatedInDecryption)
                .put("usesCustomKey",usesCustomKey);
    }
}
