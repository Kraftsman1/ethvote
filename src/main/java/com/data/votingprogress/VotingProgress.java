package com.data.votingprogress;

import com.data.JsonMappable;
import com.data.voter.SingleVoter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class VotingProgress implements JsonMappable {

    private List<SingleVoterProgress> votersProgressList;

    private SingleVoter votingAdministrator;

    private int phaseNumber;

    private Long timeVotingIsFinished;

    private Long securityThreshold;

    private String transactionId;

    public List<SingleVoterProgress> getVotersProgressList() {
        return votersProgressList;
    }

    public VotingProgress setVotersProgressList(List<SingleVoterProgress> votersProgressList) {
        this.votersProgressList = votersProgressList;
        return this;
    }

    public SingleVoter getVotingAdministrator() {
        return votingAdministrator;
    }

    public VotingProgress setVotingAdministrator(SingleVoter votingAdministrator) {
        this.votingAdministrator = votingAdministrator;
        return this;
    }

    public int getPhaseNumber() {
        return phaseNumber;
    }

    public VotingProgress setPhaseNumber(int phaseNumber) {
        this.phaseNumber = phaseNumber;
        return this;
    }

    public long getSecurityThreshold() {
        return securityThreshold;
    }

    public VotingProgress setSecurityThreshold(long securityThreshold) {
        this.securityThreshold = securityThreshold;
        return this;
    }

    public Long getTimeVotingIsFinished() {
        return timeVotingIsFinished;
    }

    public VotingProgress setTimeVotingIsFinished(Long timeVotingIsFinished) {
        this.timeVotingIsFinished = timeVotingIsFinished;
        return this;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public VotingProgress setTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    @Override
    public JSONObject toJson() {
        JSONArray votersProgressArray = new JSONArray();
        for(SingleVoterProgress singleVoterProgress : votersProgressList) {
            votersProgressArray.put(singleVoterProgress.toJson());
        }

        return new JSONObject()
                .put("votersProgress",votersProgressArray)
                .put("phaseNumber",phaseNumber)
                .put("securityThreshold",securityThreshold)
                .put("transactionId",transactionId);
    }
}
