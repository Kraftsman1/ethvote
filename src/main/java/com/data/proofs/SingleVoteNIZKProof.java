package com.data.proofs;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class SingleVoteNIZKProof implements JsonMappable {

    @JsonProperty("messageIndex")
    private Integer messageIndex;

    @JsonProperty("a")
    private String a;

    @JsonProperty("b")
    private String b;

    @JsonProperty("c")
    private String c;

    @JsonProperty("r")
    private String r;

    @JsonProperty("signature")
    private String signature;

    public SingleVoteNIZKProof setMessageIndex(Integer messageIndex) {
        this.messageIndex = messageIndex;
        return this;
    }

    public SingleVoteNIZKProof setA(String a) {
        this.a = a;
        return this;
    }

    public SingleVoteNIZKProof setB(String b) {
        this.b = b;
        return this;
    }

    public SingleVoteNIZKProof setC(String c) {
        this.c = c;
        return this;
    }

    public SingleVoteNIZKProof setR(String r) {
        this.r = r;
        return this;
    }

    public SingleVoteNIZKProof setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public Integer getMessageIndex() {
        return messageIndex;
    }

    public String getA() {
        return a;
    }

    public String getB() {
        return b;
    }

    public String getC() {
        return c;
    }

    public String getR() {
        return r;
    }

    public String getSignature() { return signature;}

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("messageIndex",messageIndex)
                .put("a",a)
                .put("b",b)
                .put("c",c)
                .put("r",r)
                .put("signature",signature);
    }
}
