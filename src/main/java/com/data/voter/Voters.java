package com.data.voter;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Voters implements JsonMappable {

    @JsonProperty("voters")
    List<SingleVoter> voterList = new ArrayList<>();

    public Voters setVoters(List<SingleVoter> voters) {
        this.voterList = voters;
        return this;
    }

    public Voters addVoter(SingleVoter singleVoter) {
        voterList.add(singleVoter);
        return this;
    }

    public List<SingleVoter> getVoters() {
        return voterList;
    }

    @Override
    public JSONObject toJson() {
        JSONArray jsonArray = new JSONArray();
        for(SingleVoter voter : voterList) {
            jsonArray.put(voter.toJson());
        }
        return new JSONObject()
                .put("voters",jsonArray);
    }

    public JSONObject toJsonWithRoles() {
        JSONArray jsonArray = new JSONArray();
        for(SingleVoter voter : voterList) {
            jsonArray.put(voter.toJsonWithRoles());
        }
        return new JSONObject()
                .put("voters",jsonArray);
    }
}
