package com.data.secretshares;

import com.data.JsonMappable;
import org.json.JSONObject;

import java.util.List;

public class SingleSecretShareForVoter implements JsonMappable {

    private final long fromVoterId;

    private final String fromVoterName;

    private final List<String> secretShare;

    private final String signature;

    public SingleSecretShareForVoter(long fromVoterId, String fromVoterName, List<String> secretShare, String signature) {
        this.fromVoterId = fromVoterId;
        this.fromVoterName = fromVoterName;
        this.secretShare = secretShare;
        this.signature = signature;
    }

    public long getFromVoterId() {
        return fromVoterId;
    }

    public String getFromVoterName() {
        return fromVoterName;
    }

    public List<String> getSecretShare() {
        return secretShare;
    }

    public String getSignature() {
        return signature;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("fromVoterId", fromVoterId)
                .put("fromVoterName", fromVoterName)
                .put("secretShare",secretShare)
                .put("signature",signature);
    }
}
