package com.data.secretshares;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SingleSecretShareFromVoter {

    @JsonProperty("forVoterId")
    private Long forVoterId;

    @JsonProperty("forVoterName")
    private String forVoterName;

    @JsonProperty("secretShare")
    private List<String> secretShare;

    @JsonProperty("signature")
    private String signature;

    public SingleSecretShareFromVoter setForVoterId(long forVoterId) {
        this.forVoterId = forVoterId;
        return this;
    }

    public SingleSecretShareFromVoter setForVoterName(String forVoterName) {
        this.forVoterName = forVoterName;
        return this;
    }

    public SingleSecretShareFromVoter setSecretShare(List<String> secretShare) {
        this.secretShare = secretShare;
        return this;
    }

    public Long getForVoterId() {
        return forVoterId;
    }

    public String getForVoterName() {
        return forVoterName;
    }

    public List<String> getSecretShare() {
        return secretShare;
    }

    public String getSignature() {
        return signature;
    }

    public SingleSecretShareFromVoter setSignature(String signature) {
        this.signature = signature;
        return this;
    }
}
