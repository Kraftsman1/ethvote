package com.data.roles;

import com.data.JsonMappable;
import com.enums.Role;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Roles implements JsonMappable {

    private List<Role> roleList;

    public Roles() {
        roleList = new ArrayList<>();
    }

    public Roles addRole(Role role) {
        roleList.add(role);
        return this;
    }

    public List<Role> getRoles() {
        return this.roleList;
    }

    @Override
    public JSONObject toJson() {
        JSONArray roleArray = new JSONArray();
        for(Role role : roleList) {
            roleArray.put(role.getRoleName());
        }


        return new JSONObject()
                .put("roles",roleArray);
    }
}
