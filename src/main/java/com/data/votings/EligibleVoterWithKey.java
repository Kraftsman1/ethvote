package com.data.votings;

import org.json.JSONObject;

public class EligibleVoterWithKey extends EligibleVoter<EligibleVoterWithKey>{

    private String publicKey;

    public String getPublicKey() {
        return publicKey;
    }

    public EligibleVoterWithKey setPublicKey(String publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    @Override
    public JSONObject toJson() {
        return super.toJson()
                .put("publicKey",publicKey);
    }
}
