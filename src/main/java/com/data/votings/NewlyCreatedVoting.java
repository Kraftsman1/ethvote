package com.data.votings;

import com.data.voter.SingleVoter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class NewlyCreatedVoting extends Voting<NewlyCreatedVoting> {

    @JsonProperty("eligibleVoters")
    private List<EligibleVoter> eligibleVoters;

    @JsonProperty("securityThreshold")
    private long securityThreshold = -1;

    @JsonProperty("numberOfEligibleVoters")
    private long numberOfEligibleVoters = -1;

    @JsonProperty("calculationParameters")
    private VotingCalculationParameters votingCalculationParameters;

    private SingleVoter votingAdministrator;

    public NewlyCreatedVoting() {
        eligibleVoters = new LinkedList<>();
        votingCalculationParameters = new VotingCalculationParameters();
    }

    public List<EligibleVoter> getEligibleVoters() {
        return this.eligibleVoters;
    }

    public NewlyCreatedVoting addParticipatingVoter(EligibleVoter eligibleVoter) {
        eligibleVoters.add(eligibleVoter);
        return this;
    }

    public long getSecurityThreshold() {
        return securityThreshold;
    }

    public NewlyCreatedVoting setSecurityThreshold(long securityThreshold) {
        this.securityThreshold = securityThreshold;
        return this;
    }

    public long getNumberOfEligibleVoters() {
        return numberOfEligibleVoters;
    }

    public NewlyCreatedVoting setNumberOfEligibleVoters(long numberOfEligibleVoters) {
        this.numberOfEligibleVoters = numberOfEligibleVoters;
        return this;
    }

    public VotingCalculationParameters getVotingCalculationParameters() {
        return votingCalculationParameters;
    }

    public NewlyCreatedVoting setVotingCalculationParameters(VotingCalculationParameters votingCalculationParameters) {
        this.votingCalculationParameters = votingCalculationParameters;
        return this;
    }

    public SingleVoter getVotingAdministrator() {
        return votingAdministrator;
    }

    public NewlyCreatedVoting setVotingAdministrator(SingleVoter votingAdministrator) {
        this.votingAdministrator = votingAdministrator;
        return this;
    }

    @Override
    public JSONObject toJson() {
        JSONObject jsonObject = super.toJson();
        jsonObject
                .put("numberOfEligibleVoters",getNumberOfEligibleVoters())
                .put("securityThreshold",getSecurityThreshold())
                .put("calculationParameters",getVotingCalculationParameters().toJson())
                .put("eligibleVoters", new JSONArray(
                        eligibleVoters.stream()
                                .map(EligibleVoter::toJson)
                                .collect(Collectors.toList())
                ));
        return jsonObject;
    }
}
