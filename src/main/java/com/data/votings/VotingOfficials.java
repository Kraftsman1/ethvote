package com.data.votings;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VotingOfficials implements JsonMappable {

    private Long votingId = null;

    private String votingAdministratorName = null;

    private List<String> trusteeNames = new ArrayList<>();

    public VotingOfficials setVotingId(Long votingId) {
        this.votingId = votingId;
        return this;
    }

    public VotingOfficials setVotingAdministrator(String votingAdministratorName) {
        this.votingAdministratorName = votingAdministratorName;
        return this;
    }

    public VotingOfficials addTrustee(String trusteeName) {
        trusteeNames.add(trusteeName);
        return this;
    }

    public Long getVotingId() {
        return this.getVotingId();
    }

    public String getVotingAdministratorName() {
        return this.votingAdministratorName;
    }

    public List<String> getTrusteeNames() {
        return this.trusteeNames;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("votingId", votingId)
                .put("votingAdministrator",votingAdministratorName)
                .put("trustees", new JSONArray(trusteeNames));
    }
}
