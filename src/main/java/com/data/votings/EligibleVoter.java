package com.data.votings;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class EligibleVoter<T extends EligibleVoter> implements JsonMappable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("position")
    private Long position;

    @JsonProperty("isTrustee")
    private Boolean isTrustee;

    @JsonProperty("canVote")
    private Boolean canVote;

    public String getName() {
        return name;
    }

    public T setName(String name) {
        this.name = name;
        return (T)this;
    }

    public Long getId() {
        return id;
    }

    public T setId(Long id) {
        this.id = id;
        return (T)this;
    }

    public Long getPosition() {
        return position;
    }

    public T setPosition(Long position) {
        this.position = position;
        return (T)this;
    }

    public Boolean getIsTrustee() {
        return isTrustee;
    }

    public T setIsTrustee(Boolean isTrustee) {
        this.isTrustee = isTrustee;
        return (T)this;
    }

    public Boolean getCanVote() {
        return canVote;
    }

    public T setCanVote(Boolean canVote) {
        this.canVote = canVote;
        return (T)this;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("id",this.id)
                .put("name",this.name)
                .put("position",this.position)
                .put("isTrustee",this.isTrustee)
                .put("canVote",this.canVote);
    }
}
