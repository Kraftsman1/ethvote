package com.data.votes;

import com.data.JsonMappable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Votes implements JsonMappable {

    private List<SingleVote> voteList;

    public Votes() {
        voteList = new ArrayList<>();
    }

    public Votes addVote(SingleVote singleVote) {
        voteList.add(singleVote);
        return this;
    }

    public List<SingleVote> getVotes() {
        return this.voteList;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("numberOfVotes",voteList.size())
                .put("votes",new JSONArray(
                        voteList.stream()
                                .map(SingleVote::toJson)
                                .collect(Collectors.toList())
                ));
    }

}
