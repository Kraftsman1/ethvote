package com.data.votes;

import com.data.JsonMappable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class SingleVote implements JsonMappable {

    @JsonProperty("fromVoterId")
    private Long fromVoterId = null;

    @JsonProperty("fromVoterName")
    private String fromVoterName = null;

    @JsonProperty("alpha")
    private String alpha = null;

    @JsonProperty("beta")
    private String beta = null;

    @JsonProperty("signature")
    private String signature = null;

    public SingleVote setFromVoterId(Long fromVoterId) {
        this.fromVoterId = fromVoterId;
        return this;
    }

    public SingleVote setFromVoterName(String fromVoterName) {
        this.fromVoterName = fromVoterName;
        return this;
    }

    public SingleVote setAlpha(String alpha) {
        this.alpha = alpha;
        return this;
    }

    public SingleVote setBeta(String beta) {
        this.beta = beta;
        return this;
    }

    public SingleVote setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public Long getFromVoterId() {
        return fromVoterId;
    }

    public String getFromVoterName() {
        return fromVoterName;
    }

    public String getAlpha() {
        return alpha;
    }

    public String getBeta() {
        return beta;
    }

    public String getSignature() {
        return signature;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject()
                .put("fromVoterId",fromVoterId)
                .put("fromVoterName",fromVoterName)
                .put("alpha",alpha)
                .put("beta",beta)
                .put("signature",signature);
    }
}
