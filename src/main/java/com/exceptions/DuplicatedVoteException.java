package com.exceptions;

public class DuplicatedVoteException extends IllegalVoteException {

    public DuplicatedVoteException(String message) {
        super(message);
    }
}
