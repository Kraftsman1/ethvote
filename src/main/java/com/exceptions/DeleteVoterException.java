package com.exceptions;

public abstract class DeleteVoterException extends Exception {

    private final String message;

    public DeleteVoterException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
