package com.exceptions.parser;

public class TooFewEligiblieVotersException extends ParserException {
    public TooFewEligiblieVotersException(String message) {
        super(message);
    }
}