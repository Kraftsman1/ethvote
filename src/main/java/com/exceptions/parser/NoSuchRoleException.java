package com.exceptions.parser;

public class NoSuchRoleException extends ParserException {

    public NoSuchRoleException(String message) {
        super(message);
    }

    public NoSuchRoleException(String message, Exception e) {
        super(message,e);
    }
}
