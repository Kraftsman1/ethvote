package com.exceptions.parser;

public class OptionWithoutPrimeNumberException extends ParserException{
    public OptionWithoutPrimeNumberException(String message) {
        super(message);
    }
}
