package com.exceptions.parser;


public class NoContentException extends ParserException {
    public NoContentException(String message) {
        super(message);
    }

    public NoContentException(String message, Exception e) {
        super(message,e);
    }
}
