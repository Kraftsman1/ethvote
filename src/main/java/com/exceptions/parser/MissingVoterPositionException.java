package com.exceptions.parser;

public class MissingVoterPositionException extends ParserException{
    public MissingVoterPositionException(String message) {
        super(message);
    }
}
