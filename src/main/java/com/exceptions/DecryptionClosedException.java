package com.exceptions;

public class DecryptionClosedException extends IllegalVoteException {

    public DecryptionClosedException(String message) {
        super(message);
    }
}
