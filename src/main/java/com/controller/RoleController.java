package com.controller;

import com.data.voter.SingleVoter;

import com.enums.Role;
import com.services.context.RequestContextHolder;
import com.services.conversion.RoleConverter;
import com.services.data.roles.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class RoleController {

    private final RoleService roleService;

    private final RequestContextHolder requestContextHolder;

    @Autowired
    public RoleController(RoleService roleService,
                          RequestContextHolder requestContextHolder) {
        this.roleService = roleService;
        this.requestContextHolder = requestContextHolder;
    }

    @GetMapping("/api/roles")
    public ResponseEntity getAvailableRoles()  {
        return ResponseEntity.ok(roleService.getAllAvailableRoles().toJson().toString());
    }

    @GetMapping("/api/voters/roles")
    public ResponseEntity getAllVotersWithTheirRoles()  {
        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The current voter is not authenticated");
        }
        SingleVoter singleVoter = optCurrentVoter.get();

        if(     !roleService.hasRole(singleVoter.getId(),Role.ADMINISTRATOR) &&
                !roleService.hasRole(singleVoter.getId(), Role.REGISTRAR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Only administrators can see all the roles");
        }

        return ResponseEntity.ok(roleService.getAllVotersWithTheirRoles().toJsonWithRoles().toString());
    }

    @PutMapping("/api/voters/{voterId}/roles/{roleName}")
    public ResponseEntity addRole(
            @PathVariable Long voterId,
            @PathVariable String roleName) {
        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();
        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The current voter is not authenticated");
        }

        SingleVoter currentVoter = optCurrentVoter.get();

        if(!roleService.hasRole(currentVoter.getId(),Role.ADMINISTRATOR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Only administrators can add roles for users");
        }

        Optional<Role> role = RoleConverter.toEnum(roleName);
        if(!role.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(String.format("The role '%s' does not exist",roleName));
        }

        boolean successful = roleService.addRoleToVoter(voterId,role.get());
        if(successful) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("Role '%s' was added to user with id '%d'",roleName,voterId));
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(String.format("Role '%s' could not be added to user with id '%d'",roleName,voterId));
    }

    @DeleteMapping("/api/voters/{voterId}/roles/{roleName}")
    public ResponseEntity deleteRole(
            @PathVariable Long voterId,
            @PathVariable String roleName) {
        Optional<SingleVoter> optCurrentVoter = requestContextHolder.getCurrentVoter();

        if(!optCurrentVoter.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The current voter is not known");
        }

        SingleVoter currentVoter = optCurrentVoter.get();

        if(!roleService.hasRole(currentVoter.getId(),Role.ADMINISTRATOR)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Only administrators can delete roles for users");
        }

        Optional<Role> role = RoleConverter.toEnum(roleName);
        if(!role.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(String.format("The role  '%s' does not exist",roleName));
        }

        boolean successful = roleService.deleteRoleFromVoter(voterId,role.get());
        if(successful) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(String.format("Role '%s' was removed from user with id '%d'",roleName,voterId));
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(String.format("Role '%s' could not be removed from user with id '%d'",roleName,voterId));
    }
}
