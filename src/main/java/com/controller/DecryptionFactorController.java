package com.controller;

import com.data.decryptionfactor.DecryptionFactors;
import com.data.decryptionfactor.SingleDecryptionFactor;
import com.data.voter.SingleVoter;

import com.exceptions.IllegalVoteException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.vote.VoteService;
import com.services.parser.SingleDecryptionFactorParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DecryptionFactorController {

    private final VoteService voteService;

    private RequestContextHolder requestContextHolder;

    @Autowired
    public DecryptionFactorController(VoteService voteService,
                                      RequestContextHolder requestContextHolder) {
        this.voteService = voteService;
        this.requestContextHolder = requestContextHolder;
    }

    @PostMapping("/api/votings/{id}/decryptionFactors/voters/{voterId}")
    public ResponseEntity addDecryptionFactor(
            @PathVariable Long id,
            @PathVariable Long voterId,
            @RequestBody String jsonData)  {
        Optional<SingleVoter> currentUser = requestContextHolder.getCurrentVoter();
        if(!currentUser.isPresent() || !currentUser.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only specify his own decryptionFactors");
        }

        SingleDecryptionFactor singleDecryptionFactor;

        try {
            singleDecryptionFactor = SingleDecryptionFactorParser.parse(jsonData);
            voteService.setDecryptionFactor(voterId,id,singleDecryptionFactor);
        } catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.CREATED)
                .body("Decryption factor was added");

    }

    @GetMapping("/api/votings/{id}/decryptionFactors")
    public ResponseEntity getAllDecryptionFactors(@PathVariable Long id) {
        DecryptionFactors decryptionFactors = voteService.getAllDecryptionFactors(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(decryptionFactors.toJson().toString());

    }

}