package com.controller;

import com.data.commitments.CommitmentFromVoter;
import com.data.commitments.Commitments;
import com.data.voter.SingleVoter;
import com.exceptions.IllegalVoteException;
import com.exceptions.parser.ParserException;
import com.services.context.RequestContextHolder;
import com.services.data.commitments.CommitmentService;
import com.services.parser.CommitmentParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class CommitmentController {

    private final CommitmentService commitmentService;
    private final RequestContextHolder requestContextHolder;

    @Autowired
    public CommitmentController(CommitmentService commitmentService,
                                RequestContextHolder requestContextHolder) {
        this.commitmentService = commitmentService;
        this.requestContextHolder = requestContextHolder;
    }

    @PostMapping("/api/votings/{id}/commitments/voters/{voterId}")
    public ResponseEntity addCommitment(
            @PathVariable Long id,
            @PathVariable Long voterId,
            @RequestBody String jsonData)  {
        long votingId = id;
        Optional<SingleVoter> currentVoter = requestContextHolder.getCurrentVoter();
        if(!currentVoter.isPresent() || !currentVoter.get().getId().equals(voterId)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("A user can only specify his own commitmentFromVoter");
        }

        CommitmentFromVoter commitmentFromVoter;
        try {
            commitmentFromVoter = CommitmentParser.parse(jsonData);
            commitmentService.setCommitmentsForUser(votingId,voterId, commitmentFromVoter);
        } catch (ParserException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        } catch (IllegalVoteException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(e.getMessage());
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Commitments were added");
    }

    @GetMapping("/api/votings/{id}/commitments/voters/{voterId}")
    public ResponseEntity getCommitment(
            @PathVariable Long id,
            @PathVariable Long voterId)  {

        long votingId = id;
        CommitmentFromVoter commitmentFromVoter = commitmentService.getCommitmentsFromUser(votingId,voterId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(commitmentFromVoter.toJson().toString());
    }

    @GetMapping("/api/votings/{id}/commitments")
    public ResponseEntity getAllCommitments(
            @PathVariable Long id)  {

        long votingId = id;
        Commitments commitments = commitmentService.getAllCommitmentsForVoting(votingId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(commitments.toJson().toString());
    }
}
