package com.controller;

import com.controller.data.request.LoginBody;
import com.controller.data.response.Token;
import com.enums.HttpErrorMessages;
import com.services.data.voters.VotersService;
import com.services.sanitization.SanitizationService;
import com.services.token.JwtTokenCreatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.services.autentication.AuthenticationService;

import java.util.Optional;

@RestController
public class AuthenticationController {

    private final AuthenticationService authService;
    private final JwtTokenCreatorService jwtTokenCreatorService;
    private final VotersService votersService;

    @Autowired
    public AuthenticationController(final AuthenticationService authenticationService,
                                    final JwtTokenCreatorService jwtTokenCreatorService,
                                    final VotersService votersService)
    {
        this.authService = authenticationService;
        this.jwtTokenCreatorService = jwtTokenCreatorService;
        this.votersService = votersService;
    }

    @PostMapping("/api/authenticate")
    public @ResponseBody ResponseEntity authenticate(
            @RequestBody LoginBody loginData) {

        String inputVoterName = loginData.getUsername();
        String inputPassword = loginData.getPassword();

        if(inputVoterName==null) {
            return ResponseEntity.
                    status(HttpStatus.BAD_REQUEST)
                    .body(HttpErrorMessages.MISSING_USERNAME_FIELD.toString());
        }

        if(inputPassword==null) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(HttpErrorMessages.MISSING_PASSWORD_FIELD.toString());
        }

        if(!sanitize(inputVoterName,inputPassword)) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(HttpErrorMessages.ILLEGAL_CHARACTER_IN_USERNAME_OR_PASSWORD.toString());
        }

        if(!votersService.getVoter(inputVoterName).isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body(HttpErrorMessages.WRONG_USERNAME_AND_PASSWORD.toString());
        }

        if(!validLoginCredentials(inputVoterName,inputPassword)) {
            return ResponseEntity
                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body(HttpErrorMessages.WRONG_USERNAME_AND_PASSWORD.toString());
        }

        Optional<String> optToken = jwtTokenCreatorService.createTokenForUser(inputVoterName);
        if(!optToken.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("JWT Token could no be generated");
        }

        String token = optToken.get();
        Token returnObject = new Token(token);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(returnObject);
    }

    private boolean sanitize(String voterName, String password) {
        return SanitizationService.hasNoSpecialCharacters(voterName) && SanitizationService.hasNoSpecialCharacters(password);
    }

    private boolean validLoginCredentials(String voterName, String password) {
        return authService.authenticate(voterName,password);
    }
}