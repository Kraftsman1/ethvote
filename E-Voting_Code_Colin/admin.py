import json
import nacl.signing
import nacl.encoding
import nacl.hash
import nacl.exceptions
import base64
import socket
import io
from math import ceil


class Admin:
    def __init__(self, settings, voters=None, bbserver_pkey=None, skey=None):
        """
        todo: currently only Ranking voting mode implemented
        :param skey: private key (string, hex encoded). will generate one if =None
        :param bbserver_pkey: public key (hex encoded). will generate one if =None. note: give bbserver_skey to bbserver
        :param voters: unsorted list of voters (as dicts with their informations)
        :param settings: dict with all voting settings
        """
        if skey is not None:
            self._skey = nacl.signing.SigningKey(skey.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
        else:
            self._skey = nacl.signing.SigningKey.generate()
        self._pkey = self._skey.verify_key
        self._pkey_hex = self._pkey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")

        if bbserver_pkey is not None:
            self._bbserver_skey = None
            self._bbserver_pkey = nacl.signing.VerifyKey(bbserver_pkey.encode("utf-8"),
                                                         encoder=nacl.encoding.HexEncoder)
        else:
            self._bbserver_skey = nacl.signing.SigningKey.generate()
            self._bbserver_skey_hex = self._bbserver_skey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")
            self._bbserver_pkey = self._bbserver_skey.verify_key
        self._bbserver_pkey_hex = self._bbserver_pkey.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8")
        self._electionid = settings["electionid"]
        self._mode = settings["mode"]  # str
        if self._mode == "ranking":
            # requires voters to vote one of the options in every question without duplicates
            self._textoptions = settings["textoptions"]  # [str]
            self._options = settings["options"]  # [int]
            self._optioncount = len(self._options)  # int
            self._questioncount = self._optioncount  # int
        else:
            raise NotImplementedError("Mode currently not supported")
        self._p = settings["p"]
        self._q = settings["q"]
        self._g = settings["g"]
        self._thresholdfraction = settings["thresholdfraction"]  # float
        if voters is not None:
            self._voterlist = self._buildvoterlist(voters)  # dict
            self._votercount = len(voters)  # int
            self._threshold = int(ceil(self._votercount * self._thresholdfraction))  # int

    def get_bbserver_skey(self):
        return self._bbserver_skey_hex

    def set_bbserver_pkey(self, key):
        """
        :param key: bbserver_pkey as hex encoded str
        """
        self._bbserver_skey = None
        self._bbserver_pkey = nacl.signing.VerifyKey(key.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
        self._bbserver_pkey_hex = key

    @staticmethod
    def _buildvoterlist(voters):
        """
        :param voters: list of voter informations (dicts)
        :return: voterlist (dict)
        """
        voterlist = dict()
        for i in range(len(voters)):
            voters[i]["nr"] = i + 1
            voterlist[str(i + 1)] = voters[i]
        return voterlist

    def sendsetupmessage(self):
        message = dict()
        message["electionid"] = self._electionid
        if self._mode == "ranking":
            message["mode"] = "ranking"
            message["textoptions"] = self._textoptions
            message["options"] = self._options
        else:
            raise NotImplementedError("Mode currently not supported")
        message["votercount"] = self._votercount
        message["threshold"] = self._threshold
        message["admin_pkey"] = self._pkey_hex
        message["bbserver_pkey"] = self._bbserver_pkey_hex
        message["p"] = self._p
        message["q"] = self._q
        message["g"] = self._g
        # todo: include information for voters on how to reach bbserver
        self._sendmessage("setup", json.dumps(message))

    def sendvoterlist(self):
        self._sendmessage("voterlist", json.dumps(self._voterlist))

    def _sendmessage(self, msgtype, content):
        """
        broadcasting a message
        :param msgtype: string
        :param content: string
        """
        signkey = self._skey
        message = {"origin": "admin", "type": msgtype, "content": content}
        message_str = json.dumps(message).encode("utf-8")
        h = nacl.hash.sha256(message_str)
        signature = signkey.sign(h).signature
        signature_b64 = base64.b64encode(signature).decode("utf-8")
        message["signature"] = signature_b64

        message_enc = (json.dumps(message)).encode("utf-8")

        #####

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = "localhost"
        port = 1234
        chunksize = 1024

        s.connect((host, port))
        print('Sending...')
        file = io.BytesIO(message_enc)
        sbuf = file.read(chunksize)
        while sbuf:
            print('Sending...')
            s.send(sbuf)
            sbuf = file.read(chunksize)
        print("Done Sending")
        s.shutdown(socket.SHUT_WR)
        #####
        bbserver_pubkey = self._bbserver_pkey
        #####
        buf = s.recv(chunksize)
        msg = []
        while buf:
            print("Receiving...")
            msg.append(buf.decode("utf-8"))
            buf = s.recv(chunksize)
        recmsg = "".join(msg)
        print("Done Receiving")
        recsig = base64.b64decode(recmsg)
        h = nacl.hash.sha256(message_enc)
        try:
            bbserver_pubkey.verify(h, recsig)
        except (ValueError, TypeError, nacl.exceptions.BadSignatureError):
            print("Invalid Server-Signature!")

        #####
        hash_hex = h.decode("utf-8")
        sig_hex = recsig.hex()
        assert len(hash_hex) == 64, "Hash wrong length!"
        assert len(sig_hex) == 128, "Signature wrong length!"
        post_hex = hash_hex + sig_hex
        self._post(post_hex)

    @staticmethod
    def _post(message):
        """
        post a (small) message to a (hopefully immutable) log
        :type message: str
        :param message: message as hexstring
        """
        # using a local .json to imitate a real blockchain

        with open("blockchain.json") as infile:
            blockchainstack = json.load(infile)

        blockchainstack.append(message)

        with open("blockchain.json", "w") as outfile:
            json.dump(blockchainstack, outfile, indent=2, ensure_ascii=False)
