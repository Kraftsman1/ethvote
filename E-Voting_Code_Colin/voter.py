import json
import pprint
from messaging import sendmessage
from utils import prod, eval_poly, randomint, elgamal_enc, mulinv, \
    recover_message, NIZKProof, ORProof, or_challenge_generator, \
    single_challenge_generator
from itertools import combinations_with_replacement
import nacl.public
import nacl.encoding
import nacl.signing
from collections import Counter


class Voter:
    def __init__(self, voterid, signkey, deckey):
        self._signkey = signkey
        self._deckey = deckey
        self._voterid = voterid

        self._messagestack = None
        self._voternr = None
        self._readsetupmessage(1001)
        self._database = self._builddatabase()

        self._poly = None
        self._points = None
        self._polycommits = None
        self._points_enc = None
        self._received_shares = None
        self._received_shares_dec = None
        self._secret_share = None
        self._textvote = None
        self._vote = None
        self._ballot = [None] * len(self._options)
        self._encryptionrandomness = [None] * len(self._options)
        self._encryptionproof = [None] * len(self._options)
        self._prod_proof = None
        self._publickey = None
        self._ballotsum = [None] * len(self._options)
        self._partialdec = [None] * len(self._options)
        self._decproof = [None] * len(self._options)
        self._allpartdec = [[] for _ in range(len(self._options))]
        self._recovered_message = [None] * len(self._options)

    def _builddatabase(self):
        self._messagestack = self._getallmessages()

        for msg in self._messagestack:

            # maybe get origin, (content) etc here to use

            if msg["type"] == "voterlist":
                database = json.loads(msg["content"])
                for key in database:
                    if database[key]["voterid"] == self._voterid:
                        self._voternr = database[key]["nr"]
                        break
            elif msg["type"] == "round1":  # currently only reading for self relevant shares
                database[str(msg["origin"])]["share_for_me"] = json.loads(msg["content"])[0][self._voternr - 1]
                database[str(msg["origin"])]["commits"] = json.loads(msg["content"])[1]

            elif msg["type"] == "round2":  # no tests or proofs
                database[str(msg["origin"])]["ballot"] = json.loads(msg["content"])["ballot"]
                database[str(msg["origin"])]["encryptionproof"] = []
                for i in range(len(self._options)):
                    database[str(msg["origin"])]["encryptionproof"].append(
                        ORProof.from_dict(json.loads(msg["content"])["proof"][0][i]))
                database[str(msg["origin"])]["prodproof"] = NIZKProof.from_dict(json.loads(msg["content"])["proof"][1])

            elif msg["type"] == "round3":  # no tests or proofs
                database[str(msg["origin"])]["partdec"] = json.loads(msg["content"])["dec"]
                database[str(msg["origin"])]["partdecproof"] = []
                for i in range(len(self._options)):
                    database[str(msg["origin"])]["partdecproof"].append(
                        NIZKProof.from_dict(json.loads(msg["content"])["proof"][i]))

        return database

    def _readsetupmessage(self, electionid):
        """
        searches for a setupmessage and sets parameters accordingly
        """
        with open("msgstack.json") as infile:
            stack = json.load(infile)
        for msg in stack:
            content = json.loads(msg["content"])
            if msg["type"] == "setup" and msg["origin"] == "admin" and content["electionid"] == electionid:
                if content["mode"] == "ranking":
                    self._mode = "ranking"
                    self._textoptions = content["textoptions"]
                    self._options = content["options"]
                    self._n = content["votercount"]
                    self._k = content["threshold"]
                    self._admin_pkey_hex = content["admin_pkey"]
                    self._admin_pkey = nacl.signing.VerifyKey(self._admin_pkey_hex.encode("utf-8"),
                                                              encoder=nacl.encoding.HexEncoder)
                    self._bbserver_pkey_hex = content["bbserver_pkey"]
                    self._bbserver_pkey = nacl.signing.VerifyKey(self._bbserver_pkey_hex.encode("utf-8"),
                                                                 encoder=nacl.encoding.HexEncoder)
                    self._p = content["p"]
                    self._q = content["q"]
                    self._g = content["g"]
                    self._h = None  # to indicate multiplicative homomorphism
                break

    def _getallmessages(self):
        """
        returns all messages that have been sent until now.
        todo: verify messages with blockchain hashes
        :return: list of messages
        """
        with open("msgstack.json") as infile:
            stack = json.load(infile)
        return stack

    def _generate_random_shares(self, n, k):
        """
        Generates a random shamir pool, returns the secret and the share
        points.
        """
        if k > n:
            raise ValueError("too few shares for order of polynomial")
        self._poly = [randomint(self._q) for _ in range(k)]
        self._points = [(eval_poly(self._poly, i, self._q)) for i in range(1, n + 1)]

        return  # self.poly[0], self.points are secret and shares

    def _encrypt_shares(self):
        """
        encrypts all secret shares with public keys of recipients (defined by index of point)
        :return: encrypted shares
        """
        points_enc = []
        for i in range(self._n):
            key_enc = self._database[str(i + 1)]["voter_enc_pk"]
            key = nacl.public.PublicKey(key_enc.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
            point = str(self._points[i])
            encrypted = nacl.public.SealedBox(key).encrypt(point.encode("utf-8"), encoder=nacl.encoding.HexEncoder)
            points_enc.append(encrypted.decode("utf-8"))
        return points_enc

    def _commit_poly(self):
        """
        :return: commitments to poly parameters
        """
        commits = [pow(self._g, self._poly[i], self._p) for i in range(self._k)]
        return commits

    def round1(self):
        self._database = self._builddatabase()
        self._generate_random_shares(self._n, self._k)
        self._points_enc = self._encrypt_shares()
        self._polycommits = self._commit_poly()

        sendmessage(self._voternr, "round1", "[" + json.dumps(self._points_enc) + ", "
                    + json.dumps(self._polycommits) + "]", self._signkey, self._bbserver_pkey)

    def _decrypt_shares(self):
        """
        takes all received shares from database and decrypts all received shares with own private key
        :return: decrypted shares
        """
        self._received_shares = [self._database[i]["share_for_me"] for i in self._database.keys()]
        received_shares_dec = []
        for i in range(self._n):
            cypher = self._received_shares[i].encode("utf-8")
            key = self._deckey
            plaintext = nacl.public.SealedBox(key).decrypt(cypher, encoder=nacl.encoding.HexEncoder).decode("utf-8")
            received_shares_dec.append(int(plaintext))
        self._received_shares_dec = received_shares_dec

    def _verify_shares(self):
        """
        check if received shares match commits
        g^sij = PROD over l( Fil ^ (j^l) )
        with Fil = commits
        """
        for i in self._database.keys():
            if pow(self._g, self._received_shares_dec[int(i) - 1], self._p) != \
                    prod([pow(self._database[i]["commits"][j], pow(self._voternr, j, self._p), self._p) for j in
                          range(len(self._database[i]["commits"]))], self._p):
                print("BADSHARE")

    def _inspect_shares(self):
        """
        calculate g^sj for verification of decryption
        todo: maybe more checks
        """
        for j in self._database.keys():
            self._database[j]["pubkeyshare"] = prod([prod([pow(self._database[i]["commits"][l], pow(int(j), l,
                                                                                                    self._p),
                                                               self._p)
                                                           for l in range(len(self._database[i]["commits"]))],
                                                          self._p)
                                                     for i in self._database.keys()], self._p)

    def _combine_share(self):
        """
        :return: secret share, obtained from combining all (decrypted) received shares
        """
        self._secret_share = sum(self._received_shares_dec) % self._q

    def _calculate_publickey(self):
        """
        :return: public elGamal key for vote encryption
        """
        relevantcommits = [self._database[i]["commits"][0] for i in self._database.keys()]

        self._publickey = prod(relevantcommits, self._p)

    def _encode_vote(self):
        """
        encodes user input into vote (if not done by user)
        :return: encoded vote
        """
        if self._mode == "ranking":
            vote = []
            for i in self._textvote:
                for j in range(len(self._options)):
                    if i == self._textoptions[j]:
                        vote.append(self._options[j])
                        break
            self._vote = vote

    def _encrypt_vote(self):
        """
        encrypts encoded vote to a "ballot"
        :return: encrypted ballot
        """
        for i in range(len(self._options)):
            self._ballot[i], self._encryptionrandomness[i] = elgamal_enc(self._vote[i], self._publickey, self._p,
                                                                         self._q, self._g, self._h)

    def _encryptionproofs(self):
        """
        generates the nizk proofs for the encryption
        """
        plaintexts = self._options
        for i in range(len(self._options)):
            self._encryptionproof[i] = self._generate_orproof(plaintexts, self._vote[i],
                                                              self._ballot[i],
                                                              self._encryptionrandomness[i],
                                                              or_challenge_generator)

        prod_randomness = sum(self._encryptionrandomness) % self._q
        self._prod_proof = self._generate_encryption_proof(prod_randomness, single_challenge_generator)

    def round2(self, textvote):
        self._database = self._builddatabase()

        self._decrypt_shares()
        self._verify_shares()
        self._calculate_publickey()
        self._textvote = textvote
        self._encode_vote()
        self._encrypt_vote()
        self._encryptionproofs()

        if self._voternr == 3:
            self._ballot[2][1] += 0  # invalidate ballot

        allproofs = []
        for i in range(len(self._options)):
            allproofs.append(self._encryptionproof[i].to_dict())
        prodproof = self._prod_proof.to_dict()

        round2content = {"ballot": self._ballot, "proof": [allproofs, prodproof]}
        sendmessage(self._voternr, "round2", json.dumps(round2content), self._signkey, self._bbserver_pkey)

    def _verify_ballots(self):
        plaintexts = self._options
        for i in self._database.keys():
            for j in range(len(self._options)):
                testedballot = self._database[i]["ballot"][j]
                testedproof = self._database[i]["encryptionproof"][j]
                if not self._verify_orproof(testedballot, plaintexts, testedproof,
                                            or_challenge_generator):
                    print("BADPROOF")
            prodballot = [prod([self._database[i]["ballot"][k][0] for k in range(len(self._options))]),
                          prod([self._database[i]["ballot"][k][1] for k in range(len(self._options))])]
            prodproof = self._database[i]["prodproof"]
            if not self._verify_encryption_proof(prodballot, prod(self._options), prodproof):
                print("BAD PROD PROOF")

    def _sum_ballots(self):
        ballots1 = [None] * len(self._options)
        ballots2 = [None] * len(self._options)
        for j in range(len(self._options)):
            ballots1[j] = prod([self._database[i]["ballot"][j][0] for i in self._database.keys()], self._p)
            ballots2[j] = prod([self._database[i]["ballot"][j][1] for i in self._database.keys()], self._p)

        self._ballotsum = [list(a) for a in zip(ballots1, ballots2)]

    def _partial_dec(self):
        self._partialdec = [pow(self._ballotsum[i][0], self._secret_share, self._p) for i in range(len(self._options))]

    def _proof_partial_dec(self):
        self._decproof = [NIZKProof.generate(self._g, self._ballotsum[i][0], self._secret_share, self._p, self._q,
                                             single_challenge_generator) for i in range(len(self._options))]

    def round3(self, inspector=False):
        self._database = self._builddatabase()
        if inspector:  # only inspectors verify
            self._calculate_publickey()
            self._verify_ballots()
        self._sum_ballots()
        self._decrypt_shares()
        self._combine_share()
        self._partial_dec()
        self._proof_partial_dec()

        allproofs = []
        for i in range(len(self._options)):
            allproofs.append(self._decproof[i].to_dict())

        round3content = {"dec": self._partialdec, "proof": allproofs}
        sendmessage(self._voternr, "round3", json.dumps(round3content), self._signkey, self._bbserver_pkey)

    def _verify_decryptions(self):
        print("decryptionproofs")
        for i in self._database.keys():
            if "partdec" in self._database[i]:
                for j in range(len(self._options)):
                    testeddec = self._database[i]["partdec"][j]
                    testedproof = self._database[i]["partdecproof"][j]
                    if not testedproof.verify(self._g, self._ballotsum[j][0], self._database[i]["pubkeyshare"],
                                              testeddec, self._p, single_challenge_generator):
                        print("BADDECR")
                print("good")

    def _reconstruct(self):
        for j in range(len(self._options)):
            for i in self._database.keys():
                if "partdec" in self._database[i]:
                    self._allpartdec[j].append([int(i), (self._database[i]["partdec"][j])])
            self._recovered_message[j] = recover_message(self._ballotsum[j][1], self._allpartdec[j], self._p,
                                                         self._q)

        print("message", self._recovered_message)

        # todo: can be made faster by iteratively trying to divide by options
        possibilities = [list(a) for a in combinations_with_replacement(self._options, self._n)]
        for j in range(len(self._options)):
            for a in possibilities:
                if prod(a) == self._recovered_message[j]:
                    # purely for aesthetic purpose:
                    c = Counter(self._textifyvote(a))
                    rank = {key: c[key] for key in self._textoptions}
                    print(j+1, rank)

    def _textifyvote(self, vote):
        if self._mode == "ranking":
            textvote = []
            for i in vote:
                for j in range(len(self._textoptions)):
                    if i == self._options[j]:
                        textvote.append(self._textoptions[j])
                        break
            return textvote

    def round4(self):
        self._database = self._builddatabase()
        self._calculate_publickey()
        self._sum_ballots()
        self._inspect_shares()
        self._verify_decryptions()
        self._reconstruct()

    def _generate_encryption_proof(self, randomness, challenge_generator):
        w = randomint(self._q)
        proof = NIZKProof()

        proof.commitment["A"] = pow(self._g, w, self._p)
        proof.commitment["B"] = pow(self._publickey, w, self._p)

        proof.challenge = challenge_generator(proof.commitment)

        proof.response = (w + (randomness * proof.challenge)) % self._q

        return proof

    def _simulate_encryption_proof(self, plaintext, ballot):
        alpha = ballot[0]
        beta = ballot[1]

        proof = NIZKProof()
        proof.challenge = randomint(self._q)
        proof.response = randomint(self._q)

        if self._h is not None:
            plaintext = pow(self._h, plaintext, self._p)  # for additive homomorphism

        beta_div_pt = (beta * mulinv(plaintext, self._p)) % self._p
        proof.commitment["A"] = (mulinv(pow(alpha, proof.challenge, self._p), self._p)
                                 * pow(self._g, proof.response, self._p)) % self._p
        proof.commitment["B"] = (mulinv(pow(beta_div_pt, proof.challenge, self._p), self._p)
                                 * pow(self._publickey, proof.response, self._p)) % self._p

        return proof

    def _generate_orproof(self, plaintexts, real_plaintext, ballot, randomness,
                          challenge_generator):

        proofs = [None for _ in plaintexts]
        real_index = None
        for i in range(len(plaintexts)):
            if plaintexts[i] == real_plaintext:
                real_index = i
                break
        if real_index is None:
            print("invalid plaintext")

        # simulate all but the real proof
        for i in range(len(plaintexts)):
            if i != real_index:
                proofs[i] = self._simulate_encryption_proof(plaintexts[i], ballot)

        def _real_challenge_generator(commitment):
            proofs[real_index] = NIZKProof()
            proofs[real_index].commitment = commitment

            commitments = [p.commitment for p in proofs]
            real_challenge = challenge_generator(commitments)
            for i in range(len(proofs)):
                if i != real_index:
                    real_challenge = real_challenge - proofs[i].challenge

            return real_challenge % self._q

        real_proof = self._generate_encryption_proof(randomness, _real_challenge_generator)
        proofs[real_index] = real_proof
        return ORProof(proofs)

    def _verify_encryption_proof(self, ballot, plaintext, proof):
        """
        Verifies that a ballot is an encryption of the plaintext
        """
        alpha = ballot[0]
        beta = ballot[1]

        if self._h is not None:
            plaintext = pow(self._h, plaintext, self._p)  # for additive homomorphism

        beta_div_pt = (beta * mulinv(plaintext, self._p)) % self._p
        return proof.verify(self._g, self._publickey, alpha, beta_div_pt, self._p)

    def _verify_orproof(self, ballot, plaintexts, proof, challenge_generator):
        """
        check proof for all plaintexts
        """
        if len(plaintexts) != len(proof.proofs):
            print("bad number of proofs")
            return False

        for i in range(len(plaintexts)):
            if not self._verify_encryption_proof(ballot, plaintexts[i], proof.proofs[i]):
                print("bad proof")
                return False
        print("good")

        return (challenge_generator([p.commitment for p in proof.proofs]) == (
                sum([p.challenge for p in proof.proofs]) % self._q))
