import json
import nacl.signing
import nacl.encoding
import nacl.hash
import nacl.exceptions
import base64
import socket
import io


def post(message):
    """
    post a (small) message to a (hopefully immutable) log
    :type message: str
    :param message: message as hexstring
    """
    # using a local .json to imitate a real blockchain

    with open("blockchain.json") as infile:
        blockchainstack = json.load(infile)

    blockchainstack.append(message)

    with open("blockchain.json", "w") as outfile:
        json.dump(blockchainstack, outfile, indent=2, ensure_ascii=False)


def sendmessage(origin, msgtype, content, signkey, bbserver_pkey):
    """
    broadcasting a message
    :param signkey: private key to sign message
    :param origin: voternr (int) or "admin" (string)
    :param msgtype: string
    :param content: string
    """
    message = {"origin": origin, "type": msgtype, "content": content}
    message_str = json.dumps(message).encode("utf-8")
    h = nacl.hash.sha256(message_str)
    signature = signkey.sign(h).signature
    signature_b64 = base64.b64encode(signature).decode("utf-8")
    message["signature"] = signature_b64

    message_enc = (json.dumps(message)).encode("utf-8")

    #####

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "localhost"
    port = 1234
    chunksize = 1024

    s.connect((host, port))
    print('Sending...')
    file = io.BytesIO(message_enc)
    sbuf = file.read(chunksize)
    while sbuf:
        print('Sending...')
        s.send(sbuf)
        sbuf = file.read(chunksize)
    print("Done Sending")
    s.shutdown(socket.SHUT_WR)
    #####
    buf = s.recv(chunksize)
    l = []
    while buf:
        print("Receiving...")
        l.append(buf.decode("utf-8"))
        buf = s.recv(chunksize)
    recmsg = "".join(l)
    print("Done Receiving")
    recsig = base64.b64decode(recmsg)
    h = nacl.hash.sha256(message_enc)
    try:
        bbserver_pkey.verify(h, recsig)
    except (ValueError, TypeError, nacl.exceptions.BadSignatureError):
        print("Invalid Server-Signature!")

    #####
    hash_hex = h.decode("utf-8")
    sig_hex = recsig.hex()
    assert len(hash_hex) == 64, "Hash wrong length!"
    assert len(sig_hex) == 128, "Signature wrong length!"
    post_hex = hash_hex+sig_hex
    post(post_hex)
