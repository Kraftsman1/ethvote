import nacl.signing
import nacl.encoding

admin_key = nacl.signing.SigningKey.generate()
admin_key_enc = admin_key.encode(encoder=nacl.encoding.HexEncoder)
admin_pubkey_enc = admin_key.verify_key.encode(encoder=nacl.encoding.HexEncoder)
print("admin_key_enc = %s" % admin_key_enc)
print("admin_pubkey_enc = %s" % admin_pubkey_enc)
file_out = open("admin_key.pem", "wb")
file_out.write(admin_key_enc)
file_out = open("admin_pubkey.pem", "wb")
file_out.write(admin_pubkey_enc)

bbserver_key = nacl.signing.SigningKey.generate()
bbserver_key_enc = bbserver_key.encode(encoder=nacl.encoding.HexEncoder)
bbserver_pubkey_enc = bbserver_key.verify_key.encode(encoder=nacl.encoding.HexEncoder)
print("bbserver_key_enc = %s" % bbserver_key_enc)
print("bbserver_pubkey_enc = %s" % bbserver_pubkey_enc)
file_out = open("bbserver_key.pem", "wb")
file_out.write(bbserver_key_enc)
file_out = open("bbserver_pubkey.pem", "wb")
file_out.write(bbserver_pubkey_enc)
