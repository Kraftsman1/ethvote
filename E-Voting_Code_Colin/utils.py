import functools
import random
import nacl.hash

randomint = functools.partial(random.SystemRandom().randint, 0)


def multiply_mod(a, b, c):
    return (a * b) % c


def prod(iterable, prime=0):
    total = 1
    if prime == 0:
        for item in iterable:
            total = total * item
        return total
    else:
        for item in iterable:
            total = multiply_mod(total, item, prime)
        return total


def eval_poly(poly, x, prime):
    """
    evaluates polynomial (coefficient tuple) at x using horner's method.
    """
    res = 0
    for coeff in reversed(poly):
        res *= x
        res += coeff
        res %= prime
    return res


def egcd(b, a):
    x0, x1, y0, y1 = 1, 0, 0, 1
    while a != 0:
        q, b, a = b // a, a, b % a
        x0, x1 = x1, x0 - q * x1
        y0, y1 = y1, y0 - q * y1
    return b, x0, y0


def mulinv(b, n):
    g, x, _ = egcd(b, n)
    if g == 1:
        return x % n


def gen_generators(p, q):
    while True:
        g = randomint(p)
        if pow(g, q, p) == 1:
            break
    while True:
        h = randomint(p)
        if pow(h, q, p) == 1 and g != h:
            break
    return g, h


def elgamal_enc(inp, key, p, q, g, h=None):
    a = randomint(q)
    e1 = pow(g, a, p)
    if h is None:
        e2 = (inp * pow(key, a, p)) % p
    else:
        e2 = (pow(h, inp, p) * pow(key, a, p)) % p   # for additive homomorphism
    return [e1, e2], a


def isqrt(x):
    if x < 0:
        raise ValueError("square root not defined for negative numbers")
    n = int(x)
    if n == 0:
        return 0
    a, b = divmod(n.bit_length(), 2)
    x = 2**(a+b)
    while True:
        y = (x + n//x)//2
        if y >= x:
            return x
        x = y


def lagrange_interpolate(x, x_s, y_s, p):
    """
    Find the y-value for the given x, given n (x, y) points;
    k points will define a polynomial of up to kth order
    """
    k = len(x_s)
    assert k == len(set(x_s)), "points must be distinct"

    nums = []
    dens = []
    for i in range(k):
        others = list(x_s)
        cur = others.pop(i)
        nums.append(prod(x - o for o in others))
        dens.append(prod(cur - o for o in others))
    den = prod(dens)
    num = sum([nums[i] * den * y_s[i] % p * mulinv(dens[i], p) % p
               for i in range(k)])
    return (num * mulinv(den, p) % p + p) % p


def recover_secret(shares, prime):
    """
    Recover the secret from share points
    (x,y points on the polynomial)
    """
    if len(shares) < 2:
        raise ValueError("need at least two shares")
    x_s, y_s = [i+1 for i in range(len(shares))], [shares[j] for j in range(len(shares))]
    return lagrange_interpolate(0, x_s, y_s, prime)


def recover_message(cypher2, partdecs, p, q):
    """
    Recover an encrypted message from partial decryptions
    :param cypher2: second part of encrypted message (first part given from partdecs)
    :param partdecs: list of many [i, partdec(i)]
    """
    x_s, y_s = zip(*partdecs)

    k = len(x_s)
    assert k == len(set(x_s)), "points must be distinct"

    lam = []
    for i in range(k):
        others = list(x_s)
        cur = others.pop(i)
        lam.append((prod((o for o in others), q) * mulinv(prod((o - cur for o in others), q), q)) % q)

    combdec = prod([pow(y_s[i], lam[i], p) for i in range(k)], p)
    div = (cypher2 * mulinv(combdec, p)) % p

    return div


class NIZKProof(object):
    """
    Non-Interactive Zero Knowledge Proof
    """
    def __init__(self):
        self.commitment = {"A": None, "B": None}
        self.challenge = None
        self.response = None

    @classmethod
    def from_dict(cls, d):
        p = cls()
        p.commitment = {"A": int(d["commitment"]["A"]), "B": int(d["commitment"]["B"])}
        p.challenge = int(d["challenge"])
        p.response = int(d["response"])
        return p

    def to_dict(self):
        return {
            "commitment": {"A": str(self.commitment["A"]), "B": str(self.commitment["B"])},
            "challenge": str(self.challenge),
            "response": str(self.response)
        }

    @classmethod
    def generate(cls, g, h, x, p, q, challenge_generator):
        """
        generate proof that [x,y] = [g^t, h^t]
        """
        w = randomint(q)
        proof = cls()

        proof.commitment["A"] = pow(g, w, p)
        proof.commitment["B"] = pow(h, w, p)

        proof.challenge = challenge_generator(proof.commitment)
        proof.response = (w + (x * proof.challenge)) % q
        return proof

    def verify(self, g, h, x, y, p, challenge_generator=None):
        """
        Verify the proof of [x,y] = [g^t, h^t]
        """
        # g^response = A * x^challenge
        verify_a = (pow(g, self.response, p) == ((pow(x, self.challenge, p) * self.commitment["A"]) % p))
        # h^response = B * y^challenge
        verify_b = (pow(h, self.response, p) == ((pow(y, self.challenge, p) * self.commitment["B"]) % p))

        verify_c = True
        if challenge_generator:
            verify_c = (self.challenge == challenge_generator(self.commitment))

        return verify_a and verify_b and verify_c


class ORProof:
    """
    OR-combination of NIZKPs
    """
    def __init__(self, proofs=None):
        self.proofs = proofs

    @classmethod
    def from_dict(cls, d):
        dp = cls()
        dp.proofs = [NIZKProof.from_dict(p) for p in d]
        return dp

    def to_dict(self):
        return [p.to_dict() for p in self.proofs]


def or_challenge_generator(commitments):
    array_to_hash = []
    for commitment in commitments:
        array_to_hash.append(str(commitment["A"]))
        array_to_hash.append(str(commitment["B"]))

    string_to_hash = ",".join(array_to_hash)
    return int(nacl.hash.sha256(string_to_hash.encode("utf-8")).decode("utf-8"), 16)


def single_challenge_generator(commitment):
    return or_challenge_generator([commitment])
