import {VoteHelperService} from '../../../app/services/vote/voteHelper.service';
import {VoteHelperServiceImpl} from '../../../app/services/vote/voteHelperImpl.service';
import {CryptoService} from '../../../app/services/crypto/crypto.service';
import {CryptoServiceImpl} from '../../../app/services/crypto/cyptoImpl.service';
import {MathService} from '../../../app/services/math/math.service';
import {MathServiceImpl} from '../../../app/services/math/mathImpl.service';
import {DataForKeyCalculation} from '../../../app/models/dataForKeyCalculation/dataForKeyCalculation.model';
import {VoteNIZKProof} from '../../../app/models/voteNIZKProof/voteNIZKProofs';
import {SingleVoteNIZKProof} from '../../../app/models/voteNIZKProof/singleVoteNIZKProof';

describe('VoteHelperServiceImpl', () => {

    const publicKey =
`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAokiRL660wU6tWddvTv0F
IUmo8Ka7gU9euLm0zTsRN+HYMKKzj3Pw6JzSQt8oeTHJ3sERARuGuScGZMNraXa0
/wdoAUwcNYKP9EfeK+b8nEiAnGTrGMYOyKInNh3hxqFPOx8W0lbYHN2rPmpYjJHR
HFBWkro02yoA9RDZ1IjHdleDPZzDp/7JstQ0ZX+DAp5Tr/5xZeKAx4O7dotSbVfo
OZ9Y52YvHaA37oz4oZYMlWhbT4TPD5As5julwRiqTwYVjq00bPheGkK6AZN2j0ZG
FB3aQZSsF6DaUlYPpUzRY/bevmgiIH/vOgizddoHqdQkEJwDMGBxX91M84xIs8HQ
cwIDAQAB
-----END PUBLIC KEY-----`;

    const privateKey =
`-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAokiRL660wU6tWddvTv0FIUmo8Ka7gU9euLm0zTsRN+HYMKKz
j3Pw6JzSQt8oeTHJ3sERARuGuScGZMNraXa0/wdoAUwcNYKP9EfeK+b8nEiAnGTr
GMYOyKInNh3hxqFPOx8W0lbYHN2rPmpYjJHRHFBWkro02yoA9RDZ1IjHdleDPZzD
p/7JstQ0ZX+DAp5Tr/5xZeKAx4O7dotSbVfoOZ9Y52YvHaA37oz4oZYMlWhbT4TP
D5As5julwRiqTwYVjq00bPheGkK6AZN2j0ZGFB3aQZSsF6DaUlYPpUzRY/bevmgi
IH/vOgizddoHqdQkEJwDMGBxX91M84xIs8HQcwIDAQABAoIBAFLi34zMHbRR/AZU
G5zrbHBZD/mCH2jnYgjViPv9vEifiG0m3LFbVWNpnZbcJe7ouCOW1pmLNp9gyEo0
6aqfH8jWPo/TEOFfwyjQgDBKNSGZgl7eyJkJp8lf0Mk6L/PktfQLZ/ucsZrkuS8S
4BFD9NsSSXV9t7ts07pXlfqQtdfB/wS2GUXZJx3hgjDJ7b02Yo1MEGLMDvCNKhdC
9qqlYk2Ao1bmEMDbEH8g8vDNzR+7kWMyeSJWeJXgFI4NEkcrIH+bq32rtXp6Fsrp
TaBYw1REsHUxZZQQldXUXYgNLJKkFhSfXM7Ly21m+s/Noxg0u1uKTqN88VY0CRMX
goSKZFkCgYEA5wZl94PE6oORNhF8wxSN1S7NRkDmRywrRjdMfJaC9YuUmpUkSxkE
sntwN+73W5hONGkDs/PjwJM13QPEWaHVKo+dwwNq62lYGGXfQGSFqfXMHglAvg1d
IOE4i4nBWVS1unL2XalrMW0u4CHKFZE/5n1/+B1O2c0DvYGgSBHrSRcCgYEAs9PA
Yl7l3fAP1fPMbg64YkVVnwEg1ITl4v/lo0QosYcdRJuPWB1TvCGNFdoZwVIUuJKj
ThVKXopAQ8djp4hTKQ+/My2lAp3Unk/bw0Birh0kL6TUm8KGhbsZntd3B2QuEeVd
H1BiqiqYrbemiOJhHBEerDvX2fL5YaAI2oG5lQUCgYEA0OeqQN14rQ93ucqqZMUr
VADUQpjhIq7AGuhaU7bOiTdEk1wVy4q+Ap84ZHvEVaEt5pqnbIw4AKWdaVPEs8Bx
P+FKKVPn1SQXM0i7TkEX1Q6dGvQHBVSUR+0g8p0Ip7y/7++Q9DUE51bxsl8bm9ES
VVxa4YOvD6ghn7WduE3r0tUCgYEAlDQkDe6gC7VZl78B8IJ5O6Rg2FaZtAyDWf4E
R/ZYg1cTrAN4s+oXpKkSGbxSFZ2MLkHMDQJG9HuEU1f0rZIQ13p+3bL0l590J7Ux
LU3XBiBGPm3eOzCqpXR5vQtesdzCi0U45iHd7tiEVEdTHOVVm0gQu2nmj11MYkT+
hvb69/ECgYAvymBHHPH0u7PSIpdGgUsmithc67opqSGpzKf6qGgE6AYT0o0TIRKm
frhZjJ+A+MGc8BzVSc6hGR2hky4AFoUyq3+v4my9xhXylcaFcBeT7iQoUd/b+cqg
E3R84jufzBh8Oh42VFPPnGksz/jLOuQ3vPVrH2LJtLJRIiaJN07wLg==
-----END RSA PRIVATE KEY-----`;

    const largeValue =
`142523207285047815321666100378262646521230142536476492122320288\
0664212750054579888097351170020013861297351957720189541182577647\
9815170056051564598499481095316046489562704145748236084746356160\
6667017537259037297148345535633099042376246995225586572301938712\
020429693617046579518142924241090629443843525190537473`;

    let cryptoService: CryptoService;
    let mathService: MathService;
    let service: VoteHelperService;

    beforeEach(() => {
        cryptoService = new CryptoServiceImpl();
        mathService = new MathServiceImpl();
        service = new VoteHelperServiceImpl(cryptoService, mathService);
    });

    it('encryption and decryption of point works', () => {
        const encrypted = service.encryptPolynomPoint(largeValue, publicKey);
        const decrypted = service.decryptPolynomPoint(encrypted, privateKey);
        expect(decrypted).toBe(largeValue);
    });

});
