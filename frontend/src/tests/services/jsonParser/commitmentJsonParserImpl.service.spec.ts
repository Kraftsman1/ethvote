import {Maybe} from '../../../app/utils/maybe.utils';
import {CommitmentJsonParserImpl} from '../../../app/services/jsonParser/commitmentJsonParserImpl.service';
import {Commitments} from '../../../app/models/commitments/commitments.model';

describe('CommitmentJsonParserImpl', () => {

    let service: CommitmentJsonParserImpl;
    // const validObject = {numberOfCommitments:3,commitments:[{commitment:'c1',coefficientNumber:0},{commitment:'c2',coefficientNumber:1},{commitment:'c3',coefficientNumber:2}]}
    const validObject = {
        commitments:
            [
                {
                    fromVoterPosition: 0,
                    fromVoterId: 2,
                    numberOfCommitments: 3,
                    commitmentsFromVoter:
                        [
                            {commitment: 'c0', coefficientNumber: 0, signature: 'sign0'},
                            {commitment: 'c1', coefficientNumber: 1, signature: 'sign1'},
                            {commitment: 'c2', coefficientNumber: 2, signature: 'sign2'},
                        ],
                    fromVoterName: 'yoda'
                },
                {
                    fromVoterPosition: 1,
                    fromVoterId: 42,
                    numberOfCommitments: 3,
                    commitmentsFromVoter:
                        [
                            {commitment: 'c3', coefficientNumber: 0, signature: 'sign3'},
                            {commitment: 'c4', coefficientNumber: 1, signature: 'sign4'},
                            {commitment: 'c5', coefficientNumber: 2, signature: 'sign5'},
                        ],
                    fromVoterName: 'luke'
                },
                {
                    fromVoterPosition: 2,
                    fromVoterId: 27,
                    numberOfCommitments: 3,
                    commitmentsFromVoter:
                        [
                            {commitment: 'c6', coefficientNumber: 0, signature: 'sign6'},
                            {commitment: 'c7', coefficientNumber: 1, signature: 'sign7'},
                            {commitment: 'c8', coefficientNumber: 2, signature: 'sign8'},
                        ],
                    fromVoterName: 'lea'
                },
            ]
    };


    beforeEach(() => {
        service = new CommitmentJsonParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<Commitments> = service.parseJson(jsonString);
        expect(parsedMaybeObject.getData().getCommitments().length).toBe(3);

        expect(parsedMaybeObject.getData().getCommitments()[0].getNumberOfCommitments()).toBe(3);
        expect(parsedMaybeObject.getData().getCommitments()[1].getNumberOfCommitments()).toBe(3);
        expect(parsedMaybeObject.getData().getCommitments()[2].getNumberOfCommitments()).toBe(3);

        expect(parsedMaybeObject.getData().getCommitments()[0].getCommitments()[0].getCommitment()).toBe('c0');
        expect(parsedMaybeObject.getData().getCommitments()[0].getCommitments()[1].getCommitment()).toBe('c1');
        expect(parsedMaybeObject.getData().getCommitments()[0].getCommitments()[2].getCommitment()).toBe('c2');
        expect(parsedMaybeObject.getData().getCommitments()[1].getCommitments()[0].getCommitment()).toBe('c3');
        expect(parsedMaybeObject.getData().getCommitments()[1].getCommitments()[1].getCommitment()).toBe('c4');
        expect(parsedMaybeObject.getData().getCommitments()[1].getCommitments()[2].getCommitment()).toBe('c5');
        expect(parsedMaybeObject.getData().getCommitments()[2].getCommitments()[0].getCommitment()).toBe('c6');
        expect(parsedMaybeObject.getData().getCommitments()[2].getCommitments()[1].getCommitment()).toBe('c7');
        expect(parsedMaybeObject.getData().getCommitments()[2].getCommitments()[2].getCommitment()).toBe('c8');

        expect(parsedMaybeObject.getData().getCommitments()[0].getCommitments()[0].getSignature()).toBe('sign0');
        expect(parsedMaybeObject.getData().getCommitments()[0].getCommitments()[1].getSignature()).toBe('sign1');
        expect(parsedMaybeObject.getData().getCommitments()[0].getCommitments()[2].getSignature()).toBe('sign2');
        expect(parsedMaybeObject.getData().getCommitments()[1].getCommitments()[0].getSignature()).toBe('sign3');
        expect(parsedMaybeObject.getData().getCommitments()[1].getCommitments()[1].getSignature()).toBe('sign4');
        expect(parsedMaybeObject.getData().getCommitments()[1].getCommitments()[2].getSignature()).toBe('sign5');
        expect(parsedMaybeObject.getData().getCommitments()[2].getCommitments()[0].getSignature()).toBe('sign6');
        expect(parsedMaybeObject.getData().getCommitments()[2].getCommitments()[1].getSignature()).toBe('sign7');
        expect(parsedMaybeObject.getData().getCommitments()[2].getCommitments()[2].getSignature()).toBe('sign8');

        expect(parsedMaybeObject.getData().getCommitments()[0].getFromVoterId()).toBe(2);
        expect(parsedMaybeObject.getData().getCommitments()[1].getFromVoterId()).toBe(42);
        expect(parsedMaybeObject.getData().getCommitments()[2].getFromVoterId()).toBe(27);

        expect(parsedMaybeObject.getData().getCommitments()[0].getFromVoterPosition()).toBe(0);
        expect(parsedMaybeObject.getData().getCommitments()[1].getFromVoterPosition()).toBe(1);
        expect(parsedMaybeObject.getData().getCommitments()[2].getFromVoterPosition()).toBe(2);

        expect(parsedMaybeObject.getData().getCommitments()[0].getFromVoterName()).toBe('yoda');
        expect(parsedMaybeObject.getData().getCommitments()[1].getFromVoterName()).toBe('luke');
        expect(parsedMaybeObject.getData().getCommitments()[2].getFromVoterName()).toBe('lea');

    });

});
