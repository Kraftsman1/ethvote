import {VotingSummaryJsonParser} from '../../../app/services/jsonParser/votingSummaryJsonParser.service';
import {VotingSummaryJsonParserImpl} from '../../../app/services/jsonParser/votingSummaryJsonParserImpl.service';
import {VotingSummary} from '../../../app/models/votingSummary/votingSummary.json.model';
import {Maybe} from '../../../app/utils/maybe.utils';

describe('VotingSummaryJsonParserImpl', () => {

    let service: VotingSummaryJsonParser;
    let validJsonObject;

    beforeEach(() => {
        service = new VotingSummaryJsonParserImpl();

        validJsonObject = {
            eligibleVoters: [
                {
                    name: '***',
                    publicKey: 'key0',
                    position: 0,
                    id: 10,
                    isTrustee: true,
                    canVote: true
                },
                {
                    name: '***',
                    publicKey: 'key1',
                    position: 1,
                    id: 11,
                    isTrustee: true,
                    canVote: false
                },
                {
                    name: '***',
                    publicKey: 'key2',
                    position: 2,
                    id: 12,
                    isTrustee: false,
                    canVote: true
                }
            ],
            decryptionPhase: [
                {
                    data: {
                        signature: 'sign0',
                        fromVoterPosition: 0,
                        fromVoterId: 10,
                        decryptionFactor: 'd0',
                        fromVoterName: '***'
                    },
                    proof: {
                        a: 'a0',
                        b: 'b0',
                        r: 'r0',
                        signature: 'sign0'
                    }
                },
                {
                    data: {
                        signature: 'sign1',
                        fromVoterPosition: 1,
                        fromVoterId: 11,
                        decryptionFactor: 'd1',
                        fromVoterName: '***'
                    },
                    proof: {
                        a: 'a1',
                        b: 'b1',
                        r: 'r1',
                        signature: 'sign1'
                    }
                },
                {
                    data: {
                        signature: 'sign2',
                        fromVoterPosition: 2,
                        fromVoterId: 12,
                        decryptionFactor: 'd2',
                        fromVoterName: '***'
                    },
                    proof: {
                        a: 'a2',
                        b: 'b2',
                        r: 'r2',
                        signature: 'sign2'
                    }
                }
            ],
            meta: {
                calculationParameters: {
                    primeQ: 'primeQ',
                    primeP: 'primeP',
                    generatorG: 'generatorG'
                },
                options: [
                    {
                        optionPrimeNumber: 2,
                        optionName: 'o0'
                    },
                    {
                        optionPrimeNumber: 3,
                        optionName: 'o1'
                    },
                    {
                        optionPrimeNumber: 5,
                        optionName: 'o2'
                    }
                ],
                info: {
                    securityThreshold: 3,
                    description: 'fantastic description',
                    title: 'fantastic title'
                }
            },
            keyGenerationPhase: [
                {
                    commitments: [
                        {
                            signature: 'commSign0',
                            commitment: 'c0',
                            coefficientNumber: 0
                        },
                        {
                            signature: 'commSign1',
                            commitment: 'c1',
                            coefficientNumber: 1
                        }
                    ],
                    fromVoterId: 10,
                    secretShares: [
                        {
                            secretShare: [
                                's00',
                                's01'
                            ],
                            signature: 'secretSign0',
                            fromVoterId: 10,
                            fromVoterName: '***'
                        },
                        {
                            secretShare: [
                                's10',
                                's11'
                            ],
                            signature: 'secretSign1',
                            fromVoterId: 11,
                            fromVoterName: '***'
                        },
                        {
                            secretShare: [
                                's20',
                                's21'
                            ],
                            signature: 'secretSign2',
                            fromVoterId: 12,
                            fromVoterName: '***'
                        }
                    ]
                },
                {
                    commitments: [
                        {
                            signature: 'commSign3',
                            commitment: 'c3',
                            coefficientNumber: 0
                        },
                        {
                            signature: 'commSign4',
                            commitment: 'c4',
                            coefficientNumber: 1
                        }
                    ],
                    fromVoterId: 11,
                    secretShares: [
                        {
                            secretShare: [
                                's30',
                                's31'
                            ],
                            signature: 'secretSign3',
                            fromVoterId: 10,
                            fromVoterName: '***'
                        },
                        {
                            secretShare: [
                                's40',
                                's41'
                            ],
                            signature: 'secretSign4',
                            fromVoterId: 11,
                            fromVoterName: '***'
                        },
                        {
                            secretShare: [
                                's50',
                                's51'
                            ],
                            signature: 'secretSign5',
                            fromVoterId: 12,
                            fromVoterName: '***'
                        }
                    ]
                },
                {
                    commitments: [
                        {
                            signature: 'commSign6',
                            commitment: 'c6',
                            coefficientNumber: 0
                        },
                        {
                            signature: 'commSign7',
                            commitment: 'c7',
                            coefficientNumber: 1
                        }
                    ],
                    fromVoterId: 12,
                    secretShares: [
                        {
                            secretShare: [
                                's60',
                                's61'
                            ],
                            signature: 'secretSign6',
                            fromVoterId: 10,
                            fromVoterName: '***'
                        },
                        {
                            secretShare: [
                                's70',
                                's71'
                            ],
                            signature: 'secretSign7',
                            fromVoterId: 11,
                            fromVoterName: '***'
                        },
                        {
                            secretShare: [
                                's80',
                                's81'
                            ],
                            signature: 'secretSign8',
                            fromVoterId: 12,
                            fromVoterName: '***'
                        }
                    ]
                }
            ],
            votingPhase: [
                {
                    data: {
                        signature: 'sign0',
                        alpha: 'alpha0',
                        fromVoterId: 10,
                        beta: 'beta0',
                        fromVoterName: '***'
                    },
                    proofs: [
                        {
                            a: 'a0',
                            b: 'b0',
                            r: 'r0',
                            c: 'c0',
                            messageIndex: 0,
                            signature: 'sign0'
                        },
                        {
                            a: 'a1',
                            b: 'b1',
                            r: 'r1',
                            c: 'c1',
                            messageIndex: 1,
                            signature: 'sign1'
                        },
                        {
                            a: 'a2',
                            b: 'b2',
                            r: 'r2',
                            c: 'c2',
                            messageIndex: 2,
                            signature: 'sign2'
                        }
                    ]
                },
                {
                    data: {
                        signature: 'sign1',
                        alpha: 'alpha1',
                        fromVoterId: 11,
                        beta: 'beta1',
                        fromVoterName: '***'
                    },
                    proofs: [
                        {
                            a: 'a3',
                            b: 'b3',
                            r: 'r3',
                            c: 'c3',
                            messageIndex: 0,
                            signature: 'sign3'
                        },
                        {
                            a: 'a4',
                            b: 'b4',
                            r: 'r4',
                            c: 'c4',
                            messageIndex: 1,
                            signature: 'sign4'
                        },
                        {
                            a: 'a5',
                            b: 'b5',
                            r: 'r5',
                            c: 'c5',
                            messageIndex: 2,
                            signature: 'sign5'
                        }
                    ]
                },
                {
                    data: {
                        signature: 'sign2',
                        alpha: 'alpha2',
                        fromVoterId: 12,
                        beta: 'beta2',
                        fromVoterName: '***'
                    },
                    proofs: [
                        {
                            a: 'a6',
                            b: 'b6',
                            r: 'r6',
                            c: 'c6',
                            messageIndex: 0,
                            signature: 'sign6'
                        },
                        {
                            a: 'a7',
                            b: 'b7',
                            r: 'r7',
                            c: 'c7',
                            messageIndex: 1,
                            signature: 'sign7'
                        },
                        {
                            a: 'a8',
                            b: 'b8',
                            r: 'r8',
                            c: 'c8',
                            messageIndex: 2,
                            signature: 'sign8'
                        }
                    ]
                }
            ]
        };
    });

    it('should be able to parse eligible voters', () => {
        const parsedMaybeObject: Maybe<VotingSummary> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const summary: VotingSummary = parsedMaybeObject.getData();

        expect(summary.getEligibleVoters().length).toBe(3);

        expect(summary.getEligibleVoters()[0].getPublicKey()).toBe('key0');
        expect(summary.getEligibleVoters()[0].getPosition()).toBe(0);
        expect(summary.getEligibleVoters()[0].getName()).toBe('***');
        expect(summary.getEligibleVoters()[0].getIsTrustee()).toBeTruthy();
        expect(summary.getEligibleVoters()[0].getCanVote()).toBeTruthy();
        expect(summary.getEligibleVoters()[0].getId()).toBe(10);

        expect(summary.getEligibleVoters()[1].getPublicKey()).toBe('key1');
        expect(summary.getEligibleVoters()[1].getPosition()).toBe(1);
        expect(summary.getEligibleVoters()[1].getName()).toBe('***');
        expect(summary.getEligibleVoters()[1].getIsTrustee()).toBeTruthy();
        expect(summary.getEligibleVoters()[1].getCanVote()).toBeFalsy();
        expect(summary.getEligibleVoters()[1].getId()).toBe(11);

        expect(summary.getEligibleVoters()[2].getPublicKey()).toBe('key2');
        expect(summary.getEligibleVoters()[2].getPosition()).toBe(2);
        expect(summary.getEligibleVoters()[2].getName()).toBe('***');
        expect(summary.getEligibleVoters()[2].getIsTrustee()).toBeFalsy();
        expect(summary.getEligibleVoters()[2].getCanVote()).toBeTruthy();
        expect(summary.getEligibleVoters()[2].getId()).toBe(12);
    });

    it('should be able to parse decrypion data', () => {
        const parsedMaybeObject: Maybe<VotingSummary> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const summary: VotingSummary = parsedMaybeObject.getData();

        expect(summary.getDecryptionFactors().length).toBe(3);

        expect(summary.getDecryptionFactors()[0].getDecryptionFactor().getDecryptionFactor()).toBe('d0');
        expect(summary.getDecryptionFactors()[0].getDecryptionFactor().getFromVoterId()).toBe(10);
        expect(summary.getDecryptionFactors()[0].getDecryptionFactor().getFromVoterPosition()).toBe(0);
        expect(summary.getDecryptionFactors()[0].getDecryptionFactor().getFromVoterName()).toBe('***');
        expect(summary.getDecryptionFactors()[0].getDecryptionFactor().getSignature()).toBe('sign0');

        expect(summary.getDecryptionFactors()[0].getProof().getA()).toBe('a0');
        expect(summary.getDecryptionFactors()[0].getProof().getB()).toBe('b0');
        expect(summary.getDecryptionFactors()[0].getProof().getR()).toBe('r0');
        expect(summary.getDecryptionFactors()[0].getProof().getSignature()).toBe('sign0');

        expect(summary.getDecryptionFactors()[1].getDecryptionFactor().getDecryptionFactor()).toBe('d1');
        expect(summary.getDecryptionFactors()[1].getDecryptionFactor().getFromVoterId()).toBe(11);
        expect(summary.getDecryptionFactors()[1].getDecryptionFactor().getFromVoterPosition()).toBe(1);
        expect(summary.getDecryptionFactors()[1].getDecryptionFactor().getFromVoterName()).toBe('***');
        expect(summary.getDecryptionFactors()[1].getDecryptionFactor().getSignature()).toBe('sign1');

        expect(summary.getDecryptionFactors()[1].getProof().getA()).toBe('a1');
        expect(summary.getDecryptionFactors()[1].getProof().getB()).toBe('b1');
        expect(summary.getDecryptionFactors()[1].getProof().getR()).toBe('r1');
        expect(summary.getDecryptionFactors()[1].getProof().getSignature()).toBe('sign1');

        expect(summary.getDecryptionFactors()[2].getDecryptionFactor().getDecryptionFactor()).toBe('d2');
        expect(summary.getDecryptionFactors()[2].getDecryptionFactor().getFromVoterId()).toBe(12);
        expect(summary.getDecryptionFactors()[2].getDecryptionFactor().getFromVoterPosition()).toBe(2);
        expect(summary.getDecryptionFactors()[2].getDecryptionFactor().getFromVoterName()).toBe('***');
        expect(summary.getDecryptionFactors()[2].getDecryptionFactor().getSignature()).toBe('sign2');

        expect(summary.getDecryptionFactors()[2].getProof().getA()).toBe('a2');
        expect(summary.getDecryptionFactors()[2].getProof().getB()).toBe('b2');
        expect(summary.getDecryptionFactors()[2].getProof().getR()).toBe('r2');
        expect(summary.getDecryptionFactors()[2].getProof().getSignature()).toBe('sign2');
    });

    it('should be able to parse voting meta data', () => {
        const parsedMaybeObject: Maybe<VotingSummary> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const summary: VotingSummary = parsedMaybeObject.getData();

        expect(summary.getMetaData().getInfo().getTitle()).toBe('fantastic title');
        expect(summary.getMetaData().getInfo().getDescription()).toBe('fantastic description');
        expect(summary.getMetaData().getInfo().getSecurityThreshold()).toBe(3);

        expect(summary.getMetaData().getOptions()[0].getOptionName()).toBe('o0');
        expect(summary.getMetaData().getOptions()[0].getOptionPrimeNumber()).toBe(2);

        expect(summary.getMetaData().getOptions()[1].getOptionName()).toBe('o1');
        expect(summary.getMetaData().getOptions()[1].getOptionPrimeNumber()).toBe(3);

        expect(summary.getMetaData().getOptions()[2].getOptionName()).toBe('o2');
        expect(summary.getMetaData().getOptions()[2].getOptionPrimeNumber()).toBe(5);

        expect(summary.getMetaData().getVotingCalculationParameters().getPrimeP()).toBe('primeP');
        expect(summary.getMetaData().getVotingCalculationParameters().getPrimeQ()).toBe('primeQ');
        expect(summary.getMetaData().getVotingCalculationParameters().getGeneratorG()).toBe('generatorG');
    });

    it('should be able to parse key generation data', () => {
        const parsedMaybeObject: Maybe<VotingSummary> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const summary: VotingSummary = parsedMaybeObject.getData();

        expect(summary.getKeyGenerationPhase()[0].getFromVoterId()).toBe(10);

        expect(summary.getKeyGenerationPhase()[0].getCommitments()[0].getSignature()).toBe('commSign0');
        expect(summary.getKeyGenerationPhase()[0].getCommitments()[0].getCommitment()).toBe('c0');
        expect(summary.getKeyGenerationPhase()[0].getCommitments()[0].getCoefficientNumber()).toBe(0);

        expect(summary.getKeyGenerationPhase()[0].getCommitments()[1].getSignature()).toBe('commSign1');
        expect(summary.getKeyGenerationPhase()[0].getCommitments()[1].getCommitment()).toBe('c1');
        expect(summary.getKeyGenerationPhase()[0].getCommitments()[1].getCoefficientNumber()).toBe(1);

        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[0].getFromVoterId()).toBe(10);
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[0].getFromVoterName()).toBe('***');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[0].getSignature()).toBe('secretSign0');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[0].getSecretShare()[0]).toBe('s00');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[0].getSecretShare()[1]).toBe('s01');

        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[1].getFromVoterId()).toBe(11);
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[1].getFromVoterName()).toBe('***');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[1].getSignature()).toBe('secretSign1');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[1].getSecretShare()[0]).toBe('s10');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[1].getSecretShare()[1]).toBe('s11');

        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[2].getFromVoterId()).toBe(12);
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[2].getFromVoterName()).toBe('***');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[2].getSignature()).toBe('secretSign2');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[2].getSecretShare()[0]).toBe('s20');
        expect(summary.getKeyGenerationPhase()[0].getSecretShares()[2].getSecretShare()[1]).toBe('s21');
    });

    it('should be able to parse key generation data', () => {
        const parsedMaybeObject: Maybe<VotingSummary> = service.parseJson(JSON.stringify(validJsonObject));
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();

        const summary: VotingSummary = parsedMaybeObject.getData();

        expect(summary.getVotePhase()[0].getVote().getAlpha()).toBe('alpha0');
        expect(summary.getVotePhase()[0].getVote().getBeta()).toBe('beta0');
        expect(summary.getVotePhase()[0].getVote().getFromVoterId()).toBe(10);
        expect(summary.getVotePhase()[0].getVote().getFromVoterName()).toBe('***');
        expect(summary.getVotePhase()[0].getVote().getSignature()).toBe('sign0');

        expect(summary.getVotePhase()[0].getProofs()[0].getMessageIndex()).toBe(0);
        expect(summary.getVotePhase()[0].getProofs()[0].getA()).toBe('a0');
        expect(summary.getVotePhase()[0].getProofs()[0].getB()).toBe('b0');
        expect(summary.getVotePhase()[0].getProofs()[0].getC()).toBe('c0');
        expect(summary.getVotePhase()[0].getProofs()[0].getR()).toBe('r0');
        expect(summary.getVotePhase()[0].getProofs()[0].getSignature()).toBe('sign0');

        expect(summary.getVotePhase()[0].getProofs()[1].getMessageIndex()).toBe(1);
        expect(summary.getVotePhase()[0].getProofs()[1].getA()).toBe('a1');
        expect(summary.getVotePhase()[0].getProofs()[1].getB()).toBe('b1');
        expect(summary.getVotePhase()[0].getProofs()[1].getC()).toBe('c1');
        expect(summary.getVotePhase()[0].getProofs()[1].getR()).toBe('r1');
        expect(summary.getVotePhase()[0].getProofs()[1].getSignature()).toBe('sign1');

        expect(summary.getVotePhase()[0].getProofs()[2].getMessageIndex()).toBe(2);
        expect(summary.getVotePhase()[0].getProofs()[2].getA()).toBe('a2');
        expect(summary.getVotePhase()[0].getProofs()[2].getB()).toBe('b2');
        expect(summary.getVotePhase()[0].getProofs()[2].getC()).toBe('c2');
        expect(summary.getVotePhase()[0].getProofs()[2].getR()).toBe('r2');
        expect(summary.getVotePhase()[0].getProofs()[2].getSignature()).toBe('sign2');
    });

});
