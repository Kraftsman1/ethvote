import {Maybe} from '../../../app/utils/maybe.utils';
import {VotesJsonParserImpl} from '../../../app/services/jsonParser/votesJsonParserImpl.service';
import {Votes} from '../../../app/models/vote/votes.json.model';

describe('VotesJsonParserServiceImpl', () => {

    let service: VotesJsonParserImpl;
    const validObject = {
        votes:
            [
                {
                    fromVoterId: 1,
                    fromVoterName: 'Yoda',
                    alpha: 'a1',
                    beta: 'b1',
                    signature: 'sign1'
                },
                {
                    fromVoterId: 2,
                    fromVoterName: 'Luke',
                    alpha: 'a2',
                    beta: 'b2',
                    signature: 'sign2'
                }
            ]
    };


    beforeEach(() => {
        service = new VotesJsonParserImpl();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<Votes> = service.parseJson(jsonString);
        expect(parsedMaybeObject.getData().getVotes().length).toBe(2);

        expect(parsedMaybeObject.getData().getVotes()[0].getFromVoterId()).toBe(1);
        expect(parsedMaybeObject.getData().getVotes()[0].getFromVoterName()).toBe('Yoda');
        expect(parsedMaybeObject.getData().getVotes()[0].getAlpha()).toBe('a1');
        expect(parsedMaybeObject.getData().getVotes()[0].getBeta()).toBe('b1');
        expect(parsedMaybeObject.getData().getVotes()[0].getSignature()).toBe('sign1');

        expect(parsedMaybeObject.getData().getVotes()[1].getFromVoterId()).toBe(2);
        expect(parsedMaybeObject.getData().getVotes()[1].getFromVoterName()).toBe('Luke');
        expect(parsedMaybeObject.getData().getVotes()[1].getAlpha()).toBe('a2');
        expect(parsedMaybeObject.getData().getVotes()[1].getBeta()).toBe('b2');
        expect(parsedMaybeObject.getData().getVotes()[1].getSignature()).toBe('sign2');

    });
});
