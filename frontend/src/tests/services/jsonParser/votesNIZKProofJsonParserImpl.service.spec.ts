import {VotesNIZKProofJsonParserImpl} from '../../../app/services/jsonParser/votesNIZKProofJsonParserImpl.service';
import {AllVoteNIZKProofs} from '../../../app/models/voteNIZKProof/allVoteNIZKProofs';
import {Maybe} from '../../../app/utils/maybe.utils';

describe('VotesNIZKProofJsonParserServiceImpl', () => {

    let service: VotesNIZKProofJsonParserImpl;
    const validObject = {
        allProofs:
            [
                {
                    proofs:
                        [
                            {
                                a: 'a1',
                                b: 'b1',
                                r: 'r1',
                                c: 'c1',
                                messageIndex: 0,
                                signature: 'sign0'
                            },
                            {
                                a: 'a2',
                                b: 'b2',
                                r: 'r2',
                                c: 'c2',
                                messageIndex: 1,
                                signature: 'sign1'
                            }
                        ],
                    fromVoterId: 1,
                    fromVoterName: 'v1'
                },
                {
                    proofs:
                        [
                            {
                                a: 'a3',
                                b: 'b3',
                                r: 'r3',
                                c: 'c3',
                                messageIndex: 0,
                                signature: 'sign2'
                            }
                        ],
                    fromVoterId: 2,
                    fromVoterName: 'v2'
                },
            ]
    };


    beforeEach(() => {
        service = new VotesNIZKProofJsonParserImpl();
    });

    it('invalid returns empty', () => {
        const jsonString = 'invalid';

        const parsedMaybeObject: Maybe<AllVoteNIZKProofs> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeTruthy();
    });

    it('should be able to parse valid json object', () => {
        const jsonString = JSON.stringify(validObject);

        const parsedMaybeObject: Maybe<AllVoteNIZKProofs> = service.parseJson(jsonString);
        expect(parsedMaybeObject.isEmpty()).toBeFalsy();
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs().length).toBe(2);

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getFromVoterId()).toBe(1);
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getFromVoterName()).toBe('v1');

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs().length).toBe(2);

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[0].getMessageIndex()).toBe(0);
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[0].getA()).toBe('a1');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[0].getB()).toBe('b1');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[0].getC()).toBe('c1');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[0].getR()).toBe('r1');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[0].getSignature()).toBe('sign0');

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[1].getMessageIndex()).toBe(1);
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[1].getA()).toBe('a2');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[1].getB()).toBe('b2');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[1].getC()).toBe('c2');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[1].getR()).toBe('r2');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[0].getZeroKnowledgeProofs()[1].getSignature()).toBe('sign1');

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getFromVoterId()).toBe(2);
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getFromVoterName()).toBe('v2');

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs().length).toBe(1);

        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs()[0].getMessageIndex()).toBe(0);
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs()[0].getA()).toBe('a3');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs()[0].getB()).toBe('b3');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs()[0].getC()).toBe('c3');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs()[0].getR()).toBe('r3');
        expect(parsedMaybeObject.getData().getAllZeroKnowledgeProofs()[1].getZeroKnowledgeProofs()[0].getSignature()).toBe('sign2');

    });

});
