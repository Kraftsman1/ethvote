import {PrimeFactorizationServiceImpl} from '../../../app/services/primeFactorization/primeFactorizationImpl.service';
import {MathServiceImpl} from '../../../app/services/math/mathImpl.service';
import {PrimesServiceImpl} from '../../../app/services/primes/primesImpl.service';
import {PrimeFactorizationService} from '../../../app/services/primeFactorization/primeFactorization.service';

describe('PrimeFactorizationServiceImpl', () => {

    let service: PrimeFactorizationService;

    beforeEach(() => {
        service = new PrimeFactorizationServiceImpl(
            new MathServiceImpl(),
            new PrimesServiceImpl()
            );
    });

    it('should be able to return prime', () => {
        const number = '4234032';
        expect(service.factorize(number)).toEqual([4, 7, 0, 0, 2]);
    });

});
