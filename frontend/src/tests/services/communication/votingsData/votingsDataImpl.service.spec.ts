import { Maybe } from './../../../../app/utils/maybe.utils';
import { NoopVotingJsonParser } from './../../jsonParser/noopVotingsJsonParser.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { VotingsDataServiceImpl } from './../../../../app/services/communication/votingsData/votingsDataImpl.service';
import { VotingsJsonParser } from '../../../../app/services/jsonParser/votingsJsonParser.service';
import { VotingsDataService } from '../../../../app/services/communication/votingsData/votingsData.service';
import { VotingsArray } from '../../../../app/models/votings/votingsArray.json.model';
import { VotingOption } from 'src/app/models/votings/votingOption.json.model';
import { EligibleVoter } from 'src/app/models/votings/eligibleVoter.json.model';
import { VotingOverview } from '../../../../app/models/votings/votingOverview.json.model';
import { NewCreatedVoting } from '../../../../app/models/votings/newCreatedVoting.json.model';
import {VotingsWithProgressJsonParser} from '../../../../app/services/jsonParser/votingsWithProgressJsonParser.service';
import {VotingsWithProgressJsonParserImpl} from '../../../../app/services/jsonParser/votingsWithProgressJsonParserImpl.service';
import {Checker} from '../../../checker.service';

describe('VotingsDataServiceImpl', () => {

    let votingsDataService: VotingsDataService;
    let httpMock: HttpTestingController;
    let noopParser: VotingsJsonParser;
    let votingsWithProgressParser: VotingsWithProgressJsonParser;

    beforeEach(() => {
        TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ],
        providers: [
            { provide: VotingsDataService , useClass: VotingsDataServiceImpl },
            { provide: VotingsJsonParser, useClass: NoopVotingJsonParser },
            { provide: VotingsWithProgressJsonParser, useClass: VotingsWithProgressJsonParserImpl}
          ],
      });

      httpMock = TestBed.get(HttpTestingController);
      votingsDataService = TestBed.get(VotingsDataService);
      noopParser = TestBed.get(VotingsJsonParser);
      votingsWithProgressParser = TestBed.get(VotingsWithProgressJsonParser);

    });

    it('should be created', async() => {
        expect(votingsDataService).toBeTruthy();
    });

    it('should return empty object if response status is not ok', async() => {
        votingsDataService.getCurrentVotings(0).subscribe(maybeVotings => expect(maybeVotings.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/phaseNumber/0');
        expect(req.request.method).toEqual('GET');
        req.flush({}, { status: 422, statusText: 'Invalid request' });
    });

    it('should make a get request', async() => {
        votingsDataService.getCurrentVotings(1).subscribe(maybeVotings => expect(maybeVotings.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/phaseNumber/1');
        expect(req.request.method).toEqual('GET');
        req.flush({}, { status: 200, statusText: 'Ok' } );
    });

    it('parser should not be called when no data field', async() => {
        spyOn(noopParser, 'parseJson').and.returnValue(Maybe.fromEmpty());
        votingsDataService.getCurrentVotings(0).subscribe(maybeVotings => expect(maybeVotings.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/phaseNumber/0');
        expect(req.request.method).toEqual('GET');
        req.flush({invalid: 'foo'}, { status: 200, statusText: 'Ok' } );
        expect(noopParser.parseJson).not.toHaveBeenCalled();
    });

    it('parser should not be called with null value', async() => {
        spyOn(noopParser, 'parseJson').and.returnValue(Maybe.fromEmpty());
        votingsDataService.getCurrentVotings(42).subscribe(maybeVotings => expect(maybeVotings.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/phaseNumber/42');
        expect(req.request.method).toEqual('GET');
        req.flush({votings: null}, { status: 200, statusText: 'Ok' } );
        expect(noopParser.parseJson).not.toHaveBeenCalled();
    });

    it('should forward data that parser provided', async() => {
        const specialTitle = 'veryspecial';

        const voting = new VotingOverview();
        voting.setTitle(specialTitle);
        voting.setOptions([]);
        voting.setDescription('description');

        const votingsArray = new VotingsArray();
        votingsArray.setVotings(new Array());
        votingsArray.getVotings().push(voting);

        spyOn(votingsWithProgressParser, 'parseJson').and.returnValue(Maybe.fromData(votingsArray));

        votingsDataService.getCurrentVotings(0)
        .subscribe(maybeVotings => expect(maybeVotings.getData().getVotings()[0].getTitle()).toBe(specialTitle));
        const req = httpMock.expectOne('/api/votings/phaseNumber/0');
        expect(req.request.method).toEqual('GET');
        req.flush({votings: 'foo'}, { status: 200, statusText: 'Ok' } );
    });

    it('should be able to get created votings data', async() => {
        const specialTitle = 'veryspecial';

        const voting = new VotingOverview();
        voting.setTitle(specialTitle);
        voting.setOptions([]);
        voting.setDescription('description');

        const votingsArray = new VotingsArray();
        votingsArray.setVotings(new Array());
        votingsArray.getVotings().push(voting);

        spyOn(noopParser, 'parseJson').and.returnValue(Maybe.fromData(votingsArray));

        votingsDataService.getCreatedVotings()
            .subscribe(maybeVotings => expect(maybeVotings.getData().getVotings()[0].getTitle()).toBe(specialTitle));
        const req = httpMock.expectOne('/api/createdVotings');
        expect(req.request.method).toEqual('GET');
        req.flush({votings: 'foo'}, { status: 200, statusText: 'Ok' } );
    });

    it('should forward data to parser service if there is a votings field', async() => {
        spyOn(votingsWithProgressParser, 'parseJson').and.returnValue(Maybe.fromEmpty());
        votingsDataService.getCurrentVotings(0).subscribe(maybeVotings => expect(maybeVotings.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/phaseNumber/0');
        expect(req.request.method).toEqual('GET');
        req.flush({votings: [], votingsVoterProgress: []}, { status: 200, statusText: 'Ok' } );
        expect(votingsWithProgressParser.parseJson).toHaveBeenCalledWith('{"votings":[],"votingsVoterProgress":[]}');
    });

    it('should issue post request with valid json string and return ok if valid', async() => {
        const options = new Array(
            new VotingOption().setOptionName('Option 1'),
            new VotingOption().setOptionName('Option 2'),
            new VotingOption().setOptionName('Option 3')
        );

        const eligibleVoters = new Array(
            new EligibleVoter().setName('voter1'),
            new EligibleVoter().setName('voter2')
        );

        const voting = new NewCreatedVoting()
                        .setTitle('title')
                        .setDescription('description')
                        .setOptions(options)
                        .setEligibleVoters(eligibleVoters)
                        .setSecurityThreshold(2)
                        .setNumberOfEligibleVoters(2);

        votingsDataService.addVoting(voting).subscribe(obsSuccessfull => expect(obsSuccessfull.valueOf()).toBeTruthy());

        const req = httpMock.expectOne('/api/votings');
        expect(req.request.body).toBe(JSON.stringify(voting));
        req.flush({}, {status: 201, statusText: ''});
        expect(req.request.method).toEqual('POST');
    });

    it('should propagate add voting error', async() => {
        const voting = new NewCreatedVoting();

        votingsDataService.addVoting(voting).subscribe(obsSuccessfull => expect(obsSuccessfull.isEmpty()).toBeTruthy());

        const req = httpMock.expectOne('/api/votings');
        req.flush({}, {status: 400, statusText: ''});
        expect(req.request.method).toEqual('POST');
    });

    it('set phase number should return empty if there is an error', async() => {
        const votingId = 27;
        const phaseNumber = 2;
        votingsDataService.setPhaseNumber(votingId, phaseNumber, false).subscribe(successful => expect(successful.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/' + votingId + '/phaseNumber');
        expect(req.request.method).toEqual('PUT');
        expect(Checker.isEqual(req.request.body, {phaseNumber: phaseNumber, sendNotificationMail: false})).toBeTruthy();
        req.flush({}, { status: 500, statusText: 'Dummy error' } );
    });

    it('set phase number should return true if everything worked', async() => {
        const votingId = 27;
        const phaseNumber = 2;

        spyOn(noopParser, 'parseSingleVoting').and.returnValue(Maybe.fromData(new VotingOverview()));

        votingsDataService.setPhaseNumber(votingId, phaseNumber, true).subscribe(successful => expect(successful !== undefined && !successful.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/' + votingId + '/phaseNumber');
        expect(req.request.method).toEqual('PUT');
        expect(Checker.isEqual(req.request.body, {phaseNumber: phaseNumber, sendNotificationMail: true})).toBeTruthy();
        req.flush({}, { status: 200, statusText: 'Ok'} );
    });

    it('set voting end date empty if there is an error', async() => {
        const votingId = 27;
        const endTime = 2168;
        votingsDataService.setVotingEndTime(votingId, endTime).subscribe(successful => expect(successful.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/' + votingId + '/timeVotingFinishes');
        expect(req.request.method).toEqual('PUT');
        expect(Checker.isEqual(req.request.body, {votingEndTime: endTime})).toBeTruthy();
        req.flush({}, { status: 500, statusText: 'Dummy error' } );
    });

    it('set voting end date should return data if everything worked', async() => {
        const votingId = 27;
        const endTime = 2168;

        spyOn(noopParser, 'parseSingleVoting').and.returnValue(Maybe.fromData(new VotingOverview()));

        votingsDataService.setVotingEndTime(votingId, endTime).subscribe(successful => expect(successful !== undefined && !successful.isEmpty()).toBeTruthy());
        const req = httpMock.expectOne('/api/votings/' + votingId + '/timeVotingFinishes');
        expect(req.request.method).toEqual('PUT');
        expect(Checker.isEqual(req.request.body, {votingEndTime: endTime})).toBeTruthy();
        req.flush({}, { status: 200, statusText: 'Ok'} );
    });

    it('delete voting propagates error', () => {
        const votingId = 27;
        votingsDataService.deleteVoting(votingId).subscribe(data => {
            expect(data.getFirst()).toBeFalsy();
            expect(data.getSecond()).toEqual('Dummy text');
        });

        const req = httpMock.expectOne(r => r.method === 'DELETE' && r.url === '/api/votings/' + votingId);
        req.flush('Dummy text', { status: 404, statusText: 'Dummy status' });
    });

    it('delete voting returns ok if worked', () => {
        const votingId = 27;
        votingsDataService.deleteVoting(votingId).subscribe(data => {
            expect(data.getFirst()).toBeTruthy();
            expect(data.getSecond()).toEqual('Successful');
        });

        const req = httpMock.expectOne(r => r.method === 'DELETE' && r.url === '/api/votings/' + votingId);
        req.flush('Successful', { status: 200, statusText: 'Dummy status' });
    });

    it('get voting summary returns data if worked', () => {
        const votingId = 27;
        votingsDataService.getVotingSummary(votingId).subscribe(data => {
            expect(data.isEmpty()).toBeFalsy();
            expect(data.getData()).toEqual('Dummy text');
        });

        const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/' + votingId + '/votingSummary');
        req.flush('Dummy text', { status: 200, statusText: 'Successful' });
    });

    it('get voting summary returns empty if not found', () => {
        const votingId = 27;
        votingsDataService.getVotingSummary(votingId).subscribe(data => {
            expect(data.isEmpty()).toBeTruthy();
        });

        const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/' + votingId + '/votingSummary');
        req.flush('Dummy text', { status: 404, statusText: 'Not found' });
    });

});
