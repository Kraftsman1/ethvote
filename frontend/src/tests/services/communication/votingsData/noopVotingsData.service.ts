import { VotingsArray } from './../../../../app/models/votings/votingsArray.json.model';
import { Maybe } from 'src/app/utils/maybe.utils';
import { Observable, of } from 'rxjs';
import { VotingsDataService } from '../../../../app/services/communication/votingsData/votingsData.service';
import { Injectable } from '@angular/core';
import { VotingOption } from '../../../../app/models/votings/votingOption.json.model';
import { NewCreatedVoting } from '../../../../app/models/votings/newCreatedVoting.json.model';
import { VotingOverview } from '../../../../app/models/votings/votingOverview.json.model';
import {VotingsWithProgressArray} from '../../../../app/models/votings/votingsWithProgressArray.json.model';
import {Pair} from '../../../../app/utils/pair.utils';

@Injectable()
export class NoopVotingsDataService implements VotingsDataService {

    getCurrentVotings(): Observable<Maybe<VotingsWithProgressArray>> {

        const voteOne = new VotingOverview();
        voteOne.setTitle('Best James Bond').setDescription('Who is the best James Bond').setOptions(new Array());
        voteOne.getOptions().push(new VotingOption().setOptionName('Sean Connery'));
        voteOne.getOptions().push(new VotingOption().setOptionName('Roger Moore'));
        voteOne.getOptions().push(new VotingOption().setOptionName('Pierce Brosnan'));
        voteOne.getOptions().push(new VotingOption().setOptionName('Daniel Craig'));

        const voteTwo = new VotingOverview();
        voteTwo.setTitle('To be or not to be').setDescription('This is the question').setOptions(new Array());
        voteTwo.getOptions().push(new VotingOption().setOptionName('To be'));
        voteTwo.getOptions().push(new VotingOption().setOptionName('Not to be'));

        const ret = new VotingsArray();
        ret.getVotings().push(voteOne);
        ret.getVotings().push(voteTwo);

        return of(Maybe.fromData(new VotingsWithProgressArray()));
    }

    addVoting(voting: NewCreatedVoting): Observable<Maybe<VotingsArray>> {
        return undefined;
    }

    getCreatedVotings(): Observable<Maybe<VotingsArray>> {
        return undefined;
    }

    setPhaseNumber(votingId: number, phaseNumber: number, sendNotificationMail: boolean): Observable<Maybe<VotingOverview>> {
        return undefined;
    }

    setVotingEndTime(votingId: number, votingEndTime: number): Observable<Maybe<VotingOverview>> {
        return undefined;
    }

    deleteVoting(votingId: Number): Observable<Pair<boolean, string>> {
        return undefined;
    }

    getVotingSummary(votingId: Number): Observable<Maybe<string>> {
        return undefined;
    }
}
