import {Injectable} from '@angular/core';
import {VotingProgressJsonParser} from '../../../../app/services/jsonParser/votingProgressJsonParser.service';
import {VotingProgress} from '../../../../app/models/votings/votingProgress.json.model';
import {Maybe} from '../../../../app/utils/maybe.utils';


@Injectable()
export class NoopVotingProgressJsonParser implements VotingProgressJsonParser {

    parseJson(jsonString: string): Maybe<VotingProgress> {
        return undefined;
    }

}
