import { environment } from './../../../../environments/environment.prod';
import { Maybe } from './../../../../app/utils/maybe.utils';
import { NoopLocalStorageService } from './../../localStorage/noopLocalStorage.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
// tslint:disable-next-line
import { AuthorizationInterceptor } from './../../../../app/services/communication/authorizationInterceptor/authorizationInterceptor.service';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocalStorageService } from '../../../../app/services/localStorage/localStorage.service';
import { UrlRewriteInterceptor } from '../../../../app/services/communication/urlRewriteInterceptor/urlRewriteInterceptor.service';

describe('UrlRewriteInterceptor', () => {

    let httpMock: HttpTestingController;
    let httpClient: HttpClient;

    beforeEach(() => {
        TestBed.configureTestingModule({
        imports: [
            HttpClientTestingModule
        ],
        providers: [
            { provide: HTTP_INTERCEPTORS, useClass: UrlRewriteInterceptor, multi: true },
          ],
      });

      httpMock = TestBed.get(HttpTestingController);
      httpClient = TestBed.get(HttpClient);
    });

    it('should rewrite urls that start with /api', async() => {
        httpClient.get('/api/dummyUrl').subscribe();
        httpMock.expectNone('/api/dummyUrl');
    });

    it('should not rewrite urls that do not start with /api', async() => {
        httpClient.get('/foo/dummyUrl').subscribe();
        httpMock.expectOne('/foo/dummyUrl');
    });

});
