import { Checker } from './../../../checker.service';
import { KeysServiceImpl } from '../../../../app/services/communication/keys/keysImpl.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, async } from '@angular/core/testing';

import { KeysService} from 'src/app/services/communication/keys/keys.service';
import { UserPublicKeyJsonParser } from 'src/app/services/jsonParser/userPublicKeyJsonParser.service';
import { UserPublicKeyJsonParserImpl } from 'src/app/services/jsonParser/userPublicKeyJsonParserImpl.service';
import { CryptoService } from 'src/app/services/crypto/crypto.service';
import { NoopCryptoService } from '../../crypto/noopCrypto.service';

describe('KeysService', () => {

  const userId = 27;
  const userKey = 'someKey';
  const token = 'dummyToken';

  let keysService: KeysService;
  let httpMock: HttpTestingController;
  let cryptoService: CryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [
        HttpClientTestingModule
    ],
    providers: [
        { provide: KeysService , useClass: KeysServiceImpl },
        { provide: UserPublicKeyJsonParser , useClass: UserPublicKeyJsonParserImpl },
        { provide: CryptoService, useClass: NoopCryptoService },
      ],
  });

  httpMock = TestBed.get(HttpTestingController);
  keysService = TestBed.get(KeysService);
  cryptoService = TestBed.get(CryptoService);
  });

  it('should be created', () => {
    expect(keysService).toBeTruthy();
  });

  it('should return empty if return code is 200 (valid answer is 201)', async(() => {
    keysService.storePublicKey(userId, userKey).subscribe(key => expect(key.isEmpty()).toBeTruthy());
    const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/voters/' + userId + '/publicKey');
    expect(Checker.isEqual(req.request.body, {publicKey: userKey})).toBeTruthy();
    req.flush({token: token}, { status: 200, statusText: 'Ok' } );
  }));

  it('should return empty if return code is 403 (valid answer is 201)', async(() => {
    keysService.storePublicKey(userId, userKey).subscribe(key => expect(key.isEmpty()).toBeTruthy());
    const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/voters/' + userId + '/publicKey');
    expect(Checker.isEqual(req.request.body, {publicKey: userKey})).toBeTruthy();
    req.flush({token: token}, { status: 403, statusText: 'Unauthorized' } );
  }));

  it('adding public key for voting should return true if everything went fine', async(() => {
    keysService.storePublicKeyForVoting(27, 42, 'specialKey').subscribe(resp => expect(resp.valueOf()).toBeTruthy());
    const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/votings/42/publicKeys/voters/27');
    req.flush({}, { status: 200, statusText: 'Key was added' });
  }));

  it('adding public key for voting should return false if error', async(() => {
    keysService.storePublicKeyForVoting(27, 42, 'specialKey').subscribe(resp => expect(resp.valueOf()).toBeFalsy());
    const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/votings/42/publicKeys/voters/27');
    req.flush({}, { status: 401, statusText: 'Forbidden' });
  }));

  it('should return empty if return code is 201 but no valid token', async(() => {
    keysService.storePublicKey(userId, userKey).subscribe(key => expect(key.isEmpty()).toBeTruthy());
    const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/voters/' + userId + '/publicKey');
    expect(Checker.isEqual(req.request.body, {publicKey: userKey})).toBeTruthy();
    req.flush({}, { status: 200, statusText: 'Ok' } );
  }));

  it('should return key form server if code is 201 and key is provided', async(() => {
    keysService.storePublicKey(userId, userKey).subscribe(key => expect(key.getData()).toBe(token));
    const req = httpMock.expectOne(r => r.method === 'PUT' && r.url === '/api/voters/' + userId + '/publicKey');
    expect(Checker.isEqual(req.request.body, {publicKey: userKey})).toBeTruthy();
    req.flush({token: token}, { status: 201, statusText: 'Ok' } );
  }));

  it('should return the right data if asked for all voters', async() => {

    keysService.getAllKeys().subscribe(keys => {
      expect(keys.length).toBe(3);
      expect(keys[0].getName()).toBe('u1');
      expect(keys[0].getPublicKey()).toBe('k1');
      expect(keys[0].getId()).toBe(1);
      expect(keys[1].getName()).toBe('u2');
      expect(keys[1].getPublicKey()).toBe('k2');
      expect(keys[1].getId()).toBe(2);
      expect(keys[2].getName()).toBe('u3');
      expect(keys[2].getPublicKey()).toBe('k3');
      expect(keys[2].getId()).toBe(3);
    });

    const mockAnswer = '{"voters":[{"publicKey":"k1","name":"u1","id":1},{"publicKey":"k2","name":"u2","id":2},{"publicKey":"k3","name":"u3","id":3}]}';
    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters?onlyIfKeySpecified=true');
    req.flush(JSON.parse(mockAnswer), { status: 200, statusText: 'Ok' });
  });

  it('should be able to parse actual public key', () => {
    const dummyFingerprint = 'dummyFingerPrint';
    spyOn(cryptoService, 'pemToSHA256Fingerprint').and.returnValue(dummyFingerprint);

    keysService.getAllKeys().subscribe(keys => {
      expect(keys.length).toBe(1);
      expect(keys[0].getName()).toBe('John Doe');
      expect(keys[0].getPublicKey()).toBe('-----BEGIN PUBLIC KEY-----\r\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxUEwYbLRZDpfyXvV2Y3B\r\nZzS8MN3MfbM+hyRsBSgy6yXhNED9wWKRVqBC9jhxbkY+YTJrFrbsLDPvOI7a8H1S\r\nJRfRSgb+YhnPYLWnhffZR2JEWsthCmtU4BN5d7uJv6NyIV3GXktFJceHbwRj0u8o\r\n9w5eag7kVpoVruAsdXcG0SKlR3GEuZd8GQX3pqL+19nD0X9PhwC9K5Ne8KwSvMTX\r\nYi04eMaChhu7MFy8kubfIEaaSuziF5omxEfvE18i+nYAkHB2fIiCg/ziIrRrP8WZ\r\nN003B4FCbxs8dpY2Nz+PVYtz0nMgz18ElJ1MmzrXRFB/FzGvb/YxjR9+flfxJqR2\r\nWwIDAQAB\r\n-----END PUBLIC KEY-----\r\n');
      expect(keys[0].getId()).toBe(3);
      expect(keys[0].getPublicKeyFingerprint()).toBe(dummyFingerprint);
    });
    const mockAnswer = '{"voters":[{"publicKey":"-----BEGIN PUBLIC KEY-----\\r\\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxUEwYbLRZDpfyXvV2Y3B\\r\\nZzS8MN3MfbM+hyRsBSgy6yXhNED9wWKRVqBC9jhxbkY+YTJrFrbsLDPvOI7a8H1S\\r\\nJRfRSgb+YhnPYLWnhffZR2JEWsthCmtU4BN5d7uJv6NyIV3GXktFJceHbwRj0u8o\\r\\n9w5eag7kVpoVruAsdXcG0SKlR3GEuZd8GQX3pqL+19nD0X9PhwC9K5Ne8KwSvMTX\\r\\nYi04eMaChhu7MFy8kubfIEaaSuziF5omxEfvE18i+nYAkHB2fIiCg/ziIrRrP8WZ\\r\\nN003B4FCbxs8dpY2Nz+PVYtz0nMgz18ElJ1MmzrXRFB/FzGvb/YxjR9+flfxJqR2\\r\\nWwIDAQAB\\r\\n-----END PUBLIC KEY-----\\r\\n","name":"John Doe","id":3}]}';
    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters?onlyIfKeySpecified=true');
    req.flush(JSON.parse(mockAnswer), { status: 200, statusText: 'Ok' });
  });

  it('should return empty if getting private key did not work', () => {
    const voterId = 42;
    keysService.getPrivateKey(voterId).subscribe(key => {
        expect(key !== undefined).toBeTruthy();
        expect(key.isEmpty()).toBeTruthy();
  });

    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters/42/privateKey');
    req.flush({}, { status: 404, statusText: 'Not found' });
  });

  it('should return empty if invalid data is returned', () => {
    const voterId = 42;
    keysService.getPrivateKey(voterId).subscribe(key => {
      expect(key !== undefined).toBeTruthy();
      expect(key.isEmpty()).toBeTruthy();
    });

    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters/42/privateKey');
    req.flush({invalid: 'dummy'}, { status: 200, statusText: 'OK' });
  });

  it('should return private key if valid data is returned', () => {
    const voterId = 42;
    keysService.getPrivateKey(voterId).subscribe(key => {
      expect(key !== undefined).toBeTruthy();
      expect(key.isEmpty()).toBeFalsy();
      expect(key.getData()).toBe('key');
    });

    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/voters/42/privateKey');
    req.flush({privateKey: 'key'}, { status: 200, statusText: 'OK' });
  });

  it('should return empty if getting public key for voting did not work', () => {
    const voterId = 42;
    const votingId = 27;
    keysService.getPublicKeyForVoting(voterId, votingId).subscribe(key => {
      expect(key !== undefined).toBeTruthy();
      expect(key.isEmpty()).toBeTruthy();
    });

    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/27/publicKeys/voters/42');
    req.flush({}, { status: 404, statusText: 'Not found' });
  });

  it('should return empty if invalid data is returned', () => {
    const voterId = 42;
    const votingId = 27;
    keysService.getPublicKeyForVoting(voterId, votingId).subscribe(key => {
      expect(key !== undefined).toBeTruthy();
      expect(key.isEmpty()).toBeTruthy();
    });

    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/27/publicKeys/voters/42');
    req.flush({invalid: 'foo'}, { status: 200, statusText: 'OK' });
  });

  it('should return empty if invalid data is returned', () => {
    const voterId = 42;
    const votingId = 27;

    const keyValue = 'key';

    keysService.getPublicKeyForVoting(voterId, votingId).subscribe(key => {
      expect(key !== undefined).toBeTruthy();
      expect(key.isEmpty()).toBeFalsy();
      expect(key.getData()).toBe(keyValue);
    });

    const req = httpMock.expectOne(r => r.method === 'GET' && r.url === '/api/votings/27/publicKeys/voters/42');
    req.flush({publicKey: keyValue}, { status: 200, statusText: 'key' });
  });


});
