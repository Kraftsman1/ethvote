import { VoterParserService } from '../../../app/services/voterParser/voterParser.service';
import {Maybe} from '../../../app/utils/maybe.utils';
import {VoterParserServiceImpl} from '../../../app/services/voterParser/voterParserImpl.service';
import {VoterToCreate} from '../../../app/data/voterToCreate';

describe('VoterParserService', () => {

  let voterParserService: VoterParserService;

  beforeEach(() => {
    voterParserService = new VoterParserServiceImpl();
  });

  it('should be created', () => {
    expect(voterParserService).toBeTruthy();
  });

  it('returns empty if opening bracket but not closing', () => {
    const missingClosing = 'foo\tmail@mail\t(Administrator';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingClosing);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('returns empty if closing bracket but not opening', () => {
    const missingClosing = 'foo \t foo@bar \t Administrator)';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingClosing);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('returns empty if closing bracket but not opening', () => {
    const missingClosing = 'foo \t foo@bar \t Administrator)';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingClosing);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('returns empty if brackets wrong', () => {
    const missingClosing = 'foo \t foo@bar \t )Administrator(';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingClosing);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('returns empty if closing bracket not at end', () => {
    const missingClosing = 'foo \t foo@bar \t (Administrator) f';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingClosing);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('single voter without roles can be parsed', () => {
    const missingClosing = '  dummy  voter \t foo@bar';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingClosing);
    expect(res.getData().getName()).toEqual('dummy  voter');
    expect(res.getData().getEmail()).toEqual('foo@bar');
  });

  it('valid user with role returns empty if not in with roles mode', () => {
    const adminRole = 'foo \t foo@bar \t (Administrator)   ';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(adminRole);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('valid user with role can be parsed if in with role mode', () => {
    const adminRole = 'foo \t foo@bar \t (Administrator)   ';
    voterParserService.setWithRoles(true);
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(adminRole);
    expect(res.getData().getName()).toEqual('foo');
    expect(res.getData().getEmail()).toEqual('foo@bar');
    expect(res.getData().isAdministrator()).toBeTruthy();
    expect(res.getData().isVoting_Administrator()).toBeFalsy();
    expect(res.getData().isRegistrar()).toBeFalsy();
  });

  it('valid user with multiple roles can be parsed if in with role mode', () => {
    const multipleRole = 'foo \t foo@bar \t (Registrar,Voting Administrator)';
    voterParserService.setWithRoles(true);
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(multipleRole);
    expect(res.getData().getName()).toEqual('foo');
    expect(res.getData().getEmail()).toEqual('foo@bar');
    expect(res.getData().isAdministrator()).toBeFalsy();
    expect(res.getData().isVoting_Administrator()).toBeTruthy();
    expect(res.getData().isRegistrar()).toBeTruthy();
  });

  it('invalid role returns empty', () => {
    const invalidRole = 'foo \t foo@bar \t (Invalid)';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(invalidRole);
    expect(res.isEmpty()).toBeTruthy();
  });

  it('voter without email can be parser', () => {
    const missingEmail = 'foo ';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(missingEmail);
    expect(res.getData().getName()).toBe('foo');
  });

  it('empty can be parser', () => {
    const empty = '';
    const res: Maybe<VoterToCreate> = voterParserService.parseLine(empty);
    expect(res.isEmpty()).toBeFalsy();
  });

});
