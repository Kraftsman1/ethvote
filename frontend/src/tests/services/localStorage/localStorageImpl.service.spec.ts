import { LocalStorageService } from '../../../app/services/localStorage/localStorage.service';
import { LocalStorageServiceImpl } from '../../../app/services/localStorage/localStorageImpl.service';

describe('JwtTokenDecoderService', () => {

    let service: LocalStorageService;

    beforeEach(() => {
        service = new LocalStorageServiceImpl();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
      });

    it('getting an item that is not there should return empty', () => {
        expect(service.get('notthere').isEmpty()).toBeTruthy();
    });

    it('inserting should work', () => {
        const key = 'theKey';
        const value = 'theValue';
        const notExistingKey = 'notThere';
        service.upsert(key, value);
        const shouldExist = service.get(key);
        const sholdNotExist = service.get(notExistingKey);
        expect(shouldExist.isEmpty()).toBeFalsy();
        expect(shouldExist.getData()).toBe(value);
        expect(sholdNotExist.isEmpty()).toBeTruthy();
    });

    it('update should work', () => {
        const key = 'key';
        const valOne = 'valOne';
        const valTwo = 'valTwo';
        service.upsert(key, valOne);
        service.upsert(key, valTwo);
        expect(service.get(key).getData()).toBe(valTwo);
    });

    it('deleting an value that is not there should return false', () => {
        const notExistingKey = 'notExisitingKey';
        expect(service.delete(notExistingKey)).toBeFalsy();
    });

    it('deleting an value that is there should be deleted', () => {
        const key = 'key';
        const value = 'value';
        service.upsert(key, value);
        const result = service.delete(key);
        expect(result).toBeTruthy();
        expect(service.get(key).isEmpty()).toBeTruthy();
    });
});
