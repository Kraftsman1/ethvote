import {LagrangeService} from '../../../app/services/lagrange/lagrange.service';
import {LagrangeServiceImpl} from '../../../app/services/lagrange/lagrangeImpl.service';
import {MathService} from '../../../app/services/math/math.service';
import {MathServiceImpl} from '../../../app/services/math/mathImpl.service';
import {LagrangePolynom} from '../../../app/data/lagrangePolynom';

describe('LagrangeService', () => {

    let service: LagrangeService;
    let mathService: MathService;

    beforeEach(() => {
        mathService = new MathServiceImpl();
        service = new LagrangeServiceImpl(mathService);
    });

    // test takes a long time
    xit('should be able to calculate right result', () => {
        const polynomDegree = 2;
        const primeP = '28716815405345534032228544684235121829153323576430273906239248066501109681081095482194640997947027327149006330209055428612405406253196014870389934056590195399279558272617767483077102977259847066561843493358656139196765129185122465953728669668583598998494439657361387793116204089330636288316420706672177731839476502118343902901125144385735823681347926912646221667290995212333474752079398825273425171467762551123619207828507902399268897366555422679142176315445851610341991658982829917860964935603100343440819635298746715726162980203283304336517566155298947636309868835883059537925411180058670877248758180567274996927399';
        const primeQ = '14358407702672767016114272342117560914576661788215136953119624033250554840540547741097320498973513663574503165104527714306202703126598007435194967028295097699639779136308883741538551488629923533280921746679328069598382564592561232976864334834291799499247219828680693896558102044665318144158210353336088865919738251059171951450562572192867911840673963456323110833645497606166737376039699412636712585733881275561809603914253951199634448683277711339571088157722925805170995829491414958930482467801550171720409817649373357863081490101641652168258783077649473818154934417941529768962705590029335438624379090283637498463699';
        const generator = '65537';

        for (let i = 0; i < 30; i++) {

            const polynom = mathService.generateRandomPolynomialWithNBytes(polynomDegree, 500);

            let expectedResult = mathService.evaluatePolynomial(polynom, '0');
            expectedResult = mathService.powerWithModulo(generator, expectedResult, primeP);

            const lagrangePolynomial = new LagrangePolynom();

            const pos1 = mathService.generateRandom('1000000000');

            let val1 = mathService.evaluatePolynomial(polynom, pos1);
            val1 = mathService.powerWithModulo(generator, val1, primeP);
            lagrangePolynomial.addPoint(pos1, val1);

            let pos2 = mathService.generateRandom('1000000000');
            if (pos1 === pos2) {
                pos2 = mathService.add(pos2, '1');
            }

            let val2 = mathService.evaluatePolynomial(polynom, pos2);
            val2 = mathService.powerWithModulo(generator, val2, primeP);
            lagrangePolynomial.addPoint(pos2, val2);

            const actualResult = service.getValueAtPosition('0', lagrangePolynomial, primeP, primeQ);

            expect(expectedResult).toBe(actualResult);
        }
    });

});
