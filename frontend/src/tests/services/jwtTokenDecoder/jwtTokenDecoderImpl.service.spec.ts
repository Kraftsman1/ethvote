import { JwtTokenDecoderServiceImpl } from './../../../app/services/jwtTokenDecoder/jwtTokenDecoderImpl.service';
import { JwtTokenDecoderService } from '../../../app/services/jwtTokenDecoder/jwtTokenDecoder.service';

describe('JwtTokenDecoderService', () => {

    let service: JwtTokenDecoderService;
    const tokenForJohnDoe =    `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.\
                                eyJzdWIiOiJKb2huIERvZSJ9.\
                                6neBz0ic4Ne9dDyyT8-MLXQOLNQnBHY8BIZIVdjqdRvJreqTbRYyvyX6xEgVK44gSItqiCAnstZY-VOUxK2MGA`;

    const tokenForJohnDoeWithAdminRole =
                                `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.\
                                eyJzdWIiOiJKb2huIERvZSIsInN1YlJvbGVzIjpbIkFkbWluaXN0YXRvciJdfQ.\
                                HJryMR8d1l3eFbaSbtEDSAmPhURZgS9itp_s1TybvAJnacl_-kzppYy-j8uVJhZXLQAqs28CO68LLkcz1PcoIQ`;

    const tokenForJohnDoeWithRoles =
                                `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.\
                                eyJzdWIiOiJKb2huIERvZSIsInN1YlJvbGVzIjpbIkFkbWluaXN0YXRvciIsIlN1cGVydXNlciJdfQ.\
                                4_LS2SyLfirw5ReXg5W_NUgTioabr3Sob5cllXHzyR5s7vj0iBLM9wEeb97QjDrIjxcpD082CpMEBbDJs3SW0A`;

    beforeEach(() => {
        service = new JwtTokenDecoderServiceImpl();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
      });

    it('should return empty if invalid token', () => {
        const invalidToken = 'invalid';
        expect( function() { service.extractSingleField('foo', invalidToken); } ).toThrowError();
    });

    it('should return empty if valid token but field missing', () => {
        const nonExistingField = 'thisFieldDoesNotExist';
        expect(service.extractSingleField(nonExistingField, tokenForJohnDoe).isEmpty).toBeTruthy();
    });

    it('should be able to extract a field', () => {
        const existingField = 'sub';
        const expectedSub = 'John Doe';
        const ret = service.extractSingleField(existingField, tokenForJohnDoe);
        expect(ret.isEmpty()).toBeFalsy();
        expect(ret.getData()).toBe(expectedSub);
    });

    it('should return empty list if field is missing', () => {
        const notExistingField = 'subRoles';
        const ret = service.extractFieldArray(notExistingField, tokenForJohnDoe);
        expect(ret.isEmpty()).toBeTruthy();
    });

    it('should be able to extract a list with one element', () => {
        const existingField = 'subRoles';
        const expectedList = ['Administator'];
        const ret = service.extractFieldArray(existingField, tokenForJohnDoeWithAdminRole);
        expect(ret.isEmpty()).toBeFalsy();
        expect(ret.getData()).toEqual(expectedList);
    });

    it('should be able to extract a list with one element', () => {
        const existingField = 'subRoles';
        const expectedList = ['Administator', 'Superuser'];
        const ret = service.extractFieldArray(existingField, tokenForJohnDoeWithRoles);
        expect(ret.isEmpty()).toBeFalsy();
        expect(ret.getData()).toEqual(expectedList);
    });

});
