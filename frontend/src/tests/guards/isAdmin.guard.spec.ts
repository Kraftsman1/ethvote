import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CurrentUserService } from '../../app/services/currentUser/currentUser.service';
import { NoopCurrentUserService } from '../services/currentUser/noopCurrentUser.service';
import { Router } from '@angular/router';
import { IsAdminGuard} from '../../app/guards/isAdmin.guard';

describe('IsAdminGuard', () => {

    let noopCurrentUserService: CurrentUserService;
    let isAdminGuard: IsAdminGuard;

    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
        });

        router = TestBed.get(Router);

        noopCurrentUserService = new NoopCurrentUserService();
        isAdminGuard = new IsAdminGuard(noopCurrentUserService, router);
    });

    it('should return false if not admin', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserAdmin').and.returnValue(false);
        expect(isAdminGuard.canActivate()).toBeFalsy();
    });

    it('should return true if admin', () => {
        spyOn(noopCurrentUserService, 'isCurrentUserAdmin').and.returnValue(true);
        expect(isAdminGuard.canActivate()).toBeTruthy();
    });

});
