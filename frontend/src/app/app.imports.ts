import { LoggedInGuard } from './guards/loggedIn.guard';
import { BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatRadioModule,
    MatCardModule,
    MatExpansionModule,
    MatSliderModule,
    MatDialogModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatSelectModule,
    MatMenuModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSortModule, MatTabsModule,
} from '@angular/material';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AngularSplitModule } from 'angular-split';
import { AceEditorModule } from 'ng2-ace-editor';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AddVotingComponent } from './components/addVoting/addVoting.component';
import { VotingsComponent } from './components/votings/votings.component';
import { KeysComponent } from './components/keys/keys.component';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RolesComponent} from './components/roles/roles.component';
import {AddVoterComponent} from './components/addVoter/addVoter.component';
import {AddVoterFailedComponent} from './components/dialogs/addVoterFailed/addVoterFailed.component';

import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/markdown/markdown';
import {ManageVotingsComponent} from './components/manageVotings/manageVotings.component';
import {IsVotingAdminGuard} from './guards/isVotingAdmin.guard';
import {StartVotingComponent} from './components/startVoting/startVoting.component';
import {EasyVoteComponent} from './components/easyVote/easyVote.component';
import {KeyDerivationComponent} from './components/keyDerivation/keyDerivation.component';
import {UploadPrivateKeyComponent} from './components/dialogs/uploadPrivateKey/uploadPrivateKey.component';
import {CountdownComponent} from './components/countdown/countdown.component';
import {DecryptionComponent} from './components/decryption/decryption.component';
import {ShowResultsComponent} from './components/dialogs/showResults/showResults.component';
import {SpecifyVotersComponent} from './components/dialogs/specifyVoters/specifyVoters.component';
import {IsAdminOrRegistrarGuard} from './guards/isAdminOrRegistrar.guard';
import {VotingInformationComponent} from './components/dialogs/votingInformation/votingInformation.component';
import {PublishComponent} from './components/publish/publish.component';
import {ViewSummaryComponent} from './components/dialogs/viewSummary/viewSummary.component';
import {AdministratorComponent} from './components/administrator/administrator.component';
import {PkiComponent} from './components/pki/pki.component';
import {TrusteeComponent} from './components/trustee/trustee.component';
import {IsTrusteeGuard} from './guards/isTrustee.guard';

export const imports =
[
    BrowserModule,
    BrowserAnimationsModule,
    // For reactive forms
    FormsModule,
    ReactiveFormsModule,
    // Material for nice form styles
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatRadioModule,
    MatCardModule,
    MatExpansionModule,
    MatSliderModule,
    MatDialogModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSortModule,
    MatTabsModule,
    DragDropModule,
    AceEditorModule,
    AngularSplitModule.forRoot(),
    CodemirrorModule,
    NgxJsonViewerModule,
    // Http Client
    HttpClientModule,
    // RouterModule
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'home',
        component: VotingsComponent,
        canActivate: [LoggedInGuard]
      },
      {
        path: 'keys',
        component: KeysComponent,
        canActivate: [LoggedInGuard]
      },
      {
        path: 'votings',
        component: VotingsComponent,
        canActivate: [LoggedInGuard],
      },
      {
        path: 'roles',
        component: RolesComponent,
        canActivate: [LoggedInGuard, IsAdminOrRegistrarGuard]
      },
      {
        path: 'addVoter',
        component: AddVoterComponent,
          children: [{
                  path: '',
                  component: AddVoterFailedComponent,
              }]
      },
      {
        path: 'manageVotings',
        component: ManageVotingsComponent,
        canActivate: [LoggedInGuard, IsVotingAdminGuard],
          children: [
              {
                  path: '',
                  component: StartVotingComponent
              },
              {
                  path: '',
                  component: AddVotingComponent,
                  canActivate: [LoggedInGuard, IsVotingAdminGuard],
              },
              {
                  path: '',
                  component: CountdownComponent,
              },
              {
                  path: '',
                  component: ShowResultsComponent,
              }]
      },
      {
        path: 'manageVotings/votingId/:votingId',
        component: ManageVotingsComponent,
        canActivate: [LoggedInGuard, IsVotingAdminGuard],
          children: [
              {
                  path: '',
                  component: StartVotingComponent
              },
              {
                  path: '',
                  component: SpecifyVotersComponent,
                  canActivate: [LoggedInGuard, IsVotingAdminGuard],
              }],
      },
      {
        path: 'vote',
        component: EasyVoteComponent,
        canActivate: [LoggedInGuard],
          children: [{
                path: '',
                component: UploadPrivateKeyComponent
          }]
      },
      {
         path: 'vote/votingId/:votingId/voterId/:voterId',
         component: EasyVoteComponent,
         canActivate: [LoggedInGuard],
            children: [{
                path: '',
                component: UploadPrivateKeyComponent
            }]
      },
      {
         path: 'pki',
         component: PkiComponent,
         canActivate: [LoggedInGuard],
      },
      {
        path: 'keyDerivation',
        component: KeyDerivationComponent,
        canActivate: [LoggedInGuard],
          children: [{
              path: '',
              component: VotingInformationComponent
          }]
      },
      {
        path: 'decryption',
        component: DecryptionComponent,
        canActivate: [LoggedInGuard]
      },
      {
        path: 'publish',
        component: PublishComponent,
        canActivate: [LoggedInGuard],
          children: [{
              path: '',
              component: ViewSummaryComponent
          }]
      },
      {
        path: 'administrator',
        component: AdministratorComponent,
      },
      {
        path: 'trustee',
        component: TrusteeComponent,
        canActivate: [LoggedInGuard, IsTrusteeGuard]
      }
    ]
    ,
        {onSameUrlNavigation: 'reload'}),
    JwtModule
];
