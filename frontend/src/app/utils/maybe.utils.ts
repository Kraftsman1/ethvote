 export class Maybe<T> {
    private data: T;
    private empty: boolean;

    private constructor(isEmpty: boolean, data: T) {
        this.empty = isEmpty;
        this.data = data;
    }

    public static fromEmpty<T>(): Maybe<T> {
        return new Maybe(true, null);
    }

    public static fromData<T>(data: T): Maybe<T> {
        return new Maybe(false, data);
    }

    public isEmpty(): boolean {
        return this.empty;
    }

    public getData(): T {
        return this.data;
    }
}
