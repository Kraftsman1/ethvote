import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('UserPublicKey')
export class UserPublicKey {

    @JsonProperty('name', String)
    name: string = null;

    @JsonProperty('publicKey', String)
    publicKey: string = null;

    @JsonProperty('id', Number)
    id: number = null;

    private keyFingerPrint = null;

    private canBeTrustee = false;

    private canParticipate = false;

    private positionInTable = 0;

    getName(): string {
        return this.name;
    }

    getPublicKey(): string {
        return this.publicKey;
    }

    getId(): number {
        return this.id;
    }

    getPublicKeyFingerprint(): string {
        return this.keyFingerPrint;
    }

    setPublicKeyFingerprint(fingerPrint: string): UserPublicKey {
        this.keyFingerPrint = fingerPrint;
        return this;
    }

    setCanParticipate(canParticipate: boolean): UserPublicKey {
        this.canParticipate = canParticipate;
        return this;
    }

    getCanParticipate(): boolean {
        return this.canParticipate;
    }

    setCanBeTrustee(canBeTrustee: boolean): UserPublicKey {
        this.canBeTrustee = canBeTrustee;
        return this;
    }

    getCanBeTrustee(): boolean {
        return this.canBeTrustee;
    }

    setTablePosition(positionInTable: number): UserPublicKey {
        this.positionInTable = positionInTable;
        return this;
    }

    getTablePosition(): number {
        return this.positionInTable;
    }

}
