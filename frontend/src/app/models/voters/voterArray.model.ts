import { JsonObject, JsonProperty } from 'json2typescript';
import { Voter } from './voter.model';

@JsonObject('VoterArray')
export class VoterArray {

    @JsonProperty('voters', [Voter])
    voters: Voter[] = new Array();

    getVoters(): Voter[] {
        return this.voters;
    }

    setVoters(voters: Voter[]): VoterArray {
        this.voters = voters;
        return this;
    }

}
