import {SingleSecretShareFromVoter} from './singleSecretShareFromVoter.model';


export class SecretSharesFromVoter {

    private secretShares: SingleSecretShareFromVoter[];

    constructor(private fromVoterId: number, private fromVoterName: string) {
        this.secretShares = new Array();
    }

    addSecretShare(share: SingleSecretShareFromVoter): SecretSharesFromVoter {
        this.secretShares.push(share);
        return this;
    }

}
