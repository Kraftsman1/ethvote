import { JsonProperty } from 'json2typescript';
import {SingleSecretShareForVoter} from './singleSecretShareForVoter';

export class SecretSharesForVoter {

    @JsonProperty('forVoterId', Number)
    private forVoterId: number = undefined;

    @JsonProperty('forVoterName', String)
    private forVoterName: string = undefined;

    @JsonProperty('forVoterPosition', Number)
    private forVoterPosition: number = undefined;

    @JsonProperty('secretShares', [SingleSecretShareForVoter])
    private secretShares: SingleSecretShareForVoter[] = undefined;

    public getForVoterId(): number {
        return this.forVoterId;
    }

    public getForVoterName(): string {
        return this.forVoterName;
    }

    public getForVoterPosition(): number {
        return this.forVoterPosition;
    }

    public getSecretShares(): SingleSecretShareForVoter[] {
        return this.secretShares;
    }

}
