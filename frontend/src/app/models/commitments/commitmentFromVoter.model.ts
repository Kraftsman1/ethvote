import {SingleCommitment} from './singleCommitment.model';
import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('CommitmentFromVoter')
export class CommitmentFromVoter {

    @JsonProperty('numberOfCommitments', Number)
    private numberOfCommitments: number = undefined;

    @JsonProperty('fromVoterPosition', Number)
    private fromVoterPosition: number = undefined;

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number = undefined;

    @JsonProperty('fromVoterName', String)
    private fromVoterName: string = undefined;

    @JsonProperty('commitmentsFromVoter', [SingleCommitment])
    private commitments: SingleCommitment[] = undefined;

    public setCommitments(commitments: SingleCommitment[]): CommitmentFromVoter {
        this.commitments = commitments;
        this.numberOfCommitments = this.commitments.length;
        return this;
    }

    public getFromVoterPosition(): Number {
        return this.fromVoterPosition;
    }

    public getFromVoterId(): number {
        return this.fromVoterId;
    }

    public getFromVoterName(): String {
        return this.fromVoterName;
    }

    public getCommitments(): SingleCommitment[] {
        return this.commitments;
    }

    public getNumberOfCommitments(): number {
        return this.numberOfCommitments;
    }

}
