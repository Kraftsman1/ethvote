import { JsonProperty, JsonObject } from 'json2typescript';
import {CommitmentFromVoter} from './commitmentFromVoter.model';

@JsonObject('Commitments')
export class Commitments {

    @JsonProperty('commitments', [CommitmentFromVoter])
    private commitments: CommitmentFromVoter[] = undefined;

    public getCommitments(): CommitmentFromVoter[] {
        return this.commitments;
    }

}
