import { JsonObject, JsonProperty } from 'json2typescript';
import { EligibleVoter } from '../votings/eligibleVoter.json.model';

@JsonObject('EligibleVoterWithKey')
export class EligibleVoterWithKey extends EligibleVoter {

    @JsonProperty('publicKey', String)
    private publicKey: string = null;

    getPublicKey(): string {
        return this.publicKey;
    }
}
