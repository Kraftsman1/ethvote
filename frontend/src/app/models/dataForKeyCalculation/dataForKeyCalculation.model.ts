import { JsonObject, JsonProperty } from 'json2typescript';
import { EligibleVoterWithKey } from './eligibleVoterWithKey.model';
import { VotingCalculationParameters } from '../votings/votingsCalculationParameters.json.model';

@JsonObject('DataForKeyCalculation')
export class DataForKeyCalculation {

    @JsonProperty('eligibleVoters', [EligibleVoterWithKey])
    private eligibleVoters: EligibleVoterWithKey[] = new Array();

    @JsonProperty('calculationParameters', VotingCalculationParameters)
    private calculationParameters: VotingCalculationParameters = null;

    @JsonProperty('securityThreshold', Number)
    private securityThreshold: number = undefined;

    @JsonProperty('numberOfEligibleVoters', Number)
    private numberOfEligibleVoters: number = undefined;

    getEligibleVoters(): EligibleVoterWithKey[] {
        return this.eligibleVoters;
    }

    getCalculationParameters(): VotingCalculationParameters {
        return this.calculationParameters;
    }

    getSecurityThreshold(): number {
        return this.securityThreshold;
    }

    getNumberOfEligibleVoters(): number {
        return this.numberOfEligibleVoters;
    }
}
