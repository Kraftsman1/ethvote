import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('VotingInfo')
export class VotingInfo {

    @JsonProperty('securityThreshold', Number)
    private securityThreshold: number = null;

    @JsonProperty('description', String)
    private description: string  = null;

    @JsonProperty('title', String)
    private title: string  = null;

    getSecurityThreshold(): number {
        return this.securityThreshold;
    }

    getDescription(): string {
        return this.description;
    }

    getTitle(): string {
        return this.title;
    }

}
