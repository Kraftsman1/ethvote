import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('Roles')
export class Roles {

    @JsonProperty('roles', [String])
    private roles: string[] = new Array();

    getRoles(): string[] {
        return this.roles;
    }
}
