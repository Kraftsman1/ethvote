import { JsonProperty, JsonObject } from 'json2typescript';
import {SingleVoterWithRoles} from './singleVoterWithRoles.model';

@JsonObject('VoterWithRoles')
export class VotersWithRoles {

    @JsonProperty('voters', [SingleVoterWithRoles])
    private votersWithRoles: SingleVoterWithRoles[] = new Array();

    getVoters(): SingleVoterWithRoles[] {
        return this.votersWithRoles;
    }
}
