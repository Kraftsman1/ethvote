import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SecretPartOfKey')
export class SecretPartOfKey {

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number = undefined;

    @JsonProperty('fromVoterName', String)
    private fromVoterName: string = undefined;

    @JsonProperty('secretPartOfKey', [String])
    private secretPartOfKey: string[] = undefined;

    public setSecretPartOfKey(secretPartOfKey: string[]): SecretPartOfKey {
        this.secretPartOfKey = secretPartOfKey;
        return this;
    }

    public getFromVoterId(): number {
        return this.fromVoterId;
    }

    public getFromVoterName(): string {
        return this.fromVoterName;
    }

    public getSecretPartOfKey(): string[] {
        return this.secretPartOfKey;
    }

}
