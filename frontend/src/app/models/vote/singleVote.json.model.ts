import {JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SingleVote')
export class SingleVote {

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number;

    @JsonProperty('fromVoterName', String)
    private fromVoterName: string;

    @JsonProperty('alpha', String)
    private alpha: string;

    @JsonProperty('beta', String)
    private beta: string;

    @JsonProperty('signature', String)
    private signature: string;

    constructor(fromVoterId: number, fromVoterName: string, alpha: string, beta: string, signature: string) {
        this.fromVoterId = fromVoterId;
        this.fromVoterName = fromVoterName;
        this.alpha = alpha;
        this.beta = beta;
        this.signature = signature;
    }

    public getFromVoterId(): number {
        return this.fromVoterId;
    }

    public getFromVoterName(): string {
        return this.fromVoterName;
    }

    public getAlpha(): string {
        return this.alpha;
    }

    public getBeta(): string {
        return this.beta;
    }

    public getSignature(): string {
        return this.signature;
    }
}
