import {JsonProperty, JsonObject } from 'json2typescript';
import {VoteNIZKProof} from './voteNIZKProofs';

@JsonObject('VoteNIZKProof')
export class AllVoteNIZKProofs {

    @JsonProperty('allProofs', [VoteNIZKProof])
    private proofs: VoteNIZKProof[] = undefined;

    public getAllZeroKnowledgeProofs(): VoteNIZKProof[] {
        return this.proofs;
    }

}
