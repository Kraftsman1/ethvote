import { JsonObject, JsonProperty } from 'json2typescript';
import {SingleVoterProgress} from './singleVoterProgress';

@JsonObject('VotingProgress')
export class VotingProgress {

    @JsonProperty('votersProgress', [SingleVoterProgress])
    private votersProgress: SingleVoterProgress[] = undefined;

    @JsonProperty('phaseNumber', Number)
    private phaseNumber: number = null;

    @JsonProperty('securityThreshold', Number)
    private securityThreshold: number = null;

    @JsonProperty('transactionId', String)
    private transactionId: string = null;

    getPhaseNumber(): Number {
        return this.phaseNumber;
    }

    getSecurityThreshold(): number {
        return this.securityThreshold;
    }

    getVotersProgress(): SingleVoterProgress[] {
        return this.votersProgress;
    }

    getTransactionId(): string {
        return this.transactionId;
    }
}
