import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('VotingCalculationParameters')
export class VotingCalculationParameters {

    @JsonProperty('primeP', String)
    primeP: string = null;

    @JsonProperty('primeQ', String)
    primeQ: string = null;

    @JsonProperty('generatorG', String)
    generatorG: string = null;

    getPrimeP(): string {
        return this.primeP;
    }

    setPrimeP(primeP: string): VotingCalculationParameters {
        this.primeP = primeP;
        return this;
    }

    getPrimeQ(): string {
        return this.primeQ;
    }

    setPrimeQ(primeQ: string): VotingCalculationParameters {
        this.primeQ = primeQ;
        return this;
    }

    getGeneratorG(): string {
        return this.generatorG;
    }

    setGeneratorG(generatorG: string): VotingCalculationParameters {
        this.generatorG = generatorG;
        return this;
    }

}
