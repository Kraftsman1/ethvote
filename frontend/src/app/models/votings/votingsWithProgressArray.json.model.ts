import {VotingOverview} from './votingOverview.json.model';
import {VotingVoterProgress} from './votingVoterProgress';
import { JsonObject, JsonProperty } from 'json2typescript';
import {VotingOfficials} from './votingOfficials.json.model';

@JsonObject('VotingsWithProgressArray')
export class VotingsWithProgressArray {

    @JsonProperty('votings', [VotingOverview])
    votings: VotingOverview[] = new Array();

    @JsonProperty('votingsVoterProgress', [VotingVoterProgress])
    votingsVoterProgress: VotingVoterProgress[] = new Array();

    @JsonProperty('votingOfficials', [VotingOfficials])
    votingOfficials: VotingOfficials[] = new Array();

    getVotings(): VotingOverview[] {
        return this.votings;
    }

    getVotingsVoterProgress(): VotingVoterProgress[] {
        return this.votingsVoterProgress;
    }

    getVotingOfficials(): VotingOfficials[] {
        return this.votingOfficials;
    }

}
