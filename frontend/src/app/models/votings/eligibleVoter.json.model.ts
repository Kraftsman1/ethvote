import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('VotingVoterPublicKey')
export class EligibleVoter {

    @JsonProperty('id', Number)
    id: number = null;

    @JsonProperty('name', String)
    name: string = null;

    @JsonProperty('position', Number)
    position: number = null;

    @JsonProperty('isTrustee', Boolean)
    isTrustee: boolean = null;

    @JsonProperty('canVote', Boolean)
    canVote: boolean = null;

    getId(): number {
        return this.id;
    }

    setId(id: number): EligibleVoter {
        this.id = id;
        return this;
    }

    getName(): string {
        return this.name;
    }

    setName(name: string): EligibleVoter {
        this.name = name;
        return this;
    }

    getPosition(): number {
        return this.position;
    }

    setPosition(position: number): EligibleVoter {
        this.position = position;
        return this;
    }

    getIsTrustee() {
        return this.isTrustee;
    }

    setIsTrustee(isTrustee: boolean): EligibleVoter {
        this.isTrustee = isTrustee;
        return this;
    }

    getCanVote() {
        return this.canVote;
    }

    setCanVote(canVote: boolean): EligibleVoter {
        this.canVote = canVote;
        return this;
    }

}
