import { JsonProperty, JsonObject } from 'json2typescript';

@JsonObject('SingleDecryptionFactor')
export class SingleDecryptionFactor {

    @JsonProperty('fromVoterId', Number)
    private fromVoterId: number = undefined;

    @JsonProperty('fromVoterName', String)
    private fromVoterName: string = undefined;

    @JsonProperty('fromVoterPosition', Number)
    private fromVoterPosition: number = undefined;

    @JsonProperty('decryptionFactor', String)
    private decryptionFactor: string = undefined;

    @JsonProperty('signature', String)
    private signature: string = undefined;

    public setDecryptionFactor(decryptionFactor: string): SingleDecryptionFactor {
        this.decryptionFactor = decryptionFactor;
        return this;
    }

    public getFromVoterId(): number {
        return this.fromVoterId;
    }

    public getFromVoterName(): string {
        return this.fromVoterName;
    }

    public getFromVoterPosition(): number {
        return this.fromVoterPosition;
    }

    public getDecryptionFactor(): string {
        return this.decryptionFactor;
    }

    public getSignature(): string {
        return this.signature;
    }

    public setSignature(signature: string): SingleDecryptionFactor {
        this.signature = signature;
        return this;
    }
}
