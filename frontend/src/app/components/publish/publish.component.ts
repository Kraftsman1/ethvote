import { Component, OnInit } from '@angular/core';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {Maybe} from '../../utils/maybe.utils';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';
import {MatDialog, MatDialogConfig, MatTableDataSource} from '@angular/material';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {VotingInformationComponent} from '../dialogs/votingInformation/votingInformation.component';
import {ViewSummaryComponent} from '../dialogs/viewSummary/viewSummary.component';

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent implements OnInit {

  displayedVotings: Array<VotingOverview> = [];
  dataSource: MatTableDataSource<VotingOverview>;
  displayedColumns: string[] = ['Title', 'TransactionId', 'ViewSummary'];

  constructor(private votingsDataService: VotingsDataService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.votingsDataService.getCurrentVotings(4).subscribe(data => this.onVotingsDataReceived(data));
  }

  onVotingsDataReceived(votings: Maybe<VotingsWithProgressArray>) {
    this.displayedVotings = votings.getData().getVotings();
    this.dataSource.data = votings.getData().getVotings();
  }

  onViewSummaryClicked(votingOverview: VotingOverview) {
    const votingId = votingOverview.getId();
    this.votingsDataService.getVotingSummary(votingId).subscribe(text => this.onVotingSummaryReceived(text, votingOverview));
  }

  onVotingSummaryReceived(votingSummary: Maybe<string>, votingOverview: VotingOverview) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '90vw';
    dialogConfig.maxHeight = '80vh';
    dialogConfig.data = {
      votingSummary: votingSummary.getData(),
      votingOverview: votingOverview
    };

    this.dialog.open(ViewSummaryComponent, dialogConfig);
  }

  showMoreInformation(voting: VotingOverview) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '90vw';
    dialogConfig.maxHeight = '80vh';
    dialogConfig.data = {
      voting: voting
    };

    this.dialog.open(VotingInformationComponent, dialogConfig);

  }

}
