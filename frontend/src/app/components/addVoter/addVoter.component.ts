import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MatTableDataSource} from '@angular/material';
import {VoterToCreate} from '../../data/voterToCreate';
import {VoterParserService} from '../../services/voterParser/voterParser.service';
import {VotersService} from '../../services/communication/voters/voters.service';
import {VoterArray} from '../../models/voters/voterArray.model';
import {Voter} from '../../models/voters/voter.model';
import {ResponseFromHttpRequest} from '../../data/responseFromHttpRequest';
import {AddVoterFailedComponent} from '../dialogs/addVoterFailed/addVoterFailed.component';
import {RolesService} from '../../services/communication/roles/roles.service';
import {forkJoin, from} from 'rxjs';
import {NewVoterLine} from '../../data/newVoterLine';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';

@Component({
  selector: 'app-add-voter',
  templateUrl: './addVoter.component.html',
  styleUrls: ['./addVoter.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddVoterComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<AddVoterComponent>,
              private voterParserService: VoterParserService,
              private voterService: VotersService,
              private roleService: RolesService,
              private dialog: MatDialog,
              private currentUserService: CurrentUserService,
              private snackBar: MatSnackBar) {
  }

  displayedColumns: string[];

  @ViewChild('editor') editor;
  data = {};

  content = '';
  codeMirror = null;

  voters: VoterToCreate[] = [];

  dataSource: MatTableDataSource<NewVoterLine> = new MatTableDataSource([]);

  ngOnInit() {
    this.dataSource.data.push(new NewVoterLine());

    const isUserAdmin = this.currentUserService.isCurrentUserAdmin();
    this.voterParserService.setWithRoles(isUserAdmin);
    if (isUserAdmin) {
      this.displayedColumns = ['Name', 'Email', 'Voting_Administrator', 'Registrar', 'Administrator', 'Delete'];
    }
    else {
      this.displayedColumns = ['Name', 'Email', 'Delete'];
    }
  }

  ngAfterViewInit() {
    this.codeMirror = this.editor.codeMirror;
    this.codeMirror.on('change', (obj, data) => {
      this.onEditorChange(obj, data);
    });

    this.dataSource.data[0].setCodeMirror(this.codeMirror).setParser(this.voterParserService);
  }

  onTextInputChange(event, index: number) {
    const newValue = event.srcElement.value;
    this.dataSource.data[index].setName(newValue);
    this.dataSource.data[index].update(index);
  }

  onEmailInputChange(event, index: number) {
    const newValue = event.srcElement.value;
    this.dataSource.data[index].setEmail(newValue);
    this.dataSource.data[index].update(index);
  }

  onAdminChange(event, index: number) {
    this.dataSource.data[index].setAdmin(event.checked);
    this.dataSource.data[index].update(index);
  }

  onVotingAdminChange(event, index: number) {
    this.dataSource.data[index].setVotingAdmin(event.checked);
    this.dataSource.data[index].update(index);
  }

  onRegistrarChange(event, index: number) {
    this.dataSource.data[index].setRegistrar(event.checked);
    this.dataSource.data[index].update(index);
  }

  deleteVoter(index: number) {
    if (this.dataSource.data.length === 1) {
      this.showErrorSnackBar('Cannot delete last voter from table.');
      return;
    }

    const lineHandle = this.codeMirror.getLineHandle(index);

    if (index === 0) {
      this.codeMirror.replaceRange('', {line: index, ch: 0}, {line: index + 1, ch: 0});
    } else {
      const beforeLineLength = this.codeMirror.getLine(index - 1).length;
      const currentLineLength = this.codeMirror.getLine(index).length;

      this.codeMirror.replaceRange('', {line: index - 1, ch: beforeLineLength}, {line: index, ch: currentLineLength});
    }

    this.dataSource.data.splice(index, 1);
    this.updateTableView();
  }

  addVoter() {
    this.codeMirror.replaceRange('\n', {line: Infinity});
    const newVoterLine = new NewVoterLine().setCodeMirror(this.codeMirror).setParser(this.voterParserService);
    this.dataSource.data.push(newVoterLine);

    this.updateTableView();
  }

  updateTableView() {
    this.dataSource.data = this.dataSource.data;
  }

  onEditorChange(obj, data) {
    if (data.origin === undefined) {
      return;
    }

    const fromLine = data.from.line;

    const removedText = data.removed;
    const newText = data.text;

    const numberOfRemovedLines = removedText.length;
    const numberOfInsertedLines = newText.length;

    const oldLength = this.dataSource.data.length;

    const unchangedStartLines = this.dataSource.data.slice(0, fromLine);
    const unchangedEndLines = this.dataSource.data.slice(fromLine + numberOfRemovedLines, oldLength);

    const newLines = [];
    for (let i = 0; i < numberOfInsertedLines; i++) {
      const newVoterLine = new NewVoterLine().setCodeMirror(this.codeMirror).setParser(this.voterParserService);
      newVoterLine.parseFromCodeMirror(fromLine + i);
      newLines.push(newVoterLine);
    }

    const newData = unchangedStartLines.concat(newLines).concat(unchangedEndLines);
    this.dataSource.data = newData;
    this.updateTableView();
  }

  public save() {
    let isDataValid = true;

    for (const voterLine of this.dataSource.data) {
      voterLine.setCheckedForValidity(true);
      isDataValid = isDataValid && voterLine.isValid();
    }

    if (isDataValid) {
      const voterArray: VoterArray = new VoterArray();
      voterArray.setVoters(this.createVoterArray());
      this.voterService.saveVoters(voterArray, true).subscribe(response => this.onDataFromSavedReceived(response));
    } else {
      this.showErrorSnackBar('There is some invalid data.');
    }
  }

  createVoterArray(): Voter[] {
    const voters: Voter[] = [];

    for (const newVoterLines of this.dataSource.data) {
      const newVoter = new Voter()
          .setName(newVoterLines.getName())
          .setEmail(newVoterLines.getEmail());

      voters.push(newVoter);
    }

    return voters;
  }

  onDataFromSavedReceived(response: ResponseFromHttpRequest<VoterArray>) {
    if (response.getStatusCode() === 201) {
      this.setRoles(response.getResponse().getData());
    } else if (response.getStatusCode() === 409) {
      const dialogConfig = new MatDialogConfig();

      dialogConfig.height = '400px';
      dialogConfig.width = '600px';
      dialogConfig.data = {
        alreadyExistingVoters: response.getResponse().getData()
      };

      const dialogRef = this.dialog.open(AddVoterFailedComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        this.onDialogClosed(result, response.getResponse().getData());
      });
    }
  }

  private setRoles(savedVoters: VoterArray) {
    const voterNameToIdMap = new Map<string, number>();

    for (const voter of savedVoters.getVoters()) {
      voterNameToIdMap.set(voter.getName(), voter.getId());
    }

    const observablesFromRequests = [];

    for (const newVoterLines of this.dataSource.data) {
      const name = newVoterLines.getName();
      const id = voterNameToIdMap.get(name);

      if (newVoterLines.getIsAdmin()) {
        observablesFromRequests.push(this.roleService.addRole(id, 'Administrator'));
      }
      if (newVoterLines.getIsVotingAdmin()) {
        observablesFromRequests.push(this.roleService.addRole(id, 'Voting_Administrator'));
      }
      if (newVoterLines.getIsRegistrar()) {
        observablesFromRequests.push(this.roleService.addRole(id, 'Registrar'));
      }

      const numberOfRoleChanged = observablesFromRequests.length;
      if (numberOfRoleChanged === 0) {
        this.showSuccessfulSnackBar('Successfully added voters.');
        this.dialogRef.close();
        return;
      }

      forkJoin(observablesFromRequests).subscribe(data => this.onAddRoleRequestsReturned(data, numberOfRoleChanged));
    }
  }

  onAddRoleRequestsReturned(responses: boolean[], numberOfExpectedRequests: number) {
    let result = true;
    if (responses.length !== numberOfExpectedRequests) {
      result = false;
    }
    for (const successful of responses) {
      result = result && successful;
    }

    if (result) {
      this.showSuccessfulSnackBar('Successfully added voters with their roles.');
      this.dialogRef.close();
    }
    else {
      this.showErrorSnackBar('Adding voters failed.');
    }
  }

  onDialogClosed(value: string, votersToDelete: VoterArray) {
    if (value !== 'delete') {
      return;
    }
    const voterNamesToDelete = [];
    for (const voter of votersToDelete.getVoters()) {
      voterNamesToDelete.push(voter.getName().toLowerCase());
    }

    const updatedVoters = [];
    const newContent = [];

    for (const voterLines of this.dataSource.data) {
      if (voterNamesToDelete.indexOf(voterLines.getName().toLowerCase()) === -1) {
        updatedVoters.push(voterLines);
        newContent.push(voterLines.createTextRepresentation());
      }
    }

    this.dataSource.data = updatedVoters;

    this.codeMirror.setValue(newContent.join('\n'));
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
