import {Component, OnInit} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {VotingsWithProgressArray} from '../../models/votings/votingsWithProgressArray.json.model';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {MatDialog, MatDialogConfig, MatSnackBar, MatTableDataSource} from '@angular/material';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {VotingVoterProgress} from '../../models/votings/votingVoterProgress';
import {forkJoin, Observable, of} from 'rxjs';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';
import {Votes} from '../../models/vote/votes.json.model';
import {SingleDecryptionFactor} from '../../models/decryptionFactors/singleDecryptionFactor.model';
import {VoteService} from '../../services/communication/vote/vote.service';
import {SecretSharesService} from '../../services/communication/secretShares/secretShares.service';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';
import {DecryptionFactorsService} from '../../services/communication/decryptionFactors/decryptionFactors.service';
import {LocalStorageService} from '../../services/localStorage/localStorage.service';
import {KeysService} from '../../services/communication/keys/keys.service';
import {VoteHelperService} from '../../services/vote/voteHelper.service';
import {CryptoService} from '../../services/crypto/crypto.service';
import {DecryptionNIZKProofService} from '../../services/communication/decryptionNIZKProof/decryptionNIZKProofs.service';
import {Pair} from '../../utils/pair.utils';
import {ZeroKnowledgeProofHelperService} from '../../services/zeroKnowledgeProofs/zeroKnowledgeProofHelper.service';
import {VoteNIZKProofService} from '../../services/communication/voteNIZKProofs/voteNIZKProofs.service';
import {AllVoteNIZKProofs} from '../../models/voteNIZKProof/allVoteNIZKProofs';
import {VotingOption} from '../../models/votings/votingOption.json.model';
import {CommitmentsService} from '../../services/communication/commitments/commitments.service';
import {Commitments} from '../../models/commitments/commitments.model';
import {VotingInformationComponent} from '../dialogs/votingInformation/votingInformation.component';
import {flatMap} from 'rxjs/operators';
import {PrivateKeyService} from '../../services/privateKey/privateKey.service';
import {DecryptionNIZKProof} from '../../models/decryptionNIZKProof/decryptionNIZKProof';
import {VotingOfficials} from '../../models/votings/votingOfficials.json.model';

class CalculationData {
  dataForKeyCalculation: DataForKeyCalculation;
  currentVoterId: number;
  votingId: number;
  privateKey: string;
  commitments: Commitments;
  secretShares: SecretSharesForVoter;
  votes: Votes;
  proofs: AllVoteNIZKProofs;
  votingOptions: VotingOption[];

  decryptionFactor: SingleDecryptionFactor;
  decryptionNIZKProof: DecryptionNIZKProof;

  successful = true;
  errorMessage: string;
}

@Component({
  selector: 'app-decryption',
  templateUrl: './decryption.component.html',
  styleUrls: ['./decryption.component.css']
})
export class DecryptionComponent implements OnInit {

  showSpinner = false;
  spinnerDescription = 'calculating decryption factor ...';

  statusMap: { [id: number]: VotingVoterProgress; } = {};
  votingOfficialsMap: { [id: number]: VotingOfficials; } = {};
  textMap: { [id: number]: String; } = {};

  displayedColumns: string[] = ['Title', 'Description', 'Decrypt'];

  displayedVotings: Array<VotingOverview> = [];
  dataSource: MatTableDataSource<VotingOverview>;

  constructor(private votingsDataService: VotingsDataService,
              private voteService: VoteService,
              private secretSharesService: SecretSharesService,
              private currentUserService: CurrentUserService,
              private decryptionFactorService: DecryptionFactorsService,
              private decryptionNIZKProofService: DecryptionNIZKProofService,
              private storageService: LocalStorageService,
              private keyService: KeysService,
              private voteNIZKProofService: VoteNIZKProofService,
              private voteHelperService: VoteHelperService,
              private commitmentsService: CommitmentsService,
              private privateKeyService: PrivateKeyService,
              private zeroKnowledgeProofHelperService: ZeroKnowledgeProofHelperService,
              private cryptoService: CryptoService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.votingsDataService.getCurrentVotings(3).subscribe(data => this.onVotingsDataReceived(data));
  }

  onVotingsDataReceived(votings: Maybe<VotingsWithProgressArray>) {
    this.displayedVotings = votings.getData().getVotings();
    this.dataSource.data = votings.getData().getVotings();

    for (const singleVoting of this.displayedVotings) {
      let text: String = singleVoting.getDescription();
      text += '\n\nPossible choices:\n\n';

      const optionTextArray = singleVoting.getOptions().map(v => v.getOptionName());
      const optionText = optionTextArray.join(' | ');
      text += optionText;

      this.textMap[singleVoting.getId().valueOf()] = text;
    }

    this.evaluateVotingsProgressData(votings.getData().getVotingsVoterProgress());
    this.evaluateVotingOfficialsData(votings.getData().getVotingOfficials());
  }

  evaluateVotingsProgressData(progress: VotingVoterProgress[]) {
    for (const singleProgress of progress) {
      this.statusMap[singleProgress.getVotingId().valueOf()] = singleProgress;
    }
  }

  evaluateVotingOfficialsData(votingOfficials: VotingOfficials[]) {
    for (const votingOffical of votingOfficials) {
      this.votingOfficialsMap[votingOffical.getVotingId().valueOf()] = votingOffical;
    }
  }

  onDecryptClicked(votingId: number, votingOptions: VotingOption[]) {

    this.showSpinner = true;
    this.spinnerDescription = 'loading data ...';

    const calcData = new CalculationData();
    calcData.currentVoterId = this.currentUserService.getUserId().getData();
    calcData.votingId = votingId;
    calcData.votingOptions = votingOptions;

    const votingsResult = this.voteService.getDataForKeyCalculation(votingId)
        .pipe(flatMap(res => this.getPrivateKey(res, calcData)))
        .pipe(flatMap(res => this.loadDataForDecyrption(res)))
        .pipe(flatMap(res => this.calculateDecryptionFactorAndProof(res)))
        .pipe(flatMap(res => this.sendDecyptionFactorAndProof(res)));

    votingsResult.subscribe(data => this.showFeedback(data));
  }

  private showFeedback(maybeCalculationsData: Maybe<CalculationData>) {
    this.showSpinner = false;

    if (maybeCalculationsData.isEmpty()) {
      this.showErrorSnackBar('Failure');
      return;
    }

    const calculationData = maybeCalculationsData.getData();
    if (calculationData.successful) {
      this.showSuccessfulSnackBar('Decryption factor was successfully added.');
    }
    else {
      this.showErrorSnackBar(calculationData.errorMessage);
    }

    this.votingsDataService.getCurrentVotings(3).subscribe(data => this.onVotingsDataReceived(data));
  }

  private getPrivateKey(maybeKeyCalculationsData: Maybe<DataForKeyCalculation>, retData): Observable<Maybe<CalculationData>> {
    if (maybeKeyCalculationsData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Loading key ...';

    retData.dataForKeyCalculation = maybeKeyCalculationsData.getData();

    const ret = this.privateKeyService.getPrivateKey(retData.dataForKeyCalculation, retData.currentVoterId).pipe(flatMap(res => this.aggregate(retData, res)));
    return ret;
  }

  private aggregate(data: CalculationData, maybePrivateKey: Maybe<string>): Observable<Maybe<CalculationData>> {
    if (maybePrivateKey.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Loading shares ...';

    data.privateKey = maybePrivateKey.getData();
    return of(Maybe.fromData(data));
  }

  private loadDataForDecyrption(maybeCalculationsData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeCalculationsData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    const calculationData = maybeCalculationsData.getData();
    const votingId = calculationData.votingId;
    const currentVoterId = calculationData.currentVoterId;

    const commitmentsObservable = this.commitmentsService.getAllCommitments(votingId);
    const secretShareObservable = this.secretSharesService.getAllSharesForVoter(votingId, currentVoterId);
    const votesObservable = this.voteService.getVotes(votingId);
    const proofsObservable = this.voteNIZKProofService.getNITKProofs(votingId);

    return forkJoin(
        commitmentsObservable,
        secretShareObservable,
        votesObservable,
        proofsObservable,
    ).pipe(flatMap(data => this.onDataForDecryptionLoaded(data, calculationData)));
  }

  private onDataForDecryptionLoaded(responses: any[], calculationData: CalculationData): Observable<Maybe<CalculationData>> {
    const maybeCommitments: Maybe<Commitments> = responses[0];
    const maybeSecretShares: Maybe<SecretSharesForVoter> = responses[1];
    const maybeVotes: Maybe<Votes> = responses[2];
    const maybeProofs: Maybe<AllVoteNIZKProofs> = responses[3];

    if (maybeCommitments.isEmpty() || maybeSecretShares.isEmpty() || maybeVotes.isEmpty() || maybeProofs.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Calculating decryption factors ...';

    calculationData.commitments = maybeCommitments.getData();
    calculationData.secretShares = maybeSecretShares.getData();
    calculationData.votes = maybeVotes.getData();
    calculationData.proofs = maybeProofs.getData();

    return of(Maybe.fromData(calculationData));
  }

  private calculateDecryptionFactorAndProof(maybeCalculationsData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeCalculationsData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    const calculationData = maybeCalculationsData.getData();

    const publicKeyToEncryptVote = this.voteHelperService.calculatePublicKey(
        calculationData.dataForKeyCalculation,
        calculationData.commitments
    );

    const decryptionFactorValue = this.voteHelperService.calculateDecryptionFactor(
        calculationData.dataForKeyCalculation,
        calculationData.secretShares,
        calculationData.votes,
        calculationData.privateKey
    );

    const decryptionFactor = new SingleDecryptionFactor().setDecryptionFactor(decryptionFactorValue);

    const messageToSignDecryptionFactor = decryptionFactor.getFromVoterPosition() + ',' + decryptionFactor.getDecryptionFactor();
    const signatureDecyrptionFactor = this.cryptoService.signSHA256(calculationData.privateKey, messageToSignDecryptionFactor).getData();
    decryptionFactor.setSignature(signatureDecyrptionFactor);

    const decryptionNIZKProof = this.zeroKnowledgeProofHelperService.calculateDecryptionFactorZeroKnowledgeProof(
        calculationData.dataForKeyCalculation.getCalculationParameters(),
        calculationData.secretShares,
        calculationData.votes,
        calculationData.privateKey
    );

    const messageToSignProof = decryptionNIZKProof.getA() + ',' + decryptionNIZKProof.getB() + ',' + decryptionNIZKProof.getR();
    const signatureProof = this.cryptoService.signSHA256(calculationData.privateKey, messageToSignProof).getData();
    decryptionNIZKProof.setSignature(signatureProof);

    const invalidVotes = this.zeroKnowledgeProofHelperService.verifyAllProofs(
        calculationData.dataForKeyCalculation.getCalculationParameters(),
        calculationData.votes,
        calculationData.proofs,
        calculationData.votingOptions,
        publicKeyToEncryptVote);

    if (invalidVotes.length !== 0) {
      // TODO: show invalid dialog
    }

    calculationData.decryptionFactor = decryptionFactor;
    calculationData.decryptionNIZKProof = decryptionNIZKProof;
    return of(Maybe.fromData(calculationData));
  }

  private sendDecyptionFactorAndProof(maybeCalculationsData: Maybe<CalculationData>): Observable<Maybe<CalculationData>> {
    if (maybeCalculationsData.isEmpty()) {
      return of(Maybe.fromEmpty());
    }

    this.spinnerDescription = 'Sending decryption factors ...';

    const calculationData = maybeCalculationsData.getData();

    const votingId = calculationData.votingId;
    const currentVoterId = calculationData.currentVoterId;
    const decryptionFactor = calculationData.decryptionFactor;
    const decryptionNIZKProof = calculationData.decryptionNIZKProof;

    const decryptionFactorObservable = this.decryptionFactorService.storeDecryptionFactor(votingId, currentVoterId, decryptionFactor);
    const decryptionNIZKProofObservable = this.decryptionNIZKProofService.sendNIZKProofs(votingId, currentVoterId, decryptionNIZKProof);

    return forkJoin(
        decryptionFactorObservable,
        decryptionNIZKProofObservable
    ).pipe(flatMap(res => this.onDecryptionFactorStored(res, calculationData)));
  }

  onDecryptionFactorStored(data, calc: CalculationData): Observable<Maybe<CalculationData>> {
    const decryptionResponse: Pair<boolean, string> = data[0];
    const proofResponse: Pair<boolean, string> = data[1];

    this.spinnerDescription = 'Checking result ...';

    if (!decryptionResponse.getFirst()) {
      calc.successful = false;
      calc.errorMessage = decryptionResponse.getSecond();
    }
    else if (!proofResponse.getFirst()) {
      calc.successful = false;
      calc.errorMessage = proofResponse.getSecond();
    }
    else {
      calc.successful = true;
    }

    return of(Maybe.fromData(calc));
  }

  showMoreInformation(voting: VotingOverview) {
    const dialogConfig = new MatDialogConfig();

    const votingOfficials: VotingOfficials = this.votingOfficialsMap[voting.getId().valueOf()];

    dialogConfig.width = '90vw';
    dialogConfig.maxHeight = '80vh';
    dialogConfig.data = {
      voting: voting,
      votingOfficials: votingOfficials
    };

    this.dialog.open(VotingInformationComponent, dialogConfig);

  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
