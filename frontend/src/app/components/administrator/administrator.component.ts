import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/communication/authentication/authentication.service';
import {MatSnackBar} from '@angular/material';
import {LocalStorageService} from '../../services/localStorage/localStorage.service';
import {AuthenticatedVoterService} from '../../services/communication/authenticatedVoter/authenticatedVoter.service';
import {Maybe} from '../../utils/maybe.utils';
import {AuthenticatedVoter} from '../../models/authenticatedVoter/authenticatedVoter.json.model';
import {CurrentUserService} from '../../services/currentUser/currentUser.service';
import {Router} from '@angular/router';
import {EnvironmentService} from '../../services/environment/environment.service';
import {SubscriberService} from '../../services/subscription/subscriber.service';

@Component({
  selector: 'app-admin',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {

  isCurrentUserLoggedIn: boolean;
  isCurrentUserAdmin: boolean;
  isCurrentUserVotingAdministrator: boolean;
  isCurrentUserRegistrar: boolean;

  isProdEnvironment: boolean;
  isCurrentVoterRegistered: boolean;

  currentVoterId: string;
  currentVoterName: string;

  loginFail = false;

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(private currentUserService: CurrentUserService,
              private authenticationService: AuthenticationService,
              private authenticatedVoterService: AuthenticatedVoterService,
              private storageService: LocalStorageService,
              private router: Router,
              private environmentService: EnvironmentService,
              private subscriberService: SubscriberService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  get username(): string {
    return this.loginForm.get('username').value;
  }

  get password(): string {
    return this.loginForm.get('password').value;
  }

  get valid(): boolean {
    return this.username !== '' && this.password !== '';
  }

  onSubmit() {
    this.authenticationService.authenticate(this.username, this.password).subscribe(maybeUser => {
      if (maybeUser.isEmpty()) {
        this.loginFail = true;
        this.showErrorSnackBar('Login failed.');
      } else {
        this.loginFail = false;
        this.storageService.upsert('token', maybeUser.getData().getToken());

        this.subscriberService.notifyTokenChanged();

        this.updateCurrentVoter();
        this.authenticatedVoterService.getAuthenticatedVoter().subscribe(data => this.onAuthenticatedVoterGet(data));
        this.showSuccessfulSnackBar('Login succeeded.');
        this.router.navigate(['/home']);
      }

    });
  }

  onAuthenticatedVoterGet(authenticatedVoter: Maybe<AuthenticatedVoter>) {
    this.isCurrentVoterRegistered = (!authenticatedVoter.isEmpty() && authenticatedVoter.getData().getIsRegistered());
  }

  updateCurrentVoter() {
    this.isCurrentUserLoggedIn = this.currentUserService.isCurrentUserLoggedIn();
    this.isCurrentUserAdmin = this.currentUserService.isCurrentUserAdmin();
    this.isCurrentUserVotingAdministrator = this.currentUserService.isCurrentUserVotingAdministrator();
    this.isCurrentUserRegistrar = this.currentUserService.isCurrentUserRegistrar();
    this.currentVoterName = this.currentUserService.getVoterName().getData();
    this.currentVoterId = this.currentUserService.getVoterName().getData();
    this.isProdEnvironment = this.environmentService.isProd();
    this.isCurrentVoterRegistered = this.currentUserService.isCurrentUserRegistered();
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
