import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {VotingOverview} from '../../../models/votings/votingOverview.json.model';
import {CryptoService} from '../../../services/crypto/crypto.service';
import {CurrentUserService} from '../../../services/currentUser/currentUser.service';
import {VotingSummaryJsonParser} from '../../../services/jsonParser/votingSummaryJsonParser.service';
import {VotingSummary} from '../../../models/votingSummary/votingSummary.json.model';
import {VotingCalculationParameters} from '../../../models/votings/votingsCalculationParameters.json.model';
import {SingleVote} from '../../../models/vote/singleVote.json.model';
import {SingleDecryptionFactor} from '../../../models/decryptionFactors/singleDecryptionFactor.model';
import {VoteHelperService} from '../../../services/vote/voteHelper.service';

@Component({
  selector: 'app-view-summary',
  templateUrl: './viewSummary.component.html',
  styleUrls: ['./viewSummary.component.css']
})
export class ViewSummaryComponent implements OnInit {

  votingSummary: string;
  prettyIndentedSummary: string;
  jsonObject: any;
  votingOverview: VotingOverview;
  hashOfOverview: string;
  viewTransactionUrl: string;

  generatedKey: boolean;
  voted: boolean;
  decrypted: boolean;
  votingResult: number[];

  @ViewChild('editorPrettyIndented') editorPrettyIndented;
  @ViewChild('editorRawValue') editorRawValue;

  constructor(@Inject(MAT_DIALOG_DATA) private data: { votingSummary: string, votingOverview: VotingOverview },
              private currentUserService: CurrentUserService,
              private votingSummaryJsonParser: VotingSummaryJsonParser,
              private voteHelperService: VoteHelperService,
              cryptoService: CryptoService) {
    this.votingSummary = data.votingSummary;
    this.votingOverview = data.votingOverview;
    this.jsonObject = JSON.parse(this.votingSummary);
    this.prettyIndentedSummary = JSON.stringify(this.jsonObject, null, '\t');
    this.hashOfOverview = cryptoService.hashSHA256(this.votingSummary);
    this.viewTransactionUrl = 'https://ropsten.etherscan.io/tx/' + this.votingOverview.getTransactionId();

    const currentVoterId = this.currentUserService.getUserId().getData();

    const votingSummary: VotingSummary  = this.votingSummaryJsonParser.parseJson(this.votingSummary).getData();

    const indexOfVoterInKeyGeneration = votingSummary.getKeyGenerationPhase().map(s => s.getFromVoterId()).indexOf(currentVoterId);
    const indexOfVoterInVote = votingSummary.getVotePhase().map(s => s.getVote().getFromVoterId()).indexOf(currentVoterId);
    const indexOfVoterInDecryption = votingSummary.getDecryptionFactors().map(s => s.getDecryptionFactor().getFromVoterId()).indexOf(currentVoterId);

    this.generatedKey = (indexOfVoterInKeyGeneration !== -1);
    this.voted = (indexOfVoterInVote !== -1);
    this.decrypted = (indexOfVoterInDecryption !== -1);

    const votingCalculationParameters: VotingCalculationParameters = votingSummary.getMetaData().getVotingCalculationParameters();
    const allVotes: SingleVote[] = votingSummary.getVotePhase().map(v => v.getVote());
    const decryptionFactors: SingleDecryptionFactor[] = votingSummary.getDecryptionFactors().map(f => f.getDecryptionFactor());
    const numberOfOptions = this.votingOverview.getOptions().length;

    this.votingResult = this.voteHelperService.calculateVotingOutCome(votingCalculationParameters, allVotes, decryptionFactors, numberOfOptions);
  }

  ngOnInit() {
  }

  updateEditorView() {
    this.editorPrettyIndented.codeMirror.refresh();
    this.editorRawValue.codeMirror.refresh();
  }

}
