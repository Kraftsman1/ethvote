import {Component, Inject, OnInit} from '@angular/core';
import {CryptoService} from '../../../services/crypto/crypto.service';
import {AsymmetricKey} from '../../../data/asymmetricKey';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-upload-private-key',
  templateUrl: './uploadPrivateKey.component.html',
  styleUrls: ['./uploadPrivateKey.component.css']
})
export class UploadPrivateKeyComponent implements OnInit {

  publicKey = '';
  privateKey = '';

  fromPkiComponent: boolean;
  keyText: string;
  displayText: string;

  constructor(@Inject(MAT_DIALOG_DATA) private data: { publicKey: string , fromPKIComponent: boolean},
              public dialogRef: MatDialogRef<UploadPrivateKeyComponent>,
              private cryptoService: CryptoService,
              private snackBar: MatSnackBar) {
    this.publicKey = data.publicKey;
    this.fromPkiComponent = data.fromPKIComponent;

    if (this.fromPkiComponent) {
      this.keyText = 'Check key';
      this.displayText = 'You already specified your own key. If you are not sure you still have the private key you can enter it below and check it.';
    }
    else {
      this.keyText = 'Save';
      this.displayText = 'Below you can find the key you created for this voting. Please enter your private key into the input box below';
    }
  }

  ngOnInit() {
  }

  validateKey() {
    const possibleKeyPair = new AsymmetricKey(this.publicKey, this.privateKey);
    const isValid = this.cryptoService.validateKeyPair(possibleKeyPair);
    if (isValid) {
      this.showSuccess('Key is valid.');
      if (!this.fromPkiComponent) {
        this.dialogRef.close(this.privateKey);
      }
    }
    else {
      this.showFailure('Wrong private key.');
    }
  }

  onChangeKeyClicked() {
    this.dialogRef.close('generate new key');
  }

  private showSuccess(successMessage: string) {
    this.snackBar.open(successMessage, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showFailure(failureMessage: string) {
    this.snackBar.open(failureMessage, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
