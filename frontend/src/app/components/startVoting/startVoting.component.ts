import {Component, Inject, OnInit} from '@angular/core';
import {DateAdapter, MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {VotingsDataService} from '../../services/communication/votingsData/votingsData.service';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';
import {Maybe} from '../../utils/maybe.utils';

@Component({
  selector: 'app-start-voting',
  templateUrl: './startVoting.component.html',
  styleUrls: ['./startVoting.component.css']
})
export class StartVotingComponent implements OnInit {

  showSpinner = false;
  spinnerDescription = '';

  hours: String[] = new Array();
  minutes: String[] = new Array();
  seconds: String[] = new Array();
  sendEmailChecked = true;

  endHour;
  endMinute;
  endSecond;
  endDate;

  votingId: number;

  hasFixedEndDate = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: { votingId: number},
              private snackBar: MatSnackBar,
              private votingsDataService: VotingsDataService,
              private dateAdapter: DateAdapter<Date>,
              private dialogRef: MatDialogRef<StartVotingComponent>) {
    dateAdapter.setLocale('en-in');
    this.votingId = this.data.votingId;
  }

  ngOnInit() {
    for (let i = 0; i < 24; i++) {
      this.hours.push(this.toTime(i));
    }

    for (let i = 0; i < 60; i++) {
      this.minutes.push(this.toTime(i));
      this.seconds.push(this.toTime(i));
    }
  }

  startVoting(phaseNumber: number) {
    if (!this.hasFixedEndDate) {
      this.votingsDataService.setPhaseNumber(this.votingId, phaseNumber, this.sendEmailChecked).subscribe(ret => this.onSetPhaseNumberReturnMessage(ret));
      return;
    }

    this.showSpinner = true;

    if (this.sendEmailChecked) {
      this.spinnerDescription = 'Start voting and sending notification mail ...';
    }
    else {
      this.spinnerDescription = 'Start voting ...';
    }

    const dayDate = new Date(this.endDate);
    const endDate = new Date( dayDate.getFullYear(),
                              dayDate.getMonth(),
                              dayDate.getDate(),
                              this.endHour,
                              this.endMinute,
                              this.endSecond);

    const endTime = Number(endDate);

    if (Number.isNaN(endTime)) {
      this.showErrorSnackBar('You provided an invalid date.');
      return;
    }

    const currentDate = new Date();

    if (Number(currentDate) > endTime) {
      this.showErrorSnackBar('The date you specified is in the past.');
      return;
    }

    this.votingsDataService.setVotingEndTime(this.votingId, endTime).subscribe(retVoting => this.onTimeVotingFinishesSet(retVoting, endTime, phaseNumber));
  }

  onTimeVotingFinishesSet(saveVoting: Maybe<VotingOverview>, expectedSetTime: number, phaseNumber: number) {
    if (!saveVoting.isEmpty() && saveVoting.getData().getTimeVotingIsFinished() === expectedSetTime) {
      this.votingsDataService.setPhaseNumber(this.votingId, phaseNumber, this.sendEmailChecked).subscribe(ret => this.onSetPhaseNumberReturnMessage(ret));
      return;
    }

    this.showErrorSnackBar('Setting the end time failed.');
  }

  onSetPhaseNumberReturnMessage(ret: Maybe<VotingOverview>) {
    this.showSpinner = false;

    if (!ret.isEmpty()) {
      this.showSuccessfulSnackBar('Phase number was increased successfully.');
      this.dialogRef.close({voting: ret.getData(), phaseNumber: 2});
    }
    else {
      this.showErrorSnackBar('Failure increasing phase number.');
    }
  }

  onSetCurrentTime() {
    const date = new Date();

    this.endHour = this.toTime(date.getHours());
    this.endMinute = this.toTime(date.getMinutes());
    this.endSecond = this.toTime(date.getSeconds());

    this.endDate = date;
  }

  private toTime(time: number): string {
    if (time < 10) {
      return '0' + time;
    }
    else {
      return '' + time;
    }
  }

  private showSuccessfulSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['green-snackbar']
    });
  }

  private showErrorSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }

}
