import {Maybe} from '../utils/maybe.utils';

export class ResponseFromHttpRequest<T> {

    readonly data: Maybe<T>;
    readonly statusCode: number;

    constructor(data: Maybe<T>, statusCode: number) {
        this.data = data;
        this.statusCode = statusCode;
    }

    getResponse(): Maybe<T> {
        return this.data;
    }

    getStatusCode(): number {
        return this.statusCode;
    }
}
