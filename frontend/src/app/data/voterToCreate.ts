export class VoterToCreate {

    Administrator = false;
    Voting_Administrator = false;
    Name = '';
    EMail = '';
    Registrar = false;

    public getEmail(): string {
        return this.EMail;
    }

    public getName(): string {
        return this.Name;
    }

    public isAdministrator(): boolean {
        return this.Administrator;
    }

    public isVoting_Administrator(): boolean {
        return this.Voting_Administrator;
    }

    public isRegistrar(): boolean {
        return this.Registrar;
    }

    public setName(name: string): VoterToCreate {
        this.Name = name;
        return this;
    }

    setEmail(mail: string): VoterToCreate {
        this.EMail = mail;
        return this;
    }

    public setAdministrator(isAdmin: boolean): VoterToCreate {
        this.Administrator = isAdmin;
        return this;
    }

    public setVoting_Administrator(isVotingAdmin: boolean): VoterToCreate {
        this.Voting_Administrator = isVotingAdmin;
        return this;
    }

    public setRegistrar(isRegistrar: boolean): VoterToCreate {
        this.Registrar = isRegistrar;
        return this;
    }

}
