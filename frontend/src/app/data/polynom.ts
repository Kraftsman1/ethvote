export class Polynom {

    private coefficients: string[];

    constructor(coefficients: string[]) {
        this.coefficients = coefficients;
    }

    getCoefficients() {
        return this.coefficients;
    }
}
