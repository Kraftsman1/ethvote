export class CalculationParams {

    constructor(private p: string, private q: string, private g) {
    }

    getPrimeP(): string {
        return this.p;
    }

    getPrimeQ(): string {
        return this.q;
    }

    getGeneratorG(): string {
        return this.g;
    }
}
