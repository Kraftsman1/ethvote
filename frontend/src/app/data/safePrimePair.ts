export class SafePrimePair {

    readonly p: string;
    readonly q: string;

    constructor(p: string, q: string) {
        this.p = p;
        this.q = q;
    }

    getP() {
        return this.p;
    }

    getQ() {
        return this.q;
    }
}
