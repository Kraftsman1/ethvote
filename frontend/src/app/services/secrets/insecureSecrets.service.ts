import { Maybe } from './../../utils/maybe.utils';
import { Injectable } from '@angular/core';
import { SecretsService } from './secrets.service';

@Injectable()
export class InsecureSecretsService implements SecretsService {

    private map: Map<string, string>;

    constructor() {
        this.map = new Map();
    }

    saveSecret(key: string, secret: string) {
        this.map.set(key, secret);
    }

    getSecret(key: string): Maybe<string> {
        const secret = this.map.get(key);
        if (secret == null) {
            return Maybe.fromEmpty();
        }
        else {
            return Maybe.fromData(secret);
        }
    }

}
