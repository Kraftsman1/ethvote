import { Maybe } from 'src/app/utils/maybe.utils';
import { Injectable } from '@angular/core';

@Injectable()
export abstract class SecretsService {

    abstract saveSecret(key: string, secret: string);

    abstract getSecret(key: string): Maybe<string>;

}
