import {Injectable} from '@angular/core';
import {SafePrimePair} from '../../data/safePrimePair';
import {Maybe} from '../../utils/maybe.utils';
import {Observable} from 'rxjs';

@Injectable()
export abstract class PrimesService {

    abstract getNthPrime(position: number): number;

    abstract getSafePrimePair(): Observable<Maybe<SafePrimePair>>;
}
