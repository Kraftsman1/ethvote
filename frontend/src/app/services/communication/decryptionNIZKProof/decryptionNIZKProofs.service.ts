import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Pair} from '../../../utils/pair.utils';
import {VoteNIZKProof} from '../../../models/voteNIZKProof/voteNIZKProofs';
import {DecryptionNIZKProof} from '../../../models/decryptionNIZKProof/decryptionNIZKProof';

@Injectable()
export abstract class DecryptionNIZKProofService {

    abstract sendNIZKProofs(voteId: Number, voterId: Number, proofs: DecryptionNIZKProof): Observable<Pair<boolean, string>>;

}
