import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Pair} from '../../../utils/pair.utils';
import {DecryptionNIZKProof} from '../../../models/decryptionNIZKProof/decryptionNIZKProof';
import {DecryptionNIZKProofService} from './decryptionNIZKProofs.service';

@Injectable()
export class DecryptionNIZKProofServiceImpl implements DecryptionNIZKProofService {

    private startUrl = '/api/votings/';

    constructor(private http: HttpClient) {
    }

    sendNIZKProofs(voteId: Number, voterId: Number, proofs: DecryptionNIZKProof): Observable<Pair<boolean, string>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Pair<boolean, string>>;

        obsResponse = this.http.post<String>(this.startUrl + voteId + '/decryptionZeroKnowledgeProofs/voters/' + voterId, JSON.stringify(proofs), {
            observe: 'response'
        });

        obsReturn = obsResponse.pipe(
            map(data => this.checkIfNIZKProofWasSetSuccessfull(data)),
            catchError(err => this.handleNIZKProofSetError(err))
        );

        return obsReturn;
    }

    private checkIfNIZKProofWasSetSuccessfull(resp: HttpResponse<any>): Pair<boolean, string> {
        const successful = resp.status === 201;
        if (successful) {
            return new Pair<boolean, string>(true, 'Successful');
        }
        else {
            return new Pair<boolean, string>(false, 'Unknown failure');
        }
    }

    private handleNIZKProofSetError(error: HttpErrorResponse): Observable<Pair<boolean, string>> {
        return of(new Pair(false, error.error));
    }
}
