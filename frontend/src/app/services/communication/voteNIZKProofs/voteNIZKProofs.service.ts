import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Pair} from '../../../utils/pair.utils';
import {VoteNIZKProof} from '../../../models/voteNIZKProof/voteNIZKProofs';
import {AllVoteNIZKProofs} from '../../../models/voteNIZKProof/allVoteNIZKProofs';
import {Maybe} from '../../../utils/maybe.utils';

@Injectable()
export abstract class VoteNIZKProofService {

    abstract sendNIZKProofs(voteId: Number, voterId: Number, proofs: VoteNIZKProof): Observable<Pair<boolean, string>>;

    abstract getNITKProofs(voteId: Number): Observable<Maybe<AllVoteNIZKProofs>>;
}
