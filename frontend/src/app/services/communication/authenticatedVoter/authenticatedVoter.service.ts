import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Maybe} from '../../../utils/maybe.utils';
import {AuthenticatedVoter} from '../../../models/authenticatedVoter/authenticatedVoter.json.model';

@Injectable()
export abstract class AuthenticatedVoterService {

    abstract getAuthenticatedVoter(): Observable<Maybe<AuthenticatedVoter>>;
}
