import {Injectable} from '@angular/core';
import {AuthenticatedVoterService} from './authenticatedVoter.service';
import {Maybe} from '../../../utils/maybe.utils';
import {Observable, of} from 'rxjs';
import {AuthenticatedVoter} from '../../../models/authenticatedVoter/authenticatedVoter.json.model';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class AuthenticatedVoterServiceImpl implements AuthenticatedVoterService {

    private url = '/api/currentVoterToken';

    constructor(private http: HttpClient) {
    }

    getAuthenticatedVoter(): Observable<Maybe<AuthenticatedVoter>> {
        let response: Observable<HttpResponse<String>>;
        let obsAuthenticatedVoter: Observable<Maybe<AuthenticatedVoter>>;

        response = this.http.get<String>(this.url, {observe: 'response'});
        obsAuthenticatedVoter = response.pipe(
            map(data => this.mapToAuthenticatedVoter(data)),
            catchError(err => this.handleError(err))
        );
        return obsAuthenticatedVoter;
    }

    private mapToAuthenticatedVoter(resp: HttpResponse<String>): Maybe<AuthenticatedVoter> {
        const votername = resp.body['voterName'];
        const registered = resp.body['registered'];

        const isRegistered = (registered === 'Yes');

        if (votername !== undefined) {
            return Maybe.fromData(new AuthenticatedVoter().setVotername(votername).setIsRegistered(isRegistered));
        }
        return Maybe.fromEmpty();
    }

    private handleError(error: HttpErrorResponse): Observable<Maybe<AuthenticatedVoter>> {
        return of(Maybe.fromEmpty());
    }

}
