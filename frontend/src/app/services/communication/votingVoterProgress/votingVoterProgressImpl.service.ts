import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Maybe} from '../../../utils/maybe.utils';
import {VotingVoterProgressService} from './votingVoterProgress.service';
import {VotingVoterProgressJsonParser} from '../../jsonParser/votingVoterProgressJsonParser.service';
import {VotingVoterProgress} from '../../../models/votings/votingVoterProgress';

@Injectable()
export class VotingVoterProgressServiceImpl implements VotingVoterProgressService {

    private startUrl = '/api/votings/';

    constructor(private http: HttpClient,
                private parser: VotingVoterProgressJsonParser) {
    }

    public getVotingVoterProgress(votingId: Number, voterId: Number): Observable<Maybe<VotingVoterProgress>> {
        let obsResponse: Observable<HttpResponse<String>>;
        let obsReturn: Observable<Maybe<VotingVoterProgress>>;

        obsResponse = this.http.get<String>(
            this.startUrl + votingId + '/progress/voters/' + voterId,
            {
                observe: 'response'
            });

        obsReturn = obsResponse.pipe(
            map(data => this.mapToVotingVoterProgress(data)),
            catchError(err => this.handleGetVotingVoterProgressError(err))
        );

        return obsReturn;
    }

    private mapToVotingVoterProgress(resp: HttpResponse<String>): Maybe<VotingVoterProgress> {
        if (resp && resp.status === 200 && resp.body) {
            return this.parser.parseJson(JSON.stringify(resp.body));
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private handleGetVotingVoterProgressError(error: HttpErrorResponse): Observable<Maybe<VotingVoterProgress>> {
        console.log(error);
        return of(Maybe.fromEmpty());
    }
}
