import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {SingleVote} from '../../../models/vote/singleVote.json.model';
import {Maybe} from '../../../utils/maybe.utils';
import {Votes} from '../../../models/vote/votes.json.model';
import {DataForKeyCalculation} from '../../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export abstract class VoteService {

    abstract vote(voteId: Number, voterId: Number, vote: SingleVote): Observable<Pair<boolean, string>>;

    abstract getVotes(voteId: number): Observable<Maybe<Votes>>;

    abstract getDataForKeyCalculation(voteId: Number): Observable<Maybe<DataForKeyCalculation>>;

}
