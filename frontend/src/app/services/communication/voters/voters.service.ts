import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {VoterArray} from '../../../models/voters/voterArray.model';
import {ResponseFromHttpRequest} from '../../../data/responseFromHttpRequest';
import {Pair} from '../../../utils/pair.utils';

@Injectable()
export abstract class VotersService {

    abstract getAllVoters(): Observable<ResponseFromHttpRequest<VoterArray>>;

    abstract saveVoters(voters: VoterArray, createKeys: boolean): Observable<ResponseFromHttpRequest<VoterArray>>;

    abstract deleteVoter(voterId: Number): Observable<Pair<boolean, string>>;

    abstract registerCurrentVoter(): Observable<Pair<boolean, string>>;

}
