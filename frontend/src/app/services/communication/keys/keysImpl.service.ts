import { CryptoService } from 'src/app/services/crypto/crypto.service';
import { UserPublicKeyJsonParser } from 'src/app/services/jsonParser/userPublicKeyJsonParser.service';
import { UserPublicKey } from '../../../models/userPublicKey/userPublicKey.model';
import { map, catchError } from 'rxjs/operators';
import { Maybe } from '../../../utils/maybe.utils';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { KeysService} from './keys.service';

interface KeysResponse {
  token: string;
}

@Injectable()
export class KeysServiceImpl implements KeysService {

  private startUrl = '/api/voters';

  constructor(private http: HttpClient,
              private userPublicKeyJsonParser: UserPublicKeyJsonParser,
              private cryptoService: CryptoService) {
  }

  storePublicKey(userId: number, publicKey: string): Observable<Maybe<string>> {

    let obsKeysResponse: Observable<HttpResponse<KeysResponse>>;
    let obsKey: Observable<Maybe<string>>;

    obsKeysResponse = this.http.put<any>(this.startUrl + '/' + userId + '/publicKey',
      {
          publicKey: publicKey,
      },
      {
          observe: 'response',
      });
      obsKey = obsKeysResponse.pipe(
        map(data => this.mapToToken(data)),
        catchError(err => this.handleError(err))
    );
    return obsKey;
  }

  private mapToToken(resp: HttpResponse<KeysResponse>): Maybe<string> {
    if (resp && resp.status === 201 && resp.body.token) {
        return Maybe.fromData(resp.body.token);
    }
    else {
        return Maybe.fromEmpty();
    }
  }

  private handleError(error: HttpErrorResponse): Observable<Maybe<string>> {
    console.log(error);
    return of(Maybe.fromEmpty());
  }

  getAllKeys(): Observable<UserPublicKey[]> {
    let obsKeysResponse: Observable<HttpResponse<string>>;
    let allKeys: Observable<UserPublicKey[]>;

    obsKeysResponse = this.http.get<string>(this.startUrl + '?onlyIfKeySpecified=true', {observe: 'response'});
    allKeys = obsKeysResponse.pipe(
      map(data => this.mapToUserPublicKey(data)),
      catchError(err => this.catchGetAllKeysError(err))
    );

    return allKeys;
  }

  private mapToUserPublicKey(resp: HttpResponse<string>): UserPublicKey[] {
    if (resp && resp.status === 200 && resp.body) {
      const stringToParse = JSON.stringify(resp.body);
      const maybeKeys = this.userPublicKeyJsonParser.parseJson(stringToParse);
      if (maybeKeys.isEmpty()) {
        return new Array();
      }
      else {
        const userPublicKeys = maybeKeys.getData().getUserPublicKeys();
        userPublicKeys.forEach(key => {
          const keyPem = key.getPublicKey();
          const fingerPrint = this.cryptoService.pemToSHA256Fingerprint(keyPem);
          key.setPublicKeyFingerprint(fingerPrint);
        });
        return maybeKeys.getData().getUserPublicKeys();
      }
    }
    else {
      return new Array();
    }
  }

  private catchGetAllKeysError(error: HttpErrorResponse): Observable<UserPublicKey[]> {
    return of(new Array());
  }

  getPrivateKey(voterId: number): Observable<Maybe<string>> {
    let obsKeysResponse: Observable<HttpResponse<string>>;
    let allKeys: Observable<Maybe<string>>;

    obsKeysResponse = this.http.get<string>(this.startUrl + '/' + voterId + '/privateKey',
        {
          observe: 'response',
        });
    allKeys = obsKeysResponse.pipe(
        map(data => this.mapToPrivateKey(data)),
        catchError(err => this.catchGetPrivateKeyError(err))
    );

    return allKeys;
  }

  private mapToPrivateKey(resp: HttpResponse<string>): Maybe<string> {

    if (resp && resp.status === 200 && resp.body) {
      const key = resp.body['privateKey'];
      if (key !== undefined && key !== null) {
        return Maybe.fromData(key);
      }
    }
    return Maybe.fromEmpty();
  }

  private catchGetPrivateKeyError(error: HttpErrorResponse): Observable<Maybe<string>> {
    return of(Maybe.fromEmpty());
  }

  storePublicKeyForVoting(voterId: number, votingId: number, publicKey: string): Observable<boolean> {
    let obsKeysResponse: Observable<HttpResponse<String>>;
    let obsKey: Observable<boolean>;

    obsKeysResponse = this.http.put('/api/votings/' + votingId + '/publicKeys/voters/' + voterId,
        {
          publicKey: publicKey,
        },
        {
          observe: 'response',
          responseType: 'text'
        });
    obsKey = obsKeysResponse.pipe(
        map(data => this.mapVotingVoterKeyAnswer(data)),
        catchError(err => this.handleVotingVoterKeyError(err))
    );
    return obsKey;
  }

  private mapVotingVoterKeyAnswer(resp: HttpResponse<String>): boolean {
    return resp.status.valueOf() === 200;
  }

  private handleVotingVoterKeyError(error: HttpErrorResponse): Observable<boolean> {
    return of(false);
  }

  getPublicKeyForVoting(voterId: number, votingId: number): Observable<Maybe<string>> {
    let obsKeysResponse: Observable<HttpResponse<String>>;
    let obsKey: Observable<Maybe<string>>;

    obsKeysResponse = this.http.get<any>('/api/votings/' + votingId + '/publicKeys/voters/' + voterId,
        {
          observe: 'response',
        });
    obsKey = obsKeysResponse.pipe(
        map(data => this.mapToPublicKey(data)),
        catchError(err => this.handlePublicKeyError(err))
    );
    return obsKey;
  }

  private mapToPublicKey(resp: HttpResponse<String>): Maybe<string> {
    if (resp && resp.status.valueOf() === 200 && resp.body) {
      const key = resp.body['publicKey'];
      if (key !== undefined && key !== null) {
        return Maybe.fromData(key);
      }
    }
    return Maybe.fromEmpty();
  }

  private handlePublicKeyError(error: HttpErrorResponse): Observable<Maybe<string>> {
    return of(Maybe.fromEmpty());
  }

}
