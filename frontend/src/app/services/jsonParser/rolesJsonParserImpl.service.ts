import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {RolesJsonParser} from './rolesJsonParser.service';
import {Roles} from '../../models/roles/roles.model';
import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import {VotersWithRoles} from '../../models/roles/votersWithRoles.model';

@Injectable()
export class RolesJsonParserImpl implements RolesJsonParser {

    parseAvailableRolesJson(jsonString: string): Maybe<Roles> {
        let availableRoles: Roles;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            availableRoles = jsonConvert.deserializeObject(jsonObj, Roles);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(availableRoles);
    }

    parseVotersWithRoles(jsonString: string): Maybe<VotersWithRoles> {
        let votersWithRoles: VotersWithRoles;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votersWithRoles = jsonConvert.deserializeObject(jsonObj, VotersWithRoles);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votersWithRoles);
    }
}
