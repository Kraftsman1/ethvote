import { Injectable } from '@angular/core';
import { Maybe } from '../../utils/maybe.utils';
import {VotingVoterProgress} from '../../models/votings/votingVoterProgress';


@Injectable()
export abstract class VotingVoterProgressJsonParser {

    abstract parseJson(jsonString: string): Maybe<VotingVoterProgress>;

}
