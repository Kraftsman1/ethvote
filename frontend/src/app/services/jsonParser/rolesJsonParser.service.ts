import {Injectable} from '@angular/core';
import {Maybe} from '../../utils/maybe.utils';
import {Roles} from '../../models/roles/roles.model';
import {VotersWithRoles} from '../../models/roles/votersWithRoles.model';

@Injectable()
export abstract class RolesJsonParser {

    abstract parseAvailableRolesJson(jsonString: string): Maybe<Roles>;

    abstract parseVotersWithRoles(jsonString: string): Maybe<VotersWithRoles>;

}
