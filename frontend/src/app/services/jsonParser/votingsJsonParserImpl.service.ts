import { VotingsArray } from './../../models/votings/votingsArray.json.model';
import { Maybe } from './../../utils/maybe.utils';

import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import { Injectable } from '@angular/core';
import {VotingsJsonParser} from './votingsJsonParser.service';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';

@Injectable()
export class VotingsJsonParserImpl implements VotingsJsonParser {

    parseJson(jsonString: string): Maybe<VotingsArray> {
        let votings: VotingsArray;
        try {
        const jsonObj: object = JSON.parse(jsonString);
        const jsonConvert: JsonConvert = new JsonConvert();
        jsonConvert.ignorePrimitiveChecks = false;
        jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votings = jsonConvert.deserializeObject(jsonObj, VotingsArray);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votings);
    }

    parseSingleVoting(jsonString: string): Maybe<VotingOverview> {
        let voting: VotingOverview;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            voting = jsonConvert.deserializeObject(jsonObj, VotingOverview);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(voting);
    }
}
