import { Maybe } from './../../utils/maybe.utils';

import { JsonConvert, ValueCheckingMode } from 'json2typescript';
import { Injectable } from '@angular/core';
import {VotingSummaryJsonParser} from './votingSummaryJsonParser.service';
import {VotingSummary} from '../../models/votingSummary/votingSummary.json.model';

@Injectable()
export class VotingSummaryJsonParserImpl implements VotingSummaryJsonParser {

    parseJson(jsonString: string): Maybe<VotingSummary> {
        let votings: VotingSummary;
        try {
            const jsonObj: object = JSON.parse(jsonString);
            const jsonConvert: JsonConvert = new JsonConvert();
            jsonConvert.ignorePrimitiveChecks = false;
            jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL;
            votings = jsonConvert.deserializeObject(jsonObj, VotingSummary);
        } catch (e) {
            console.log(e);
            return Maybe.fromEmpty();
        }
        return Maybe.fromData(votings);
    }

}
