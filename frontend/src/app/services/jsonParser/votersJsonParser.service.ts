import { Maybe } from './../../utils/maybe.utils';

import { Injectable } from '@angular/core';
import {VoterArray} from '../../models/voters/voterArray.model';

@Injectable()
export abstract class VotersJsonParser {

    abstract parseJson(jsonString: string): Maybe<VoterArray>;

}
