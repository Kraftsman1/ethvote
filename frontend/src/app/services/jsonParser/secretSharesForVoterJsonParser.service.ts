import {Maybe} from '../../utils/maybe.utils';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';

export abstract class SecretSharesForVoterJsonParser {
    abstract parseJson(jsonString: string): Maybe<SecretSharesForVoter>;
}
