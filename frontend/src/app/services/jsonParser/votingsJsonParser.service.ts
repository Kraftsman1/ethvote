import { VotingsArray } from './../../models/votings/votingsArray.json.model';
import { Maybe } from './../../utils/maybe.utils';


import { Injectable } from '@angular/core';
import {Voting} from '../../models/votings/voting.json.model';
import {VotingOverview} from '../../models/votings/votingOverview.json.model';

@Injectable()
export abstract class VotingsJsonParser {

    abstract parseJson(jsonString: string): Maybe<VotingsArray>;

    abstract parseSingleVoting(jsonString: string): Maybe<VotingOverview>;

}
