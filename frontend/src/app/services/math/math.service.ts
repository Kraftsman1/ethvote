import { Injectable } from '@angular/core';
import { Polynom } from 'src/app/data/polynom';

@Injectable()
export abstract class MathService {

  abstract generateRandom(upperBound: string): string;

  abstract generateNBytesRandomNumber(numberOfBytes: number): string;

  abstract toBitLength(n: string): number;

  abstract toByteLengthRoundedUp(n: string);

  abstract isPositive(n: string): boolean;

  abstract generateRandomPolynomial(numberOfExponents: number, upperBoundOfExponents: string): Polynom;

  abstract generateRandomPolynomialWithNBytes(numberOfExponents: number, numberOfBytes: number): Polynom;

  abstract evaluatePolynomial(poly: Polynom, pos: string): string;

  abstract powerWithModulo(base: string, exponent: string, mod: string): string;

  abstract multiplyAllWithModulo(numbers: string[], mod: string): string;

  abstract multiplyWithModulo(factor1: string, factor2: string, mod: string): string;

  abstract multiply(factor1: string, factor2: string): string;

  abstract add(summand1: string, summand2: string): string;

  abstract addAllWithModulo(numbers: string[], mod: string): string;

  abstract addWithModulo(summand1: string, summand2: string, mod: string): string;

  abstract subtractWithModulo(subtly: string, subtle: string, modulo: string): string;

  abstract subtract(subtly: string, subtle: string): string;

  abstract divide(dividend: string, divisor: string): string;

  abstract modInverse(i: string, n: string): string;

  abstract mod(n: string, mod: string): string;

  abstract abs(n: string): string;

  abstract hexToNumber(n: string): string;
}
