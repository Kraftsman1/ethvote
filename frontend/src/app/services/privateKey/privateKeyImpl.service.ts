import {Injectable} from '@angular/core';
import {PrivateKeyService} from './privateKey.service';
import {Maybe} from '../../utils/maybe.utils';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {UploadPrivateKeyComponent} from '../../components/dialogs/uploadPrivateKey/uploadPrivateKey.component';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {LocalStorageService} from '../localStorage/localStorage.service';
import {AsymmetricKey} from '../../data/asymmetricKey';
import {CryptoService} from '../crypto/crypto.service';
import {Observable, of} from 'rxjs';
import {KeysService} from '../communication/keys/keys.service';
import {flatMap, mergeMap} from 'rxjs/operators';

@Injectable()
export class PrivateKeyServiceImpl implements PrivateKeyService {

    constructor(private dialog: MatDialog,
                private storageService: LocalStorageService,
                private keyService: KeysService,
                private cryptoService: CryptoService) {
    }

    getPrivateKey(data: DataForKeyCalculation, voterId: number): Observable<Maybe<string>> {
        const publicKey = this.getPublicKey(data, voterId);

        const keyObservable: Observable<Maybe<string>> = this.tryGetPrivateKeyFromStorageService(publicKey)
            .pipe(mergeMap(res => this.tryGetPrivateKeyFromDatabase(res, voterId, publicKey)))
            .pipe(mergeMap(res => this.tryGetPrivateKeyWithUserInteraction(res, publicKey)));

        return keyObservable;
    }

    private getPublicKey(data: DataForKeyCalculation, voterId: number): string {
        for (const voter of data.getEligibleVoters()) {
            if (voter.getId() === voterId) {
                return voter.getPublicKey();
            }
        }
        return null;
    }

    private tryGetPrivateKeyFromStorageService(publicKey: string): Observable<Maybe<string>> {
        const maybeKeyObservable: Observable<Maybe<string>> = of(this.storageService.get('privateKey'));
        return maybeKeyObservable.pipe(flatMap(potentialKey => this.filterIfInvalid(publicKey, potentialKey)));
    }

    private tryGetPrivateKeyFromDatabase(prevRes: Maybe<string>, voterId: number, publicKey: string): Observable<Maybe<string>> {
        if (!prevRes.isEmpty()) {
            return of(prevRes);
        }

        const maybeKeyObservable: Observable<Maybe<string>> = this.keyService.getPrivateKey(voterId);
        return maybeKeyObservable.pipe(flatMap(potentialKey => this.filterIfInvalid(publicKey, potentialKey)));
    }

    private tryGetPrivateKeyWithUserInteraction(prevRes: Maybe<string>, publicKey: string): Observable<Maybe<string>> {
        if (!prevRes.isEmpty()) {
            return of(prevRes);
        }

        const dialogConfig = new MatDialogConfig();

        dialogConfig.height = '550px';
        dialogConfig.width = '600px';
        dialogConfig.data = {
            publicKey: publicKey,
            fromPKIComponent: false
        };

        const dialogSubscribable = this.dialog.open(UploadPrivateKeyComponent, dialogConfig)
            .afterClosed().pipe(flatMap(res => this.filterFromDialog(res)));
        return dialogSubscribable;
    }

    private filterIfInvalid(publicKey: string, maybePrivateKey: Maybe<string>): Observable<Maybe<string>> {
        if (maybePrivateKey.isEmpty()) {
            return of(Maybe.fromEmpty());
        }

        const privateKey = maybePrivateKey.getData();
        const keyPairToCheck = new AsymmetricKey(publicKey, privateKey);

        if (this.cryptoService.validateKeyPair(keyPairToCheck)) {
            return of(maybePrivateKey);
        }
        else {
            return of(Maybe.fromEmpty());
        }
    }

    private filterFromDialog(privateKey: string): Observable<Maybe<string>> {
        if (privateKey === undefined) {
            return of(Maybe.fromEmpty());
        }
        else {
            return of(Maybe.fromData(privateKey));
        }
    }

}
