import { Maybe } from './../../utils/maybe.utils';
import { AsymmetricKey } from './../../data/asymmetricKey';
import { Injectable } from '@angular/core';
import { CalculationParams } from 'src/app/data/calculationParams';

@Injectable()
export abstract class CryptoService {

    abstract validateKeyPair(key: AsymmetricKey): boolean;

    abstract generateKeyPair(): Promise<Maybe<AsymmetricKey>>;

    abstract signSHA256(privateKeyPem: string, message: string): Maybe<string>;

    abstract verifySHA256Signature(publicKeyPem: string, message: string, signature: string): boolean;

    abstract encrypt(publicKeyPem: string, message: string): string;

    abstract decrypt(privateKeyPem: string, cipherText: string): Maybe<string>;

    abstract pemToSHA256Fingerprint(pem: string): string;

    abstract generateCalculationParams(): Promise<Maybe<CalculationParams>>;

    abstract hashSHA256(message: string): string;
}
