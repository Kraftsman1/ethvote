import { Maybe } from 'src/app/utils/maybe.utils';
import { AsymmetricKey } from './../../data/asymmetricKey';
import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';

import * as Forge from 'node-forge';

import { CalculationParams } from 'src/app/data/calculationParams';

@Injectable()
export class CryptoServiceImpl implements CryptoService {
    private message = 'dummyMessage';

    constructor() {
    }

    mapToKeyPair(keypair: Forge.pki.rsa.KeyPair): Maybe<AsymmetricKey> {
        const pub: Forge.pki.rsa.PublicKey = keypair.publicKey;
        const priv: Forge.pki.rsa.PrivateKey = keypair.privateKey;

        const pubPem  = Forge.pki.publicKeyToPem(pub);
        const privPem  = Forge.pki.privateKeyToPem(priv);

        return Maybe.fromData(new AsymmetricKey(pubPem, privPem));
    }

    generateKeyPair(): Promise<Maybe<AsymmetricKey>> {
        const forge = require('node-forge');
        const rsa = forge.pki.rsa;

        return new Promise((resolve, reject) => rsa.generateKeyPair({bits: 2048, workers: 2}, (err, keypair: Forge.pki.rsa.KeyPair) => err ? reject(err) : resolve(this.mapToKeyPair(keypair))));
    }

    validateKeyPair(key: AsymmetricKey): boolean {
        const publicKeyPem = key.getPublicKey();
        const privateKeyPem = key.getPrivateKey();

        const signature = this.signSHA256(privateKeyPem, this.message);
        if (signature.isEmpty()) {
            return false;
        }

        return this.verifySHA256Signature(publicKeyPem, this.message, signature.getData());
    }

    verifySHA256Signature(publicKeyPem: string, message: string, signature: string): boolean {
        try {
            const publicKey = this.convertPublicPemToKey(publicKeyPem);
            const md = Forge.md.sha256.create();
            md.update(message, 'utf8');
            return publicKey.verify(md.digest().bytes(), atob(signature));
        } catch (err) {
            return false;
        }

    }

    signSHA256(privateKeyPem: string, message: string): Maybe<string> {
        const privateKey: Maybe<Forge.pki.rsa.PrivateKey> = this.convertPrivatePemToKey(privateKeyPem);
        if (privateKey.isEmpty()) {
            return Maybe.fromEmpty();
        }

        const md = Forge.md.sha256.create();
        md.update(message, 'utf8');
        const signature = btoa(privateKey.getData().sign(md));
        return Maybe.fromData(signature);
    }

    encrypt(publicKeyPem: string, message: string): string {
        const publicKey = this.convertPublicPemToKey(publicKeyPem);
        return btoa(publicKey.encrypt(message));
    }

    decrypt(privateKeyPem: string, cipherText: string): Maybe<string> {
        const privateKey: Maybe<Forge.pki.rsa.PrivateKey> = this.convertPrivatePemToKey(privateKeyPem);
        if (privateKey.isEmpty()) {
            return Maybe.fromEmpty();
        }

        const decryptedMessage = privateKey.getData().decrypt(atob(cipherText));
        return Maybe.fromData(decryptedMessage);
    }

    pemToSHA256Fingerprint(pem: string): string {
        try {
            const stripedPem = this.stripHeaderAndFooter(pem);
            const asBase64 = atob(stripedPem);
            const md = Forge.md.sha256.create();
            // @ts-ignore
            md.update(asBase64, 'base64');
            const sha256Hash = md.digest().toHex();
            const base64Hash = Buffer.from(sha256Hash, 'hex').toString('base64');
            return this.removeLastChar(base64Hash);
        } catch (err) {
            console.log(err);
        }
    }

    hashSHA256(message: string): string {
        const md = Forge.md.sha256.create();
        md.update(message, 'utf8');
        return md.digest().toHex();
    }

    private stripHeaderAndFooter(s: string) {
        const keyHeader = '-----BEGIN PUBLIC KEY-----';
        const keyFooter = '-----END PUBLIC KEY-----';
        return s.replace(keyHeader, '').replace(keyFooter, '');
      }

    private removeLastChar(str: string): string {
        return str.slice(0, -1);
    }

    private convertPublicPemToKey(pubicKeyPem: string): Forge.pki.rsa.PublicKey {
        // @ts-ignore
        return Forge.pki.publicKeyFromPem(pubicKeyPem);
    }

    private convertPrivatePemToKey(privateKeyPem: string): Maybe<Forge.pki.rsa.PrivateKey> {
        let key: Forge.pki.PrivateKey;
        try {
            key = Forge.pki.privateKeyFromPem(privateKeyPem);
        } catch (e) {
            return Maybe.fromEmpty();
        }
        // @ts-ignore
        return Maybe.fromData(key);
    }

    generateCalculationParams(): Promise<Maybe<CalculationParams>> {
        const forge = require('node-forge');
        const rsa = forge.pki.rsa;

        return new Promise((resolve, reject) => rsa.generateKeyPair({bits: 2048, workers: 2}, (err, keypair) => err ? reject(err) : resolve(this.mapToCalculationParams(keypair))));
    }

    private mapToCalculationParams(keypair): Maybe<CalculationParams> {
        const priv = keypair.privateKey;
        return Maybe.fromData(new CalculationParams(priv.p.toString(), priv.q.toString(), priv.e.toString()));
    }

}
