import { Injectable } from '@angular/core';
import {VoterParserService} from './voterParser.service';
import {Maybe} from '../../utils/maybe.utils';
import {VoterToCreate} from '../../data/voterToCreate';

@Injectable({
    providedIn: 'root'
})
export class VoterParserServiceImpl implements VoterParserService {

    private withRoles = false;

    constructor() { }

    setWithRoles(withRoles: boolean) {
        this.withRoles = withRoles;
    }

    parseLine(line: string): Maybe<VoterToCreate> {
        const splitted = line.split('\t');
        if (splitted.length === 1) {
            return this.parseName(splitted);
        }
        else if (splitted.length === 2) {
            return this.parseWithoutRoles(splitted);
        }
        else if (splitted.length === 3 && this.withRoles) {
            return this.parseWithRoles(splitted);
        }
        else {
            return Maybe.fromEmpty();
        }
    }

    private parseName(splittedText: string[]): Maybe<VoterToCreate> {
        const name = splittedText[0].trim();
        return Maybe.fromData(new VoterToCreate().setName(name));
    }

    private parseWithoutRoles(splittedText: string[]): Maybe<VoterToCreate> {
        const name = splittedText[0].trim();
        const email = splittedText[1].trim();
        return Maybe.fromData(new VoterToCreate().setName(name).setEmail(email));
    }

    private parseWithRoles(splittedText: string[]): Maybe<VoterToCreate> {
        const name = splittedText[0].trim();
        const email = splittedText[1].trim();
        const roles = splittedText[2].trim();

        const voter = new VoterToCreate().setName(name).setEmail(email);

        if (!roles.startsWith('(') || !roles.endsWith(')')) {
            return Maybe.fromEmpty();
        }

        const stringWithoutBrackets = roles.substring(1, roles.length - 1);
        const splitted = stringWithoutBrackets.split(',');

        for (const role of splitted) {
            if (role.trim() === 'Administrator') {
                voter.setAdministrator(true);
            }
            else if (role.trim() === 'Voting Administrator') {
                voter.setVoting_Administrator(true);
            }
            else if (role.trim() === 'Registrar') {
                voter.setRegistrar(true);
            }
            else {
                return Maybe.fromEmpty();
            }
        }

        return Maybe.fromData(voter);
    }
}
