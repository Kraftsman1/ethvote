import {Injectable} from '@angular/core';
import {DataForKeyCalculation} from '../../models/dataForKeyCalculation/dataForKeyCalculation.model';
import {CommitmentFromVoter} from '../../models/commitments/commitmentFromVoter.model';
import {SingleSecretShareForVoter} from '../../models/secretShares/singleSecretShareForVoter';
import {Commitments} from '../../models/commitments/commitments.model';
import {SecretSharesForVoter} from '../../models/secretShares/secretSharesForVoter.model';
import {Votes} from '../../models/vote/votes.json.model';
import {VotingCalculationParameters} from '../../models/votings/votingsCalculationParameters.json.model';
import {SingleVote} from '../../models/vote/singleVote.json.model';
import {SingleDecryptionFactor} from '../../models/decryptionFactors/singleDecryptionFactor.model';

@Injectable()
export abstract class VoteHelperService {

    abstract encryptPolynomPoint(polynomValue: string, publicKey: string): string[];

    abstract decryptPolynomPoint(point: string[], privateKey: string): string;

    abstract validateShares(data: DataForKeyCalculation, commitments: Commitments, secretShares: SecretSharesForVoter, privateKey): [number, boolean][];

    abstract validateShare(data: DataForKeyCalculation, commitment: CommitmentFromVoter, share: SingleSecretShareForVoter, forVoterPosition: string, privateKey: string): boolean;

    abstract calculatePublicKey(data: DataForKeyCalculation, commitments: Commitments): string;

    abstract encryptVote(data: DataForKeyCalculation, encryptionKey: string, vote: string): [string, string, string];

    abstract calculateSecretPartOfKey(data: DataForKeyCalculation, secretShares: SecretSharesForVoter, privateKey: string): string;

    abstract calculateDecryptionFactor(data: DataForKeyCalculation, secretShares: SecretSharesForVoter, votes: Votes, privateKey: string);

    abstract calculateVotingOutCome(votingCalculationParameters: VotingCalculationParameters, votesArray: SingleVote[], decryptionFactors: SingleDecryptionFactor[], numberOfOptions): number[];
}
